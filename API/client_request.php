<?php

// curl operation
$ch = curl_init();
# set url
curl_setopt($ch, CURLOPT_URL, "http://localhost/API/server.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('query' => base64_encode($_POST["query"]))));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
# execute post
$output = curl_exec($ch);
# close connection
curl_close($ch);

// return output
# decode the output
$output = base64_decode($output);
# encode to json array
// $output = json_decode($output);
print_r($output);

?>