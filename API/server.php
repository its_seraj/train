<?php

// Database Connection start here
define('SERVER_NAME','localhost');
define('DB','comapany_details');
define('USER_NAME','seraj');
define('PASSWORD','seraj');
$conn;
try{
    $conn = new PDO("mysql:host=".SERVER_NAME.";dbname=".DB, USER_NAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e) 
{
    echo "Connection failed: " . $e->getMessage();
}
// Database Connection end here


// Get API data start here
$encoded_query = $_POST["query"];
# Decode the query
$decoded_query = base64_decode($encoded_query);
// Get API data end here

// store the query in query_Logs
try{
    $sql = $conn->query("INSERT INTO query_logs VALUES(NULL, '$decoded_query', NULL)");
    // $result = $sql->fetchAll(PDO::FETCH_ASSOC);
}catch(PDOException $e) 
{
    // echo $e->getMessage();
}


// Execute decoded query start
$result;
try{
    $sql = $conn->query($decoded_query);
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
}catch(PDOException $e) 
{
    // echo $e->getMessage();
}
// Execute decoded query end


// Return the result start
# convert result array to json string
$result = json_encode($result);
# encode the output result
$result_encoded = base64_encode($result);

echo $result_encoded;
// Return the result end

?>