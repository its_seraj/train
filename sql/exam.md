#### Question - 1
- Provide Query for Auto Suggest of Place which Will Find CIty for which we are doing hotel Booking. Following Columns Should be returned

1. City Name
2. State 
3. Country

```
SELECT
c.vCity AS City,
CONCAT(s.vState, ', ', co.vCountry) AS 'State & Country'

FROM
city AS c,
state AS s,
country AS co

WHERE
c.vCity LIKE 'a%' AND c.iStateId = s.iStateId AND s.iCountryId = co.iCountryId;
```

#### Question - 2

- Provide List of All Hotels Belonging to Specific City.
1. Provide Listing Based on Highest Rated Hotel should come First
```
SELECT
h.iHotelId AS 'Hotel Id',
h.vHotelName AS Name,
h.price AS Price,
h.average_rating AS Rate

FROM
hotels AS h

WHERE
h.iCityId = 1

ORDER BY h.average_rating DESC;
```

2. Provide Listing based on Cheapest Hotel Should Come First
```
SELECT
h.iHotelId AS 'Hotel Id',
h.vHotelName AS Name,
h.price AS Price,
h.average_rating AS Rate

FROM
hotels AS h

WHERE
h.iCityId = 1

ORDER BY h.price ASC;
```

#### Question - 3

- Provide Count of Hotels for Specific City with Following Grouping 
1. Grouping Based on Price Range

Upto 999
```
SELECT
COUNT(h.price) AS Count

FROM
hotels AS h

WHERE
h.iCityId = 2 AND h.price < 1000;
```

1000 to 3000
```
SELECT
COUNT(h.price) AS Count

FROM
hotels AS h

WHERE
h.iCityId = 2 AND h.price BETWEEN 1000 AND 3000;
```

3000 to 5000
```
SELECT
COUNT(h.price) AS Count

FROM
hotels AS h

WHERE
h.iCityId = 2 AND h.price BETWEEN 3000 AND 5000;
```

5000 to 10000
```
SELECT
COUNT(h.price) AS Count

FROM
hotels AS h

WHERE
h.iCityId = 2 AND h.price BETWEEN 5000 AND 10000;
```

10000+
```
SELECT
COUNT(h.price) AS Count

FROM
hotels AS h

WHERE
h.iCityId = 2 AND h.price > 10000;
```

2. Grouping based on Area

```
SELECT
COUNT(h.iAreaId) AS Count,
h.iAreaId AS 'Area Id'

FROM
hotels AS h

WHERE
h.iCityId = 2

GROUP BY h.iAreaId;
```

3. Grouping Based on Ratings
```
SELECT
COUNT(h.average_rating) AS Count,
h.average_rating AS Rating

FROM
hotels AS h

WHERE
h.iCityId = 2

GROUP BY h.average_rating;
```

#### Question - 4

- Fetch Basic Hotel Details considering Following Search criteira
1. Hotel should be from Ahmedabad
2. Hotel Must have Average rating > 3.5
3. 2000  < Hotel room price < 3000
4. Hotel Should be belong from the “satellite” Area
5. Hotel Should Have Free-wifi Facility
```
SELECT
h.iHotelId AS 'Hotel Id',
h.vHotelName AS Name,
h.iAreaId AS Area,
h.price AS Price,
f.vFacilityName AS Facility

FROM
hotels AS h

INNER JOIN
hotel_facilities AS hf

ON h.iHotelId = hf.iHotelId

INNER JOIN
facilities AS f

ON f.iFacilityId = hf.iFacilityId

WHERE
h.iCityId = 2 
AND h.average_rating >= 3.5
AND h.price BETWEEN 2000 AND 3000
AND h.iAreaId = 4
AND f.vFacilityCode = 'fac_1';
```

#### Question - 5

Create Trigger which will Update Average Ratings of the Hotel for the Following cases
1. Case - 1 :  When New Rating is Added by customer 
```
CREATE TRIGGER `insertReview` AFTER INSERT ON `reviews`
 FOR EACH ROW UPDATE
hotels

SET
hotels.average_rating = avgHotel(NEW.iHotelId)

WHERE
hotels.iHotelId = NEW.iHotelId
```

2. Case - 2  : When existing Rating is Changed by customer 
```
CREATE TRIGGER `UpdateReview` AFTER UPDATE ON `reviews`
 FOR EACH ROW BEGIN
UPDATE
hotels

SET
hotels.average_rating = avgHotel(NEW.iHotelId)

WHERE
hotels.iHotelId = NEW.iHotelId;


UPDATE
hotels

SET
hotels.average_rating = avgHotel(OLD.iHotelId)

WHERE
hotels.iHotelId = OLD.iHotelId;
END
```

3. Case - 3  : When existing Rating is Removed
```
CREATE TRIGGER `deleteReview` AFTER DELETE ON `reviews`
 FOR EACH ROW UPDATE
hotels

SET
hotels.average_rating = avgHotel(OLD.iHotelId)

WHERE
hotels.iHotelId = OLD.iHotelId
```

#### Question - 6

Create Trigger which will Generate Facilities code automatically considering following Constraints

1. Facility code should be lowercase  
2. all Blank space will be replaced by “_” 

Example : Let say you enter Facility as Swimming Pool so the Facility code should be swimming_pool 
```
DROP TRIGGER IF EXISTS `facilitiesCode`;
CREATE DEFINER=`seraj`@`localhost` TRIGGER `facilitiesCode` 
BEFORE INSERT ON `facilities` 
FOR EACH ROW 
SET
NEW.vFacilityCode = LOWER(REPLACE(NEW.vFacilityName, ' ', '_'));
```

#### Question - 7

Create function which will Calculate the Average Rating of the Hotel Based on the Hotel Id 
Note : make sure to use the Reviews transaction table for this calculation don’t directly use the field from the Hotels table 

Function name : getHotelRatings(hotelId INT)
```
DELIMITER $$
CREATE DEFINER=`seraj`@`localhost` FUNCTION `getHotelRatings`(`id` INT) RETURNS float(2,1)
    NO SQL
RETURN(
SELECT
AVG(r.iRating) AS Rate

FROM
reviews AS r

WHERE
r.iHotelId = id

GROUP BY
r.iHotelId
)$$
DELIMITER ;
```

#### Question - 8

Fetch all the Details of the Specific Hotel Considering Below Image
```
SELECT
h.vHotelName AS Name,
h.price AS Price,
h.average_rating AS Rating,
COUNT(reviews.iReviewId) AS 'Total Reviews',
area.vAreaName AS Area,
c.vCity AS City,
GROUP_CONCAT(f.vFacilityName) AS 'Facility Name'

FROM
hotels AS h

INNER JOIN
area
ON area.iAreaId = h.iAreaId

INNER JOIN
city AS c
ON c.iCityId = h.iCityId

JOIN
reviews
ON reviews.iHotelId = h.iHotelId

LEFT JOIN
hotel_facilities hf
ON hf.iHotelId = h.iHotelId

LEFT JOIN
facilities AS f
ON f.iFacilityId = hf.iFacilityId

WHERE h.iHotelId = 1

GROUP BY
reviews.iHotelId
```

#### Question - 9
Fetch All the answers of Specific Question considering Below image

Note : the Above Image Provides detail of 1 review only you need to Provide Query which will Provide Details of All reviwes Specific to that Hotel.
```
SELECT
CONCAT(u.vFirstName, ' ', u.vLastName),
r.tReview AS Review,
r.dtAddedTime AS Date,
r.iRating AS Rate

FROM
reviews AS r

INNER JOIN
users AS u
ON u.iUserId = r.iUserId

WHERE
r.iHotelId = 1;
```

#### Question - 10
Fetch Related Hotels Based on the Area being used inside the Main Hotel
```
SELECT
h.vHotelName AS Name,
h.price AS Price,
h.average_rating AS Rating,
COUNT(reviews.iReviewId) AS 'Total Reviews',
area.vAreaName AS Area,
c.vCity AS City,
GROUP_CONCAT(f.vFacilityName) AS 'Facility Name'

FROM
hotels AS h

INNER JOIN
area
ON area.iAreaId = h.iAreaId

INNER JOIN
city AS c
ON c.iCityId = h.iCityId

JOIN
reviews
ON reviews.iHotelId = h.iHotelId

LEFT JOIN
hotel_facilities hf
ON hf.iHotelId = h.iHotelId

LEFT JOIN
facilities AS f
ON f.iFacilityId = hf.iFacilityId

WHERE h.iHotelId != 1

GROUP BY
reviews.iHotelId
```
