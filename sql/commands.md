#### without join
```
SELECT
u.vFirstName, u.vLastName, u.iMobile, u.vEmail, p.vPinCode, 
c.vCityName, s.vStateCity, co.vCountryName

FROM
users u, pincode p, city c, state s, country co

WHERE
u.iPinId = p.iPinId 
AND p.iCityId=c.iCityId 
AND c.iStateId=s.iStateId 
AND s.iCountryId=co.iCountryId
```

<hr>

#### Inner Join w/ condition
```
SELECT
u.vFirstName, u.vLastName, u.iMobile, u.vEmail, p.vPinCode
FROM
users u
INNER JOIN
pincode p
WHERE
u.iPinId = p.iPinId
```


#### Inner Join without condition
```
SELECT
u.vFirstName, u.vLastName, u.iMobile, u.vEmail, p.vPinCode
FROM
users u
INNER JOIN
pincode p
```


#### Inner join w/ ON
```
SELECT
u.vFirstName, u.vLastName, u.iMobile, u.vEmail, p.vPinCode
FROM
users u
INNER JOIN
pincode p
ON
u.iPinId=p.iPinId
```

<hr>

#### multiple join
```
SELECT
*
FROM
users
INNER JOIN
addresses
ON addresses.iUserId=users.iUserId
INNER JOIN
city
```

<hr>

#### Order By
```
SELECT * FROM users
ORDER BY users.vFirstName
```

###### by default return in ASC
```
SELECT * FROM users
ORDER BY users.vFirstName ASC
```

###### multiple
```
SELECT * FROM users
ORDER BY users.vFirstName ASC, users.vLastName DESC
```

<hr>

#### Insert
```
INSERT INTO users(vFirstName, vLastName, iMobile, vEmail, vProfilePic, iPinId) 
VALUES (value-1, value-2, value-3, value-4, value-5, value-6)
```

<hr>

#### Null
```
SELECT
*
FROM
users
WHERE users.vFirstName IS NULL
```

#### Not Null
```
SELECT
*
FROM
users
WHERE users.vFirstName IS NOT NULL
```

<hr>

#### Update 
```
UPDATE users 
SET users.vLastName="khan" 
WHERE users.iUserId=1;
```

<hr>

#### delete
```
DELETE FROM users
WHERE users.iUserId=1;
```

<hr>

#### smallest/biggest value of table
```
SELECT MIN(users.vFirstName)
FROM users

SELECT MAX(users.vFirstName)
FROM users
```

<hr>

#### count list
```
SELECT COUNT(users.vFirstName)
FROM users
```


<hr>

#### First Quetion

```
SELECT
r.iRestaurantId AS EntityId, "Restaurant" as EntityType, r.vRestaurantName AS Name, 
r.vRestaurantIcon AS Image, AVG(r1.iRateRestaurant) AS Rate

FROM 
restaurant AS r, rateRestaurant AS r1

WHERE 
r.vRestaurantName LIKE "%a%"

GROUP BY r1.iRestaurantId

UNION

SELECT
m.iMenuId AS EntityId, "Menu" as EntityType, m.vMenuName AS Name, 
m.vMenuIcon AS Image, AVG(r2.iRateMenu) AS Rate

FROM 
menu AS m INNER JOIN rateMenu r2 on r2.iMenuId=m.iMenuId

WHERE m.vMenuName LIKE "%a%"

GROUP BY r2.iMenuId
```
<hr>
<!-- -- first quetion end -->

#### Second Question

```
SELECT
r.vRestaurantName AS Name, r.vRestaurantAddress AS Address, r.vRestaurantIcon AS Image, 
AVG(rate.iRateRestaurant) AS Rate, r.iMobileId AS MobileId, r.iCuisineId AS CuisineId

FROM
city, cuisines AS c, restaurantMobile AS m, restaurant AS r

INNER JOIN rateRestaurant AS rate ON r.iRestaurantId=rate.iRestaurantId

GROUP BY rate.iRestaurantId;
```

##### Mobile List

```
SELECT
m.iMobileNumber AS Mobile, r.iRestaurantId
FROM
restaurantMobile m, restaurant AS r
WHERE m.iMobileId=r.iMobileId;
```

##### Cuisines List

```
SELECT
c.vCuisineName, r.iRestaurantId
FROM
cuisines c, restaurant AS r
WHERE c.iCuisineId=r.iCuisineId;
```
<hr>

#### Third Question

###### Restaurant Details
```
SELECT
r.vRestaurantName AS Name, r.vRestaurantAddress AS Address, r.vRestaurantIcon AS Image,
r.tOpenTime AS 'Open At', r.tCloseTime AS 'Close At', AVG(rate.iRateRestaurant) AS Rate

FROM
restaurant AS r, rateRestaurant AS rate

GROUP BY rate.iRestaurantId
```

###### Category List
```
SELECT
c.vCategoryName AS Category, r.vRestaurantName AS Restaurant

FROM
category AS c, restaurant AS r

WHERE
c.iRestaurantId = r.iRestaurantId;

```

###### Dish Details
```
SELECT
m.vMenuName AS Dish, AVG(rate.iRateMenu) AS Rate, price.fItemPrice as Price

FROM
menu AS m

JOIN items as price 
ON m.iMenuId = price.iMenuId

JOIN 
rateMenu AS rate

ON rate.iMenuId = m.iMenuId

GROUP BY rate.iMenuId;
```

#### Fourth Question
###### Review Counter || reviewCount()
```
SELECT COUNT(r.vReview) AS 'Total Review', r.iRestaurantId AS R_Id 

FROM review AS r 

GROUP BY r.iRestaurantId;
```
###### Review w/ user & rating
```
SELECT
r.iRestaurantId AS Id, r.vReview AS Review, r.iUserId AS User, 
rate.iRateRestaurant AS Rate

FROM
review AS r
INNER JOIN
rateRestaurant AS rate

ON
r.iRestaurantId = rate.iRestaurantId AND r.iUserId = rate.iUserId;
```

#### Age Calculate Function
```
RETURN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), dDOB)), "%y Year : %m Month : %d Day");
```

#### Full Name (e.g. Anand T.)
```

```