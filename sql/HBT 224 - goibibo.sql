-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 18, 2022 at 01:10 PM
-- Server version: 5.7.36
-- PHP Version: 7.0.33-57+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goibibo`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`seraj`@`localhost` PROCEDURE `userDetails` (IN `id` INT)  NO SQL
SELECT
        userFullName(id) AS Name, u.iMobile AS Mobile,
        u.vEmail AS Email, userAge(id) AS Age,
        pincode.vPinCode AS PIN,
        CONCAT(city.vCityName, ', ',
        state.vStateCity, ', ',
        country.vCountryName) AS Address
        
        
        FROM
        users AS u
        
        LEFT JOIN pincode
        ON u.iPinCode = pincode.vPinCode
        
        LEFT JOIN city
        ON pincode.iCityId = city.iCityId
        
        LEFT JOIN state
        ON city.iStateId = state.iStateId
        
        LEFT JOIN country
        ON country.iCountryId = state.iCountryId
        
        WHERE u.iUserId = id$$

--
-- Functions
--
CREATE DEFINER=`seraj`@`localhost` FUNCTION `userAge` (`id` INT) RETURNS VARCHAR(16) CHARSET utf8 RETURN(
	SELECT 
	DATE_FORMAT(
        FROM_DAYS(
            DATEDIFF(NOW(), u.dDOB)
        ), "%y Years"
    )

	FROM
	users AS u

	WHERE
	u.iUserId = id
)$$

CREATE DEFINER=`seraj`@`localhost` FUNCTION `userFullName` (`id` INT) RETURNS VARCHAR(128) CHARSET utf8 NO SQL
RETURN(
    SELECT
    CONCAT(
        UPPER(SUBSTRING(u.vFirstName, 1, 1)),
        SUBSTRING(u.vFirstName, 2), ' ', 
        UPPER(SUBSTRING(u.vMiddleName, 1, 1)),
        SUBSTRING(u.vMiddleName, 2), ' ',
		UPPER(SUBSTRING(u.vLastName, 1, 1)),
        SUBSTRING(u.vLastName, 2)
    ) AS FullName
    
    FROM
    users AS u
    
    WHERE
    u.iUserId = id
)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `iBillId` int(11) NOT NULL,
  `fPrice` float(10,2) NOT NULL,
  `iHotelid` int(11) NOT NULL,
  `iRoomId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `iCityId` int(11) NOT NULL,
  `vCityName` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `iStateId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`iCityId`, `vCityName`, `iStateId`) VALUES
(1, 'Patna', 1),
(2, 'Ahmedabad', 2),
(3, 'Amaravati', 6),
(4, 'Itanagar', 7),
(5, 'Dispur', 8),
(7, 'Raipur', 10),
(8, 'Panaji', 13),
(9, 'Gandhinagar', 17),
(10, 'Chandigarh', 23),
(11, 'Shimla', 20),
(12, 'Ranchi', 25),
(13, 'Bengaluru', 2),
(14, 'Thiruvananthapuram', 23),
(15, 'Bhopal', 24),
(16, 'Mumbai', 14),
(17, 'Imphal', 31);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `iCountryId` int(11) NOT NULL,
  `vCountryName` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`iCountryId`, `vCountryName`) VALUES
(1, 'India'),
(2, 'Nepal'),
(3, 'Pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `iFormId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL,
  `iHotelid` int(11) NOT NULL,
  `iRoomId` int(11) NOT NULL,
  `dCheckIn` date NOT NULL,
  `dCheckOut` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`iFormId`, `iUserId`, `iHotelid`, `iRoomId`, `dCheckIn`, `dCheckOut`) VALUES
(6, 3, 1, 12, '2022-02-01', NULL),
(7, 1, 1, 5, '2022-02-09', NULL),
(8, 5, 2, 13, '2022-02-04', NULL),
(9, 5, 8, 22, '2022-02-09', NULL);

--
-- Triggers `forms`
--
DELIMITER $$
CREATE TRIGGER `roomStatus` AFTER INSERT ON `forms` FOR EACH ROW BEGIN

IF NEW.dCheckOut IS NULL THEN
    UPDATE rooms
    SET rooms.bIsAvailable = 0
    WHERE rooms.iHotelid = NEW.iHotelId 
    AND rooms.iRoomId = NEW.iRoomId;

END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateRoomStatus` AFTER UPDATE ON `forms` FOR EACH ROW BEGIN
	IF NEW.dCheckOut IS NOT NULL THEN
        UPDATE rooms
        SET rooms.bIsAvailable = 1
        WHERE rooms.iHotelid = OLD.iHotelId 
        AND rooms.iRoomId = OLD.iRoomId;
        
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `iHotelId` int(11) NOT NULL,
  `vHotelName` varchar(128) NOT NULL,
  `vPinCode` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`iHotelId`, `vHotelName`, `vPinCode`) VALUES
(1, 'Ebony Cloud Hotel', '380015'),
(2, 'Illustrious Tropic Resort', '380015'),
(3, 'Bronze Dune Resort & Spa', '380018'),
(4, 'Light Peninsula Hotel', '462021'),
(5, 'Bronze Refuge Hotel', '462022'),
(6, 'Secluded Aurora Hotel & Spa', '843321'),
(7, 'Stardust Hotel', '840000'),
(8, 'Jade Hotel', '462022'),
(9, 'Delight Hotel', '380018'),
(10, 'Spare Time Motel', '462021');

-- --------------------------------------------------------

--
-- Table structure for table `pincode`
--

CREATE TABLE `pincode` (
  `vPinCode` varchar(16) NOT NULL,
  `iCityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pincode`
--

INSERT INTO `pincode` (`vPinCode`, `iCityId`) VALUES
('840000', 1),
('843321', 1),
('380015', 2),
('380018', 2),
('462021', 15),
('462022', 15);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `iRoomId` int(11) NOT NULL,
  `iHotelId` int(11) NOT NULL,
  `bIsAvailable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`iRoomId`, `iHotelId`, `bIsAvailable`) VALUES
(1, 3, 1),
(2, 3, 1),
(3, 5, 1),
(4, 9, 1),
(5, 1, 0),
(6, 2, 1),
(7, 8, 1),
(8, 4, 1),
(9, 6, 1),
(10, 10, 1),
(11, 7, 1),
(12, 1, 0),
(13, 2, 0),
(14, 4, 1),
(15, 4, 1),
(16, 6, 1),
(17, 10, 1),
(18, 7, 1),
(19, 8, 1),
(20, 9, 1),
(21, 4, 1),
(22, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `iStateId` int(11) NOT NULL,
  `vStateCity` varchar(64) NOT NULL,
  `iCountryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`iStateId`, `vStateCity`, `iCountryId`) VALUES
(1, 'Bihar', 1),
(2, 'Gujarat', 1),
(6, 'Andhra Pradesh', 1),
(7, 'Arunachal Pradesh', 1),
(8, 'Assam', 1),
(9, 'Bihar', 1),
(10, 'Chhattisgarh', 1),
(11, 'Goa', 1),
(12, 'Gujarat', 1),
(13, 'Haryana', 1),
(14, 'Himachal Pradesh', 1),
(15, 'Jharkhand', 1),
(16, 'Karnataka', 1),
(17, 'Kerala', 1),
(18, 'Madhya Pradesh', 1),
(19, 'Maharashtra', 1),
(20, 'Manipur', 1),
(21, 'Meghalaya', 1),
(22, 'Mizoram', 1),
(23, 'Nagaland', 1),
(24, 'Odisha', 1),
(25, 'Punjab', 1),
(26, 'Rajasthan', 1),
(27, 'Sikkim', 1),
(28, 'Tamil Nadu', 1),
(29, 'Telangana', 1),
(30, 'Tripura', 1),
(31, 'Uttar Pradesh', 1),
(32, 'Uttarakhand', 1),
(33, 'West Bengal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(11) NOT NULL,
  `vFirstName` varchar(64) NOT NULL,
  `vLastName` varchar(64) NOT NULL,
  `vMiddleName` varchar(64) DEFAULT NULL,
  `dDOB` date NOT NULL,
  `iMobile` int(11) NOT NULL,
  `vEmail` varchar(128) DEFAULT NULL,
  `iPinCode` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vFirstName`, `vLastName`, `vMiddleName`, `dDOB`, `iMobile`, `vEmail`, `iPinCode`) VALUES
(1, 'Anand', 'Thakkar', 'daasBhai', '1991-05-17', 927628395, 'anand.thakkar@gmail.com', '380018'),
(2, 'Seraj', 'Kumar', '', '2000-02-08', 229020345, 'seraj@khan.in', '843321'),
(3, 'Aman', 'Kashyap', 'Raj', '2002-02-01', 927628390, 'amanrajkashyap@khan.in', '380018'),
(4, 'Prashant', 'khan', NULL, '2012-02-16', 927628391, 'prashant@mail.com', '462021'),
(5, 'Kshitij', 'KUMAR', NULL, '2008-02-08', 823029922, 'desari@kjesari.com', '462022');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`iCityId`),
  ADD KEY `iStateId` (`iStateId`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`iCountryId`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`iFormId`),
  ADD KEY `iHotelid` (`iHotelid`,`iRoomId`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iRoomId` (`iRoomId`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`iHotelId`),
  ADD KEY `iPinId` (`vPinCode`);

--
-- Indexes for table `pincode`
--
ALTER TABLE `pincode`
  ADD PRIMARY KEY (`vPinCode`),
  ADD UNIQUE KEY `vPinCode` (`vPinCode`),
  ADD KEY `iCityId` (`iCityId`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`iRoomId`,`iHotelId`),
  ADD KEY `iHotelId` (`iHotelId`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`iStateId`),
  ADD KEY `iCountryId` (`iCountryId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`),
  ADD KEY `iPinId` (`iPinCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `iFormId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `iHotelId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `iRoomId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`iStateId`) REFERENCES `state` (`iStateId`);

--
-- Constraints for table `forms`
--
ALTER TABLE `forms`
  ADD CONSTRAINT `forms_ibfk_1` FOREIGN KEY (`iHotelid`) REFERENCES `rooms` (`iHotelId`),
  ADD CONSTRAINT `forms_ibfk_2` FOREIGN KEY (`iRoomId`) REFERENCES `rooms` (`iRoomId`),
  ADD CONSTRAINT `forms_ibfk_3` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `hotel_ibfk_1` FOREIGN KEY (`vPinCode`) REFERENCES `pincode` (`vPinCode`);

--
-- Constraints for table `pincode`
--
ALTER TABLE `pincode`
  ADD CONSTRAINT `pincode_ibfk_1` FOREIGN KEY (`iCityId`) REFERENCES `city` (`iCityId`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`iHotelId`) REFERENCES `hotel` (`iHotelId`);

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_ibfk_1` FOREIGN KEY (`iCountryId`) REFERENCES `country` (`iCountryId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
