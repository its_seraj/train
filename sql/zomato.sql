-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 15, 2022 at 01:10 PM
-- Server version: 5.7.36
-- PHP Version: 7.0.33-57+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zomato`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `iAddressId` int(11) NOT NULL,
  `vMapLocation` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `eAddressType` enum('Home','Work','Hotel','Other') COLLATE utf8_general_mysql500_ci NOT NULL,
  `vAddressComplete` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vPinCode` varchar(16) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`iAddressId`, `vMapLocation`, `eAddressType`, `vAddressComplete`, `vPinCode`, `iUserId`) VALUES
(1, '', 'Home', 'Law Garden, Ahmedabad', '380018', 1),
(2, '', 'Work', 'Hidden Brains', '380015', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `iCategoryId` int(11) NOT NULL,
  `vCategoryName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iRestaurantId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`iCategoryId`, `vCategoryName`, `iRestaurantId`) VALUES
(2, 'Combo and Siziers', 1),
(3, 'Soups and Salads', 1),
(4, 'Starters', 1),
(5, 'Main Course', 2),
(6, 'Breads', 2);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `iCityId` int(11) NOT NULL,
  `vCityName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iStateId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`iCityId`, `vCityName`, `iStateId`) VALUES
(1, 'Patna', 1),
(2, 'Ahmedabad', 2),
(3, 'Amaravati', 6),
(4, 'Itanagar', 7),
(5, 'Dispur', 8),
(7, 'Raipur', 10),
(8, 'Panaji', 13),
(9, 'Gandhinagar', 17),
(10, 'Chandigarh', 23),
(11, 'Shimla', 20),
(12, 'Ranchi', 25),
(13, 'Bengaluru', 2),
(14, 'Thiruvananthapuram', 23),
(15, 'Bhopal', 24),
(16, 'Mumbai', 14),
(17, 'Imphal', 31);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `iCountryId` int(11) NOT NULL,
  `vCountryName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`iCountryId`, `vCountryName`) VALUES
(1, 'India'),
(2, 'Nepal'),
(3, 'Pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `cuisines`
--

CREATE TABLE `cuisines` (
  `iCuisinesId` int(11) NOT NULL,
  `vCuisineName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iCuisineId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `cuisines`
--

INSERT INTO `cuisines` (`iCuisinesId`, `vCuisineName`, `iCuisineId`) VALUES
(1, 'North Indian', 1),
(2, 'Chinese', 1),
(3, 'Italian', 1),
(4, 'Thai', 1),
(5, 'Fast Food', 1),
(6, 'Beverages', 1),
(7, 'Mexican', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `iItemId` int(11) NOT NULL,
  `fItemPrice` int(11) NOT NULL,
  `iMenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`iItemId`, `fItemPrice`, `iMenuId`) VALUES
(1, 200, 1),
(2, 150, 2),
(3, 120, 3);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `iMenuId` int(11) NOT NULL,
  `vMenuName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vMenuIcon` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iRestaurantId` int(11) NOT NULL,
  `iCategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`iMenuId`, `vMenuName`, `vMenuIcon`, `iRestaurantId`, `iCategoryId`) VALUES
(1, 'Biryani', 'https://fgklf.fddf/dfd.jpg', 2, 2),
(2, 'Burger', 'https://fgklf.fddf/dfd.jpg', 2, 2),
(3, 'Chilly Paneer', 'https://fgklf.fddf/dfd.jpg', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `iOrderId` int(11) NOT NULL,
  `dOrderDate` date NOT NULL,
  `fOrderValue` float NOT NULL,
  `bOrderStatus` tinyint(1) NOT NULL,
  `iUserId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`iOrderId`, `dOrderDate`, `fOrderValue`, `bOrderStatus`, `iUserId`) VALUES
(1, '2022-02-01', 570.7, 1, 1),
(2, '2022-02-03', 120, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pincode`
--

CREATE TABLE `pincode` (
  `iPinId` int(11) NOT NULL,
  `vPinCode` varchar(16) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iCityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `pincode`
--

INSERT INTO `pincode` (`iPinId`, `vPinCode`, `iCityId`) VALUES
(1, '380015', 2),
(2, '380018', 2),
(3, '462022', 15),
(4, '462021', 15),
(5, '843321', 1),
(6, '840000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quantity`
--

CREATE TABLE `quantity` (
  `iQtyId` int(11) NOT NULL,
  `iQty` int(11) NOT NULL,
  `iItemId` int(11) NOT NULL,
  `iOrderId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `quantity`
--

INSERT INTO `quantity` (`iQtyId`, `iQty`, `iItemId`, `iOrderId`) VALUES
(1, 2, 1, 1),
(2, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rateMenu`
--

CREATE TABLE `rateMenu` (
  `irateMenuId` int(11) NOT NULL,
  `iRateMenu` int(11) NOT NULL,
  `iMenuId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `rateMenu`
--

INSERT INTO `rateMenu` (`irateMenuId`, `iRateMenu`, `iMenuId`, `iUserId`) VALUES
(1, 3, 3, 3),
(2, 5, 1, 1),
(3, 4, 2, 24);

-- --------------------------------------------------------

--
-- Table structure for table `rateRestaurant`
--

CREATE TABLE `rateRestaurant` (
  `iRateRestaurantId` int(11) NOT NULL,
  `iRateRestaurant` int(11) DEFAULT NULL,
  `iRestaurantId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `rateRestaurant`
--

INSERT INTO `rateRestaurant` (`iRateRestaurantId`, `iRateRestaurant`, `iRestaurantId`, `iUserId`) VALUES
(1, 5, 1, 1),
(2, 4, 1, 2),
(3, 3, 2, 5),
(4, 5, 2, 24);

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `iRestaurantId` int(11) NOT NULL,
  `vRestaurantName` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vRestaurantAddress` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vRestaurantIcon` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iCityId` int(11) NOT NULL,
  `iMobileId` int(11) NOT NULL,
  `iCuisineId` int(11) NOT NULL,
  `tOpenTime` time NOT NULL,
  `tCloseTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`iRestaurantId`, `vRestaurantName`, `vRestaurantAddress`, `vRestaurantIcon`, `iCityId`, `iMobileId`, `iCuisineId`, `tOpenTime`, `tCloseTime`) VALUES
(1, 'Swastik Pure veg', '1st Floor, Sachet 4, Jodhpur', 'https://fgklf.fddf/dfd.jpg', 2, 1, 1, '09:00:00', '11:00:00'),
(2, 'Bahrouz Biryani', '2nd Floor, Sachet 4, Jodhpur', 'https://fgklf.fddf/dfd.jpg', 2, 2, 1, '09:00:00', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `restaurantMobile`
--

CREATE TABLE `restaurantMobile` (
  `iNumberId` int(11) NOT NULL,
  `iMobileNumber` int(11) NOT NULL,
  `iMobileId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `restaurantMobile`
--

INSERT INTO `restaurantMobile` (`iNumberId`, `iMobileNumber`, `iMobileId`) VALUES
(1, 823233431, 1),
(2, 823111431, 1),
(3, 854233431, 1),
(4, 923233431, 2),
(5, 932334312, 2);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `iReviewId` int(11) NOT NULL,
  `vReview` varchar(1064) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iRestaurantId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`iReviewId`, `vReview`, `iRestaurantId`, `iUserId`) VALUES
(1, 'We had an amazing time and the food was too good. Apart from that, the cocktails are to die for!! Our server was Vinod and he was extremely helpful and good to us. We were a group of 7 and we had the best time w them. Do visit and chill here', 2, 25),
(2, 'We had an amazing time and the food was too good. Apart from that, the cocktails are to die for!! Our server was Vinod and he was extremely helpful and good to us. We were a group of 7 and we had the best time w them. Do visit and chill here', 2, 29),
(3, 'We had an amazing time and the food was too good. Apart from that, the cocktails are to die for!! Our server was Vinod and he was extremely helpful and good to us. We were a group of 7 and we had the best time w them. Do visit and chill here', 1, 27);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `iStateId` int(11) NOT NULL,
  `vStateCity` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iCountryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`iStateId`, `vStateCity`, `iCountryId`) VALUES
(1, 'Bihar', 1),
(2, 'Gujarat', 1),
(6, 'Andhra Pradesh', 1),
(7, 'Arunachal Pradesh', 1),
(8, 'Assam', 1),
(9, 'Bihar', 1),
(10, 'Chhattisgarh', 1),
(11, 'Goa', 1),
(12, 'Gujarat', 1),
(13, 'Haryana', 1),
(14, 'Himachal Pradesh', 1),
(15, 'Jharkhand', 1),
(16, 'Karnataka', 1),
(17, 'Kerala', 1),
(18, 'Madhya Pradesh', 1),
(19, 'Maharashtra', 1),
(20, 'Manipur', 1),
(21, 'Meghalaya', 1),
(22, 'Mizoram', 1),
(23, 'Nagaland', 1),
(24, 'Odisha', 1),
(25, 'Punjab', 1),
(26, 'Rajasthan', 1),
(27, 'Sikkim', 1),
(28, 'Tamil Nadu', 1),
(29, 'Telangana', 1),
(30, 'Tripura', 1),
(31, 'Uttar Pradesh', 1),
(32, 'Uttarakhand', 1),
(33, 'West Bengal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `iChatId` int(11) NOT NULL,
  `vChatMessage` varchar(1064) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `iUserId` int(11) NOT NULL,
  `dtChatTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vReply` varchar(1064) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `bChatStatus` tinyint(1) NOT NULL,
  `iOrderId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`iChatId`, `vChatMessage`, `iUserId`, `dtChatTime`, `vReply`, `bChatStatus`, `iOrderId`) VALUES
(1, 'i didn\'t got my order please refund my money', 1, '2022-02-10 10:44:45', 'Please wait, i am going to verify regarding your order. Thank you for Order with us.', 0, 1),
(2, '', 1, '2022-02-09 10:50:28', 'Sorry for that happens to you, i am going to proceed with refund yo the same transaction method.', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(11) NOT NULL,
  `vFirstName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vLastName` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iMobile` int(11) NOT NULL,
  `vEmail` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `vProfilePic` varchar(128) COLLATE utf8_general_mysql500_ci NOT NULL,
  `iPinId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vFirstName`, `vLastName`, `iMobile`, `vEmail`, `vProfilePic`, `iPinId`) VALUES
(1, 'Anand', 'Thakkar', 927628395, 'anand.thakkar@gmail.com', 'https://zomato.com/users/profile/b343.jpg', 1),
(2, 'Seraj', 'Kumar', 229020345, 'seraj@khan.in', 'https://zomato.com/seraj.jpg', 1),
(3, 'Aman', 'khan', 229020347, 'seraj@khan.in', 'https://zomato.com/seraj.jpg', 2),
(4, 'Prashant', 'khan', 927628391, 'prashant@mail.com', 'https://domain.com/pr.jpg', 2),
(5, 'Kshitij', 'khan', 927628390, 'prashant@mail.com', 'https://domain.com/pr.jpg', 1),
(6, 'Wooklie', 'khan', 1232432, 'wookie@shoke.org', 'https://wookie.jpg', 1),
(19, 'Rahu', 'khan', 82937377, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(20, 'Sau8rabh', 'khan', 83937376, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(21, 'Rahul', 'khan', 82917375, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(22, 'Ramu', 'khan', 82937374, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(23, 'Jacob', 'khan', 82937370, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(24, 'Jogindrer', 'khan', 8293739, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(25, 'Aiyer', 'khan', 8293737, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(26, 'Layera', 'khan', 8283737, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(27, 'Hondu', 'khan', 8292737, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(28, 'Pigoosee', 'khan', 82393737, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(29, 'Niuyaqti', 'khan', 8243737, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1),
(30, 'Kayali', 'khan', 8297337, 'desari@kjesari.com', 'https://lndore.state/gfjhd.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`iAddressId`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iPinCode` (`vPinCode`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`iCategoryId`),
  ADD KEY `iRestaurantId` (`iRestaurantId`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`iCityId`),
  ADD KEY `iPinId` (`iStateId`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`iCountryId`);

--
-- Indexes for table `cuisines`
--
ALTER TABLE `cuisines`
  ADD PRIMARY KEY (`iCuisinesId`),
  ADD KEY `iCuisineId` (`iCuisineId`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`iItemId`),
  ADD KEY `iMenuId` (`iMenuId`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`iMenuId`),
  ADD KEY `iRestaurantId` (`iRestaurantId`),
  ADD KEY `iCategoryId` (`iCategoryId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`iOrderId`),
  ADD UNIQUE KEY `iMobile_2` (`iUserId`),
  ADD KEY `iMobile` (`iUserId`),
  ADD KEY `iMobile_3` (`iUserId`),
  ADD KEY `iMobile_4` (`iUserId`),
  ADD KEY `dOrderDate` (`dOrderDate`);

--
-- Indexes for table `pincode`
--
ALTER TABLE `pincode`
  ADD PRIMARY KEY (`iPinId`),
  ADD UNIQUE KEY `vPinCode_2` (`vPinCode`),
  ADD KEY `iAddressId` (`iCityId`),
  ADD KEY `vPinCode` (`vPinCode`);

--
-- Indexes for table `quantity`
--
ALTER TABLE `quantity`
  ADD PRIMARY KEY (`iQtyId`),
  ADD KEY `iItemId` (`iItemId`),
  ADD KEY `iUserId` (`iOrderId`);

--
-- Indexes for table `rateMenu`
--
ALTER TABLE `rateMenu`
  ADD PRIMARY KEY (`irateMenuId`),
  ADD KEY `iMenuId` (`iMenuId`),
  ADD KEY `iUserId` (`iUserId`);

--
-- Indexes for table `rateRestaurant`
--
ALTER TABLE `rateRestaurant`
  ADD PRIMARY KEY (`iRateRestaurantId`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iRestaurantId` (`iRestaurantId`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`iRestaurantId`),
  ADD KEY `iCityId` (`iCityId`),
  ADD KEY `iMobileId` (`iMobileId`),
  ADD KEY `iCuisinesId` (`iCuisineId`);

--
-- Indexes for table `restaurantMobile`
--
ALTER TABLE `restaurantMobile`
  ADD PRIMARY KEY (`iNumberId`),
  ADD KEY `iMobileId` (`iMobileId`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`iReviewId`),
  ADD KEY `iRestaurantId` (`iRestaurantId`),
  ADD KEY `iUserId` (`iUserId`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`iStateId`),
  ADD KEY `iCityId` (`iCountryId`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`iChatId`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iOrderId` (`iOrderId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`),
  ADD UNIQUE KEY `iMobile` (`iMobile`),
  ADD KEY `iPinId` (`iPinId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `iAddressId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `iCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `iCityId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `iCountryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cuisines`
--
ALTER TABLE `cuisines`
  MODIFY `iCuisinesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `iItemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `iMenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `iOrderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pincode`
--
ALTER TABLE `pincode`
  MODIFY `iPinId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `quantity`
--
ALTER TABLE `quantity`
  MODIFY `iQtyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rateMenu`
--
ALTER TABLE `rateMenu`
  MODIFY `irateMenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rateRestaurant`
--
ALTER TABLE `rateRestaurant`
  MODIFY `iRateRestaurantId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `iRestaurantId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restaurantMobile`
--
ALTER TABLE `restaurantMobile`
  MODIFY `iNumberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `iReviewId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `iStateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `iChatId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`iRestaurantId`) REFERENCES `restaurant` (`iRestaurantId`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`iStateId`) REFERENCES `state` (`iStateId`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`iMenuId`) REFERENCES `menu` (`iMenuId`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`iRestaurantId`) REFERENCES `restaurant` (`iRestaurantId`),
  ADD CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`iCategoryId`) REFERENCES `category` (`iCategoryId`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `pincode`
--
ALTER TABLE `pincode`
  ADD CONSTRAINT `pincode_ibfk_1` FOREIGN KEY (`iCityId`) REFERENCES `city` (`iCityId`);

--
-- Constraints for table `quantity`
--
ALTER TABLE `quantity`
  ADD CONSTRAINT `quantity_ibfk_1` FOREIGN KEY (`iItemId`) REFERENCES `items` (`iItemId`),
  ADD CONSTRAINT `quantity_ibfk_2` FOREIGN KEY (`iOrderId`) REFERENCES `orders` (`iOrderId`);

--
-- Constraints for table `rateMenu`
--
ALTER TABLE `rateMenu`
  ADD CONSTRAINT `rateMenu_ibfk_1` FOREIGN KEY (`iMenuId`) REFERENCES `menu` (`iMenuId`),
  ADD CONSTRAINT `rateMenu_ibfk_2` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `rateRestaurant`
--
ALTER TABLE `rateRestaurant`
  ADD CONSTRAINT `rateRestaurant_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`),
  ADD CONSTRAINT `rateRestaurant_ibfk_2` FOREIGN KEY (`iRestaurantId`) REFERENCES `restaurant` (`iRestaurantId`);

--
-- Constraints for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD CONSTRAINT `restaurant_ibfk_1` FOREIGN KEY (`iCityId`) REFERENCES `city` (`iCityId`),
  ADD CONSTRAINT `restaurant_ibfk_2` FOREIGN KEY (`iCuisineId`) REFERENCES `cuisines` (`iCuisineId`),
  ADD CONSTRAINT `restaurant_ibfk_3` FOREIGN KEY (`iMobileId`) REFERENCES `restaurantMobile` (`iMobileId`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`iRestaurantId`) REFERENCES `restaurant` (`iRestaurantId`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_ibfk_1` FOREIGN KEY (`iCountryId`) REFERENCES `country` (`iCountryId`);

--
-- Constraints for table `support`
--
ALTER TABLE `support`
  ADD CONSTRAINT `support_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`),
  ADD CONSTRAINT `support_ibfk_2` FOREIGN KEY (`iOrderId`) REFERENCES `orders` (`iOrderId`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`iPinId`) REFERENCES `pincode` (`iPinId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
