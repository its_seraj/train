-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2022 at 05:49 PM
-- Server version: 5.7.37
-- PHP Version: 7.1.33-44+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whattsapp`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `statusTime` (`statusTime` TIMESTAMP) RETURNS TIME NO SQL
BEGIN
DECLARE TodayDate TIMESTAMP;
SELECT CURRENT_TIMESTAMP() INTO TodayDate;
RETURN TIMEDIFF(TodayDate,statusTime);
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `statusTimecount` (`tViewTime` TIMESTAMP, `tStatusTime` TIMESTAMP) RETURNS TIME NO SQL
BEGIN

    RETURN  TIMEDIFF(tStatusTime,tViewTime);
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `iChatId` int(3) NOT NULL,
  `iUserId` int(3) NOT NULL,
  `iUserIdTo` int(3) NOT NULL,
  `eType` enum('text','video','call') NOT NULL,
  `eChatInfo` enum('delivered','read') NOT NULL,
  `vChatDis` varchar(64) NOT NULL,
  `dtChatTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`iChatId`, `iUserId`, `iUserIdTo`, `eType`, `eChatInfo`, `vChatDis`, `dtChatTime`) VALUES
(1, 1, 2, 'text', 'read', 'hi', '2022-02-04 13:40:40'),
(2, 1, 3, 'video', 'delivered', 'video call', '2022-02-03 13:41:02'),
(3, 2, 1, 'text', 'read', 'hi', '2022-02-04 13:42:48'),
(4, 4, 5, 'text', 'read', 'hello', '2022-02-15 13:41:20'),
(5, 1, 5, 'text', 'delivered', 'task is done', '2022-02-14 13:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `iConId` int(3) NOT NULL,
  `iContactNo` int(12) NOT NULL,
  `iUserId` int(3) NOT NULL,
  `iCountryCode` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`iConId`, `iContactNo`, `iUserId`, `iCountryCode`) VALUES
(1, 123456789, 1, 91),
(2, 23456718, 2, 91),
(3, 123345567, 1, 92),
(4, 1233245, 2, 1),
(5, 124365778, 4, 1),
(6, 123324543, 5, 92),
(7, 1234677877, 6, 93),
(8, 1233245555, 7, 91),
(9, 1234677866, 3, 91);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `iCountryId` int(3) NOT NULL,
  `vCountryName` varchar(64) NOT NULL,
  `vCountryCode` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`iCountryId`, `vCountryName`, `vCountryCode`) VALUES
(3, 'Canada', 1),
(2, 'India', 91),
(1, 'Pakistan', 92),
(4, 'Afghanistan ', 93);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `iGId` int(3) NOT NULL,
  `vProfileImg` varchar(64) NOT NULL,
  `vGName` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`iGId`, `vProfileImg`, `vGName`) VALUES
(1, 'HB-img', 'HBA'),
(2, 'TCS-img', 'TCS');

-- --------------------------------------------------------

--
-- Table structure for table `groupsChatDis`
--

CREATE TABLE `groupsChatDis` (
  `iGChatDis` int(3) NOT NULL,
  `vMedia` varchar(128) DEFAULT NULL,
  `vDoc` varchar(128) DEFAULT NULL,
  `vLink` varchar(128) DEFAULT NULL,
  `iGChat` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupsChatDis`
--

INSERT INTO `groupsChatDis` (`iGChatDis`, `vMedia`, `vDoc`, `vLink`, `iGChat`) VALUES
(1, NULL, 'marksheet', NULL, 1),
(2, 'image', NULL, NULL, 2),
(7, 'video', NULL, NULL, 3),
(8, NULL, NULL, 'hiddenbrains.in', 4);

-- --------------------------------------------------------

--
-- Table structure for table `groupsMembers`
--

CREATE TABLE `groupsMembers` (
  `iGMId` int(3) NOT NULL,
  `iGId` int(3) NOT NULL,
  `iUserId` int(3) NOT NULL,
  `eIsAdmin` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupsMembers`
--

INSERT INTO `groupsMembers` (`iGMId`, `iGId`, `iUserId`, `eIsAdmin`) VALUES
(1, 1, 1, 'yes'),
(2, 1, 2, 'no'),
(3, 2, 1, 'no'),
(4, 2, 2, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `groupsMembersChat`
--

CREATE TABLE `groupsMembersChat` (
  `iGChat` int(3) NOT NULL,
  `iGId` int(3) NOT NULL,
  `iGMId` int(3) NOT NULL,
  `eType` enum('text','video','call') NOT NULL,
  `vChatDis` varchar(64) NOT NULL,
  `dtGChatTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groupsMembersChat`
--

INSERT INTO `groupsMembersChat` (`iGChat`, `iGId`, `iGMId`, `eType`, `vChatDis`, `dtGChatTime`) VALUES
(1, 1, 3, 'text', 'hi', '2022-02-01 13:37:57'),
(2, 1, 3, 'text', 'hello', '2022-02-02 13:38:38'),
(3, 2, 1, 'video', 'video call', '2022-02-03 13:38:45'),
(4, 2, 3, 'text', 'htr', '2022-02-02 13:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `statusView`
--

CREATE TABLE `statusView` (
  `iStatusViewId` int(8) NOT NULL,
  `iStatusId` int(3) NOT NULL,
  `iUserId` int(3) NOT NULL,
  `tViewTime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statusView`
--

INSERT INTO `statusView` (`iStatusViewId`, `iStatusId`, `iUserId`, `tViewTime`) VALUES
(1, 1, 2, '2022-02-02 13:50:44'),
(2, 1, 5, '2022-02-09 13:50:59'),
(3, 5, 3, '2022-02-11 09:10:14'),
(4, 6, 3, '2022-02-11 09:11:14'),
(5, 5, 1, '2022-02-11 09:11:47'),
(6, 6, 1, '2022-02-11 09:12:47'),
(7, 1, 7, '2022-02-01 15:20:22'),
(8, 2, 6, '2022-02-11 09:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(3) NOT NULL,
  `vUserName` varchar(64) NOT NULL,
  `vProfileImg` varchar(64) DEFAULT NULL,
  `vUserAbout` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vUserName`, `vProfileImg`, `vUserAbout`) VALUES
(1, 'prashant ', 'prashant-img', 'At office'),
(2, 'ritik', 'ritik-img', 'At school'),
(3, 'ankit', 'ankit-img', 'At home'),
(4, 'aman', 'aman-img', 'At office'),
(5, 'seraj', '', 'At home'),
(6, 'kshitij', '', 'At collage'),
(7, 'lala', 'lala-img', 'at bank');

-- --------------------------------------------------------

--
-- Table structure for table `u_status`
--

CREATE TABLE `u_status` (
  `iStatusId` int(3) NOT NULL,
  `eStatusType` enum('text','image','video','link') NOT NULL,
  `iUserId` int(3) NOT NULL,
  `tStatusTime` timestamp NULL DEFAULT NULL,
  `eIsActive` enum('Yes','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `u_status`
--

INSERT INTO `u_status` (`iStatusId`, `eStatusType`, `iUserId`, `tStatusTime`, `eIsActive`) VALUES
(1, 'image', 1, '2022-02-01 13:51:57', 'Yes'),
(2, 'image', 1, '2022-02-09 12:52:12', 'Yes'),
(5, 'image', 2, '2022-02-11 09:10:06', 'Yes'),
(6, 'image', 2, '2022-02-11 09:11:06', 'Yes'),
(7, 'video', 5, '2022-02-14 11:08:53', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`iChatId`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iUserIdTo` (`iUserIdTo`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`iConId`),
  ADD UNIQUE KEY `iContactNo` (`iContactNo`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `vConutryCode` (`iCountryCode`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`vCountryCode`),
  ADD UNIQUE KEY `iCountryId` (`iCountryId`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`iGId`);

--
-- Indexes for table `groupsChatDis`
--
ALTER TABLE `groupsChatDis`
  ADD PRIMARY KEY (`iGChatDis`),
  ADD UNIQUE KEY `iGChat` (`iGChat`),
  ADD KEY `iGChat_2` (`iGChat`);

--
-- Indexes for table `groupsMembers`
--
ALTER TABLE `groupsMembers`
  ADD PRIMARY KEY (`iGMId`),
  ADD KEY `iGId` (`iGId`),
  ADD KEY `IUserId` (`iUserId`);

--
-- Indexes for table `groupsMembersChat`
--
ALTER TABLE `groupsMembersChat`
  ADD PRIMARY KEY (`iGChat`),
  ADD KEY `iGId` (`iGId`),
  ADD KEY `iGMId` (`iGMId`);

--
-- Indexes for table `statusView`
--
ALTER TABLE `statusView`
  ADD PRIMARY KEY (`iStatusViewId`),
  ADD KEY `iStatusId` (`iStatusId`),
  ADD KEY `iUserId_2` (`iUserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`);

--
-- Indexes for table `u_status`
--
ALTER TABLE `u_status`
  ADD PRIMARY KEY (`iStatusId`),
  ADD KEY `IUserId` (`iUserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `iChatId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `iConId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `iGId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `groupsChatDis`
--
ALTER TABLE `groupsChatDis`
  MODIFY `iGChatDis` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `groupsMembers`
--
ALTER TABLE `groupsMembers`
  MODIFY `iGMId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groupsMembersChat`
--
ALTER TABLE `groupsMembersChat`
  MODIFY `iGChat` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `statusView`
--
ALTER TABLE `statusView`
  MODIFY `iStatusViewId` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `u_status`
--
ALTER TABLE `u_status`
  MODIFY `iStatusId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`),
  ADD CONSTRAINT `chats_ibfk_2` FOREIGN KEY (`iUserIdTo`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`),
  ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`iCountryCode`) REFERENCES `country` (`vCountryCode`);

--
-- Constraints for table `groupsChatDis`
--
ALTER TABLE `groupsChatDis`
  ADD CONSTRAINT `groupsChatDis_ibfk_1` FOREIGN KEY (`iGChat`) REFERENCES `groupsMembersChat` (`iGChat`);

--
-- Constraints for table `groupsMembers`
--
ALTER TABLE `groupsMembers`
  ADD CONSTRAINT `groupsMembers_ibfk_1` FOREIGN KEY (`iGId`) REFERENCES `groups` (`iGId`),
  ADD CONSTRAINT `groupsMembers_ibfk_2` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `groupsMembersChat`
--
ALTER TABLE `groupsMembersChat`
  ADD CONSTRAINT `groupsMembersChat_ibfk_1` FOREIGN KEY (`iGId`) REFERENCES `groups` (`iGId`),
  ADD CONSTRAINT `groupsMembersChat_ibfk_3` FOREIGN KEY (`iGMId`) REFERENCES `groupsMembers` (`iGMId`);

--
-- Constraints for table `statusView`
--
ALTER TABLE `statusView`
  ADD CONSTRAINT `statusView_ibfk_1` FOREIGN KEY (`iStatusId`) REFERENCES `u_status` (`iStatusId`),
  ADD CONSTRAINT `statusView_ibfk_2` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `u_status`
--
ALTER TABLE `u_status`
  ADD CONSTRAINT `u_status_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
