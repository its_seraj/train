$(() => {
    $(".signup-page").on("click", () => {
        $(".container").html(`
        <form id="register_account" onsubmit="return false">
            <p class="head">SignUp Form
                <button class="signin-page btn float-right">
                     <span class="badge badge-primary">SignIn</span>
                </button>
            </p>
            <div class="form-group">
              <label for=""></label>
              <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="helpId" placeholder="First Name" requred>
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="helpId" placeholder="Last Name" requred>
              </div>
            <div class="form-group">
                <label for=""></label>
                <input type="email" class="form-control" name="email" id="email" aria-describedby=""
                    placeholder="Enter Email">
            </div>
            <div class="form-group">
              <label for=""></label>
              <input type="file" class="form-control-file" name="profile_pic" id="profile_pic" placeholder="Choose Image" accept=".img,.png,.jpg,.jpeg,.svg">
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="cnf_password" id="cnf_password" placeholder="Confirm Password">
            </div>
            <button type="submit" class="btn btn-primary">Register</button>
        </form>
        `);
        register_account();
        $(".signin-page").on("click", () => { location.reload() });
    })
    function verify_account() {
        $("#login_form").on("submit", (e) => {
            e.preventDefault();
            $.ajax({
                url: "module.php",
                type: "post",
                data: { formData: $(e.target).serialize(), function: "login_form" },
                success: (data) => {
                    console.log(data)
                    if (data == 1) location.reload();
                    else $("#password").val("");

                }
            })
        })
    }
    verify_account();

    function register_account() {
        $("#register_account").on("submit", (e) => {
            e.preventDefault();
            var formData = new FormData(e.target);
            formData.append("function", "register_account");
            console.log(formData)

            $.ajax({
                url: "module.php",
                type: "post",
                contentType: false,
                processData: false,
                cache: false,
                data: formData,
                success: (data) => {
                    if (data == 1) {
                        $(e.target).trigger("reset");
                        $(e.target).append('<p class="success-msg">successfully created</p>');
                    }
                }
            })
        })
    }

    $(".logout").on("click", () => {
        $.ajax({
            url: "module.php",
            type: "post",
            data: { function: "logout" },
            success: (data) => {
                location.reload();
            }
        })
    });

    // menu slider
    $('.carousel').flickity({
        accessibility: true,
        adaptiveHeight: false,
        autoPlay: 2000,
        cellAlign: 'center',
        cellSelector: undefined,
        contain: false,
        draggable: '>1',
        dragThreshold: 3,
        friction: 0.2,
        groupCells: false,
        lazyLoad: true,
        initialIndex: 0,
        percentPosition: true,
        prevNextButtons: true,
        pageDots: true,
        resize: true,
        rightToLeft: false,
        watchCSS: false,
        wrapAround: false
    });


    // Homepage lazy load
    var start = 0;
    $(window).on("scroll", () => {
        if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
            start += 15;
            // console.log(start)
            data_on_scroll();
        }

    })
    // Live search on keyup
    $("#searchBox").on("keyup", search);

    function search() {
        var key = $("#searchBox").val();
        // reset start
        start = 0;

        $.ajax({
            url: "module.php",
            type: "POST",
            data: {
                key: key,
                function: "search",
                start: start
            },
            success: function (data) {
                $(".row").html(data);
            }
        })
    }
    // data on scroll
    function data_on_scroll() {
        var key = $("#searchBox").val();

        $.ajax({
            url: "module.php",
            type: "POST",
            data: {
                key: key,
                function: "search",
                start: start
            },
            success: function (data) {
                $(".row").append(data);
            }
        })
    }
    data_on_scroll();

    // scroll to top
    var page = document.querySelector(".pageUp");
    window.onscroll = () => {
        if (window.scrollY > $(window).height()) page.style.display = "block";
        else page.style.display = "none";
    }
    page.addEventListener("click", () => {
        window.scrollTo({top: 0, behavior: 'smooth'});
    })


    // logout caall
    $("#abs-btn").on("click", () => {
        $.ajax({
            url: "module.php",
            type: "POST",
            data: {function: "logout"},
            success: (data) => {location.reload()}
        })
    })

})