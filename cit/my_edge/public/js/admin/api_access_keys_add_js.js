/** api_access_keys module script */
Project.modules.api_access_keys = 
{
    init: function() 
    {
        valid_more_elements = [];
    },
    validate: function ()
    {
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore:".ignore-valid, .ignore-show-hide",
            rules : {
                "maa_accesskey": {
                    "required": true
                },
                "maa_apiclient": {
                    "required": true
                },
                "maa_apiversion": {
                    "required": true
                },
                "maa_expire_date": {
                    "required": true
                },
                "maa_status": {
                    "required": true
                }
            },
            messages : {
                "maa_accesskey": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.API_ACCESS_KEYS_ACCESSKEY)
                },
                "maa_apiclient": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.API_ACCESS_KEYS_API_CLIENT)
                },
                "maa_apiversion": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.API_ACCESS_KEYS_API_VERSION)
                },
                "maa_expire_date": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.API_ACCESS_KEYS_EXPIRE_DATE)
                },
                "maa_status": {
                    "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.API_ACCESS_KEYS_STATUS)
                }
            },
            errorPlacement : function(error, element)
            {
                switch(element.attr("name"))
                {
                    case 'maa_accesskey':
                        $('#'+element.attr('id')+'Err').html(error);
                        break;
                    case 'maa_apiclient':
                        $('#'+element.attr('id')+'Err').html(error);
                        break;
                    case 'maa_apiversion':
                        $('#'+element.attr('id')+'Err').html(error);
                        break;
                    case 'maa_expire_date':
                        $('#'+element.attr('id')+'Err').html(error);
                        break;
                    case 'maa_status':
                        $('#'+element.attr('id')+'Err').html(error);
                        break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
                
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {                    
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
        
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.CCEvents();
        callGoogleMapEvents();
        
    },
    callChilds: function(){
        
        callGoogleMapEvents();
    },
    initEvents: function(elem)
    {
        $('#maa_expire_date').datepicker({
            dateFormat : getAdminJSFormat('date'), 
            showOn : 'focus', 
            changeMonth : true, 
            changeYear : true, 
            yearRange : 'c-100:c+100',
            beforeShow: function(input, inst) {
                var cal = inst.dpDiv;
                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                setTimeout(function() {
                    cal.css({
                        'left': left
                    });
                }, 10);
            }
        });
        if(el_general_settings.mobile_platform){
            $('#maa_expire_date').attr('readonly', true);
        }
    },
    childEvents: function(elem, eleObj)
    {
        
    },
    CCEvents: function()
    {
        
    }
}
Project.modules.api_access_keys.init();
