$(document).ready(function () {
    
});

$(function(){
    $('#frmverifyprotected').validate({
        ignore: '.ignore-valid',
        rules: {
            vPassword: {
                required: true,
            },
        },
        messages: {
            vPassword: {
                required: js_lang_label.GENERIC_LOGIN_PASSWORD_ERR,
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "vPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
        },
        submitHandler: function () {
            submitForm();
            return false;
        }
    });
});

function closeWindow() {
    if (isFancyBoxActive()) {
        parent.$.fancybox.close();
    } else {
        window.location.hash = cus_enc_url_json['dashboard_sitemap'];
    }
}

function submitForm() {
    Project.show_adaxloading_div();
    var options = {
        url: verify_pwd_url,
        beforeSubmit: function (formData, jqForm, options) {
            prepareFinalFormData(formData, frm_key_settings);
        },
        success: function (respText, statText, xhr) {
            Project.ajaxSuccessCallback(respText, statText, xhr);
            var result = parseJSONString(respText);
            if (!result.success) {
                Project.setMessage(result.message, 0);
                Project.refreshCSRFToken(result);
                return false;
            } else {
                if (isFancyBoxActive()) {
                    parent.window.location.reload();
                    parent.$.fancybox.close();
                }
            }
        },
        complete: function (xhr, statTextt) {
            Project.hide_adaxloading_div();
            Project.ajaxCompleteCallback(xhr, statTextt);
        }
    };
    $('#frmverifyprotected').ajaxSubmit(options);
}