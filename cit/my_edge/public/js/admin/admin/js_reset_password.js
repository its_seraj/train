$(document).ready(function () {
    $('#password').focus();
    if (typeof is_ajax_submit != 'undefined') {
        resetPasswordValidateWithRemote();
    } else {
        resetPasswordValidate();
    }
});

if ($("#is_pattern").val() == "yes") {
    function setPattern(pattern) {
        if (pattern.length) {
            $("#password").val(pattern.join(""));
        }
    }
    $('#passwd-div').pattern({
        stop: function (event, ui) {
            setPattern(ui.pattern);
        }
    });
    $("#password").css('display', 'none');
}

function resetpassword() {
    if (!$("#frmresetpwd").valid()) {
        return false;
    }
}

function submitResetPwdForm() {
    Project.show_adaxloading_div();
    var options = {
        url: reset_pwd_url,
        type: "POST",
        beforeSubmit: function (formData, jqForm, options) {
            prepareFinalFormData(formData, frm_key_settings);
        },
        success: function (respText, statText, xhr) {
            Project.ajaxSuccessCallback(respText, statText, xhr);
            var result = parseJSONString(respText);
            if (!result.success) {
                Project.setMessage(result.message, 0);
                Project.refreshCSRFToken(result);
            } else {
                Project.setMessage(result.message, 1);
                if (result.url) {
                    window.location.href = result.url;
                }
            }
        },
        complete: function (xhr, statTextt) {
            Project.hide_adaxloading_div();
            Project.ajaxCompleteCallback(xhr, statTextt);
        }
    };
    $('#frmresetpwd').ajaxSubmit(options);
}

function resetPasswordValidate() {
    $("#frmresetpwd").validate({
        onfocusout: false,
        ignore: ".ignore-valid, .ignore-show-hide",
        rules: {
            "password": {
                "required": true,
                "minlength": 8,
                "alpha_numeric_capital_and_special_compulsory": true,
            },
            "retypepasswd": {
                "required": true,
                "equalTo": "#password"
            },
            "securitycode": {
                "required": true
            }
        },
        messages: {
            "password": {
                "required": js_lang_label.GENERIC_RESET_PASSWORD_ERR,
                "minlength": js_lang_label.GENERIC_MINIMUN_LENGTH_OF_PASSWORD_IS_8,
                "alpha_numeric_capital_and_special_compulsory": js_lang_label.GENERIC_ALPHA_NUMERIC_CAPITAL_AND_SPECIAL_COMPULSORY,
            },
            "retypepasswd": {
                "required": js_lang_label.GENERIC_RESET_RETYPE_PASSWORD_ERR,
                "equalTo": js_lang_label.GENERIC_RESET_RETYPE_NOT_MATCH_ERR
            },
            "securitycode": {
                "required": js_lang_label.GENERIC_RESET_SECURITY_CODE_ERR
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name")) {
                $('#' + element.attr('id') + 'Err').html(error);
            }
        },
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
}

function resetPasswordValidateWithRemote() {
    $("#frmresetpwd").validate({
        onfocusout: false,
        ignore: ".ignore-valid, .ignore-show-hide",
        rules: {
            "password": {
                "required": true,
                "minlength": 8,
                "alpha_numeric_capital_and_special_compulsory": true,
                "remote": {
                    url: check_pwd_url,
                    type: 'POST',
                    data: {
                        password: function () {
                            return $("#password").val();
                        }
                    },
                    dataFilter: function (response) {
                        response = $.parseJSON(response);
                        if (response.success)
                            return true;
                        else {
                            message = response.message;
                            return false;
                        }
                    }
                }
            },
            "retypepasswd": {
                "required": true,
                "equalTo": "#password"
            },
            "securitycode": {
                "required": true
            }
        },
        messages: {
            "password": {
                "required": js_lang_label.GENERIC_RESET_PASSWORD_ERR,
                "minlength": js_lang_label.GENERIC_MINIMUN_LENGTH_OF_PASSWORD_IS_8,
                "alpha_numeric_capital_and_special_compulsory": js_lang_label.GENERIC_ALPHA_NUMERIC_CAPITAL_AND_SPECIAL_COMPULSORY,
                remote: function (response) {
                    return message;
                }
            },
            "retypepasswd": {
                "required": js_lang_label.GENERIC_RESET_RETYPE_PASSWORD_ERR,
                "equalTo": js_lang_label.GENERIC_RESET_RETYPE_NOT_MATCH_ERR
            },
            "securitycode": {
                "required": js_lang_label.GENERIC_RESET_SECURITY_CODE_ERR
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name")) {
                $('#' + element.attr('id') + 'Err').html(error);
            }
        },
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        submitHandler: function (form) {
            submitResetPwdForm();
            return false;
        }
    });
}