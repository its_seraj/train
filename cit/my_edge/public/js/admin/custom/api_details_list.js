function afterStatusChange() {
	$.ajax({
        method: "POST",
        url: admin_url + cus_enc_url_json.replace_cache_file_content,
        dataType: 'json',
	data:{},
        success: function(data) {
			
        }
    });
}

$(document).on("click",".remove-cache-api-detail",function () {
    var api_name = $(this).attr('aria-api');
    $.ajax({
        method: "POST",
        url: admin_url + cus_enc_url_json.remove_api_cache,
        dataType: 'json',
            data:{
                'api_name': api_name
            },
        success: function(data) {
            Project.setMessage(data.message, data.success);
        }
    });
});