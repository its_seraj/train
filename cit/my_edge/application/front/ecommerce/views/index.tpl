<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LogIn</title>
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->
    <!-- bootstrap -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script> -->
    <!-- Google icons -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
    <!-- flickity from a CDN -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/flickity@latest/dist/flickity.min.css" rel="stylesheet"> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/flickity@2.2.1/dist/flickity.pkgd.min.js"></script> -->
    <!-- custom css -->
    <!-- <link rel="stylesheet" href="style.css"> -->
    <%$this->js->add_js("https://cdn.jsdelivr.net/npm/flickity@2.2.1/dist/flickity.pkgd.min.js")%>
    <%$this->css->add_css('ecommerce/https://cdn.jsdelivr.net/npm/flickity@latest/dist/flickity.min.css',
        'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons')
    %>
    <%$this->js->add_js('ecommerce/js.js')%>
</head>
<body>
    
    <!-- // Display Login Page -->
    <!-- <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <a class="navbar-brand" href="index.php">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav> -->

    <div class="container">'
        <!-- <form id="login_form" onsubmit="return false">
            <p class="head">LogIn Form
                <button class="signup-page btn float-right">
                    New <span class="badge badge-primary">SignUp</span>
                </button>
            </p>
            <div class="form-group">
                <label for=""></label>
                <input type="email" class="form-control" name="user_id" id="user_id" aria-describedby="helpId" placeholder="User Id">
                <div class="error"><?php echo $userId_error; ?></div>
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Enter Password">
                <div class="error"><?php echo $pass_error; ?></div>
            </div>
            <button type="submit" class="btn btn-primary">Sign In</button>
        </form> -->
    </div>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">'.$_SESSION["user_id"].'</a>
                </li>
            </ul>
        </div>

        <!-- <form onsubmit="return false" class="form-inline">
            <input class="form-control mr-sm-2" type="text" id="searchBox" placeholder="Search">
        </form> -->
    </nav>

    <!-- // homepage slider -->
    <div class="carousel"><!-- data-flickity="{ options here }" -->
        <div class="carousel-cell">Slide 1
            <img src="" alt="">
        </div>
        <div class="carousel-cell">Slide 2
            <img src="" alt="">
        </div>
        <div class="carousel-cell">Slide 3
            <img src="" alt="">
        </div>
    </div>

    <!-- // main container is here -->
    <div class="container">
        <div class="row">

            <%foreach from=$data key=k item=i%>
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22285%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20285%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17f4f7e3bf6%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17f4f7e3bf6%22%3E%3Crect%20width%3D%22285%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2298.77603912353516%22%20y%3D%2296.40000019073486%22%3EImage%20cap%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                    <div class="card-body">
                        <div class="product_name"><%$i['vProductName']%>></div>
                        <div class="product_cost"><%$i['dCostPrice']%>></div>
                        <div class="product_sell"><%$i['dSellingPrice']%>></div>
                    </div>
                </div>
            <%/foreach%>

        </div>
    </div>
    <div class="pageUp">
        <i class="material-icons" style="color: #fff;">keyboard_arrow_up</i>
    <!-- </div>
    <div class="abs-btn">
        <button id="btn-logout" class="btn btn-outline-primary">
            <i class="material-icons">power_settings_new</i>
        </button>
    </div> -->

    <!-- // logout button -->
    <button class="logout btn btn-outline-primary">
        <i class="material-icons">power_settings_new</i>
    </button>

</body>
</html>
