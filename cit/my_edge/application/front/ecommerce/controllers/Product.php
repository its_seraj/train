<?php
defined('BASEPATH') || exit('No direct script access allowed');

class Product extends Cit_Controller
{

    public function index(){
        $this->load->model('model_product');
        $data["data"] = $this->model_product->get_all();
        // print_r($data);exit;
        $this->loadView('index', $data);
    }

}