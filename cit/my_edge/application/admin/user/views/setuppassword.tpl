<div class="loginContainer login-form reset-page<%if $is_patternlock eq 'yes' %> reset-pattern<%/if%>">
    <div class="login-headbg"><%$this->lang->line('GENERIC_SETUP_PASSWORD')%></div>
    <div class="loginbox-border">
        <div>
            <form name="frmsetuppwd" id="frmsetuppwd" action="<%$setup_password_url%>" method="post" >
                <input type="hidden" name="userid" id="userid" value="<%$id%>">
                <input type="hidden" name="<%$csrf['name']%>" value="<%$csrf['hash']%>" />
                <div width="28%" class="bmatter1 relative">
                    <label for="passwd"><span class="icomoon-icon-locked-2 icon-user-pw"></span></label>
                    <input type="password"  title="<%$this->lang->line('GENERIC_NEW_PASSWORD')%>" name="password" id="password"  class="text" value="" size="25" maxlength="50"  placeholder="<%$this->lang->line('GENERIC_NEW_PASSWORD')%>"/>
                </div>
                <div class="clear"></div>
                <div class="error-msg login-error-msg" id='passwordErr'></div>
                <div class="bmatter relative">
                    <label for="passwd"><span class="icomoon-icon-locked-2 icon-user-pw"></span></label>
                    <input type="password" title="<%$this->lang->line('GENERIC_RETYPE_PASSWORD')%>" name="retypepasswd" id="retypepasswd" size="25" value="" maxlength="50" placeholder="<%$this->lang->line('GENERIC_RETYPE_PASSWORD')%>"/>
                </div>
                <div class="clear"></div>
                <div class="error-msg login-error-msg" id='retypepasswdErr'></div>

                <div class="reset-button-div">
                    <input type="submit" class="btn btn-info right" value="Setup Password" onclick="return setuppassword();">
                </div>
            </form>
        </div>
    </div>
</div>
<%javascript%>
    var frm_key_settings = JSON.parse('<%$frm_enc_info%>');
    var is_ajax_submit = 1;
    
    var setup_pwd_url = '<%$setup_password_url%>';
    var check_pwd_url = '<%$check_pwd_url%>';
<%/javascript%>
<%$this->js->add_js("admin/admin/js_setup_password.js")%>