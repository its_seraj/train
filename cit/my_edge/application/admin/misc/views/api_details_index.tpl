<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-list-container">
    <%include file="api_details_index_strip.tpl"%>
    <div class="<%$module_name%>" data-list-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content top-list-spacing">
                <div class="grid-data-container pad-calc-container">
                    <div class="top-list-tab-layout" id="top_list_grid_layout">
                    </div>
                    <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <!-- Module Listing Block -->
                            <td id="grid_data_col" class="<%$rl_theme_arr['grid_search_toolbar']%>">
                                <div id="pager2"></div>
                                <table id="list2"></table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" name="selAllRows" value="" id="selAllRows" />
    </div>
</div>
<!-- Module Listing Javascript -->
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module '] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';

    el_grid_settings['index_page_url'] = '<%$mod_enc_url["index"]%>';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['print_url'] =  admin_url+'<%$mod_enc_url["print_listing"]%>?<%$extra_qstr%>';
        
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = $.parseJSON('<%$hide_admin_rec|@json_encode%>');;
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '';
    el_grid_settings['hide_del_btn'] = '1';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['show_saved_search'] = 'No';
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = 'No';
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$edit_access%>';
    el_grid_settings['permit_view_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    el_grid_settings['permit_print_btn'] = '<%$print_access%>';
        
    el_grid_settings['group_search'] = '';
    el_grid_settings['default_sort'] = 'mad_apiname';
    el_grid_settings['sort_order'] = 'asc';
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['auto_refresh'] = 'No';
    el_grid_settings['lazy_loading'] = 'No';
    el_grid_settings['print_rec'] = 'No';
    el_grid_settings['print_list'] = 'No';
    
    el_grid_settings['subgrid'] = 'No';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['global_filter'] = 'No';
    
    el_grid_settings['search_slug'] = '<%$search_slug%>';
    el_grid_settings['search_list'] = $.parseJSON('<%$search_preferences|@json_encode%>');
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [{
        "name": "add",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_ADD_NEW')%>",
        "title": "<%$this->lang->line('API_DETAILS_ADD_NEW')%>",
        "icon": "icomoon-icon-plus-2",
        "icon_only": "No"
    },
    {
        "name": "del",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_DELETE')%>",
        "title": "<%$this->lang->line('API_DETAILS_DELETE_SELECTED_ROW')%>",
        "icon": "icomoon-icon-remove-6",
        "icon_only": "No"
    },
    {
        "name": "status_yes",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_YES')%>",
        "title": "<%$this->lang->line('API_DETAILS_YES')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "status_no",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_NO')%>",
        "title": "<%$this->lang->line('API_DETAILS_NO')%>",
        "icon": "silk-icon-popout",
        "icon_only": "No"
    },
    {
        "name": "search",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_SEARCH')%>",
        "title": "<%$this->lang->line('API_DETAILS_ADVANCE_SEARCH')%>",
        "icon": "icomoon-icon-search-3",
        "icon_only": "No"
    },
    {
        "name": "refresh",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_SHOW_ALL')%>",
        "title": "<%$this->lang->line('API_DETAILS_SHOW_ALL_LISTING_RECORDS')%>",
        "icon": "icomoon-icon-loop-2",
        "icon_only": "No"
    },
    {
        "name": "columns",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_COLUMNS')%>",
        "title": "<%$this->lang->line('API_DETAILS_HIDE_C47SHOW_COLUMNS')%>",
        "icon": "silk-icon-columns",
        "icon_only": "No"
    },
    {
        "name": "export",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_EXPORT')%>",
        "title": "<%$this->lang->line('API_DETAILS_EXPORT')%>",
        "icon": "icomoon-icon-out",
        "icon_only": "No"
    },
    {
        "name": "print",
        "type": "default",
        "text": "<%$this->lang->line('API_DETAILS_PRINT')%>",
        "title": "<%$this->lang->line('API_DETAILS_PRINT')%>",
        "icon": "icomoon-icon-printer-2",
        "icon_only": "No"
    },
    {
        "name": "custom_btn_1",
        "type": "custom",
        "text": "<%$this->lang->line('API_DETAILS_REMOVE_CACHE')%>",
        "title": "<%$this->lang->line('API_DETAILS_REMOVE_CACHE_FROM_ALL_APIS')%>",
        "icon": "icomoon-icon-remove-6",
        "icon_only": "Yes",
        "confirm": {
            "type": "confirm",
            "module": "<%$this->general->getAdminEncodeURL('misc\/api_details')%>",
            "title": "<%$this->lang->line('API_DETAILS_CONFIRM')%>",
            "body": {
                "type": "simple",
                "value": "<%$this->lang->line('API_DETAILS_ARE_YOU_SURE_WANT_TO_REMOVE_ALL_CACHE_C63')%>"
            },
            "action": {
                "type": "extended",
                "value": "removeAllAPIsFromCache"
            },
            "button": ["<%$this->lang->line('API_DETAILS_SUBMIT')%>",
            "<%$this->lang->line('API_DETAILS_CANCEL')%>"],
            "width": "300",
            "height": "150"
        }
    }];
    el_grid_settings['callbacks'] = [];
    el_grid_settings['message_arr'] = {
        "delete_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD')%>",
        "delete_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
        "status_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD_TO__C35STATUS_C35')%>",
        "status_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO__C35STATUS_C35_THIS_RECORDS_C63')%>",
    };
    
    js_col_name_json = [{
        "name": "mad_apiname",
        "label": "<%$list_config['mad_apiname']['label_lang']%>"
    },
    {
        "name": "mad_apifunction",
        "label": "<%$list_config['mad_apifunction']['label_lang']%>"
    },
    {
        "name": "mad_apicategory",
        "label": "<%$list_config['mad_apicategory']['label_lang']%>"
    },
    {
        "name": "mad_apimethod",
        "label": "<%$list_config['mad_apimethod']['label_lang']%>"
    },
    {
        "name": "mad_cache_enable",
        "label": "<%$list_config['mad_cache_enable']['label_lang']%>"
    },
    {
        "name": "mad_cache_expires",
        "label": "<%$list_config['mad_cache_expires']['label_lang']%>"
    },
    {
        "name": "action_api_details",
        "label": "<%$list_config['action_api_details']['label_lang']%>"
    }];
    
    js_col_model_json = [{
        "name": "mad_apiname",
        "index": "mad_apiname",
        "label": "<%$list_config['mad_apiname']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mad_apiname']['width']%>",
        "search": <%if $list_config['mad_apiname']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_apiname']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_apiname']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_apiname']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_apiname']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_apiname']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_apiname']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_apiname",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mad_apiname']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_apiname",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mad_apiname']['default']%>",
        "filterSopt": "bw",
        "formatter": formatAdminModuleEditLink,
        "unformat": unformatAdminModuleEditLink
    },
    {
        "name": "mad_apifunction",
        "index": "mad_apifunction",
        "label": "<%$list_config['mad_apifunction']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mad_apifunction']['width']%>",
        "search": <%if $list_config['mad_apifunction']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_apifunction']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_apifunction']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_apifunction']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_apifunction']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_apifunction']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_apifunction']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_apifunction",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mad_apifunction']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_apifunction",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mad_apifunction']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mad_apicategory",
        "index": "mad_apicategory",
        "label": "<%$list_config['mad_apicategory']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mad_apicategory']['width']%>",
        "search": <%if $list_config['mad_apicategory']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_apicategory']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_apicategory']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_apicategory']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_apicategory']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_apicategory']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_apicategory']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_apicategory",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mad_apicategory']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_apicategory",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mad_apicategory']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mad_apimethod",
        "index": "mad_apimethod",
        "label": "<%$list_config['mad_apimethod']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mad_apimethod']['width']%>",
        "search": <%if $list_config['mad_apimethod']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_apimethod']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_apimethod']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_apimethod']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_apimethod']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_apimethod']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_apimethod']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_apimethod",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mad_apimethod']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_apimethod",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mad_apimethod']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "mad_cache_enable",
        "index": "mad_cache_enable",
        "label": "<%$list_config['mad_cache_enable']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mad_cache_enable']['width']%>",
        "search": <%if $list_config['mad_cache_enable']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_cache_enable']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_cache_enable']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_cache_enable']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_cache_enable']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_cache_enable']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_cache_enable']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_cache_enable",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['mad_cache_enable']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["mad_cache_enable"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mad_cache_enable&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["mad_cache_enable"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["mad_cache_enable"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['mad_cache_enable']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mad_cache_enable"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_cache_enable",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mad_cache_enable&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['mad_cache_enable']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mad_cache_enable"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'API_DETAILS_CACHE_STATUS')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['mad_cache_enable']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mad_cache_expires",
        "index": "mad_cache_expires",
        "label": "<%$list_config['mad_cache_expires']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['mad_cache_expires']['width']%>",
        "search": <%if $list_config['mad_cache_expires']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mad_cache_expires']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mad_cache_expires']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mad_cache_expires']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mad_cache_expires']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mad_cache_expires']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mad_cache_expires']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": "mad_cache_expires",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['mad_cache_expires']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": "mad_cache_expires",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['mad_cache_expires']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "action_api_details",
        "index": "action_api_details",
        "label": "<%$list_config['action_api_details']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['action_api_details']['width']%>",
        "search": <%if $list_config['action_api_details']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['action_api_details']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['action_api_details']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['action_api_details']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['action_api_details']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['action_api_details']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['action_api_details']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "api_details",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['action_api_details']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "api_details",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['action_api_details']['default']%>",
        "filterSopt": "bw"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%$this->js->add_js("admin/custom/api_details_list.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 