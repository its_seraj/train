<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="api_details_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="api_details" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="api_details" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <textarea style="display:none;" name="mad_input_params" id="mad_input_params"  class='ignore-valid ' ><%$data['mad_input_params']%></textarea>
                        <input type="hidden" name="mad_apicategory" id="mad_apicategory" value="<%$data['mad_apicategory']|@htmlentities%>"  class='ignore-valid ' />
                        <input type="hidden" name="mad_apimethod" id="mad_apimethod" value="<%$data['mad_apimethod']|@htmlentities%>"  class='ignore-valid ' />
                        <textarea style="display:none;" name="mad_settings_json" id="mad_settings_json"  class='ignore-valid ' ><%$data['mad_settings_json']%></textarea>
                        <input type="hidden" name="mad_status" id="mad_status" value="<%$data['mad_status']%>"  class='ignore-valid ' />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('API_DETAILS_API_DETAILS')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_mad_apiname">
                                            <label class="form-label span3 ">
                                                <%$form_config['mad_apiname']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                                <%if $mode eq "Update"%>
                                                    <input type="hidden" class="ignore-valid" name="mad_apiname" id="mad_apiname" value="<%$data['mad_apiname']|@htmlentities%>" />
                                                    <span class="frm-data-label">
                                                        <strong>
                                                            <%if $data['mad_apiname'] neq ""%>
                                                                <%$data['mad_apiname']%>
                                                            <%else%>
                                                            <%/if%>
                                                        </strong></span>
                                                    <%else%>
                                                        <input type="text" placeholder="" value="<%$data['mad_apiname']|@htmlentities%>" name="mad_apiname" id="mad_apiname" title="<%$this->lang->line('API_DETAILS_API_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-large'  />
                                                    <%/if%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='mad_apinameErr'></label></div>
                                            </div>
                                            <div class="form-row row-fluid " id="cc_sh_mad_apifunction">
                                                <label class="form-label span3 ">
                                                    <%$form_config['mad_apifunction']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                                    <%if $mode eq "Update"%>
                                                        <input type="hidden" class="ignore-valid" name="mad_apifunction" id="mad_apifunction" value="<%$data['mad_apifunction']|@htmlentities%>" />
                                                        <span class="frm-data-label">
                                                            <strong>
                                                                <%if $data['mad_apifunction'] neq ""%>
                                                                    <%$data['mad_apifunction']%>
                                                                <%else%>
                                                                <%/if%>
                                                            </strong></span>
                                                        <%else%>
                                                            <input type="text" placeholder="" value="<%$data['mad_apifunction']|@htmlentities%>" name="mad_apifunction" id="mad_apifunction" title="<%$this->lang->line('API_DETAILS_API_FUNCTION')%>"  data-ctrl-type='textbox'  class='frm-size-large'  />
                                                        <%/if%>
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='mad_apifunctionErr'></label></div>
                                                </div>
                                                <div class="form-row row-fluid " id="cc_sh_mad_cache_params">
                                                    <label class="form-label span3 ">
                                                        <%$form_config['mad_cache_params']['label_lang']%>
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <%assign var="opt_selected" value=","|@explode:$data['mad_cache_params']%>
                                                        <%$this->dropdown->display("mad_cache_params","mad_cache_params[]","  title='<%$this->lang->line('API_DETAILS_CACHE_PARAMS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-large'  multiple='multiple'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'API_DETAILS_CACHE_PARAMS')%>'  ", "", "", $opt_selected,"mad_cache_params")%>
                                                        <a class='tipR' style='text-decoration: none;' href='javascript://' title="<%$this->lang->line('GENERIC_SELECT_ALL')%>" aria-chosen-select='mad_cache_params' aria-chosen-type='select' >
                                                            <span class='silk-icon-arrow-left arrow-image'></span>
                                                        </a>
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='mad_cache_paramsErr'></label></div>
                                                </div>
                                                <div class="form-row row-fluid " id="cc_sh_mad_description">
                                                    <label class="form-label span3 ">
                                                        <%$form_config['mad_description']['label_lang']%>
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <textarea placeholder=""  name="mad_description" id="mad_description" title="<%$this->lang->line('API_DETAILS_DESCRIPTION')%>"  data-ctrl-type='textarea'  class='elastic frm-size-large'  ><%$data['mad_description']%></textarea>
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='mad_descriptionErr'></label></div>
                                                </div>
                                                <div class="form-row row-fluid " id="cc_sh_mad_cache_enable">
                                                    <label class="form-label span3 ">
                                                        <%$form_config['mad_cache_enable']['label_lang']%>
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <%assign var="opt_selected" value=$data['mad_cache_enable']%>
                                                        <%$this->dropdown->display("mad_cache_enable","mad_cache_enable","  title='<%$this->lang->line('API_DETAILS_CACHE_ENABLE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-large'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'API_DETAILS_CACHE_ENABLE')%>'  ", "|||", "", $opt_selected,"mad_cache_enable")%>
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='mad_cache_enableErr'></label></div>
                                                </div>
                                                <%assign var="cb_mad_cache_expires" value=$func["mad_cache_expires"]%>
                                                <%if $cb_mad_cache_expires eq 0%>
                                                    <input type="hidden" name="mad_cache_expires" id="mad_cache_expires" value="<%$data['mad_cache_expires']|@htmlentities%>"  class='ignore-valid'  />
                                                <%else%>
                                                    <div class="form-row row-fluid " id="cc_sh_mad_cache_expires">
                                                        <label class="form-label span3 ">
                                                            <%$form_config['mad_cache_expires']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  <%if cb_mad_cache_expires eq 2%>frm-elements-div<%/if%> ">
                                                            <%if $cb_mad_cache_expires eq 2%>
                                                                <input type="hidden" class="ignore-valid" name="mad_cache_expires" id="mad_cache_expires" value="<%$data['mad_cache_expires']|@htmlentities%>" />
                                                                <span class="frm-data-label">
                                                                    <strong>
                                                                        <%if $data['mad_cache_expires'] neq ""%>
                                                                            <%$data['mad_cache_expires']%>
                                                                        <%else%>
                                                                        <%/if%>
                                                                    </strong></span>
                                                                <%else%>
                                                                    <input type="text" placeholder="" value="<%$data['mad_cache_expires']|@htmlentities%>" name="mad_cache_expires" id="mad_cache_expires" title="<%$this->lang->line('API_DETAILS_CACHE_EXPIRES')%>"  data-ctrl-type='textbox'  class='frm-size-large'  <%if $cb_mad_cache_expires eq 3%> disabled <%/if%>  />
                                                                <%/if%>
                                                            </div>
                                                            <div class="error-msg-form "><label class='error' id='mad_cache_expiresErr'></label></div>
                                                        </div>
                                                    <%/if%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                                            <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                                <%assign var='rm_ctrl_directions' value=true%>
                                            <%/if%>
                                            <%include file="api_details_add_buttons.tpl"%>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/api_details_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.api_details.callEvents();
<%/javascript%>