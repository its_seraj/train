<%assign var="method" value=$this->uri->segment(4)%>
<ul class="nav nav-tabs module-tab-container">
    <%if $method eq "add"%>
        <li class="active">
    <%else%>
        <li>
    <%/if%>
            <a class="tab-item item-country" 
                <%if $mode eq "Add" %>
                    title="<%$this->lang->line('GENERIC_ADD')%> API Key"
                <%else%>
                    title="<%$this->lang->line('GENERIC_EDIT')%> API Key"
                <%/if%>
                <%if $method eq "add"%> 
                    href="javascript://"
                <%else%> 
                    href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('tools/api_access_keys/add')%>|mode|<%$this->general->getAdminEncodeURL('Update')%>|id|<%$this->general->getAdminEncodeURL($iAccesskeyId)%>" 
                <%/if%>
                >
                <%if $mode eq "Add"%>
                    <%$this->lang->line('GENERIC_ADD')%> API Key
                <%else%>
                    <%$this->lang->line('GENERIC_EDIT')%> API Key
                <%/if%>
            </a>
        </li>
    <%if $method eq "apilist"%>
        <li class="active">
    <%else%>
        <li>
    <%/if%>
            <a class="tab-item item-state"  title="APIs <%$this->lang->line('GENERIC_LIST')%>" 
                <%if $mode eq "Update"%>
                    href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('tools/api_access_keys/apilist')%>|parID|<%$this->general->getAdminEncodeURL($data['iAccesskeyId'])%>"
                <%else%>
                    href="javascript://" aria-disabled="true" 
                <%/if%>                    
            >
                APIs <%$this->lang->line('GENERIC_LIST')%>
            </a>
        </li>
</ul>            