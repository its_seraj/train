<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container">
    <%include file="api_access_keys_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="api_access_keys" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="api_access_keys" class="frm-module-block frm-view-block frm-stand-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <input type="hidden" name="maa_apilist" id="maa_apilist" value="<%$data['maa_apilist']|@htmlentities%>"  class='ignore-valid ' />
                    <textarea style="display:none;" name="maa_request_limit" id="maa_request_limit"  class='ignore-valid ' ><%$data['maa_request_limit']%></textarea>
                    <textarea style="display:none;" name="maa_request_usage" id="maa_request_usage"  class='ignore-valid ' ><%$data['maa_request_usage']%></textarea>
                    <input type="hidden" name="maa_usage_updated" id="maa_usage_updated" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['maa_usage_updated'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="maa_added_by" id="maa_added_by" value="<%$data['maa_added_by']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="maa_date_added" id="maa_date_added" value="<%$this->general->dateSystemFormat($data['maa_date_added'])%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('API_ACCESS_KEYS_API_ACCESS_KEYS')%></h4></div>
                                <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                    <div class="form-row row-fluid " id="cc_sh_maa_accesskey">
                                        <label class="form-label span3">
                                            <%$form_config['maa_accesskey']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['maa_accesskey']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_maa_apiclient">
                                        <label class="form-label span3">
                                            <%$form_config['maa_apiclient']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['maa_apiclient']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_maa_apiversion">
                                        <label class="form-label span3">
                                            <%$form_config['maa_apiversion']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['maa_apiversion']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_maa_expire_date">
                                        <label class="form-label span3">
                                            <%$form_config['maa_expire_date']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div  input-append text-append-prepend ">
                                            <span class="frm-data-label"><strong><%$this->general->dateSystemFormat($data['maa_expire_date'])%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_maa_access_type">
                                        <label class="form-label span3">
                                            <%$form_config['maa_access_type']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['maa_access_type'], $opt_arr['maa_access_type'])%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_requests_per_minute">
                                        <label class="form-label span3">
                                            <%$form_config['requests_per_minute']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div  input-append text-append-prepend">
                                            <span id="cc_md_requests_per_minute">
                                                <span class="frm-data-label"><strong><%$data['requests_per_minute']%></strong></span>
                                            </span>
                                            <span id="cc_md_requests_per_hour">
                                                <span class="frm-data-label"><strong><%$data['requests_per_hour']%></strong></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_attempts_per_hour">
                                        <label class="form-label span3">
                                            <%$form_config['attempts_per_hour']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div  input-append text-append-prepend">
                                            <span id="cc_md_attempts_per_hour">
                                                <span class="frm-data-label"><strong><%$data['attempts_per_hour']%></strong></span>
                                            </span>
                                            <span id="cc_md_attempts_per_day">
                                                <span class="frm-data-label"><strong><%$data['attempts_per_day']%></strong></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_sys_static_field_1">
                                        <div class="form-right-div frm-elements-div form-static-div"><%include file="api_access_keys_cit_login_apis.tpl" %></div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_maa_status">
                                        <label class="form-label span3">
                                            <%$form_config['maa_status']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['maa_status'], $opt_arr['maa_status'])%></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
