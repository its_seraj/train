<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="api_access_keys_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="api_access_keys" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
                <div id="ad_form_outertab" class="module-navigation-tabs">
                    <%include file="api_access_keys_index_tabs.tpl" %>
                </div>
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="api_access_keys" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <input type="hidden" name="maa_apilist" id="maa_apilist" value="<%$data['maa_apilist']|@htmlentities%>"  class='ignore-valid ' />
                        <textarea style="display:none;" name="maa_request_limit" id="maa_request_limit"  class='ignore-valid ' ><%$data['maa_request_limit']%></textarea>
                        <textarea style="display:none;" name="maa_request_usage" id="maa_request_usage"  class='ignore-valid ' ><%$data['maa_request_usage']%></textarea>
                        <input type="hidden" name="maa_added_by" id="maa_added_by" value="<%$data['maa_added_by']|@htmlentities%>"  class='ignore-valid ' />
                        <input type="hidden" name="maa_date_added" id="maa_date_added" value="<%$this->general->dateSystemFormat($data['maa_date_added'])%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date' />
                        <input type="hidden" name="maa_usage_updated" id="maa_usage_updated" value="<%$this->general->dateTimeSystemFormat($data['maa_usage_updated'])%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date_and_time', 'dateFormat')%>'  aria-time-format='<%$this->general->getAdminJSFormats('date_and_time', 'timeFormat')%>'  aria-format-type='datetime' />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('API_ACCESS_KEYS_API_ACCESS_KEYS')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_maa_accesskey">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_accesskey']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['maa_accesskey']|@htmlentities%>" name="maa_accesskey" id="maa_accesskey" title="<%$this->lang->line('API_ACCESS_KEYS_ACCESSKEY')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  readonly='true'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_accesskeyErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_maa_apiclient">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_apiclient']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['maa_apiclient']|@htmlentities%>" name="maa_apiclient" id="maa_apiclient" title="<%$this->lang->line('API_ACCESS_KEYS_API_CLIENT')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_apiclientErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_maa_apiversion">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_apiversion']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['maa_apiversion']|@htmlentities%>" name="maa_apiversion" id="maa_apiversion" title="<%$this->lang->line('API_ACCESS_KEYS_API_VERSION')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_apiversionErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_maa_expire_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_expire_date']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateSystemFormat($data['maa_expire_date'])%>" placeholder="" name="maa_expire_date" id="maa_expire_date" title="<%$this->lang->line('API_ACCESS_KEYS_EXPIRE_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='<%$this->general->getAdminJSFormats('date', 'dateFormat')%>'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_expire_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_maa_access_type">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_access_type']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['maa_access_type']%>
                                                <%$this->dropdown->display("maa_access_type","maa_access_type","  title='<%$this->lang->line('API_ACCESS_KEYS_ACCESS_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'API_ACCESS_KEYS_ACCESS_TYPE')%>'  ", "|||", "", $opt_selected,"maa_access_type")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_access_typeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_requests_per_minute">
                                            <label class="form-label span3  frm-labels-merge">
                                                <%$form_config['requests_per_minute']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  frm-controls-merge">
                                                <div class="frm-merge-ctrl-block frm-merge-ctrl-25 input-append text-append-prepend" style='width:25%!important;' id="cc_md_requests_per_minute">
                                                    <input type="text" placeholder="" value="<%$data['requests_per_minute']|@htmlentities%>" name="requests_per_minute" id="requests_per_minute" title="<%$this->lang->line('API_ACCESS_KEYS_REQUESTS')%>"  data-ctrl-type='textbox'  class='frm-size-tiny ctrl-append-prepend '  />
                                                    <span class='add-on text-addon append-addon'>Per Minute</span>
                                                </div>
                                                <div class="frm-merge-ctrl-block frm-merge-ctrl-25 input-append text-append-prepend" style='width:25%!important;' id="cc_md_requests_per_hour">
                                                    <input type="text" placeholder="" value="<%$data['requests_per_hour']|@htmlentities%>" name="requests_per_hour" id="requests_per_hour" title="<%$this->lang->line('API_ACCESS_KEYS_REQUESTS_PER_HOUR')%>"  data-ctrl-type='textbox'  class='frm-size-tiny ctrl-append-prepend '  />
                                                    <span class='add-on text-addon append-addon'>Per Hour</span>
                                                </div>
                                            </div>
                                            <div class="error-msg-form  frm-errormsg-merge"><div class="frm-merge-eror-block frm-merge-ctrl-25" style='width:25%!important;'><label class='error' id='requests_per_minuteErr'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-25" style='width:25%!important;'><label class='error' id='requests_per_hourErr'></label></div></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_attempts_per_hour">
                                            <label class="form-label span3  frm-labels-merge">
                                                <%$form_config['attempts_per_hour']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  frm-controls-merge">
                                                <div class="frm-merge-ctrl-block frm-merge-ctrl-25 input-append text-append-prepend" style='width:25%!important;' id="cc_md_attempts_per_hour">
                                                    <input type="text" placeholder="" value="<%$data['attempts_per_hour']|@htmlentities%>" name="attempts_per_hour" id="attempts_per_hour" title="<%$this->lang->line('API_ACCESS_KEYS_ATTEMPTS')%>"  data-ctrl-type='textbox'  class='frm-size-tiny ctrl-append-prepend '  />
                                                    <span class='add-on text-addon append-addon'>Per Hour</span>
                                                </div>
                                                <div class="frm-merge-ctrl-block frm-merge-ctrl-25 input-append text-append-prepend" style='width:25%!important;' id="cc_md_attempts_per_day">
                                                    <input type="text" placeholder="" value="<%$data['attempts_per_day']|@htmlentities%>" name="attempts_per_day" id="attempts_per_day" title="<%$this->lang->line('API_ACCESS_KEYS_ATTEMPTS_PER_DAY')%>"  data-ctrl-type='textbox'  class='frm-size-tiny ctrl-append-prepend '  />
                                                    <span class='add-on text-addon append-addon'>Per Day</span>
                                                </div>
                                            </div>
                                            <div class="error-msg-form  frm-errormsg-merge"><div class="frm-merge-eror-block frm-merge-ctrl-25" style='width:25%!important;'><label class='error' id='attempts_per_hourErr'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-25" style='width:25%!important;'><label class='error' id='attempts_per_dayErr'></label></div></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_sys_static_field_1">
                                            <div class="form-right-div form-static-div"><%include file="api_access_keys_cit_login_apis.tpl" %></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_maa_status">
                                            <label class="form-label span3 ">
                                                <%$form_config['maa_status']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['maa_status']%>
                                                <%$this->dropdown->display("maa_status","maa_status","  title='<%$this->lang->line('API_ACCESS_KEYS_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'API_ACCESS_KEYS_STATUS')%>'  ", "|||", "", $opt_selected,"maa_status")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='maa_statusErr'></label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                                <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                    <%assign var='rm_ctrl_directions' value=true%>
                                <%/if%>
                                <%include file="api_access_keys_add_buttons.tpl"%>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/api_access_keys_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.api_access_keys.callEvents();
<%/javascript%>