<?php

namespace ZxcvbnPhp\Matchers;

use ZxcvbnPhp\Matcher;

class CustomLengthMatch extends Match
{

    static $_match_found = false;

    /**
     * @param string $password
     * @param array $userInputs
     * @return CustomLengthMatch[]
     */
    public static function match($password, array $userInputs = [])
    {
        $matches = [];
        $groups = static::findAll($password, "/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\!\@\#\$\%\^&\*\(\)_\+\|\~\-=\\`\{\}\[\]:\";'\<\>\?,\.\/])([a-zA-Z0-9\!\@\#\$\%\^&\*\(\)_\+\|\~\-=\\`\{\}\[\]:\";'\<\>\?,\.\/]+)/u");
        if (is_array($groups) && count($groups) > 0) {
            self::$_match_found = true;
        } else {
            self::$_match_found = false;
            $matches[] = new static($password, 0, strlen($password)-1, $password);
        }
        return $matches;
    }

    public function getFeedback($isSoleMatch)
    {
        return [
            'warning' => "Easy pattern to guess",
            'suggestions' => [
                'Make sure it has digits,punctuations as well as letters',
            ]
        ];
    }

    protected function getRawGuesses()
    {
        return (self::$_match_found == false) ? 1 : 0;
    }
}
