<?php
defined('BASEPATH') || exit('No direct script access allowed');

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

/**
 * Description of PDF Export Library
 *
 * @category third_party
 * 
 * @package third_party
 *
 * @module XLSExport
 * 
 * @class Xls_export.php
 * 
 * @path application\third_party\Xls_export.php
 * 
 * @version 4.0
 * 
 * @author CIT Dev Team | Jon Gales
 * 
 * @since 28.12.2019
 * 
 */
class XLS_Writer
{

    public $module_name;
    public $ctrl_object;
    public $content_info;
    public $writer;
    public $data;
    public $name;
    protected $CI;

    /**
     * Loads data. Data is assumed to be an array
     *
     * @param array $data
     * 
     * @return void
     */
    public function __construct($data = array())
    {
        if (!is_array($data)) {
            throw new Exception('XLS_Writer only accepts data as array');
        }
        $this->CI = & get_instance();
        $this->data = $data;
    }

    /**
     * init mentod to be initialised.
     *
     * @return void
     */
    public function initialize($header = 'Records')
    {
        $this->setWriter();

        if (is_object($this->ctrl_object) && method_exists($this->ctrl_object, "citXLSInitializeCallback")) {
            $callback_res = $this->ctrl_object->citXLSInitializeCallback($this, $header);
            if ($callback_res == 2) {
                return;
            }
        }
        if (method_exists($this->CI->general, "citXLSInitializeCallback")) {
            $callback_res = $this->CI->general->citXLSInitializeCallback($this, $header);
            if ($callback_res == 2) {
                return;
            }
        }
    }

    /**
     * Writer object to be initialised.
     *
     * @return object
     */
    private function setWriter()
    {
        require_once $this->CI->config->item('third_party') . 'Excel/Spout/Autoloader/autoload.php';
        $this->writer = WriterFactory::create(Type::XLSX); // for XLSX files
    }

    /**
     * Module Name to be set.
     *
     * @return void
     */
    public function setModule($module = '')
    {
        $this->module_name = $module_name;
    }
    
    /**
     * Module Content to be set.
     *
     * @return void
     */
    public function setContent($content = array())
    {
        $this->content_info = $content;
    }

    /**
     * Controller Object to be set.
     *
     * @return void
     */
    public function setController($ctrl_obj = '')
    {
        $this->ctrl_object = $ctrl_obj;
    }

    /**
     * Output to be rendered here.
     *
     * @return void
     */
    public function output($headers = array(), $data = array(), $widths = array(), $aligns = array())
    {
        if (is_object($this->ctrl_object) && method_exists($this->ctrl_object, "citXLSWriteTableCallback")) {
            $callback_res = $this->ctrl_object->citXLSWriteTableCallback($this, $headers, $data, $widths, $aligns);
            if ($callback_res == 2) {
                return;
            }
            $this->data = array_merge(array($headers), $data);
        }
        if (method_exists($this->CI->general, "citXLSWriteTableCallback")) {
            $callback_res = $this->CI->general->citXLSWriteTableCallback($this, $headers, $data, $widths, $aligns);
            if ($callback_res == 2) {
                return;
            }
            $this->data = array_merge(array($headers), $data);
        }
        
        $file_name = $this->name;
        $file_name = str_replace(" ", "-", $file_name);
        $file_name = preg_replace('/[^A-Za-z0-9@.-_]/', '', $file_name);
        $file_name = $file_name . '-' . time() . '.xlsx';
        $temp_path = $this->CI->config->item('admin_upload_temp_path');
        if (!is_dir($temp_path)) {
            mkdir($temp_path, 0777);
            chmod($temp_path, 0777);
        }
        $file_path = $temp_path . $file_name;

        $this->writer->setTempFolder($temp_path);
        $this->writer->openToFile($file_path);
        $this->writer->addRows($this->data);
        $this->writer->close();

        $this->download($file_path);
        unlink($file_path);
    }

    /**
     * Download the XLS file
     *
     * @param string $path
     * 
     * @return void
     */
    public function download($path = '')
    {
        $mimetype = get_mime_by_extension($path);
        if (ob_get_length() > 0) {
            ob_end_clean();
        }
        ob_start();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Cache-Control: private", FALSE);
        header('Content-Disposition: attachment; filename=' . $this->name . '.xlsx');
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($path));
        if ($mimetype) {
            header("Content-Type: " . $mimetype);
        }
        flush();
        readfile($path);
    }

    /**
     * Sets proper attachment for the XLS output.
     *
     * @param string $name
     * 
     * @return void
     */
    public function headers($name)
    {
        $this->name = $name;
//        header('Content-Type: application/csv; charset=utf-8');
//        header("Content-disposition: attachment; filename={$name}.csv");
    }
}
