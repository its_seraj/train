<?php

require_once "vendor/autoload.php";

class Validator extends GUMP
{
    public $common_time_format = 'H:i';

    // public function __construct()
    // {
    //     parent::__construct();
    //     $rule_valid_msg_labels = array(
    //         "cit_nowhitespace"      => 'GENERIC_SERVER_VALIDATE_NOWHITESPACE',
    //         "cit_alpha_numeric_compulsory"          => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_COMPULSORY',
    //         "cit_alpha_numeric_special_compulsory"  => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_SPECIAL_COMPULSORY',
    //         "cit_alpha_numeric_capital_compulsory"  => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_CAPITAL_COMPULSORY',
    //         "cit_date"              => 'GENERIC_SERVER_VALIDATE_DATE',
    //         "cit_datetime"          => 'GENERIC_SERVER_VALIDATE_DATETIME',
    //         "cit_dateEqualTo"       => 'GENERIC_SERVER_VALIDATE_DATEEQUALTO',
    //         "cit_dateGreaterEqual"  => 'GENERIC_SERVER_VALIDATE_DATEGREATEREQUAL',
    //         "cit_dateGreaterThan"   => 'GENERIC_SERVER_VALIDATE_DATEGREATERTHAN',
    //         "cit_dateLessEqual"     => 'GENERIC_SERVER_VALIDATE_DATELESSEQUAL',
    //         "cit_dateLessThan"      => 'GENERIC_SERVER_VALIDATE_DATELESSTHAN',
    //         "cit_numEqualTo"        => 'GENERIC_SERVER_VALIDATE_NUMEQUALTO',
    //         "cit_numGreaterEqual"   => 'GENERIC_SERVER_VALIDATE_NUMGREATEREQUAL',
    //         "cit_numGreaterThan"    => 'GENERIC_SERVER_VALIDATE_NUMGREATERTHAN',
    //         "cit_numLessEqual"      => 'GENERIC_SERVER_VALIDATE_NUMLESSEQUAL',
    //         "cit_numLessThan"       => 'GENERIC_SERVER_VALIDATE_NUMLESSTHAN',
    //         "cit_range"             => 'GENERIC_SERVER_VALIDATE_RANGE',
    //         "cit_time"              => 'GENERIC_SERVER_VALIDATE_TIME',
    //         "cit_timeEqualTo"       => 'GENERIC_SERVER_VALIDATE_TIMEEQUALTO',
    //         "cit_timeGreaterEqual"  => 'GENERIC_SERVER_VALIDATE_TIMEGREATEREQUAL',
    //         "cit_timeGreaterThan"   => 'GENERIC_SERVER_VALIDATE_TIMEGREATERTHAN',
    //         "cit_timeLessEqual"     => 'GENERIC_SERVER_VALIDATE_TIMELESSEQUAL',
    //         "cit_timeLessThan"      => 'GENERIC_SERVER_VALIDATE_TIMELESSTHAN',
    //         "cit_file_ext"          => 'GENERIC_VALIDATION_FILE_TYPE_IS_NOT_ACCEPTABLE',
    //     );
    // }

    protected function getFieldConfig($field='')
    {
        if (array_key_exists($field, $this->validation_rules)) 
        {
            return $this->validation_rules[$field];
        }
        return array();
    }

	protected function validate_cit_nowhitespace($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/\S+$/i", $value) > 0;
    }
    
	protected function validate_cit_alpha_numeric_compulsory($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/[a-z].*[0-9]|[0-9].*[a-z]/i", $value) > 0;
    }

	protected function validate_cit_alpha_numeric_special_compulsory($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/^(?=.*[0-9])(?=.*[!@#$%^&*?_-])[a-zA-Z0-9!@#$%^&*?_-]{3,}$/", $value) > 0;
    }

	protected function validate_cit_alpha_numeric_capital_compulsory($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/^(?=.*d)(?=.*[a-z])(?=.*[A-Z])/", $value) > 0;
    }

	protected function validate_cit_unsigned_number($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/^+?[0-9]+$/", $value) > 0;
    }

	protected function validate_cit_zip_code($field, $input=array(), $params=array(), $value)
    {
        return preg_match("/^(?:[A-Z0-9]+([- ]?[A-Z0-9]+)*)?$/", $value) > 0;
    }

	protected function validate_cit_range($field, $input=array(), $params=array(), $value)
    {
    	return ($value >= $params[0] && $value <= $params[1]);
    }

	// Number related functions
	protected function validate_cit_numEqualTo($field, $input=array(), $params=array(), $value)
    {
    	$is_numeric = $this->is_valid($input, array($field => "numeric"));
        return ($is_numeric === true && $value == $input[$params[0]]);
    }

	protected function validate_cit_numGreaterEqual($field, $input=array(), $params=array(), $value)
    {
    	$is_numeric = $this->is_valid($input, array($field => "numeric"));
        return ($is_numeric === true && $value >= $input[$params[0]]);
    }

	protected function validate_cit_numGreaterThan($field, $input=array(), $params=array(), $value)
    {
    	$is_numeric = $this->is_valid($input, array($field => "numeric"));
        return ($is_numeric === true && $value > $input[$params[0]]);
    }

	protected function validate_cit_numLessEqual($field, $input=array(), $params=array(), $value)
    {
    	$is_numeric = $this->is_valid($input, array($field => "numeric"));
        return ($is_numeric === true && $value <= $input[$params[0]]);
    }

	protected function validate_cit_numLessThan($field, $input=array(), $params=array(), $value)
    {
    	$is_numeric = $this->is_valid($input, array($field => "numeric"));
        return ($is_numeric === true && $value < $input[$params[0]]);
    }

    // Date related functions
	protected function validate_cit_date($field, $input=array(), $params=array(), $value)
    {
    	global $CI;
        $is_valid_format = $this->is_valid($input, array($field => array("date"=>array($params[0]))));
        return ($is_valid_format === true);
    }

	protected function validate_cit_datetime($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $is_valid_format = $this->is_valid($input, array($field => array("date"=>array($params[0]))));
        return ($is_valid_format === true);
    }
    
	protected function validate_cit_dateEqualTo($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        
        $source_config = $this->getFieldConfig($field);
        if (array_key_exists("cit_datetime", $source_config)) {
            $source_key = "cit_datetime";
            $is_source_valid = $this->validate_cit_datetime($field, $input, $source_config["cit_datetime"], $value);
            $source_value = $CI->general->formatServerDateTime($source_config["cit_datetime"][0], $value);
        } else {
            $source_key = "cit_date";
            $is_source_valid = $this->validate_cit_date($field, $input, $source_config["cit_date"], $value);
            $source_value = $CI->general->formatServerDate($source_config["cit_date"][0], $value);
        }

        $target_config = $this->getFieldConfig($params[0]);
        if (array_key_exists("cit_datetime", $target_config)) {
            $target_key = "cit_datetime";
            $is_target_valid = $this->validate_cit_datetime($params[0], $input, $target_config["cit_datetime"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDateTime($target_config["cit_datetime"][0], $input[$params[0]]);
        } else {
            $target_key = "cit_date";
            $is_target_valid = $this->validate_cit_date($params[0], $input, $target_config["cit_date"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDate($target_config["cit_date"][0], $input[$params[0]]);
        }
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) == strtotime($target_value);
    }
    
	protected function validate_cit_dateGreaterEqual($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        
        $source_config = $this->getFieldConfig($field);
        if (array_key_exists("cit_datetime", $source_config)) {
            $source_key = "cit_datetime";
            $is_source_valid = $this->validate_cit_datetime($field, $input, $source_config["cit_datetime"], $value);
            $source_value = $CI->general->formatServerDateTime($source_config["cit_datetime"][0], $value);
        } else {
            $source_key = "cit_date";
            $is_source_valid = $this->validate_cit_date($field, $input, $source_config["cit_date"], $value);
            $source_value = $CI->general->formatServerDate($source_config["cit_date"][0], $value);
        }

        $target_config = $this->getFieldConfig($params[0]);
        if (array_key_exists("cit_datetime", $target_config)) {
            $target_key = "cit_datetime";
            $is_target_valid = $this->validate_cit_datetime($params[0], $input, $target_config["cit_datetime"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDateTime($target_config["cit_datetime"][0], $input[$params[0]]);
        } else {
            $target_key = "cit_date";
            $is_target_valid = $this->validate_cit_date($params[0], $input, $target_config["cit_date"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDate($target_config["cit_date"][0], $input[$params[0]]);
        }
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) >= strtotime($target_value);
    }
    
	protected function validate_cit_dateGreaterThan($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        
        $source_config = $this->getFieldConfig($field);
        if (array_key_exists("cit_datetime", $source_config)) {
            $source_key = "cit_datetime";
            $is_source_valid = $this->validate_cit_datetime($field, $input, $source_config["cit_datetime"], $value);
            $source_value = $CI->general->formatServerDateTime($source_config["cit_datetime"][0], $value);
        } else {
            $source_key = "cit_date";
            $is_source_valid = $this->validate_cit_date($field, $input, $source_config["cit_date"], $value);
            $source_value = $CI->general->formatServerDate($source_config["cit_date"][0], $value);
        }

        $target_config = $this->getFieldConfig($params[0]);
        if (array_key_exists("cit_datetime", $target_config)) {
            $target_key = "cit_datetime";
            $is_target_valid = $this->validate_cit_datetime($params[0], $input, $target_config["cit_datetime"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDateTime($target_config["cit_datetime"][0], $input[$params[0]]);
        } else {
            $target_key = "cit_date";
            $is_target_valid = $this->validate_cit_date($params[0], $input, $target_config["cit_date"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDate($target_config["cit_date"][0], $input[$params[0]]);
        }
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) > strtotime($target_value);
    }

	protected function validate_cit_dateLessEqual($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        
        $source_config = $this->getFieldConfig($field);
        if (array_key_exists("cit_datetime", $source_config)) {
            $source_key = "cit_datetime";
            $is_source_valid = $this->validate_cit_datetime($field, $input, $source_config["cit_datetime"], $value);
            $source_value = $CI->general->formatServerDateTime($source_config["cit_datetime"][0], $value);
        } else {
            $source_key = "cit_date";
            $is_source_valid = $this->validate_cit_date($field, $input, $source_config["cit_date"], $value);
            $source_value = $CI->general->formatServerDate($source_config["cit_date"][0], $value);
        }

        $target_config = $this->getFieldConfig($params[0]);
        if (array_key_exists("cit_datetime", $target_config)) {
            $target_key = "cit_datetime";
            $is_target_valid = $this->validate_cit_datetime($params[0], $input, $target_config["cit_datetime"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDateTime($target_config["cit_datetime"][0], $input[$params[0]]);
        } else {
            $target_key = "cit_date";
            $is_target_valid = $this->validate_cit_date($params[0], $input, $target_config["cit_date"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDate($target_config["cit_date"][0], $input[$params[0]]);
        }
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) <= strtotime($target_value);
    }

	protected function validate_cit_dateLessThan($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        
        $source_config = $this->getFieldConfig($field);
        if (array_key_exists("cit_datetime", $source_config)) {
            $source_key = "cit_datetime";
            $is_source_valid = $this->validate_cit_datetime($field, $input, $source_config["cit_datetime"], $value);
            $source_value = $CI->general->formatServerDateTime($source_config["cit_datetime"][0], $value);
        } else {
            $source_key = "cit_date";
            $is_source_valid = $this->validate_cit_date($field, $input, $source_config["cit_date"], $value);
            $source_value = $CI->general->formatServerDate($source_config["cit_date"][0], $value);
        }

        $target_config = $this->getFieldConfig($params[0]);
        if (array_key_exists("cit_datetime", $target_config)) {
            $target_key = "cit_datetime";
            $is_target_valid = $this->validate_cit_datetime($params[0], $input, $target_config["cit_datetime"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDateTime($target_config["cit_datetime"][0], $input[$params[0]]);
        } else {
            $target_key = "cit_date";
            $is_target_valid = $this->validate_cit_date($params[0], $input, $target_config["cit_date"], $input[$params[0]]);
            $target_value = $CI->general->formatServerDate($target_config["cit_date"][0], $input[$params[0]]);
        }
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) < strtotime($target_value);
    }

    // Time related functions
    protected function validate_cit_time($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $is_valid_format = false;
        $conv_time_val = $CI->general->timeDefinedFormat($params[0], $value);
        if ($value == $conv_time_val) {
            $is_valid_format = true;
        }
        return ($is_valid_format === true);
    }
    
    protected function validate_cit_timeEqualTo($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $field_key = "cit_time";
        
        $source_config = $this->getFieldConfig($field);
        $is_source_valid = $this->validate_cit_time($field, $input, $source_config[$field_key], $value);
        $source_value = $CI->general->timeDefinedFormat($this->common_time_format, $value);

        $target_config = $this->getFieldConfig($params[0]);
        $is_target_valid = $this->validate_cit_time($params[0], $input, $target_config[$field_key], $input[$params[0]]);
        $target_value = $CI->general->timeDefinedFormat($this->common_time_format, $input[$params[0]]);
        
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) == strtotime($target_value);
    }
    
    protected function validate_cit_timeGreaterEqual($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $field_key = "cit_time";
        
        $source_config = $this->getFieldConfig($field);
        $is_source_valid = $this->validate_cit_time($field, $input, $source_config[$field_key], $value);
        $source_value = $CI->general->timeDefinedFormat($this->common_time_format, $value);

        $target_config = $this->getFieldConfig($params[0]);
        $is_target_valid = $this->validate_cit_time($params[0], $input, $target_config[$field_key], $input[$params[0]]);
        $target_value = $CI->general->timeDefinedFormat($this->common_time_format, $input[$params[0]]);
        
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) >= strtotime($target_value);
    }
    
    protected function validate_cit_timeGreaterThan($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $field_key = "cit_time";
        
        $source_config = $this->getFieldConfig($field);
        $is_source_valid = $this->validate_cit_time($field, $input, $source_config[$field_key], $value);
        $source_value = $CI->general->timeDefinedFormat($this->common_time_format, $value);

        $target_config = $this->getFieldConfig($params[0]);
        $is_target_valid = $this->validate_cit_time($params[0], $input, $target_config[$field_key], $input[$params[0]]);
        $target_value = $CI->general->timeDefinedFormat($this->common_time_format, $input[$params[0]]);
        
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) > strtotime($target_value);
    }

    protected function validate_cit_timeLessEqual($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $field_key = "cit_time";
        
        $source_config = $this->getFieldConfig($field);
        $is_source_valid = $this->validate_cit_time($field, $input, $source_config[$field_key], $value);
        $source_value = $CI->general->timeDefinedFormat($this->common_time_format, $value);

        $target_config = $this->getFieldConfig($params[0]);
        $is_target_valid = $this->validate_cit_time($params[0], $input, $target_config[$field_key], $input[$params[0]]);
        $target_value = $CI->general->timeDefinedFormat($this->common_time_format, $input[$params[0]]);
        
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) <= strtotime($target_value);
    }

    protected function validate_cit_timeLessThan($field, $input=array(), $params=array(), $value)
    {
        global $CI;
        $field_key = "cit_time";
        
        $source_config = $this->getFieldConfig($field);
        $is_source_valid = $this->validate_cit_time($field, $input, $source_config[$field_key], $value);
        $source_value = $CI->general->timeDefinedFormat($this->common_time_format, $value);

        $target_config = $this->getFieldConfig($params[0]);
        $is_target_valid = $this->validate_cit_time($params[0], $input, $target_config[$field_key], $input[$params[0]]);
        $target_value = $CI->general->timeDefinedFormat($this->common_time_format, $input[$params[0]]);
        
        return $is_source_valid === true && $is_target_valid === true && strtotime($source_value) < strtotime($target_value);
    }
}