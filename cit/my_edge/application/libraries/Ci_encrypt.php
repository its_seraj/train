<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Encryption Library
 *
 * @category libraries
 * 
 * @package libraries
 *
 * @module Encrypt
 * 
 * @class Ci_encrypt.php
 * 
 * @path application\libraries\Ci_encrypt.php
 * 
 * @version 4.0
 * 
 * @author CIT Dev Team
 * 
 * @since 01.08.2016
 */
class Ci_encrypt
{

    protected $CI;
    private $_decryptArr;
    private $_urlAlgo;
    private $_urlEnc;
    private $_urlKey;
    private $_urlIV;
    private $_dataAlgo;
    private $_dataEnc;
    private $_dataKey;
    private $_dataIV;

    public function __construct()
    {
        #$this->CI = & get_instance();
        $this->config = & load_class('Config', 'core');
        $this->_decryptArr = $this->config->item("FRAMEWORK_ENCRYPTS");

        $this->_urlEnc = $this->config->item("ADMIN_ENC_KEY");
        $this->_urlKey = md5($this->_urlEnc);
        $this->_urlAlgo = 'aes-128-cbc';
        $this->_urlIV = substr(sha1($this->_urlEnc), 0, 16);

        $this->_dataEnc = $this->config->item("DATA_ENCRYPT_KEY");
        $this->_dataKey = md5($this->_dataEnc);
        $this->_dataAlgo = $this->config->item("data_encryption_algorithm");
        if (empty($this->_dataAlgo)) {
            $this->_dataAlgo = 'aes-256-cbc';
        }
        $this->_dataIV = substr(sha1($this->_dataEnc), 0, 16);
    }

    public function decryptPostParameters($uri = '')
    {
        if (is_array($_POST) && count($_POST) > 0) {
            $custom_uris = $this->config->item('server', 'post_data_encryption_uris');
            if (is_array($custom_uris) && in_array($uri, $custom_uris)) {
                $_POST = $this->inputDecrpt($_POST);
            } else {
                $uri_arr = explode("/", $uri);
                $module = $uri_arr[2];
                $method = $uri_arr[3];
                if (in_array($method, array("addAction", "saveTabWiseBlock"))) {
                    $this->config->load('cit_modules', TRUE);
                    $all_modules = $this->config->item('cit_modules');
                    $module_info = $all_modules[$module];
                    if ($module_info['form_data_encryption']) {
                        $_POST = $this->inputDecrpt($_POST);
                    }
                }
            }
        }
        return true;
    }

    public function validateChecksumValue($uri = '')
    {
        if (is_array($_POST) && count($_POST) > 0) {
            $custom_uris = $this->config->item('server', 'checksum_validation_uris');
            if (is_array($custom_uris) && in_array($uri, $custom_uris)) {
                $result = $this->verifyChecksum($_POST);
                if (!$result['success']) {
                    show_error('The action you have requested is not allowed. ' . $result['message'], 403);
                }
            } else {
                $uri_arr = explode("/", $uri);
                $module = $uri_arr[2];
                $method = $uri_arr[3];
                if (in_array($method, array("addAction", "saveTabWiseBlock"))) {
                    $this->config->load('cit_modules', TRUE);
                    $all_modules = $this->config->item('cit_modules');
                    $module_info = $all_modules[$module];
                    if ($module_info['checksum_validation']) {
                        $result = $this->verifyChecksum($_POST);
                        if (!$result['success']) {
                            show_error('The action you have requested is not allowed. ' . $result['message'], 403);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function decryptURLParameters($uri = '')
    {
        if (is_array($_REQUEST) && count($_REQUEST) > 0) {
            $_REQUEST = $this->decryptKeyValuePairs($_REQUEST);
        }
        if (is_array($_GET) && count($_GET) > 0) {
            $_GET = $this->decryptKeyValuePairs($_GET);
        }
        if (is_array($_POST) && count($_POST) > 0) {
            $_POST = $this->decryptKeyValuePairs($_POST);
        }
        return true;
    }

    public function decryptKeyValuePairs($arr = array())
    {
        if (!is_array($arr) || count($arr) == 0) {
            return $arr;
        }
        foreach ($arr as $key => $val) {
            if (in_array($key, $this->_decryptArr) && $val != "") {
                if (in_array($key, array("id", "switchIDs")) && strstr($val, ",") !== FALSE) {
                    $list = explode(",", $val);
                    $item = array();
                    foreach ($list as $id) {
                        $item[] = $this->decrypt($id);
                    }
                    $arr[$key] = implode(",", $item);
                } else {
                    $arr[$key] = $this->decrypt($val);
                }
            }
        }
        return $arr;
    }

    public function isAllowEncURL($string = '')
    {
        $url_arr = explode("/", $string);
        if (!is_array($url_arr) || count($url_arr) == 0) {
            return false;
        }
        $omit_urls = $this->config->item("FRAMEWORK_URLS");
        $module_arr = $omit_urls[$url_arr[0]];
        if (!is_array($module_arr) || count($module_arr) == 0) {
            return false;
        }
        $ctrl_arr = $module_arr[$url_arr[1]];
        if (!is_array($ctrl_arr) || count($ctrl_arr) == 0) {
            return false;
        }
        if (in_arrray($url_arr[2], $ctrl_arr)) {
            return true;
        }
        return false;
    }

    public function encrypt($string = '', $url = false)
    {
        if ($url == true && $this->isAllowEncURL()) {
            return $string;
        }

        $crypted_text = openssl_encrypt($string, $this->_urlAlgo, $this->_urlKey, OPENSSL_RAW_DATA, $this->_urlIV);
        $enc_str = bin2hex($crypted_text);

        return $enc_str;
    }

    public function decrypt($string = '', $url = false)
    {
        if ($url == true && $this->isAllowEncURL()) {
            return $string;
        }

        if (function_exists("hex2bin")) {
            $string = hex2bin($string);
        } else {
            $string = $this->hextobin($string);
        }
        $dec_str = openssl_decrypt($string, $this->_urlAlgo, $this->_urlKey, OPENSSL_RAW_DATA, $this->_urlIV);

        return trim($dec_str);
    }

    public function hextobin($hexstr = '')
    {
        $n = strlen($hexstr);
        $sbin = "";
        $i = 0;
        while ($i < $n) {
            $a = substr($hexstr, $i, 2);
            $c = pack("H*", $a);
            if ($i == 0) {
                $sbin = $c;
            } else {
                $sbin .= $c;
            }
            $i += 2;
        }
        return $sbin;
    }

    public function dataEncrypt($string = '')
    {
        $string = trim($string);
        if ($string == "") {
            return $string;
        }

        $crypted_text = openssl_encrypt($string, $this->_dataAlgo, $this->_dataKey, OPENSSL_RAW_DATA, $this->_dataIV);
        $enc_str = bin2hex($crypted_text);

        return $enc_str;
    }

    public function dataDecrypt($string = '')
    {
        $string = trim($string);
        if ($string == "") {
            return $string;
        }
        if (function_exists("hex2bin")) {
            $string = hex2bin($string);
        } else {
            $string = $this->hextobin($string);
        }
        $dec_str = openssl_decrypt($string, $this->_dataAlgo, $this->_dataKey, OPENSSL_RAW_DATA, $this->_dataIV);

        return trim($dec_str);
    }

    public function inputDecrpt($data = array())
    {
        if (!is_array($data) || count($data) == 0) {
            return $data;
        }

        $salt_phrase = 'CIT';
        $iterations = '999';
        $key_phrase = md5($this->config->item("ADMIN_ENC_KEY"));
        $iv_phrase = substr(sha1($this->config->item("ADMIN_ENC_KEY")), 0, 16);
        $bypass_params = array('_', 'cit_checksum', 'ci_csrf_token', $this->config->item('csrf_token_name'));

        foreach ($data as $key => $val) {
            if (!empty($key) && in_array($key, $bypass_params)) {
                continue;
            }
            $param_val = str_replace(' ', '+', $val);
            $hash_key = hash_pbkdf2("sha256", $key_phrase, $salt_phrase, $iterations, 64);
            if (function_exists("hex2bin")) {
                $hash_key = hex2bin($hash_key);
            } else {
                $hash_key = $this->hextobin($hash_key);
            }
            if (is_array($val)) {
                $data[$key] = $this->inputDecrpt($val);
            } else {
                $str_output = openssl_decrypt(base64_decode($val), 'aes-256-cbc', $hash_key, OPENSSL_RAW_DATA, $iv_phrase);
                $data[$key] = $str_output;
            }
        }
        return $data;
    }

    public function verifyChecksum($data = array())
    {
        ksort($data);
        $params_arr = array();
        $bypass_params = array('_', 'cit_checksum', 'ci_csrf_token', $this->config->item('csrf_token_name'));

        foreach ($data as $key => $val) {
            if (!empty($key) && !in_array($key, $bypass_params)) {
                if (is_array($val)) {
                    if (!empty(array_filter($val, 'strlen'))) {
                        $params_arr[] = $key . "=" . json_encode($val);
                    }
                } else {
                    $params_arr[] = $key . "=" . $val;
                }
            }
        }

        $params_str = implode("", $params_arr);
        $return_hash = sha1($params_str);
        if (empty($data['cit_checksum'])) {
            $res_arr['success'] = 0;
            $res_arr['message'] = "Checksum not found..!";
        } elseif ($return_hash != $data['cit_checksum']) {
            $res_arr['success'] = 0;
            $res_arr['message'] = "Checksum failed..!";
        } else {
            $res_arr['success'] = 1;
            $res_arr['message'] = "Checksum successful..!";
        }
        return $res_arr;
    }

}

/* End of file Ci_encrypt.php */
/* Location: ./application/libraries/Ci_encrypt.php */