<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Customer Add Model
 *
 * @category webservice
 *
 * @package user
 *
 * @subpackage models
 *
 * @module Customer Add
 *
 * @class Customer_add_model.php
 *
 * @path application\webservice\user\models\Customer_add_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 17.11.2021
 */

class Customer_add_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
