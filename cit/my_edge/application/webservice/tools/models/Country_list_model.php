<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Country List Model
 *
 * @category webservice
 *
 * @package tools
 *
 * @subpackage models
 *
 * @module Country List
 *
 * @class Country_list_model.php
 *
 * @path application\webservice\tools\models\Country_list_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 17.11.2021
 */

class Country_list_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
