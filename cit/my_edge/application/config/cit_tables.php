<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array();

#####GENERATED_CONFIG_SETTINGS_START#####

$config["mod_admin"] = array(
    "name" => "mod_admin",
    "category" => "user",    
    "primary_key" => "iAdminId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_group"] = array(
    "name" => "mod_admin_group",
    "category" => "user",    
    "primary_key" => "iAdminGroupId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_menu"] = array(
    "name" => "mod_admin_menu",
    "category" => "misc",    
    "primary_key" => "iAdminMenuId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_navigation_log"] = array(
    "name" => "mod_admin_navigation_log",
    "category" => "user",    
    "primary_key" => "iNavigationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_notifications"] = array(
    "name" => "mod_admin_notifications",
    "category" => "user",    
    "primary_key" => "iNotificationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_passwords"] = array(
    "name" => "mod_admin_passwords",
    "category" => "user",    
    "primary_key" => "iPasswordId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_admin_preferences"] = array(
    "name" => "mod_admin_preferences",
    "category" => "user",    
    "primary_key" => "iAdminPreferencesId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_api_accesskeys"] = array(
    "name" => "mod_api_accesskeys",
    "category" => "misc",    
    "primary_key" => "iAccesskeyId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_api_accesslog"] = array(
    "name" => "mod_api_accesslog",
    "category" => "misc",    
    "primary_key" => "iAccessLogId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_api_details"] = array(
    "name" => "mod_api_details",
    "category" => "misc",    
    "primary_key" => "iAPIDetailsId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_application_master"] = array(
    "name" => "mod_application_master",
    "category" => "misc",    
    "primary_key" => "iApplicationMasterId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_application_version"] = array(
    "name" => "mod_application_version",
    "category" => "misc",    
    "primary_key" => "iApplicationVersionId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_cache_tables"] = array(
    "name" => "mod_cache_tables",
    "category" => "misc",    
    "primary_key" => "iCacheTableId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_capability_category"] = array(
    "name" => "mod_capability_category",
    "category" => "user",    
    "primary_key" => "iCapabilityCategoryId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_capability_master"] = array(
    "name" => "mod_capability_master",
    "category" => "user",    
    "primary_key" => "iCapabilityId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_city"] = array(
    "name" => "mod_city",
    "category" => "tools",    
    "primary_key" => "iCityId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_country"] = array(
    "name" => "mod_country",
    "category" => "tools",    
    "primary_key" => "iCountryId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_customer"] = array(
    "name" => "mod_customer",
    "category" => "user",    
    "primary_key" => "iCustomerId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_db_changelog"] = array(
    "name" => "mod_db_changelog",
    "category" => "misc",    
    "primary_key" => "iDBChangelogId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_desktop_notifications"] = array(
    "name" => "mod_desktop_notifications",
    "category" => "misc",    
    "primary_key" => "iDesktopNotificationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_desktop_notify_template"] = array(
    "name" => "mod_desktop_notify_template",
    "category" => "mod",    
    "primary_key" => "iDesktopNotifyTemplateId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_desktop_notify_template_vars"] = array(
    "name" => "mod_desktop_notify_template_vars",
    "category" => "mod",    
    "primary_key" => "iDesktopNotifyVariableId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_email_notifications"] = array(
    "name" => "mod_email_notifications",
    "category" => "misc",    
    "primary_key" => "iEmailNotificationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_executed_notifications"] = array(
    "name" => "mod_executed_notifications",
    "category" => "misc",    
    "primary_key" => "iExecutedNotificationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_form_drafts"] = array(
    "name" => "mod_form_drafts",
    "category" => "misc",    
    "primary_key" => "iFormDraftsId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_group_capabilities"] = array(
    "name" => "mod_group_capabilities",
    "category" => "user",    
    "primary_key" => "iGroupCapabilitiesId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_group_master"] = array(
    "name" => "mod_group_master",
    "category" => "user",    
    "primary_key" => "iGroupId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_group_rights"] = array(
    "name" => "mod_group_rights",
    "category" => "user",    
    "primary_key" => "iGroupRightId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_language"] = array(
    "name" => "mod_language",
    "category" => "misc",    
    "primary_key" => "iLangId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_language_label"] = array(
    "name" => "mod_language_label",
    "category" => "misc",    
    "primary_key" => "iLanguageLabelId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_language_label_lang"] = array(
    "name" => "mod_language_label_lang",
    "category" => "misc",    
    "primary_key" => "vLangCode",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_log_history"] = array(
    "name" => "mod_log_history",
    "category" => "user",    
    "primary_key" => "iLogId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_notify_operation_values"] = array(
    "name" => "mod_notify_operation_values",
    "category" => "tools",    
    "primary_key" => "iOperationScheduleId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_notify_schedule"] = array(
    "name" => "mod_notify_schedule",
    "category" => "tools",    
    "primary_key" => "iNotifyScheduleId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_page_settings"] = array(
    "name" => "mod_page_settings",
    "category" => "tools",    
    "primary_key" => "iPageId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_push_notifications"] = array(
    "name" => "mod_push_notifications",
    "category" => "misc",    
    "primary_key" => "iPushNotifyId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_push_notify_template"] = array(
    "name" => "mod_push_notify_template",
    "category" => "tools",    
    "primary_key" => "iPushNotifyTemplateId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_push_notify_template_vars"] = array(
    "name" => "mod_push_notify_template_vars",
    "category" => "tools",    
    "primary_key" => "iPushNotifyVariableId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_release_notes"] = array(
    "name" => "mod_release_notes",
    "category" => "misc",    
    "primary_key" => "iReleaseNotesId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_release_note_details"] = array(
    "name" => "mod_release_note_details",
    "category" => "misc",    
    "primary_key" => "iReleaseNoteDetailsId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_setting"] = array(
    "name" => "mod_setting",
    "category" => "tools",    
    "primary_key" => "vName",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_setting_lang"] = array(
    "name" => "mod_setting_lang",
    "category" => "tools",    
    "primary_key" => "",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_shortcuts"] = array(
    "name" => "mod_shortcuts",
    "category" => "tools",    
    "primary_key" => "iShortcutId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_sms_notifications"] = array(
    "name" => "mod_sms_notifications",
    "category" => "misc",    
    "primary_key" => "iSMSNotificationId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_sms_notify_template"] = array(
    "name" => "mod_sms_notify_template",
    "category" => "tools",    
    "primary_key" => "iSMSNotifyTemplateId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_sms_notify_template_vars"] = array(
    "name" => "mod_sms_notify_template_vars",
    "category" => "tools",    
    "primary_key" => "iSMSNotifyVariableId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_state"] = array(
    "name" => "mod_state",
    "category" => "tools",    
    "primary_key" => "iStateId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_system_email"] = array(
    "name" => "mod_system_email",
    "category" => "tools",    
    "primary_key" => "iEmailTemplateId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_system_email_vars"] = array(
    "name" => "mod_system_email_vars",
    "category" => "tools",    
    "primary_key" => "iEmailVariableId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_ws_messages"] = array(
    "name" => "mod_ws_messages",
    "category" => "misc",    
    "primary_key" => "iWSMessageId",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_ws_messages_lang"] = array(
    "name" => "mod_ws_messages_lang",
    "category" => "misc",    
    "primary_key" => "",
    "soft_delete" => "No",
    "audit_log" => "No"
);
$config["mod_ws_token"] = array(
    "name" => "mod_ws_token",
    "category" => "misc",    
    "primary_key" => "iWSTokenId",
    "soft_delete" => "No",
    "audit_log" => "No"
);#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file cit_tables.php */
/* Location: ./application/config/cit_tables.php */