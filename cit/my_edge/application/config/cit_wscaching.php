<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array();

#####GENERATED_CONFIG_SETTINGS_START#####

$config["change_password"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["country_list"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["country_with_states"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["customer_add"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["customer_detail"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["customer_login"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["customer_update"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);
$config["forgot_password"] = array(
    "cache" => "No",
    "expires" => "86400",
    "params" => array(
        
    )
);#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file cit_wscaching.php */
/* Location: ./application/config/cit_wscaching.php */