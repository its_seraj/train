<?php

defined('BASEPATH') OR exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

$config["admin"] = array(
    "title" => "Admin",
    "table" => "mod_admin",
    "module" => "admin",
    "folder" => "user",
    "add_url" => "user/admin/add",
    "list_url" => "user/admin/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["country"] = array(
    "title" => "Country",
    "table" => "mod_country",
    "module" => "country",
    "folder" => "tools",
    "add_url" => "tools/country/add",
    "list_url" => "tools/country/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["customers"] = array(
    "title" => "Customers",
    "table" => "mod_customer",
    "module" => "customers",
    "folder" => "user",
    "add_url" => "user/customers/add",
    "list_url" => "user/customers/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["group"] = array(
    "title" => "Group",
    "table" => "mod_group_master",
    "module" => "group",
    "folder" => "user",
    "add_url" => "user/group/add",
    "list_url" => "user/group/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["loghistory"] = array(
    "title" => "Log History",
    "table" => "mod_log_history",
    "module" => "loghistory",
    "folder" => "user",
    "add_url" => "user/loghistory/add",
    "list_url" => "user/loghistory/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["state"] = array(
    "title" => "State",
    "table" => "mod_state",
    "module" => "state",
    "folder" => "tools",
    "add_url" => "tools/state/add",
    "list_url" => "tools/state/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["staticpages"] = array(
    "title" => "Static Pages",
    "table" => "mod_page_settings",
    "module" => "staticpages",
    "folder" => "tools",
    "add_url" => "tools/staticpages/add",
    "list_url" => "tools/staticpages/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["systememails"] = array(
    "title" => "System Emails",
    "table" => "mod_system_email",
    "module" => "systememails",
    "folder" => "tools",
    "add_url" => "tools/systememails/add",
    "list_url" => "tools/systememails/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);#####GENERATED_CONFIG_SETTINGS_END#####
