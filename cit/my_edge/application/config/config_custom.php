<?php
defined('BASEPATH') || exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

// Mysql installation directory
$config['mysqlexportinstalldir'] = "/usr/bin/mysqldump";
$config['mysqlimportinstalldir'] = "/usr/bin/mysql";

// ImageMagic installation directory
$config['imagemagickinstalldir'] = "/usr/bin/";

// Physical record delete activate flag
$config["PHYSICAL_RECORD_DELETE"] = TRUE;                    

// DB record limit - to avoid hitting the database server from large data sets fetching.
$config['db_max_limit'] = 0;

// Common JS/CSS Path
$config['cmn_js_path'] = '';
$config['cmn_css_path'] = '';

// CDN server details - For more info, please read this file ../public/cdn/readme.txt.
$config['cdn_activate'] = FALSE;
$config['cdn_http_url'] = '';
if ($config['cdn_activate'] === TRUE) {
    $config['images_url'] = $config['cdn_http_url'] . $config['assets_folder'] . '/' . $config['images_folder'] . '/';
    $config['admin_images_url'] = $config['images_url'] . 'admin' . '/';
}

// Callback general methods
$config['login_callback'] = '';
$config['auth_callback'] = '';
$config['menu_callback'] = '';

// Security settings

$config['admin_2fa_authentication'] = TRUE;
$config['admin_password_history'] = 5;
$config['admin_password_expiry'] = 90;
$config['admin_account_lock_attempts'] = 5;
$config['admin_account_locked_duration'] = 15;
$config['admin_password_change_remainder'] = 90;
$config['admin_login_with_google_captcha'] = FALSE;
$config['verify_admin_access_capabilities'] = FALSE;
$config['data_encryption_algorithm'] = 'aes-256-cbc';
$config['admin_form_csrf_verification'] = FALSE;
$config['admin_form_checksum_validation'] = FALSE;
$config['admin_form_post_data_encryption'] = FALSE;

if(!$config['admin_form_checksum_validation']) {
    $config['checksum_validation_uris'] = array(
        'client' => array(),
        'server' => array()
    );
}
if(!$config['admin_form_checksum_validation']) {
    $config['post_data_encryption_uris'] = array(
        'client' => array(),
        'server' => array()
    );
}

// CSRF settings
$config['csrf_admin_module_uris'] = array(
);
if (is_array($config['csrf_specific_uris'])) {
    $config['csrf_specific_uris'] = array_merge($config['csrf_specific_uris'], $config['csrf_admin_module_uris']);
}
#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file config_custom.php */
/* Location: ./application/config/config_custom.php */