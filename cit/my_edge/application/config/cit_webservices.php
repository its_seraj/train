<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array();

#####GENERATED_CONFIG_SETTINGS_START#####

$config["change_password"] = array(
    "title" => "Change Password",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id",
        "new_password",
        "old_password"
    )
);
$config["country_list"] = array(
    "title" => "Country List",
    "folder" => "tools",
    "method" => "GET_POST",
    "params" => array(
    )
);
$config["country_with_states"] = array(
    "title" => "Country With States",
    "folder" => "tools",
    "method" => "GET_POST",
    "params" => array(
        "country_id"
    )
);
$config["customer_add"] = array(
    "title" => "Customer Add",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "email",
        "first_name",
        "last_name",
        "password",
        "profile_image",
        "username"
    )
);
$config["customer_detail"] = array(
    "title" => "Customer Detail",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id"
    )
);
$config["customer_login"] = array(
    "title" => "Customer Login",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "password",
        "username"
    )
);
$config["customer_update"] = array(
    "title" => "Customer Update",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id",
        "first_name",
        "last_name",
        "profile_image"
    )
);
$config["forgot_password"] = array(
    "title" => "Forgot Password",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "email"
    )
);#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file cit_webservices.php */
/* Location: ./application/config/cit_webservices.php */
    
