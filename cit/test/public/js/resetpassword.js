Project.modules.login = (function () {
    var objReturn = {}
    
    function init() {
        Common.initValidator();
        passwordValidate();
    }
    function passwordValidate() {
        $('#resetpassword').validate({
            rules: {
                'new_password': {
                    required: true
                },
                'retype_password': {
                    required: true,
                    equalTo: "#new_password"
                },
                'reset_code': {
                    required: true
                }
            },
            messages: {
                'new_password': {
                    required: 'Please enter Password'
                },
                'retype_password': {
                    required: 'Please enter Password',
                    equalTo: "Password does not match"
                },
                'reset_code': {
                    required: 'Please  enter Reset Code'
                }
            }
        });
    }
    
    objReturn.init = init;
    return objReturn;
})();