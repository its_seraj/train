$(function () {
    $('form.ws').submit(function (e) {
        var el = $(this);
        e.preventDefault();
        el.next().hide();
        el.next().next().hide();
        executeAPI($(this).attr('action'), $(this).serializeArray(), function (a, b, c) {
            handleAPIResponse(el, a, b, c);
        }, function (a, b, c) {
            handleAPIResponse(el, a, b, c);
        });
    });
});
function getKeyPhrase(keyPhrase, saltPhrase) {
    var key = CryptoJS.PBKDF2(keyPhrase, saltPhrase, {
        hasher: CryptoJS.algo.SHA256,
        keySize: 64 / 8,
        iterations: enc_keys.iterations
    });
    return key;
}
function encryptData(plainText, encKeys) {
    var keyPhrase = encKeys.key_phrase;
    var ivPhrase = encKeys.iv_phrase;
    var saltPhrase = encKeys.salt_phrase;

    var key = getKeyPhrase(keyPhrase, saltPhrase);
    var encrypted = CryptoJS.AES.encrypt(plainText, key, {
        iv: CryptoJS.enc.Utf8.parse(ivPhrase)
    });
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
}
function decryptData(encryptedText, encKeys) {
    var keyPhrase = encKeys.key_phrase;
    var ivPhrase = encKeys.iv_phrase;
    var saltPhrase = encKeys.salt_phrase;

    var key = getKeyPhrase(keyPhrase, saltPhrase);
    var decrypted = CryptoJS.AES.decrypt(encryptedText, key, {
        iv: CryptoJS.enc.Utf8.parse(ivPhrase)
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}
function executeAPI(method, data_array, success_callback, error_callback) {
    var data = {};
    if ($.isArray(data_array) && data_array.length) {
        for (var i in data_array) {
            data[data_array[i]['name']] = data_array[i]['value'];
        }
    }
    if (token_enabled == 'Y') {
        var diff = Math.abs(new Date() - new Date(token_created_at));
        var minutes = Math.floor((diff / 1000) / 60);
        if (minutes > parseInt(token_expiry)) {
            createToken();
        }
        data['ws_token'] = ws_token_value;
    }
    if (token_enabled == 'Y') {
        data['ws_checksum'] = generateChecksum(data);
    }
    if (encryption_enabled == "Y") {
        var bypass_keys = ['ws_token', 'ws_checksum', 'ws_debug', 'ws_cache', 'ws_log', '_'];
        for (var i in data) {
            if (data[i] != "" && $.inArray(i, bypass_keys) == -1) {
                data[i] = encryptData(data[i], enc_keys);
            }
        }
    }
    processAPI(method, data, success_callback, error_callback);
}
function createToken() {
    $.ajax({
        url: token_url,
        type: 'POST',
        async: false,
        success: function (response) {
            try {
                if (encryption_enabled == "Y") {
                    response = decryptData(response, enc_keys);
                }
                response = JSON.parse(response);
                ws_token_value = response.data.ws_token;
                token_created_at = new Date();
            } catch (err) {

            }
        }
    });
}
function generateChecksum(data) {
    var checksum, keys, pair = '';
    keys = Object.keys(data).sort();
    for (var i in keys) {
        pair += keys[i] + '=' + data[keys[i]];
    }
    checksum = CryptoJS.SHA1(pair).toString();
    return checksum;
}
function processAPI(method, data, success_callback, error_callback) {
    $.ajax({
        url: ws_base_url + method,
        type: "POST",
        dataType: 'json',
        data: data,
        crossDomain: true,
        error: function (jqXHR, textStatus, errorThrown) {
            if (typeof (error_callback) == 'function')
                error_callback(textStatus, errorThrown, jqXHR);
        },
        success: function (data, status, jqXHR) {
            if (typeof (success_callback) == 'function')
                success_callback(data, status, jqXHR);
        }
    });
}

function handleAPIResponse(el, a, b, c) {
    var response;
    try {
        var responseText = c.responseText;
        if (encryption_enabled == "Y") {
            responseText = decryptData(responseText, enc_keys);
        }
        response = JSON.parse(responseText);
        response = processAPIResponse(response);
        el.next().show().html(response);
    } catch (err) {
        response = c.responseText;
        el.next().next().show().contents().find("body").html(response);
        var fr_height = el.next().next().contents().find("html").outerHeight();
        el.next().next().height(fr_height + 12);
    }
}
function processAPIResponse(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}