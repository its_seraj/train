/** unit module script */
Project.modules.unit = {
    init: function() {
        
        valid_more_elements = [];
        
        
    },
    validate: function (){
        
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore:".ignore-valid, .ignore-show-hide",
            rules : {
		    "um_unit_name": {
		        "required": true
		    },
		    "um_unit_code": {
		        "required": true
		    },
		    "um_added_date": {
		        "required": true
		    },
		    "um_added_by": {
		        "required": true
		    },
		    "um_company_id": {
		        "required": true
		    },
		    "um_status": {
		        "required": true
		    }
		},
            messages : {
		    "um_unit_name": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_UNIT_NAME)
		    },
		    "um_unit_code": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_UNIT_CODE)
		    },
		    "um_added_date": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_ADDED_DATE)
		    },
		    "um_added_by": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_ADDED_BY)
		    },
		    "um_company_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_COMPANY_ID)
		    },
		    "um_status": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.UNIT_STATUS)
		    }
		},
            errorPlacement : function(error, element) {
                switch(element.attr("name")){
                    
                        case 'um_unit_name':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'um_unit_code':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'um_added_date':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'um_added_by':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'um_company_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'um_status':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
                
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {                    
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
        
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.toggleEvents();
        callGoogleMapEvents();
        
    },
    callChilds: function(){
        
        callGoogleMapEvents();
    },
    initEvents: function(elem){
        
            
                        $('#um_added_date').datepicker({
                            dateFormat : 'yy-mm-dd', 
showOn : 'focus', 
changeMonth : true, 
changeYear : true, 
yearRange : 'c-100:c+100',
                            beforeShow: function(input, inst) {
                                var cal = inst.dpDiv;
                                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                                setTimeout(function() {
                                    cal.css({
                                        'left': left
                                    });
                                }, 10);
                            },
                            onSelect: function(value, inst) {
                                $(this).valid();
                                $(this).trigger('change');
                            } 
                        });
                        if(el_general_settings.mobile_platform){
                            $('#um_added_date').attr('readonly', true);
                        }
                        
            
                        $('#um_updated_date').datepicker({
                            dateFormat : 'yy-mm-dd', 
showOn : 'focus', 
changeMonth : true, 
changeYear : true, 
yearRange : 'c-100:c+100',
                            beforeShow: function(input, inst) {
                                var cal = inst.dpDiv;
                                var left = ($(this).offset().left + $(this).outerWidth()) - cal.outerWidth();
                                setTimeout(function() {
                                    cal.css({
                                        'left': left
                                    });
                                }, 10);
                            },
                            onSelect: function(value, inst) {
                                $(this).valid();
                                $(this).trigger('change');
                            } 
                        });
                        if(el_general_settings.mobile_platform){
                            $('#um_updated_date').attr('readonly', true);
                        }
                        
    },
    childEvents: function(elem, eleObj){
        
    },
    toggleEvents: function(){
        
    },
    dropdownLayouts:function(elem){
        
    }
}
Project.modules.unit.init();
