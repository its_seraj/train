/** product_specification module script */
Project.modules.product_specification = {
    init: function() {
                $(document).off("input paste change", "[name='ps_title']");
         
        valid_more_elements = [];
        
        
        cc_json_1 = [
	    {
	        "cond_type": "AND",
	        "show_list": [
	            {
	                "id": "color_picker"
	            }
	        ],
	        "hide_list": [
	            {
	                "id": "ps_value"
	            }
	        ],
	        "cond_list": [
	            {
	                "id": "ps_title",
	                "type": "textbox",
	                "oper": "eq",
	                "value": [
	                    "color"
	                ]
	            }
	        ]
	    }
	];
        $(document).on("input paste change", "[name='ps_title']", function() {
            checkCCEventValues(cc_json_1);
        });
    },
    validate: function (){
        
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore:".ignore-valid, .ignore-show-hide",
            
            errorPlacement : function(error, element) {
                switch(element.attr("name")){
                    
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
                
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {                    
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
        
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.toggleEvents();
        callGoogleMapEvents();
        
    },
    callChilds: function(){
        
        callGoogleMapEvents();
    },
    initEvents: function(elem){
        
             
            $('#color_picker').focus(function(){
                $('#color_picker_span').colpickSetColor($('#color_picker').val());
                $('#color_picker_span').colpickShow();
            });
            $('#color_picker_span').colpick({
                submit:false,
                onShow: function (colpkr) {
                    $(colpkr).fadeIn(500);
                    return false;
                },
                onHide: function (colpkr) {
                    $(colpkr).fadeOut(500);
                    return false;
                },
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $('#color_picker').val('#' + hex);
                    
                    $('#color_picker_preview').css('backgroundColor', '#' + hex);
                }
            }).bind('keyup', function(){
                $(this).colpickSetColor($('#color_picker').val());
            }).bind('click', function(){
                $(this).colpickSetColor($('#color_picker').val());
            });
            if(el_general_settings.mobile_platform){
                $('#color_picker').attr('readonly', true);
            }
            
    },
    childEvents: function(elem, eleObj){
        
    },
    toggleEvents: function(){
        
        pre_cond_code_arr.push(cc_json_1);
    },
    dropdownLayouts:function(elem){
        
    }
}
Project.modules.product_specification.init();
