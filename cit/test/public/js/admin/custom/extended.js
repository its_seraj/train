$(() => {
    $("#p_manufacturer_id").on("change", (e) => {
        var id = $(e.target).val();
        $.ajax({
            url: admin_url + "master/brands/listing",
            dataType: "json",
            success: (data) => {
                data = data.data;
                var output = `<option value="" selected="selected"></option>`;
                $.each(data, (i, row) => {
                    if(row.b_manufacturer_id == id){
                        output += `
                            <option value="${row.b_brand_id}">${row.b_brand}</option>
                        `;
                    }
                })
                $("#p_brand_id").html(output);
                $("#p_brand_id").trigger("chosen:updated");
            }
        })
        
        
    })
})
