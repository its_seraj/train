$(document).ready(function () {
    $('#login_name').focus();
    var js_curr_hash = window.location.hash;
    $('#handle_url').val(js_curr_hash);
    $('#username').keyup(function (e) {
        if ($(this).val() != '') {
            $('#usernameErr').html('');
            $('#username').addClass('forgot-valid');
            $('#username').removeClass('forgot-err');
        } else {
            $('#usernameErr').html('<div class="err">' + js_lang_label.GENERIC_FORGOT_PASSWORD_USERNAME_ERR + '</div>');
            $('#username').removeClass('forgot-valid');
            $('#username').addClass('forgot-err');
        }
        if (e.which == '13') {
            validateSendForgotPassword();
        }
    });

    if (is_pattern == "yes") {
        $("#secretlogin").change(function () {
            if ($("#secretlogin").is(":checked")) {
                $('#passwd-div').pattern('clearPattern', true);
                $('#passwd-div').pattern({
                    showPattern: false
                });
            } else {
                $('#passwd-div').pattern({
                    showPattern: true
                });
            }
        })
        function setPattern(pattern) {
            if (pattern.length) {
                $("#passwd").val(pattern.join(""));
                if (!login(document.frmlogin)) {
                    $('#passwd-div').pattern('clearPattern', true)
                }
            }
        }
        $('#passwd-div').pattern({
            stop: function (event, ui) {
                setPattern(ui.pattern);
            }
        });
        $("#passwd").css('display', 'none');
    }

    $("#frmlogin").validate({
        onfocusout: false,
        ignore: ".ignore-valid, .ignore-show-hide",
        rules: {
            "login_name": {
                "required": true
            },
            "passwd": {
                "required": true,
            }
        },
        messages: {
            "login_name": {
                "required": js_lang_label.GENERIC_LOGIN_USERNAME_ERR
            },
            "passwd": {
                "required": js_lang_label.GENERIC_LOGIN_PASSWORD_ERR,
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name")) {
                $('#' + element.attr('id') + 'Err').html(error);
            }
        },
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        submitHandler: function (form) {
            if (typeof is_ajax_submit != 'undefined') {
                if ($.isPlainObject(window.grecaptcha)) {
                    grecaptcha.ready(function () {
                        grecaptcha.execute(captcha_site_key, {action: 'admin_login'}).then(function (token) {
                            $('#frmlogin').prepend('<input type="hidden" name="token" value="' + token + '">');
                            $('#frmlogin').prepend('<input type="hidden" name="action" value="admin_login">');
                            submitLoginForm();
                        });
                    });
                } else {
                    submitLoginForm();
                }
                return false;
            } else {
                form.submit();
            }
        }
    });
    $('#frmlogin').keyup(function (e) {
        if (e.keyCode === 13) {
            login();
        }
    });

    $('#pwd_show_hide').on("click", function () {
        var status = $('#pwd_icon').attr('status');
        if (status == 'hide') {
            $('#pwd_icon').removeClass('fa-eye-slash').addClass('fa-eye').attr('status', 'show');
            $('#passwd').attr('type', 'text');
        } else {
            $('#pwd_icon').removeClass('fa-eye').addClass('fa-eye-slash').attr('status', 'hide');
            $('#passwd').attr('type', 'password');
        }
    });
});

function login(frm) {
    if (!$("#frmlogin").valid()) {
        return false;
    } else {
        $("#frmlogin").submit();
        return true;
    }
}
function submitLoginForm() {
    Project.show_adaxloading_div();
    var options = {
        url: entry_action_url,
        type: "POST",
        beforeSubmit: function (formData, jqForm, options) {
            prepareFinalFormData(formData, frm_key_settings);
        },
        success: function (respText, statText, xhr) {
            Project.ajaxSuccessCallback(respText, statText, xhr);
            var result = parseJSONString(respText);
            if (!result.success) {
                Project.setMessage(result.message, 0);
                Project.refreshCSRFToken(result);
            } else {
                window.location.href = result.redirect;
            }
        },
        complete: function (xhr, statText) {
            Project.hide_adaxloading_div();
            Project.ajaxCompleteCallback(xhr, statText);
        }
    };
    $('#frmlogin').ajaxSubmit(options);
}
function hideForgotPassword() {
    $('#username').val('');
    $('#forgot_div').hide();
    $('#login_div').show();
    $('#login_name').focus();
}
function showForgotPassword() {
    $('#login_div').hide();
    $('#forgot_div').show();
    $('#username').focus();

}
function validateSendForgotPassword() {
    var js_username = $('#username').val();
    if (js_username == '') {
        $('#username').removeClass('forgot-valid');
        $('#username').addClass('forgot-err');
        $('#usernameErr').html('<div class="err">' + js_lang_label.GENERIC_FORGOT_PASSWORD_USERNAME_ERR + '</div>');
        return false;
    } else {
        $('#username').addClass('forgot-valid');
        $('#username').removeClass('forgot-err');
        $('#usernameErr').html('');
        $('#send_button').hide();
        $('#loader_img').show();
        $.ajax({
            url: forgot_pwd_url,
            type: "POST",
            data: {'username': js_username},
            success: function (respText, statText, xhr) {
                Project.ajaxSuccessCallback(respText, statText, xhr);
                var result = parseJSONString(respText);
                if (result.success) {
                    if (result.url) {
                        window.location.href = result.url;
                    } else {
                        hideForgotPassword();
                    }
                } else {
                    Project.refreshCSRFToken(result);
                }
                $('#send_button').show();
                $('#loader_img').hide();
                parent.Project.setMessage(result.message, result.success);
            },
            complete: function (xhr, statText) {
                Project.ajaxCompleteCallback(xhr, statText);
            }
        });
    }
    return false;
}