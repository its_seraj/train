/** rating module script */
Project.modules.rating = {
    init: function() {
        
        valid_more_elements = [];
        
        
    },
    validate: function (){
        
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore:".ignore-valid, .ignore-show-hide",
            rules : {
		    "rm_product_id": {
		        "required": true
		    },
		    "rm_user_id": {
		        "required": true
		    },
		    "rm_rate": {
		        "required": true
		    }
		},
            messages : {
		    "rm_product_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.RATING_PRODUCT)
		    },
		    "rm_user_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.RATING_USER)
		    },
		    "rm_rate": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.RATING_RATE)
		    }
		},
            errorPlacement : function(error, element) {
                switch(element.attr("name")){
                    
                        case 'rm_product_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'rm_user_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'rm_rate':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
                
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {                    
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
        
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.toggleEvents();
        callGoogleMapEvents();
        
    },
    callChilds: function(){
        
        callGoogleMapEvents();
    },
    initEvents: function(elem){
        
            
            $('#rating_rm_rate').raty({
                number : 5, 
cancel : true, 
half : true, 
targetKeep : true, 
precision : true, 
ratyIconSize : 'icon18', 
cancelSize : 'icon15',
                clear:(($('#rm_rate').attr('aria-raty-clear') == 'false') ? false : true),
                readOnly:(($('#rm_rate').attr('aria-raty-readonly') == 'false') ? true : false),
                score:$('#rm_rate').val(),
                hints : ['1','2','3','4','5'],
                target : '#rm_rate'
            });
    },
    childEvents: function(elem, eleObj){
        
    },
    toggleEvents: function(){
        
    },
    dropdownLayouts:function(elem){
        
    }
}
Project.modules.rating.init();
