/** products module script */
Project.modules.products = {
    init: function() {
        
        valid_more_elements = ["child[product_image][pi_image]","child[product_specification][ps_title]","child[product_specification][ps_value]","child[product_specification][color_picker]"];
        
        
    },
    validate: function (){
        
        $("#frmaddupdate").validate({
            onfocusout: false,
            ignore:".ignore-valid, .ignore-show-hide",
            rules : {
		    "p_product_name": {
		        "required": true
		    },
		    "p_category_id": {
		        "required": true
		    },
		    "p_price": {
		        "required": true
		    },
		    "p_retail_price": {
		        "required": true
		    },
		    "p_manufacturer_id": {
		        "required": true
		    },
		    "p_brand_id": {
		        "required": true
		    },
		    "p_status": {
		        "required": true
		    }
		},
            messages : {
		    "p_product_name": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRODUCT_NAME)
		    },
		    "p_category_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_CATEGORY)
		    },
		    "p_price": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRICE)
		    },
		    "p_retail_price": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_RETAIL_PRICE)
		    },
		    "p_manufacturer_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_MANUFACTURER_ID)
		    },
		    "p_brand_id": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_BRAND)
		    },
		    "p_status": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_STATUS)
		    }
		},
            errorPlacement : function(error, element) {
                switch(element.attr("name")){
                    
                        case 'p_product_name':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_category_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_price':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_retail_price':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_manufacturer_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_brand_id':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                        case 'p_status':
                            $('#'+element.attr('id')+'Err').html(error);
                            break;
                    default:
                        printErrorMessage(element, valid_more_elements, error);
                        break;
                }
                
            },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {                    
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                getAdminFormValidate();
                return false;
            }
        });
        
    },
    callEvents: function() {
        this.validate();
        this.initEvents();
        this.toggleEvents();
        callGoogleMapEvents();
        
    },
    callChilds: function(){
        
            this.childEvents("product_image", "#child_module_product_image");
            this.childEvents("product_specification", "#child_module_product_specification");
        callGoogleMapEvents();
    },
    initEvents: function(elem){
        
            $('#p_description').elastic();
            this.childEvents("product_image", "#child_module_product_image");
            this.childEvents("product_specification", "#child_module_product_specification");
    },
    childEvents: function(elem, eleObj){
        switch(elem){
                
                case "product_image" :
                    var is_popup = $("#childModulePopup_product_image").val();
                    if(is_popup != "Yes"){
                        
                if($("[name^='child[product_image][pi_image]']").length){
                    $("[name^='child[product_image][pi_image]']").each(function(){
                        $(this).rules("add", {
		    "required": true,
		    "messages": {
		        "required": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCT_IMAGE_IMAGE)
		    }
		}
                        );
                    });
                }
                
            
            var tarObj = $(eleObj).find("[id='uploadify_child_product_image_pi_image_0']");
            if(tarObj && tarObj.length){
                var ele_id = $(tarObj).attr('id');
                var last_id = ele_id.split('_').pop();
                var act_id = ele_id.split('_').slice(1).join('_');
                var temp_id  = 'child_product_image_temp_pi_image_'+last_id;
                var id_val = '';
                if($('#child_product_image_enc_id_'+last_id).length){
                    id_val = $('#child_product_image_enc_id_'+last_id).val();
                }
                $('#upload_drop_zone_' + act_id).width($(tarObj).width() + 18);
                $(tarObj).fileupload({
                    name: act_id,
                    temp: temp_id,
                    url : admin_url+''+$('#childModuleUploadURL_product_image').val()+'?', 
paramName : 'Filedata', 
maxFileSize : '102400', 
acceptFileTypes : 'gif|png|jpg|jpeg|jpe|bmp|ico',
                    dropZone: $('#upload_drop_zone_' + act_id + ', #upload_drop_zone_' + act_id + ' + .upload-src-zone'),
                    formData: {
                        'unique_name' : 'pi_image', 
                        'id' : id_val,
                        'type' : 'uploadify'
                    },
                    add: function(e, data) {
                        var upload_errors = [];
                        var _input_name = $(this).fileupload('option', 'name');
                        var _temp_name = $(this).fileupload('option', 'temp');
                        var _form_data = $(this).fileupload('option', 'formData');
                        var _file_size = $(this).fileupload('option', 'maxFileSize');
                        var _file_type = $(this).fileupload('option', 'acceptFileTypes');
                        
                        var _input_val = data.originalFiles[0]['name'];
                        var _input_size = data.originalFiles[0]['size'];
                        if(_file_type != '*'){
                            var _input_ext = (_input_val) ? _input_val.substr(_input_val.lastIndexOf('.')) : '';
                            var accept_file_types = new RegExp('(\.|\/)(' + _file_type + ')$', 'i');
                            if (_input_ext && !accept_file_types.test(_input_ext)) {
                                upload_errors.push(js_lang_label.ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE);
                                var valid_ext = $('#' + _input_name).attr('aria-extensions');
                                if(valid_ext){
                                    upload_errors.push(js_lang_label.GENERIC_VALID_EXTENSIONS + ' : ' + valid_ext);
                                }
                            }
                        }
                        _file_size = _file_size * 1000;
                        if (_input_size && _input_size > _file_size) {
                            if(!upload_errors.length){
                                upload_errors.push(js_lang_label.ACTION_FILE_SIZE_IS_TOO_LARGE);
                                var valid_size = $('#' + _input_name).attr('aria-valid-size');
                                if(valid_size){
                                    upload_errors.push(js_lang_label.GENERIC_VALID_SIZE + ' : ' + valid_size);
                                }
                            }
                        }
                        if (upload_errors.length > 0) {
                            Project.setMessage(upload_errors.join('\n'), 0);
                        } else {
                            $(this).fileupload('option', 'total', data.originalFiles.length);
                            $('#practive_' + _input_name).css('width', '0%');
                            $('#progress_' + _input_name).show();
                            var xhr = data.submit();
                            $('#progress_' + _input_name + ' .upload-cancel').click(function (e) {
                                e.preventDefault();
                                xhr.abort();
                            });
                        }
                    },
                    done: function(e, data) {
                        if (data && data.result) {
                            var _input_name = $(this).fileupload('option', 'name');
                            var jparse_data = $.parseJSON(data.result);
                            if (jparse_data.success == '0') {
                                Project.setMessage(jparse_data.message, 0);
                                $('#progress_' + _input_name).hide();
                            } else {
                                addAdminOntheFlyImage('product_image', 'pi_image', jparse_data);
                            }
                            
                        }
                    },
                    fail: function(e, data) {
                        if(data.textStatus == 'abort'){
                            data.messages.uploadedBytes = 'File Upload Cancelled';
                            var _input_name = $(this).fileupload('option', 'name');
                            $('#progress_' + _input_name).hide();
                        }
                        $.each(data.messages, function(index, error) {
                            Project.setMessage(error, 0);
                        });
                    },
                    progressall: function(e, data) {
                        var _input_name = $(this).fileupload('option', 'name');
                        var tot = $(this).fileupload('option', 'total');
                        var cnt = $(this).fileupload('option', 'count');
                        cnt = (cnt) ? cnt + 1 : 1;
                        $(this).fileupload('option', 'count', cnt);
                        
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#practive_' + _input_name).css('width', progress + '%');
                        if(cnt >= tot){
                            setTimeout(function() {
                                $('#progress_' + _input_name).hide();
                            }, 1000);
                        }
                    }
                });
            }
            
                    }
                    break;
                case "product_specification" :
                    var is_popup = $("#childModulePopup_product_specification").val();
                    if(is_popup != "Yes"){
                        
                
             
            $(eleObj).find("[name^='child[product_specification][color_picker]']").each(function(){
                var ele_id = $(this).attr('id');
                $('#'+ele_id).focus(function(){
                    $('#'+ele_id+'_span').colpickSetColor($('#'+ele_id).val());
                    $('#'+ele_id+'_span').colpickShow();
                });
                $('#'+ele_id+'_span').colpick({
                    submit:false,
                    onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                    },
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        $('#'+ele_id).val('#' + hex);
                        
                        $('#'+ele_id+'_preview').css('backgroundColor', '#' + hex);
                    }
                }).bind('keyup', function(){
                    $(this).colpickSetColor($('#'+ele_id).val());
                }).bind('click', function(){
                    $(this).colpickSetColor($('#'+ele_id).val());
                });
                if(el_general_settings.mobile_platform){
                    $(this).attr('readonly', true);
                }
            });
                    }
                    break;
            }
    },
    toggleEvents: function(){
        
    },
    dropdownLayouts:function(elem){
        
    }
}
Project.modules.products.init();
