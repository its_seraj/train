$(document).ready(function() {
    $("#google_authentication").validate({
        onfocusout: false,
        ignore: ".ignore-valid, .ignore-show-hide",
        rules: {
            "2fa_code": {
                "required": true,
            },
        },
        messages: {
            "2fa_code": {
                "required": js_lang_label.GENERIC_PLEASE_ENTER_SECURITY_CODE,
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name")) {
                $('#' + element.attr('id') + 'Err').html(error);
            }
        },
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        submitHandler: function (form) {
            if (typeof is_ajax_submit != 'undefined') {
                submitForm();
                return false;
            } else {
                form.submit();
            }
        }
    });
});

function validateCode() {
    if (!$("#google_authentication").valid()) {
        return false;
    }
}
function submitForm() {
    Project.show_adaxloading_div();
     var options = {
        url: validate_code_url,
        type: "POST",
        beforeSubmit: function (formData, jqForm, options) {
            prepareFinalFormData(formData, frm_key_settings);
        },
        success: function (respText, statText, xhr) {
            Project.ajaxSuccessCallback(respText, statText, xhr);
            var result = parseJSONString(respText);
            if (!result.success) {
                Project.setMessage(result.message, 0);
                Project.refreshCSRFToken(result);
            } else {
                window.location.href = result.redirect;
            }
        },
        complete: function (xhr, statTextt) {
            Project.hide_adaxloading_div();
            Project.ajaxCompleteCallback(xhr, statTextt);
        }
    };
    $('#google_authentication').ajaxSubmit(options);
}