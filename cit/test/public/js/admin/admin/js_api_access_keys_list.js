Project.modules.api_access_keys = {
    init: function () {

    },
    showAPIListing: function () {
        var that = this, total_rows, js_col_name_arr = [], grid_comp_time = true, load_comp_time = true;
        var grid_id = el_tpl_settings.main_grid_id, pager_id = el_tpl_settings.main_pager_id, wrapper_id = el_tpl_settings.main_wrapper_id;
        for (var i in js_col_name_json) {
            js_col_name_arr.push(js_col_name_json[i]['label']);
        }
        var force_width = $("#main_content_div").width() - 30;
        getColumnsWidth(el_grid_settings.enc_location + '_cw', grid_id, js_col_model_json);
        jQuery("#list2").jqGrid({
            editurl: el_grid_settings.delete_url,
            data: js_data_json,
            datatype: "local",
            colNames: js_col_name_arr,
            colModel: js_col_model_json,
            rowNum: el_tpl_settings.grid_rec_limit,
            pgnumbers: (el_theme_settings.grid_pgnumbers) ? true : false,
            pgnumlimit: parseInt(el_theme_settings.grid_pgnumlimit),
            pagingpos: el_theme_settings.grid_pagingpos,
            rowList: [10, 20, 30, 50, 100, 200, 500],
            sortname: el_grid_settings.default_sort,
            sortorder: el_grid_settings.sort_order,
            altRows: true,
            altclass: 'evenRow',
            multiselectWidth: 30,
            viewrecords: true,
            multiselect: true,
            multiboxonly: true,
            caption: false,
            hidegrid: false,
            pager: (el_tpl_settings.grid_bot_menu == 'Y') ? "#pager2" : "",
            toppager: (el_tpl_settings.grid_top_menu == 'Y') ? true : false,
            toppaging: (el_tpl_settings.grid_top_menu == 'Y') ? true : false,
            sortable: {
                update: function (permutation) {
                    setColumnsPosition(el_grid_settings.enc_location + '_cp', permutation, grid_id, js_col_model_json);
                }
            },
            searchGrid: {
                multipleSearch: true
            },
            forceApply: true,
            forceWidth: force_width,
            width: force_width,
            height: 400,
            autowidth: true,
            shrinkToFit: 800,
            fixed: true,
            grouping: false,
            beforeRequest: function () {
                getColumnsPosition(el_grid_settings.enc_location + '_cp', grid_id);
            },
            loadComplete: function (data) {
                $("#" + grid_id + "_messages_html").remove();
                $("#selAllRows").val('false');
                if (data) {
                    total_rows = data.records;
                }
                // Resizing Grid
                if (load_comp_time) {
                    load_comp_time = false;
                } else {
                    resizeGridWidth();
                    checkColumnsWidth(el_grid_settings.enc_location + '_cw', grid_id);
                }
            },
            gridComplete: function () {
                $(".ui-jqgrid-sortable").mousedown(function () {
                    $(this).css('cursor', 'crosshair');
                });
                $(".ui-jqgrid-sortable").mouseup(function () {
                    $(this).css({
                        cursor: 'pointer'
                    });
                });
                // Resizing Grid
                if (grid_comp_time) {
                    grid_comp_time = false;
                } else {
                    resizeGridWidth();
                }
            },
            onSortCol: function (index, iCol, sortorder) {

            },
            resizeStop: function (newwidth, index) {
                setColumnsWidth(el_grid_settings.enc_location + '_cw', grid_id);
            },
            beforeSelectRow: function (rowid, e) {
                multiSelectHandler(rowid, e);
            }
        });

        jQuery("#" + grid_id).jqGrid('filterToolbar', {
            stringResult: true,
            searchOnEnter: false,
            searchOperators: (el_theme_settings.grid_searchopt) ? true : false,
            operandTitle: js_lang_label.GENERIC_CLICK_TO_SELECT_SEARCH_OPERATION,
            clearTitle: js_lang_label.GENERIC_CLEAR_SEARCH_VALUE
        });

        jQuery("#" + grid_id).jqGrid('navGrid', '#' + pager_id, {
            cloneToTop: true,
            add: false,
            edit: false,
            search: false,
            del: (el_grid_settings.permit_del_btn == '1') ? true : false,
            deltext: js_lang_label.GENERIC_GRID_DELETE,
            alerttext: js_lang_label.GENERIC_PLEASE_SELECT_ANY_API,
            refreshicon_p: (el_theme_settings.grid_icons.refresh) ? 'uigrid-refresh-btn refresh-icon-only' : "uigrid-refresh-btn",
            refreshtext: (el_theme_settings.grid_icons.refresh) ? '' : js_lang_label.GENERIC_GRID_SHOW_ALL,
            refreshtitle: js_lang_label.GENERIC_REFRESH_LISTING,
            afterRefresh: function () {
                $(".search-chosen-select").find("option").removeAttr("selected");
                $(".search-chosen-select").trigger("chosen:updated");
            }
        }, {
            // edit options
        }, {
            // add options
        }, {
            // delete options
            width: 350,
            caption: js_lang_label.GENERIC_GRID_DELETE,
            msg: js_lang_label.GENERIC_ARE_YOU_SURE_WANT_TO_DELETE_SELECTED_API,
            bSubmit: js_lang_label.GENERIC_GRID_DELETE,
            bCancel: js_lang_label.GENERIC_CANCEL,
            modal: true,
            closeOnEscape: true
        }, {
            //del options
        });

        if (el_grid_settings.permit_add_btn == '1') {
            jQuery("#" + grid_id).navButtonAdd('#' + grid_id + '_toppager_left', {
                caption: (el_theme_settings.grid_icons.export) ? '' : js_lang_label.GENERIC_ADD_API,
                title: js_lang_label.GENERIC_ADD_API,
                buttonicon: "ui-icon-plus",
                buttonicon_p: 'uigrid-add-btn',
                onClickButton: function () {
                    $("#apidialog").dialog('open');
                },
                position: "first",
                id: 'btn_add_api'
            });
        }

        var orgViewModal = $.jgrid.viewModal;
        $.extend($.jgrid, {
            viewModal: function (selector, o) {
                if (selector == '#searchmodfbox_' + o.gid || selector == '#alertmod' || selector == "#delmod" + o.gid || selector == "#info_dialog") {
                    var of = jQuery("#gbox_" + el_tpl_settings.main_grid_id).offset();
                    var w = jQuery("#gbox_" + el_tpl_settings.main_grid_id).width();
                    var h = jQuery("#gbox_" + el_tpl_settings.main_grid_id).height();
                    var w1 = $(selector).width();
                    var h1 = $(selector).height();
                    $(selector).css({
                        'top': of.top + ((h - h1) / 2) - 40,
                        'left': 'calc(50% - ' + w1 / 2 + 'px)'
                    });
                }
                orgViewModal.call(this, selector, o);
            }
        });
        var oldInfoDialog = $.jgrid.info_dialog;
        $.extend($.jgrid, {
            info_dialog: function (caption, content, c_b, modalopt) {
                return oldInfoDialog.call(this, caption, content, c_b, modalopt);
            }
        });

    },
    addAPI: function () {
        var val = [];
        $(':checkbox:checked').each(function (i) {
            val[i] = $(this).val();
        });
        var iAccesskeyId = $('#iAccesskeyId').val();
        if (val.length === 0) {
            alert('Select API');
        } else {
            Project.show_adaxloading_div();
            $.ajax({
                type: "POST",
                url: js_api_add_url,
                data: {
                    iAccesskeyId: iAccesskeyId,
                    val: val
                },
                success: function (response) {
                    var result = JSON.parse(response);
                    $('#apidialog').dialog('close');
                    $('#apidialog').dialog('destroy');
                    console.log(result);
                    Project.setMessage(result.message, 1);
                    if (result.success == 1){
                        window.location.hash = result.url + "|_|" + $.now();
                    }
                },
                complete: function () {
                    Project.hide_adaxloading_div();
                }
            });
        }
    },
    multiSelectAPI: function () {
        $('select[multiple].active.3col').multiselect({
            columns: 2,
            search: true,
            searchOptions: {
                'default': 'Search API'
            },
            selectAll: true,
            maxWidth: 600,
            maxPlaceholderWidth: 600,
            showCheckbox: false,
            selectGroup: true //select all for opt group

        });
    },
    apiDialogBox: function () {
        $("#apidialog").dialog({
            autoOpen: false,
            modal: true,
            height: 400,
            width: 620,
            buttons: [
                {
                    text: js_lang_label.GENERIC_ADD_API,
                    class: "btn btn-success",
                    id: "save_value",
                    click: function (e) {
                        Project.modules.api_access_keys.addAPI();
                    }
                },
                {
                    text: "Close",
                    class: "btn btn-default",
                    click: function (e) {
                        $('#apidialog').dialog('close');
                    }
                }
            ]
        });
    }
}
Project.modules.api_access_keys.init();
    