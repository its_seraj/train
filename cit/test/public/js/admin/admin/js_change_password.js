$(document).ready(function () {
    if ($("#patternLock").val() == "yes") {
        $("#vConfirmPassword").addClass('ignore-valid');
    } else {
        $("#vConfirmPassword").removeClass('ignore-valid');
    }

    if ($("#patternLock").val() == "yes") {
        function setOldPattern(pattern) {
            if (pattern.length) {
                $("#vOldPassword").val(pattern.join(""));
            }
        }
        $('#old_passwd_div').pattern({
            stop: function (event, ui) {
                setOldPattern(ui.pattern);
            }
        });
        $("#vOldPassword").css('display', 'none');


        function setPattern(pattern) {
            if (pattern.length) {
                $("#vPassword").val(pattern.join(""));
            }
        }
        $('#passwd_div').pattern({
            stop: function (event, ui) {
                setPattern(ui.pattern);
            }
        });
        $("#vPassword").css('display', 'none');
    }
});

$(function () {
    $.validator.addMethod("notEqual", function (value, element, id) {
        return value != $(id).val();
    }, js_lang_label.GENERIC_NEW_PASSWORD_SHOULD_NOT_BE_OLD_PASSWORD);

    if (typeof is_ajax_submit != 'undefined') {
        resetPasswordValidateWithRemote();
    } else {
        resetPasswordValidate();
    }
});

function resetPasswordValidate() {
    $('#frmchangepassword').validate({
        ignore: '.ignore-valid',
        rules: {
            vOldPassword: {
                required: true
            },
            vPassword: {
                required: true,
                notEqual: "#vOldPassword",
                minlength: 8,
                alpha_numeric_capital_and_special_compulsory: true,
            },
            vConfirmPassword: {
                required: true,
                equalTo: "#vPassword"
            }
        },
        messages: {
            vOldPassword: {
                required: js_lang_label.GENERIC_PLEASE_ENTER_OLD_PASSWORD
            },
            vPassword: {
                required: js_lang_label.GENERIC_PLEASE_ENTER_NEW_PASSWORD,
                notEqual: js_lang_label.GENERIC_NEW_PASSWORD_SHOULD_NOT_BE_OLD_PASSWORD,
                minlength: js_lang_label.GENERIC_MINIMUN_LENGTH_OF_PASSWORD_IS_8,
                alpha_numeric_capital_and_special_compulsory: js_lang_label.GENERIC_ALPHA_NUMERIC_CAPITAL_AND_SPECIAL_COMPULSORY,
            },
            vConfirmPassword: {
                required: js_lang_label.GENERIC_PLEASE_REENTER_NEW_PASSWORD,
                equalTo: js_lang_label.GENERIC_PASSWORD_DOES_NOT_MATCH
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "vOldPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
            if (element.attr("name") == "vPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
            if (element.attr("name") == "vConfirmPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
        },
        submitHandler: function () {
            Project.show_adaxloading_div();
            var options = {
                url: jajax_action_url,
                beforeSubmit: function (formData, jqForm, options) {
                    showAdminAjaxRequest(formData);
                    prepareFinalFormData(formData, frm_key_settings);
                },
                success: function (respText, statText, xhr) {
                    Project.ajaxSuccessCallback(respText, statText, xhr);
                    var result = parseJSONString(respText);
                    if (!result.success) {
                        Project.refreshCSRFToken(result);
                        responseAjaxDataSubmission(result);
                        return false;
                    } else {
                        if (isFancyBoxActive()) {
                            parent.responseAjaxDataSubmission(result);
                            parent.$.fancybox.close();
                        } else {
                            responseAjaxDataSubmission(result);
                            window.location.hash = cus_enc_url_json['dashboard_sitemap'];
                        }
                    }
                },
                complete: function (xhr, statTextt) {
                    Project.hide_adaxloading_div();
                    Project.ajaxCompleteCallback(xhr, statTextt);
                }
            };
            $('#frmchangepassword').ajaxSubmit(options);
        }
    });
}

function resetPasswordValidateWithRemote() {
    $('#frmchangepassword').validate({
        ignore: '.ignore-valid',
        rules: {
            vOldPassword: {
                required: true
            },
            vPassword: {
                required: true,
                notEqual: "#vOldPassword",
                minlength: 8,
                alpha_numeric_capital_and_special_compulsory: true,
                "remote": {
                    url: check_pwd_url,
                    type: 'POST',
                    data: {
                        password: function () {
                            return $("#vPassword").val();
                        }
                    },
                    dataFilter: function (response) {
                        response = $.parseJSON(response);
                        if (response.success) {
                            return true;
                        } else {
                            message = response.message;
                            return false;
                        }
                    }
                }
            },
            vConfirmPassword: {
                required: true,
                equalTo: "#vPassword"
            }
        },
        messages: {
            vOldPassword: {
                required: js_lang_label.GENERIC_PLEASE_ENTER_OLD_PASSWORD
            },
            vPassword: {
                required: js_lang_label.GENERIC_PLEASE_ENTER_NEW_PASSWORD,
                notEqual: js_lang_label.GENERIC_NEW_PASSWORD_SHOULD_NOT_BE_OLD_PASSWORD,
                minlength: js_lang_label.GENERIC_MINIMUN_LENGTH_OF_PASSWORD_IS_8,
                alpha_numeric_capital_and_special_compulsory: js_lang_label.GENERIC_ALPHA_NUMERIC_CAPITAL_AND_SPECIAL_COMPULSORY,
                remote: function (response) {
                    return message;
                }
            },
            vConfirmPassword: {
                required: js_lang_label.GENERIC_PLEASE_REENTER_NEW_PASSWORD,
                equalTo: js_lang_label.GENERIC_PASSWORD_DOES_NOT_MATCH
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "vOldPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
            if (element.attr("name") == "vPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
            if (element.attr("name") == "vConfirmPassword") {
                var jd_id = element.attr("id");
                error.appendTo("#" + jd_id + "Err");
            }
        },
        submitHandler: function () {
            Project.show_adaxloading_div();
            var options = {
                url: jajax_action_url,
                beforeSubmit: function (formData, jqForm, options) {
                    showAdminAjaxRequest(formData);
                    prepareFinalFormData(formData, frm_key_settings);
                },
                success: function (respText, statText, xhr) {
                    Project.ajaxSuccessCallback(respText, statText, xhr);
                    var result = parseJSONString(respText);
                    if (!result.success) {
                        Project.refreshCSRFToken(result);
                        responseAjaxDataSubmission(result);
                        return false;
                    } else {
                        if (isFancyBoxActive()) {
                            parent.responseAjaxDataSubmission(result);
                            parent.$.fancybox.close();
                        } else {
                            responseAjaxDataSubmission(result);
                            window.location.hash = cus_enc_url_json['dashboard_sitemap'];
                        }
                    }
                },
                complete: function (xhr, statTextt) {
                    Project.hide_adaxloading_div();
                    Project.ajaxCompleteCallback(xhr, statTextt);
                }
            };
            $('#frmchangepassword').ajaxSubmit(options);
        }
    });
}

function closeWindow() {
    if (isFancyBoxActive()) {
        parent.$.fancybox.close();
    } else {
        window.location.hash = cus_enc_url_json['dashboard_sitemap'];
    }
}