<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Category Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Category
 *
 * @class Category_model.php
 *
 * @path application\webservice\master\models\Category_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Category_model extends CI_Model
{
    public $default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->default_lang = $this->general->getLangRequestValue();
    }

    /**
     * query_2 method is used to execute database queries for Category wise Product List API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param string $category_id2 category_id2 is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_2($category_id2 = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("category AS c");

            $this->db->select("c.iCategoryId AS category_id");
            $this->db->select("c.vCategoryName AS category_name");
            $this->db->where_in("c.eStatus", array('Active'));
            if (isset($category_id2) && $category_id2 != "")
            {
                $this->db->where("c.iCategoryId =", $category_id2);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
