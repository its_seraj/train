<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Category wise Product List Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Category wise Product List
 *
 * @class Category_wise_product_list_model.php
 *
 * @path application\webservice\master\models\Category_wise_product_list_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Category_wise_product_list_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
