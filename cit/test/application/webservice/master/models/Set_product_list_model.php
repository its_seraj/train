<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Set Product List Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Set Product List
 *
 * @class Set_product_list_model.php
 *
 * @path application\webservice\master\models\Set_product_list_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Set_product_list_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
