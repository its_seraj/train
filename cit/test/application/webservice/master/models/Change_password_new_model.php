<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Change Password New Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Change Password New
 *
 * @class Change_password_new_model.php
 *
 * @path application\webservice\master\models\Change_password_new_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Change_password_new_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
