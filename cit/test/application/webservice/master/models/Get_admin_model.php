<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Get Admin Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Get Admin
 *
 * @class Get_admin_model.php
 *
 * @path application\webservice\master\models\Get_admin_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Get_admin_model extends CI_Model
{
    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
