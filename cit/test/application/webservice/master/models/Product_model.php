<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Product Model
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage models
 *
 * @module Product
 *
 * @class Product_model.php
 *
 * @path application\webservice\master\models\Product_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Product_model extends CI_Model
{
    public $default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->default_lang = $this->general->getLangRequestValue();
    }

    /**
     * query method is used to execute database queries for Get Product List API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param string $name name is used to process query block.
     * @param string $price price is used to process query block.
     * @param array $settings_params settings_params are used for paging parameters.
     * @return array $return_arr returns response of query block.
     */
    public function query($name = '', $price = '', $page_index = 1, &$settings_params = array())
    {
        try
        {
            $result_arr = array();

            $this->db->start_cache();
            $this->db->from("product AS p");
            $this->db->join("category AS c", "p.iCategoryId = c.iCategoryId", "left");
            $this->db->join("brand AS b", "p.iBrandId = b.iBrandId", "left");
            $this->db->join("manufacturer AS m", "p.iManufacturerId = m.iManufacturerId", "left");
            $this->db->join("product_image AS pi", "p.iProductId = pi.iProductId", "left");

            $this->db->where_in("p.eStatus", array('Available'));
            if (isset($name) && $name != "")
            {
                $this->db->like("p.vProductName", $name, "both");
            }
            if (isset($price) && $price != "")
            {
                $this->db->where("p.fPrice <=", $price);
            }

            $this->db->stop_cache();
            $total_records = $this->db->count_all_results();

            $this->db->select("p.vProductName AS p_product_name");
            $this->db->select("p.iProductId AS p_product_id");
            $this->db->select("c.vCategoryName AS c_category_name");
            $this->db->select("b.vBrand AS b_brand");
            $this->db->select("p.fPrice AS p_price");
            $this->db->select("p.fRetailPrice AS p_retail_price");
            $this->db->select("pi.vImage AS pi_image");

            $settings_params['count'] = $total_records;

            $record_limit = 10;
            $current_page = intval($page_index) > 0 ? intval($page_index) : 1;
            $total_pages = getTotalPages($total_records, $record_limit);
            $start_index = getStartIndex($total_records, $current_page, $record_limit);
            $settings_params['per_page'] = $record_limit;
            $settings_params['curr_page'] = $current_page;
            $settings_params['prev_page'] = ($current_page > 1) ? 1 : 0;
            $settings_params['next_page'] = ($current_page+1 > $total_pages) ? 0 : 1;

            $this->db->limit($record_limit, $start_index);
            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            $this->db->flush_cache();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_1 method is used to execute database queries for Set Product List API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $params_arr params_arr array to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_1($params_arr = array())
    {
        try
        {
            $result_arr = array();
            if (!is_array($params_arr) || count($params_arr) == 0)
            {
                throw new Exception("Insert data not found.");
            }

            $this->db->set("vProductName", $params_arr["_vproductname"]);
            $this->db->set("iCategoryId", $params_arr["_icategoryid"]);
            $this->db->set("iBrandId", $params_arr["_ibrandid"]);
            $this->db->set("iManufacturerId", $params_arr["_imanufacturerid"]);
            $this->db->set("fPrice", $params_arr["_fprice"]);
            $this->db->set("fRetailPrice", $params_arr["_fretailprice"]);
            $this->db->set("dtAddedDate", $params_arr["_dtaddeddate"]);
            $this->db->set("iAddedBy", $params_arr["_iaddedby"]);
            $this->db->set("eStatus", $params_arr["_estatus"]);
            $this->db->set("tDescription", $params_arr["_tdescription"]);
            $this->db->insert("product");
            $insert_id = $this->db->insert_id();
            if (!$insert_id)
            {
                throw new Exception("Failure in insertion.");
            }
            $result_param = "insert_id";
            $result_arr[0][$result_param] = $insert_id;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * product_list method is used to execute database queries for Category wise Product List API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param string $category_id category_id is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function product_list($category_id = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("product AS p");

            $this->db->select("p.vProductName AS p_product_name");
            $this->db->select("p.iCategoryId AS p_category_id");
            $this->db->select("p.iBrandId AS p_brand_id");
            $this->db->select("p.iManufacturerId AS p_manufacturer_id");
            $this->db->select("p.tDescription AS p_description");
            $this->db->select("p.fPrice AS p_price");
            $this->db->select("p.fRetailPrice AS p_retail_price");
            $this->db->select("p.eStatus AS p_status");
            if (isset($category_id) && $category_id != "")
            {
                $this->db->where("p.iCategoryId =", $category_id);
            }

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_6 method is used to execute database queries for Change Product Price API.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $params_arr params_arr array to process query block.
     * @param array $where_arr where_arr are used to process where condition(s).
     * @return array $return_arr returns response of query block.
     */
    public function query_6($params_arr = array(), $where_arr = array())
    {
        try
        {
            $result_arr = array();
            if (isset($where_arr["product_id"]) && $where_arr["product_id"] != "")
            {
                $this->db->where("iProductId =", $where_arr["product_id"]);
            }
            if (isset($params_arr["price"]))
            {
                $this->db->set("fPrice", $params_arr["price"]);
            }
            $res = $this->db->update("product");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1)
            {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[0][$result_param] = $affected_rows;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_7 method is used to execute database queries for Change Product Price API.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param string $product_id product_id is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_7($product_id = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("product AS p");

            $this->db->select("p.iProductId AS p_product_id");
            $this->db->select("p.vProductName AS p_product_name");
            $this->db->select("p.fPrice AS p_price");
            $this->db->select("p.fRetailPrice AS p_retail_price");
            if (isset($product_id) && $product_id != "")
            {
                $this->db->where("p.iProductId =", $product_id);
            }

            $this->db->limit(1);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_8 method is used to execute database queries for Change Product Price API.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param string $product_id product_id is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_8($product_id = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("product AS p");

            $this->db->select("p.iProductId AS p_product_id_1");
            $this->db->select("p.vProductName AS p_product_name_1");
            $this->db->select("p.fPrice AS p_price_1");
            $this->db->select("p.fRetailPrice AS p_retail_price_1");
            if (isset($product_id) && $product_id != "")
            {
                $this->db->where("p.iProductId =", $product_id);
            }

            $this->db->limit(1);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
