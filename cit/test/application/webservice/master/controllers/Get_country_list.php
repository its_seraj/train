<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Get Country List Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Get Country List
 *
 * @class Get_country_list.php
 *
 * @path application\webservice\master\controllers\Get_country_list.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Get_country_list extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $multiple_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->multiple_keys = array(
            "get_country_list_new",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('get_country_list_model');
        $this->load->model("tools/country_model");
    }

    /**
     * rules_get_country_list method is used to validate api input params.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_get_country_list($request_arr = array())
    {
        $valid_arr = array();
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "get_country_list");

        return $valid_res;
    }

    /**
     * start_get_country_list method is used to initiate api execution flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_get_country_list($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_get_country_list($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->get_country_list_new($input_params);

            $output_response = $this->mod_country_finish_success($input_params);
            return $output_response;
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * get_country_list_new method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function get_country_list_new($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $this->block_result = $this->country_model->get_country_list_new();
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["get_country_list_new"] = $this->block_result["data"];

        return $input_params;
    }

    /**
     * mod_country_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mod_country_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "mod_country_finish_success",
        );
        $output_fields = array(
            'mc_country',
            'mc_country_code',
            'mc_country_code_iso_3',
            'mc_country_flag',
            'mc_dial_code',
        );
        $output_keys = array(
            'get_country_list_new',
        );
        $ouput_aliases = array(
            "mc_country" => "country",
            "mc_country_code" => "country_code",
            "mc_country_code_iso_3" => "country_code_iso",
            "mc_country_flag" => "country_flag",
            "mc_dial_code" => "dial_code",
        );

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_country_list";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["output_alias"] = $ouput_aliases;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
