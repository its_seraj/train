<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Get Product List Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Get Product List
 *
 * @class Get_product_list.php
 *
 * @path application\webservice\master\controllers\Get_product_list.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Get_product_list extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $multiple_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->multiple_keys = array(
            "query",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('get_product_list_model');
        $this->load->model("master/product_model");
    }

    /**
     * rules_get_product_list method is used to validate api input params.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_get_product_list($request_arr = array())
    {
        $valid_arr = array();
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "get_product_list");

        return $valid_res;
    }

    /**
     * start_get_product_list method is used to initiate api execution flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_get_product_list($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_get_product_list($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->query($input_params);

            $output_response = $this->product_finish_success($input_params);
            return $output_response;
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $name = isset($input_params["name"]) ? $input_params["name"] : "";
            $price = isset($input_params["price"]) ? $input_params["price"] : "";
            $page_index = isset($input_params["page_index"]) ? $input_params["page_index"] : 1;
            $this->block_result = $this->product_model->query($name, $price, $page_index, $this->settings_params);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
            $result_arr = $this->block_result["data"];
            if (is_array($result_arr) && count($result_arr) > 0)
            {
                $i = 0;
                foreach ($result_arr as $data_key => $data_arr)
                {

                    $data = $data_arr["pi_image"];
                    $image_arr = array();
                    $image_arr["image_name"] = $data;
                    $image_arr["ext"] = implode(",", $this->config->item("IMAGE_EXTENSION_ARR"));
                    $image_arr["width"] = "50";
                    $image_arr["width"] = "50";
                    $p_key = ($data_arr["p_product_id"] != "") ? $data_arr["p_product_id"] : $input_params["p_product_id"];
                    $image_arr["pk"] = $p_key;
                    $image_arr["def_img"] = "Yes";
                    $dest_path = "product_image";
                    $image_arr["path"] = $this->general->getImageNestedFolders($dest_path);
                    $data = $this->general->get_image($image_arr);

                    $result_arr[$data_key]["pi_image"] = $data;

                    $i++;
                }
                $this->block_result["data"] = $result_arr;
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query"] = $this->block_result["data"];

        return $input_params;
    }

    /**
     * product_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function product_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "product_finish_success",
        );
        $output_fields = array(
            'p_product_name',
            'c_category_name',
            'b_brand',
            'p_price',
            'p_retail_price',
            'pi_image',
        );
        $output_keys = array(
            'query',
        );

        $output_array["settings"] = array_merge($this->settings_params, $setting_fields);
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_product_list";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
