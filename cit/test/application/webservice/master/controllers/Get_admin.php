<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Get Admin Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Get Admin
 *
 * @class Get_admin.php
 *
 * @path application\webservice\master\controllers\Get_admin.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Get_admin extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array(
            "query_3",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('get_admin_model');
        $this->load->model("user/admin_ws_model");
    }

    /**
     * rules_get_admin method is used to validate api input params.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_get_admin($request_arr = array())
    {
        $valid_arr = array(
            "id" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message" => "id_required",
                )
            )
        );
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "get_admin");

        return $valid_res;
    }

    /**
     * start_get_admin method is used to initiate api execution flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_get_admin($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_get_admin($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->query_3($input_params);

            $condition_res = $this->condition($input_params);
            if ($condition_res["success"])
            {

                $output_response = $this->mod_admin_finish_success($input_params);
                return $output_response;
            }

            else
            {

                $output_response = $this->mod_admin_finish_success_1($input_params);
                return $output_response;
            }
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query_3 method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_3($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $id = isset($input_params["id"]) ? $input_params["id"] : "";
            $this->block_result = $this->admin_ws_model->query_3($id);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_3"] = $this->block_result["data"];
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $this->block_result["data"]);

        return $input_params;
    }

    /**
     * condition method is used to process conditions.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process condition flow.
     * @return array $block_result returns result of condition block as array.
     */
    public function condition($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $cc_lo_0 = (empty($input_params["query_3"]) ? 0 : 1);
            $cc_ro_0 = 1;

            $cc_fr_0 = ($cc_lo_0 == $cc_ro_0) ? TRUE : FALSE;
            if (!$cc_fr_0)
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
            $message = "Conditions matched.";
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->block_result["success"] = $success;
        $this->block_result["message"] = $message;
        return $this->block_result;
    }

    /**
     * mod_admin_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mod_admin_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "mod_admin_finish_success",
        );
        $output_fields = array(
            'ma_admin_id',
            'ma_name',
            'ma_email',
            'ma_user_name',
            'ma_password',
        );
        $output_keys = array(
            'query_3',
        );

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_admin";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["single_keys"] = $this->single_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * mod_admin_finish_success_1 method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function mod_admin_finish_success_1($input_params = array())
    {

        $setting_fields = array(
            "success" => "0",
            "message" => "mod_admin_finish_success_1",
        );
        $output_fields = array();

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "get_admin";
        $func_array["function"]["single_keys"] = $this->single_keys;

        $this->wsresponse->setResponseStatus(404);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
