<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Category wise Product List Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Category wise Product List
 *
 * @class Category_wise_product_list.php
 *
 * @path application\webservice\master\controllers\Category_wise_product_list.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Category_wise_product_list extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $multiple_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->multiple_keys = array(
            "query_2",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('category_wise_product_list_model');
        $this->load->model("master/category_model");
        $this->load->model("master/product_model");
    }

    /**
     * rules_category_wise_product_list method is used to validate api input params.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_category_wise_product_list($request_arr = array())
    {
        $valid_arr = array(
            "category_id2" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message" => "category_id2_required",
                )
            )
        );
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "category_wise_product_list");

        return $valid_res;
    }

    /**
     * start_category_wise_product_list method is used to initiate api execution flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_category_wise_product_list($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_category_wise_product_list($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->query_2($input_params);

            $condition_res = $this->condition_1($input_params);
            if ($condition_res["success"])
            {

                $input_params = $this->start_loop($input_params);

                $input_params = $this->email_notification($input_params);

                $output_response = $this->category_finish_success($input_params);
                return $output_response;
            }

            else
            {

                $output_response = $this->category_finish_success_1($input_params);
                return $output_response;
            }
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query_2 method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_2($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $category_id2 = isset($input_params["category_id2"]) ? $input_params["category_id2"] : "";
            $this->block_result = $this->category_model->query_2($category_id2);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_2"] = $this->block_result["data"];

        return $input_params;
    }

    /**
     * condition_1 method is used to process conditions.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process condition flow.
     * @return array $block_result returns result of condition block as array.
     */
    public function condition_1($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $cc_lo_0 = (empty($input_params["query_2"]) ? 0 : 1);
            $cc_ro_0 = 1;

            $cc_fr_0 = ($cc_lo_0 == $cc_ro_0) ? TRUE : FALSE;
            if (!$cc_fr_0)
            {
                throw new Exception("Some conditions does not match.");
            }
            $success = 1;
            $message = "Conditions matched.";
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->block_result["success"] = $success;
        $this->block_result["message"] = $message;
        return $this->block_result;
    }

    /**
     * start_loop method is used to process loop flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function start_loop($input_params = array())
    {
        $this->iterate_start_loop($input_params["query_2"], $input_params);
        return $input_params;
    }

    /**
     * product_list method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function product_list($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $category_id = isset($input_params["category_id"]) ? $input_params["category_id"] : "";
            $this->block_result = $this->product_model->product_list($category_id);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["product_list"] = $this->block_result["data"];

        return $input_params;
    }

    /**
     * email_notification method is used to process email notification.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function email_notification($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $email_arr["vEmail"] = "thisthat@yopmail.com";

            $email_arr["vSubject"] = "Hello there please visit seraj.engineer";

            $email_arr["NAME"] = $input_params["category_id"];
            $email_arr["EMAIL"] = $input_params["category_name"];
            $email_arr["COMMENT"] = $input_params["p_product_name"];

            $success = $this->general->sendMail($email_arr, "CONTACT_US", $input_params);

            $log_arr = array();
            $log_arr['eEntityType'] = 'General';
            $log_arr['vReceiver'] = is_array($email_arr["vEmail"]) ? implode(",", $email_arr["vEmail"]) : $email_arr["vEmail"];
            $log_arr['eNotificationType'] = "EmailNotify";
            $log_arr['vSubject'] = $this->general->getEmailOutput("subject");
            $log_arr['tContent'] = $this->general->getEmailOutput("content");
            if (!$success)
            {
                $log_arr['tError'] = $this->general->getNotifyErrorOutput();
            }
            $log_arr['dtSendDateTime'] = date('Y-m-d H:i:s');
            $log_arr['eStatus'] = ($success) ? "Executed" : "Failed";
            $this->general->insertExecutedNotify($log_arr);
            if (!$success)
            {
                throw new Exception("Failure in sending mail.");
            }
            $success = 1;
            $message = "Email notification send successfully.";
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->block_result["success"] = $success;
        $this->block_result["message"] = $message;
        $input_params["email_notification"] = $this->block_result["success"];

        return $input_params;
    }

    /**
     * category_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function category_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "category_finish_success",
        );
        $output_fields = array(
            'category_id',
            'category_name',
            'product_list',
            'p_product_name',
            'p_category_id',
            'p_brand_id',
            'p_manufacturer_id',
            'p_description',
            'p_price',
            'p_retail_price',
            'p_status',
        );
        $output_keys = array(
            'query_2',
        );
        $inner_keys = array(
            'product_list',
        );

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "category_wise_product_list";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["inner_keys"] = $inner_keys;
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * category_finish_success_1 method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function category_finish_success_1($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "category_finish_success_1",
        );
        $output_fields = array();

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "category_wise_product_list";
        $func_array["function"]["multiple_keys"] = $this->multiple_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }

    /**
     * iterate_start_loop method is used to iterate loop.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $query_2_lp_arr query_2_lp_arr array to iterate loop.
     * @param array $input_params_addr $input_params_addr array to address original input params.
     */
    public function iterate_start_loop(&$query_2_lp_arr = array(), &$input_params_addr = array())
    {

        $input_params_loc = $input_params_addr;
        $_loop_params_loc = $query_2_lp_arr;
        $_lp_ini = 0;
        $_lp_end = count($_loop_params_loc);
        for ($i = $_lp_ini; $i < $_lp_end; $i += 1)
        {
            $query_2_lp_pms = $input_params_loc;

            unset($query_2_lp_pms["query_2"]);
            if (is_array($_loop_params_loc[$i]))
            {
                $query_2_lp_pms = $_loop_params_loc[$i]+$input_params_loc;
            }
            else
            {
                $query_2_lp_pms["query_2"] = $_loop_params_loc[$i];
                $_loop_params_loc[$i] = array();
                $_loop_params_loc[$i]["query_2"] = $query_2_lp_pms["query_2"];
            }

            $query_2_lp_pms["i"] = $i;
            $input_params = $query_2_lp_pms;

            $input_params = $this->product_list($input_params);

            $query_2_lp_arr[$i] = $this->wsresponse->filterLoopParams($input_params, $_loop_params_loc[$i], $query_2_lp_pms);
        }
    }
}
