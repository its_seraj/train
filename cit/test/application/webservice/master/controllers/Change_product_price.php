<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Change Product Price Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Change Product Price
 *
 * @class Change_product_price.php
 *
 * @path application\webservice\master\controllers\Change_product_price.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Change_product_price extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array(
            "query_7",
            "query_6",
            "query_8",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('change_product_price_model');
        $this->load->model("master/product_model");
    }

    /**
     * rules_change_product_price method is used to validate api input params.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_change_product_price($request_arr = array())
    {
        $valid_arr = array(
            "price" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message" => "price_required",
                )
            ),
            "product_id" => array(
                array(
                    "rule" => "required",
                    "value" => TRUE,
                    "message" => "product_id_required",
                )
            )
        );
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "change_product_price");

        return $valid_res;
    }

    /**
     * start_change_product_price method is used to initiate api execution flow.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_change_product_price($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_change_product_price($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->query_7($input_params);

            $input_params = $this->query_6($input_params);

            $input_params = $this->query_8($input_params);

            $output_response = $this->product_finish_success($input_params);
            return $output_response;
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query_7 method is used to process query block.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_7($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $product_id = isset($input_params["product_id"]) ? $input_params["product_id"] : "";
            $this->block_result = $this->product_model->query_7($product_id);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_7"] = $this->block_result["data"];
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $this->block_result["data"]);

        return $input_params;
    }

    /**
     * query_6 method is used to process query block.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_6($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $params_arr = $where_arr = array();
            if (isset($input_params["product_id"]))
            {
                $where_arr["product_id"] = $input_params["product_id"];
            }
            if (isset($input_params["price"]))
            {
                $params_arr["price"] = $input_params["price"];
            }
            $this->block_result = $this->product_model->query_6($params_arr, $where_arr);
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_6"] = $this->block_result["data"];
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $this->block_result["data"]);

        return $input_params;
    }

    /**
     * query_8 method is used to process query block.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_8($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $product_id = isset($input_params["product_id"]) ? $input_params["product_id"] : "";
            $this->block_result = $this->product_model->query_8($product_id);
            if (!$this->block_result["success"])
            {
                throw new Exception("No records found.");
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_8"] = $this->block_result["data"];
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $this->block_result["data"]);

        return $input_params;
    }

    /**
     * product_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 06.04.2022
     * @modified Mdseraj Khan | 06.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function product_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "product_finish_success",
        );
        $output_fields = array(
            'p_product_id',
            'p_product_name',
            'p_price',
            'p_retail_price',
            'affected_rows',
            'p_product_id_1',
            'p_product_name_1',
            'p_price_1',
            'p_retail_price_1',
        );
        $output_keys = array(
            'query_7',
            'query_6',
            'query_8',
        );

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "change_product_price";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["single_keys"] = $this->single_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
