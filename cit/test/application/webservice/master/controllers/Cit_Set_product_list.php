<?php

   
/**
 * Description of Set Product List Extended Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Extended Set Product List
 *
 * @class Cit_Set_product_list.php
 *
 * @path application\webservice\master\controllers\Cit_Set_product_list.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
 
Class Cit_Set_product_list extends Set_product_list {
        public function __construct()
{
    parent::__construct();
}
public function product_added_by(){
    return $this->session->userdata('vName');
}
}
