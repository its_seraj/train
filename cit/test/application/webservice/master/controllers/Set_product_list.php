<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Set Product List Controller
 *
 * @category webservice
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Set Product List
 *
 * @class Set_product_list.php
 *
 * @path application\webservice\master\controllers\Set_product_list.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 05.04.2022
 */

class Set_product_list extends Cit_Controller
{
    public $settings_params;
    public $output_params;
    public $single_keys;
    public $block_result;

    /**
     * __construct method is used to set controller preferences while controller object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->settings_params = array();
        $this->output_params = array();
        $this->single_keys = array(
            "query_1",
        );
        $this->block_result = array();

        $this->load->library('wsresponse');
        $this->load->model('set_product_list_model');
        $this->load->model("master/product_model");
    }

    /**
     * rules_set_product_list method is used to validate api input params.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @return array $valid_res returns output response of API.
     */
    public function rules_set_product_list($request_arr = array())
    {
        $valid_arr = array();
        $valid_res = $this->wsresponse->validateInputParams($valid_arr, $request_arr, "set_product_list");

        return $valid_res;
    }

    /**
     * start_set_product_list method is used to initiate api execution flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $request_arr request_arr array is used for api input.
     * @param bool $inner_api inner_api flag is used to idetify whether it is inner api request or general request.
     * @return array $output_response returns output response of API.
     */
    public function start_set_product_list($request_arr = array(), $inner_api = FALSE)
    {
        try
        {
            $validation_res = $this->rules_set_product_list($request_arr);
            if ($validation_res["success"] == "-5")
            {
                if ($inner_api === TRUE)
                {
                    return $validation_res;
                }
                else
                {
                    $this->wsresponse->sendValidationResponse($validation_res);
                }
            }
            $output_response = array();
            $input_params = $validation_res['input_params'];

            $input_params = $this->query_1($input_params);

            $output_response = $this->product_finish_success($input_params);
            return $output_response;
        }
        catch(Exception $e)
        {
            $message = $e->getMessage();
        }
        return $output_response;
    }

    /**
     * query_1 method is used to process query block.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $input_params returns modfied input_params array.
     */
    public function query_1($input_params = array())
    {

        $this->block_result = array();
        try
        {

            $params_arr = array();
            $params_arr["_vproductname"] = '{%REQUEST.product_name%}';
            $params_arr["_icategoryid"] = '{%REQUEST.category_id%}';
            $params_arr["_ibrandid"] = '{%REQUEST.brand_id%}';
            $params_arr["_imanufacturerid"] = '{%REQUEST.manufacturer_id%}';
            $params_arr["_fprice"] = '{%REQUEST.price%}';
            $params_arr["_fretailprice"] = '{%REQUEST.retail_price%}';
            $params_arr["_dtaddeddate"] = '{%REQUEST.added_date%}';
            $params_arr["_iaddedby"] = "";
            if (method_exists($this, "product_added_by"))
            {
                $params_arr["_iaddedby"] = $this->product_added_by($params_arr["_iaddedby"], $input_params);
            }
            $params_arr["_estatus"] = '{%REQUEST.%}';
            $params_arr["_tdescription"] = '{%REQUEST.description%}';
            $this->block_result = $this->product_model->query_1($params_arr);
        }
        catch(Exception $e)
        {
            $success = 0;
            $this->block_result["data"] = array();
        }
        $input_params["query_1"] = $this->block_result["data"];
        $input_params = $this->wsresponse->assignSingleRecord($input_params, $this->block_result["data"]);

        return $input_params;
    }

    /**
     * product_finish_success method is used to process finish flow.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $input_params input_params array to process loop flow.
     * @return array $responce_arr returns responce array of api.
     */
    public function product_finish_success($input_params = array())
    {

        $setting_fields = array(
            "success" => "1",
            "message" => "product_finish_success",
        );
        $output_fields = array(
            'insert_id',
        );
        $output_keys = array(
            'query_1',
        );

        $output_array["settings"] = $setting_fields;
        $output_array["settings"]["fields"] = $output_fields;
        $output_array["data"] = $input_params;

        $func_array["function"]["name"] = "set_product_list";
        $func_array["function"]["output_keys"] = $output_keys;
        $func_array["function"]["single_keys"] = $this->single_keys;

        $this->wsresponse->setResponseStatus(200);

        $responce_arr = $this->wsresponse->outputResponse($output_array, $func_array);

        return $responce_arr;
    }
}
