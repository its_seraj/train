<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Admin Ws Model
 *
 * @category webservice
 *
 * @package user
 *
 * @subpackage models
 *
 * @module Admin Ws
 *
 * @class Admin_ws_model.php
 *
 * @path application\webservice\user\models\Admin_ws_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 06.04.2022
 */

class Admin_ws_model extends CI_Model
{
    public $default_lang = 'EN';

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('listing');
        $this->default_lang = $this->general->getLangRequestValue();
    }

    /**
     * query_3 method is used to execute database queries for Get Admin API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param string $id id is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_3($id = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("mod_admin AS ma");

            $this->db->select("ma.iAdminId AS ma_admin_id");
            $this->db->select("ma.vName AS ma_name");
            $this->db->select("ma.vEmail AS ma_email");
            $this->db->select("ma.vUserName AS ma_user_name");
            $this->db->select("ma.vPassword AS ma_password");
            $this->db->where_in("ma.eStatus", array('Active'));
            if (isset($id) && $id != "")
            {
                $this->db->where("ma.iAdminId =", $id);
            }
            $this->db->where("ma.iAdminId > 0", FALSE, FALSE);

            $this->db->limit(1);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_4 method is used to execute database queries for Change Password New API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param array $params_arr params_arr array to process query block.
     * @param array $where_arr where_arr are used to process where condition(s).
     * @return array $return_arr returns response of query block.
     */
    public function query_4($params_arr = array(), $where_arr = array())
    {
        try
        {
            $result_arr = array();
            if (isset($where_arr["admin_id"]) && $where_arr["admin_id"] != "")
            {
                $this->db->where("iAdminId =", $where_arr["admin_id"]);
            }
            $this->db->where_in("eStatus", array('Active'));
            if (isset($params_arr["new_password"]))
            {
                $this->db->set("vPassword", $params_arr["new_password"]);
            }
            $res = $this->db->update("mod_admin");
            $affected_rows = $this->db->affected_rows();
            if (!$res || $affected_rows == -1)
            {
                throw new Exception("Failure in updation.");
            }
            $result_param = "affected_rows";
            $result_arr[0][$result_param] = $affected_rows;
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $this->db->flush_cache();
        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }

    /**
     * query_5 method is used to execute database queries for Change Password New API.
     * @created Mdseraj Khan | 05.04.2022
     * @modified Mdseraj Khan | 05.04.2022
     * @param string $admin_id admin_id is used to process query block.
     * @return array $return_arr returns response of query block.
     */
    public function query_5($admin_id = '')
    {
        try
        {
            $result_arr = array();

            $this->db->from("mod_admin AS ma");

            $this->db->select("ma.vUserName AS ma_user_name");
            if (isset($admin_id) && $admin_id != "")
            {
                $this->db->where("ma.iAdminId =", $admin_id);
            }

            $this->db->limit(1);

            $result_obj = $this->db->get();
            $result_arr = is_object($result_obj) ? $result_obj->result_array() : array();
            if (!is_array($result_arr) || count($result_arr) == 0)
            {
                throw new Exception('No records found.');
            }
            $success = 1;
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }

        $this->db->_reset_all();
        //echo $this->db->last_query();
        $return_arr["success"] = $success;
        $return_arr["message"] = $message;
        $return_arr["data"] = $result_arr;
        return $return_arr;
    }
}
