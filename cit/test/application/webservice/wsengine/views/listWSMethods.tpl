<%$this->css->add_css("ws_validate.css")%>
<%$this->js->add_js("crypto-js.js","application_ws.js")%>
<script type="text/javascript">
    var ws_base_url = "<%$ws_url%>";
    var token_url = "<%$token_url%>";
    
    var token_expiry = "<%$token_expiry%>";
    var token_enabled = "<%$token_enabled%>";
    var checksum_enabled = "<%$checksum_enabled%>";
    var encryption_enabled = "<%$encryption_enabled%>";
    
    var enc_keys = {
        "iterations" : '<%$iterations%>', // 999
        "salt_phrase" : '<%$salt_phrase%>', // CIT
        "key_phrase" : '<%$key_phrase%>', // fbb5df78ead3a05497d3af0df3e85e31
        "iv_phrase" : '<%$iv_phrase%>', // 5b8b4a775277c63f
    };
    
    var ws_token_value = '';
    var token_created_at = null;
</script>

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">Welcome to API Console</div>
    </div>
    <div class="panel-body">
        <%foreach name="j" from=$all_methods key=key item=item%>    
            <div class="col-md-12 api-block">
                <div class="method-name">
                    <%$smarty.foreach.j.iteration%>. <%$item.title%>
                </div>
                <div class="inputparams">
                    <form action="<%$key%>?ws_debug=1" method="post" class="form-horizontal ws">
                        <%assign var="ws_params" value=$item.params%>
                        <%section name="i" loop=$ws_params%>
                            <%if $ws_params[i] neq ''%>
                                <div class="form-group">
                                    <label class="col-xs-1 control-label">
                                        <%$ws_params[i]%>
                                    </label>
                                    <div class="col-xs-4">
                                        <input class="form-control" id="<%$ws_params[i]%>" name="<%$ws_params[i]%>" value="">          
                                    </div>
                                </div>
                            <%/if%>
                        <%/section%>  
                        <div class="form-group">
                            <label class="col-xs-1 control-label"></label>
                            <div class="col-xs-10">
                                <button type="submit" class="btn btn-default">Send Request</button>
                            </div>
                        </div>
                    </form>
                    <pre class="code"></pre>
                    <iframe class="text"></iframe>
                </div>
            </div>
        <%/foreach%>
    </div>
</div>


