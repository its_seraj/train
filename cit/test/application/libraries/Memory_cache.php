<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Memcached Library
 *
 * @category libraries
 * 
 * @package libraries
 *
 * @module Memory_cache
 * 
 * @class Memory_cache.php
 * 
 * @path application\libraries\Memory_cache.php
 * 
 * @version 4.0
 * 
 * @author CIT Dev Team
 * 
 * @since 01.05.2018
 */
class Memory_cache
{

    private $m;
    private $settings;
    private $client_type;
    protected $CI;
    protected $errors = array();
    public $message = '';
    
    public function __construct()
    {
        $this->CI = & get_instance();
        if(!$this->CI->config->item('ENABLE_MEMCACHED')){
            return false;
        }
        
        $settings = array();
        $engine = 'Memcached';
        $use_memcache = class_exists($engine) ? $engine : FALSE;
        if ($use_memcache == FALSE) {
            $engine = 'Memcache';
            $use_memcache = class_exists($engine) ? $engine : FALSE;
        }
        
        $settings['servers'] = array(
            'default' => $this->CI->config->item('MEMCACHED_CONFIG')
        );

        $settings['config'] = array(
            'engine' => $engine, // Set which caching engine you are using. Acceptable values: Memcached or Memcache
            'prefix' => '', // Prefixes every key value (useful for multi environment setups)
            'compression' => FALSE, // Default: FALSE or MEMCACHE_COMPRESSED Compression Method (Memcache only).
            // Not necessary if you already are using 'compression'
            'auto_compress_tresh' => FALSE, // Controls the minimum value length before attempting to compress automatically.
            'auto_compress_savings' => 0.2, // Specifies the minimum amount of savings to actually store the value compressed. The supplied value must be between 0 and 1.
            'expiration' => 60, // Default content expiration value (in seconds)
            'delete_expiration' => 0, // Default time between the delete command and the actual delete action occurs (in seconds)
            'use_memcache' => $use_memcache
        );

        $this->settings = $settings;

        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        // Lets try to load Memcache or Memcached Class
        $this->client_type = class_exists($this->settings['config']['engine']) ? $this->settings['config']['engine'] : FALSE;

        if ($this->client_type) {
            // Which one should be loaded
            switch ($this->client_type) {
                case 'Memcached':
                    $this->m = new Memcached();
                    break;
                case 'Memcache':
                    $this->m = new Memcache();
                    // Set Automatic Compression Settings
                    if ($this->settings['config']['auto_compress_tresh']) {
                        $this->setcompressthreshold($this->settings['config']['auto_compress_tresh'], $this->settings['config']['auto_compress_savings']);
                    }
                    break;
            }
            $this->log_message('debug', "Memcached Library: " . $this->client_type . " Class Loaded");

            $this->auto_connect();
        } else {
            $this->log_message('error', "Memcached Library: Failed to load Memcached or Memcache Class");
        }
    }
    /*
      +-------------------------------------+
      Name: auto_connect
      Purpose: runs through all of the servers defined in
      the configuration and attempts to connect to each
      @param return : none
      +-------------------------------------+
     */

    private function auto_connect()
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        foreach ($this->settings['servers'] as $key => $server) {
            if (!$this->add_server($server)) {
                $this->errors[] = "Memcached Library: Could not connect to the server named $key";
                $this->log_message('error', 'Memcached Library: Could not connect to the server named "' . $key . '"');
            } else {
                $this->log_message('debug', 'Memcached Library: Successfully connected to the server named "' . $key . '"');
            }
        }
    }

    public function log_message($code = '', $message = '')
    {
        $this->message = $message;
    }
    /*
      +-------------------------------------+
      Name: add_server
      Purpose:
      @param return : TRUE or FALSE
      +-------------------------------------+
     */

    public function add_server($server)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        extract($server);
        return $this->m->addServer($host, $port, $weight);
    }
    /*
      +-------------------------------------+
      Name: add
      Purpose: add an item to the memcache server(s)
      @param return : TRUE or FALSE
      +-------------------------------------+
     */

    public function add($key = NULL, $value = NULL, $expiration = NULL)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        if (is_null($expiration)) {
            $expiration = $this->settings['config']['expiration'];
        }
        if (is_array($key)) {
            foreach ($key as $multi) {
                if (!isset($multi['expiration']) || $multi['expiration'] == '') {
                    $multi['expiration'] = $this->settings['config']['expiration'];
                }
                $this->add($this->key_name($multi['key']), $multi['value'], $multi['expiration']);
            }
        } else {
            switch ($this->client_type) {
                case 'Memcache':
                    $add_status = $this->m->add($this->key_name($key), $value, $this->settings['config']['compression'], $expiration);
                    break;

                case 'Memcached':
                    $add_status = $this->m->add($this->key_name($key), $value, $expiration);
                    break;
            }

            return $add_status;
        }
    }
    /*
      +-------------------------------------+
      Name: set
      Purpose: similar to the add() method but uses set
      @param return : TRUE or FALSE
      +-------------------------------------+
     */

    public function set($key = NULL, $value = NULL, $expiration = NULL)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        if (is_null($expiration)) {
            $expiration = $this->settings['config']['expiration'];
        }
        
        if (is_array($key)) {
            foreach ($key as $multi) {
                if (!isset($multi['expiration']) || $multi['expiration'] == '') {
                    $multi['expiration'] = $this->settings['config']['expiration'];
                }
                $this->set($this->key_name($multi['key']), $multi['value'], $multi['expiration']);
            }
        } else {
            if ($this->m) {
                switch ($this->client_type) {
                    case 'Memcache':
                        $add_status = $this->m->set($this->key_name($key), $value, $this->settings['config']['compression'], $expiration);
                        break;

                    case 'Memcached':
                        $add_status = $this->m->set($this->key_name($key), $value, $expiration);
                        break;
                }
            } else {
                $add_status = false;
            }
            return $add_status;
        }
    }
    /*
      +-------------------------------------+
      Name: get
      Purpose: gets the data for a single key or an array of keys
      @param return : array of data or multi-dimensional array of data
      +-------------------------------------+
     */

    public function get($key = NULL)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        if ($this->m) {
            if (is_null($key)) {
                $this->errors[] = 'The key value cannot be NULL';
                return FALSE;
            }

            if (is_array($key)) {
                foreach ($key as $n => $k) {
                    $key[$n] = $this->key_name($k);
                }
                return $this->m->getMulti($key);
            } else {
                return $this->m->get($this->key_name($key));
            }
        }
        return FALSE;
    }
    
    

     public function getKeyExists($key) {
        $item = $this->m->get($key);
        if (isset($item) && $item != '') {
            return 'Yes';
        } else {
            return 'No';
        }
    }
    
    /*
      +-------------------------------------+
      Name: get_all_keys
      Purpose: gets the data for all keys
      @param return : array of data or multi-dimensional array of data
      +-------------------------------------+
     */

    public function get_all_keys()
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }
        
        if ($this->m) {
            switch ($this->client_type) {
                    case 'Memcache':
                        $all_keys = $this->get_memcache_keys();
                        break;
                    case 'Memcached':
                        $all_keys = $this->m->getAllKeys();
                        break;
                }
                
            return $all_keys;
        }
        
        return FALSE;
    }
    /*
      +-------------------------------------+
      Name: getMemcacheKeys
      Purpose: gets the data for all keys of memcache
      @param return : array of data or multi-dimensional array of data
      +-------------------------------------+
     */
     
    public function get_memcache_keys() {
        
        $list = array();
        $all_slabs = $this->m->getExtendedStats('slabs');
        if (is_array($all_slabs) && count($all_slabs) > 0) {
            foreach($all_slabs as $server => $slabs) {
                foreach($slabs as $slab_id => $slab_meta) {
                   if (!is_int($slab_id)) { continue; }
                   $cdump = $this->m->getExtendedStats('cachedump',(int)$slab_id);
                    foreach($cdump as $keys => $arr_val) {
                        if (!is_array($arr_val)) continue;
                        foreach($arr_val as $k => $v) {                   
                            $list[] =  $k;
                        }
                   }
                }
            }
        }
        
        return $list;  
    }
    /*
      +-------------------------------------+
      Name: delete
      Purpose: deletes a single or multiple data elements from the memached servers
      @param return : none
      +-------------------------------------+
     */

    public function delete($key, $expiration = NULL)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        if (is_null($key)) {
            $this->errors[] = 'The key value cannot be NULL';
            return FALSE;
        }

        if (is_null($expiration)) {
            $expiration = $this->settings['config']['delete_expiration'];
        }

        if (is_array($key)) {
            foreach ($key as $multi) {
                $this->delete($multi, $expiration);
            }
        } else {
            return $this->m->delete($this->key_name($key), $expiration);
        }
    }
    /*
      +-------------------------------------+
      Name: replace
      Purpose: replaces the value of a key that already exists
      @param return : none
      +-------------------------------------+
     */

    public function replace($key = NULL, $value = NULL, $expiration = NULL)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        if (is_null($expiration)) {
            $expiration = $this->settings['config']['expiration'];
        }
        if (is_array($key)) {
            foreach ($key as $multi) {
                if (!isset($multi['expiration']) || $multi['expiration'] == '') {
                    $multi['expiration'] = $this->settings['config']['expiration'];
                }
                $this->replace($multi['key'], $multi['value'], $multi['expiration']);
            }
        } else {
            switch ($this->client_type) {
                case 'Memcache':
                    $replace_status = $this->m->replace($this->key_name($key), $value, $this->settings['config']['compression'], $expiration);
                    break;

                case 'Memcached':
                    $replace_status = $this->m->replace($this->key_name($key), $value, $expiration);
                    break;
            }

            return $replace_status;
        }
    }
    /*
      +-------------------------------------+
      Name: increment
      Purpose: increments a value
      @param return : none
      +-------------------------------------+
     */

    public function increment($key = null, $by = 1)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        return $this->m->increment($this->key_name($key), $by);
    }
    /*
      +-------------------------------------+
      Name: decrement
      Purpose: decrements a value
      @param return : none
      +-------------------------------------+
     */

    public function decrement($key = null, $by = 1)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        return $this->m->decrement($this->key_name($key), $by);
    }
    /*
      +-------------------------------------+
      Name: flush
      Purpose: flushes all items from cache
      @param return : none
      +-------------------------------------+
     */

    public function flush()
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        return $this->m->flush();
    }
    /*
      +-------------------------------------+
      Name: getversion
      Purpose: Get Server Vesion Number
      @param Returns a string of server version number or FALSE on failure.
      +-------------------------------------+
     */

    public function getversion()
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        return $this->m->getVersion();
    }
    /*
      +-------------------------------------+
      Name: getstats
      Purpose: Get Server Stats
      Possible: "reset, malloc, maps, cachedump, slabs, items, sizes"
      @param returns an associative array with server's statistics. Array keys correspond to stats parameters and values to parameter's values.
      +-------------------------------------+
     */

    public function getstats($type = "items")
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        switch ($this->client_type) {
            case 'Memcache':
                $stats = $this->m->getStats($type);
                break;

            case 'Memcached':
                $stats = $this->m->getStats();
                break;
        }
        return $stats;
    }
    /*
      +-------------------------------------+
      Name: setcompresstreshold
      Purpose: Set When Automatic compression should kick-in
      @param return TRUE/FALSE
      +-------------------------------------+
     */

    public function setcompressthreshold($tresh, $savings = 0.2)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }

        switch ($this->client_type) {
            case 'Memcache':
                $setcompressthreshold_status = $this->m->setCompressThreshold($tresh, $savings = 0.2);
                break;

            case 'Memcached':
                $setcompressthreshold_status = TRUE;
                break;
        }
        return $setcompressthreshold_status;
    }
    /*
      +-------------------------------------+
      Name: key_name
      Purpose: standardizes the key names for memcache instances
      @param return : md5 key name
      +-------------------------------------+
     */

    protected function key_name($key)
    {
        if (!$this->settings['config']['use_memcache']) {
            return false;
        }
        
        return $key;
        //return md5(strtolower($this->settings['config']['prefix'] . $key));
    }
}