<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Extended Form validation Library
 *
 * @category libraries
 * 
 * @package libraries
 *
 * @module FormValidation
 * 
 * @class Cit_Form_validation.php
 * 
 * @path application\libraries\Cit_Form_validation.php
 * 
 * @version 4.0
 * 
 * @author CIT Dev Team
 * 
 * @since 01.08.2016
 */
class Cit_Form_validation extends CI_Form_validation
{

    protected $CI;

    public function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
    }

    public function validateThroughGUMP($module_name = '', $form_config = array(), $form_data = array(), $mode = '')
    {
        try {
            $rule_mapper = array(
                "nowhitespace" => "cit_nowhitespace",
                "alpha_numeric_compulsory" => "cit_alpha_numeric_compulsory",
                "alpha_numeric_special_compulsory" => "cit_alpha_numeric_special_compulsory",
                "alpha_numeric_capital_compulsory" => "cit_alpha_numeric_capital_compulsory",
                "credit_card" => "valid_cc",
                "date" => "cit_date",
                "datetime" => "cit_datetime",
                "dateEqualTo" => "cit_dateEqualTo",
                "dateGreaterEqual" => "cit_dateGreaterEqual",
                "dateGreaterThan" => "cit_dateGreaterThan",
                "dateLessEqual" => "cit_dateLessEqual",
                "dateLessThan" => "cit_dateLessThan",
                "digits" => "integer",
                "email" => "valid_email",
                "equalTo" => "equalsfield",
                "ip_address" => "valid_ip",
                "alpha_numeric_without_spaces" => "alpha_numeric",
                "alpha_numeric_with_spaces" => "alpha_numeric_space",
                "alpha_numeric_without_special_chars" => "alpha_numeric_dash",
                "alpha_without_spaces" => "alpha",
                "alpha_with_spaces" => "alpha_space",
                "alpha_without_special_chars" => "alpha_dash",
                "max" => "max_numeric",
                "maxlength" => "max_len",
                "min" => "min",
                "minlength" => "min_len",
                "notEqualTo" => "",
                "number" => "numeric",
                "numEqualTo" => "cit_numEqualTo",
                "numGreaterEqual" => "cit_numGreaterEqual",
                "numGreaterThan" => "cit_numGreaterThan",
                "numLessEqual" => "cit_numLessEqual",
                "numLessThan" => "cit_numLessThan",
                "phone_number" => "phone_number",
                "range" => "cit_range",
                "rangelength" => "between_len",
                "required" => "required",
                "time" => "cit_time",
                "timeEqualTo" => "cit_timeEqualTo",
                "timeGreaterEqual" => "cit_timeGreaterEqual",
                "timeGreaterThan" => "cit_timeGreaterThan",
                "timeLessEqual" => "cit_timeLessEqual",
                "timeLessThan" => "cit_timeLessThan",
                "unsigned_number" => "unsigned_number",
                "url" => "valid_url",
                "validate_editor" => "",
                "zip_code" => "zip_code",
                // Extra support for Enum/Yesno/Status configured kind of fields provided with fixed set of values
                "contains" => "contains_list",
            );
            $rule_valid_msg_labels = array(
                "cit_nowhitespace" => 'GENERIC_SERVER_VALIDATE_NOWHITESPACE',
                "cit_alpha_numeric_compulsory" => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_COMPULSORY',
                "cit_alpha_numeric_special_compulsory" => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_SPECIAL_COMPULSORY',
                "cit_alpha_numeric_capital_compulsory" => 'GENERIC_SERVER_VALIDATE_ALPHA_NUMERIC_CAPITAL_COMPULSORY',
                "cit_date" => 'GENERIC_SERVER_VALIDATE_DATE',
                "cit_datetime" => 'GENERIC_SERVER_VALIDATE_DATETIME',
                "cit_dateEqualTo" => 'GENERIC_SERVER_VALIDATE_DATEEQUALTO',
                "cit_dateGreaterEqual" => 'GENERIC_SERVER_VALIDATE_DATEGREATEREQUAL',
                "cit_dateGreaterThan" => 'GENERIC_SERVER_VALIDATE_DATEGREATERTHAN',
                "cit_dateLessEqual" => 'GENERIC_SERVER_VALIDATE_DATELESSEQUAL',
                "cit_dateLessThan" => 'GENERIC_SERVER_VALIDATE_DATELESSTHAN',
                "cit_numEqualTo" => 'GENERIC_SERVER_VALIDATE_NUMEQUALTO',
                "cit_numGreaterEqual" => 'GENERIC_SERVER_VALIDATE_NUMGREATEREQUAL',
                "cit_numGreaterThan" => 'GENERIC_SERVER_VALIDATE_NUMGREATERTHAN',
                "cit_numLessEqual" => 'GENERIC_SERVER_VALIDATE_NUMLESSEQUAL',
                "cit_numLessThan" => 'GENERIC_SERVER_VALIDATE_NUMLESSTHAN',
                "cit_range" => 'GENERIC_SERVER_VALIDATE_RANGE',
                "cit_time" => 'GENERIC_SERVER_VALIDATE_TIME',
                "cit_timeEqualTo" => 'GENERIC_SERVER_VALIDATE_TIMEEQUALTO',
                "cit_timeGreaterEqual" => 'GENERIC_SERVER_VALIDATE_TIMEGREATEREQUAL',
                "cit_timeGreaterThan" => 'GENERIC_SERVER_VALIDATE_TIMEGREATERTHAN',
                "cit_timeLessEqual" => 'GENERIC_SERVER_VALIDATE_TIMELESSEQUAL',
                "cit_timeLessThan" => 'GENERIC_SERVER_VALIDATE_TIMELESSTHAN',
                "cit_file_ext" => 'GENERIC_VALIDATION_FILE_TYPE_IS_NOT_ACCEPTABLE',
            );

            $ret_arr = array();
            $ret_arr["success"] = 1;
            $ret_arr["errors"] = array();
            $ret_arr["data"] = $form_data;

            if (is_array($form_config) && count($form_config) > 0) {
                $form_valid_names = $form_valid_rules = $form_valid_msgs = array();
                foreach ($form_config as $fcg_key => $fcg_val) {
                    $rule_arr = $msg_arr = array();
                    if (is_array($fcg_val["vrules"]) && count($fcg_val["vrules"]) > 0) {
                        foreach ($fcg_val["vrules"] as $vkey => $vval) {
                            $rule_key = trim($rule_mapper[$vkey]);
                            if ($rule_key == "") {
                                continue;
                            }
                            if (is_array($vval) && count($vval) > 0) {
                                $rule_arr[$rule_key] = $vval; // rule with params
                            } else {
                                $rule_arr[] = $rule_key; // rule without params
                            }

                            if (isset($fcg_val["vmessages"][$vkey])) {
                                $msg_arr[$rule_key] = str_replace("#FIELD#", $fcg_val["label_lang"], $fcg_val["vmessages"][$vkey]);
                            }
                        }
                    }
                    if (is_array($rule_arr) && count($rule_arr) > 0) {
                        $form_valid_names[$fcg_key] = $fcg_val["label_lang"];
                        $form_valid_rules[$fcg_key] = $rule_arr;
                    }
                    if (is_array($msg_arr) && count($msg_arr) > 0) {
                        $form_valid_msgs[$fcg_key] = $msg_arr;
                    }
                }

                if (is_array($form_valid_rules) && count($form_valid_rules) > 0) {
                    require_once($this->CI->config->item('third_party') . 'validator_gump/Validator.php');
                    $gump = new CITValidator();
                    $gump->validation_rules($form_valid_rules);
                    $gump->set_field_names($form_valid_names);
                    //Field wise messages
                    if (is_array($rule_valid_msg_labels) && count($rule_valid_msg_labels) > 0) {
                        foreach ($rule_valid_msg_labels as $rlk => $rlv) {
                            $rule_valid_msg_labels[$rlk] = $this->CI->general->processMessageLabel($rlv);
                        }
                        $gump->set_error_messages($rule_valid_msg_labels);
                    }
                    if (is_array($form_valid_msgs) && count($form_valid_msgs) > 0) {
                        $gump->set_fields_error_messages($form_valid_msgs);
                    }
                    $valid_data = $gump->run($form_data);
                    if ($gump->errors()) {
                        $ret_arr["success"] = 0;
                        $ret_arr["errors"] = $gump->get_errors_array();
                    } else {
                        $ret_arr["data"] = $valid_data;
                    }
                }
            }
        } catch (Exception $e) {
            $ret_arr["success"] = 0;
            $ret_arr["errors"] = array($e->getMessage());
        }
        return $ret_arr;
    }

    public function validateFormPostData($module_name = '', $form_config = array(), $form_data = array(), $mode = '')
    {
        $ret_arr = $this->validateThroughGUMP($module_name, $form_config, $form_data, $mode);
        return $ret_arr;
    }
}

/* End of file Cit_Form_validation.php */
/* Location: ./application/libraries/Cit_Form_validation.php */