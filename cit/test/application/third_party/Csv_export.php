<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of CSV Export Library
 *
 * @category third_party
 * 
 * @package third_party
 *
 * @module CSVExport
 * 
 * @class Csv_export.php
 * 
 * @path application\third_party\Csv_export.php
 * 
 * @version 4.0
 * 
 * @author CIT Dev Team | Jon Gales
 * 
 * @since 28.12.2019
 * 
 */
class CSV_Writer
{

    public $module_name;
    public $ctrl_object;
    public $content_info;
    public $deliminator;
    public $data;
    public $name;
    protected $CI;

    /**
     * Loads data and optionally a deliminator. Data is assumed to be an array.
     *
     * @param array $data
     * 
     * @param string $deliminator
     * 
     * @return void
     */
    public function __construct($data = array(), $deliminator = ",")
    {
        if (!is_array($data)) {
            throw new Exception('CSV_Writer only accepts data as array');
        }
        $this->CI = & get_instance();
        $this->data = $data;
        $this->deliminator = $deliminator;
    }

    /**
     * Module Name to be set.
     *
     * @return void
     */
    public function setModule($module = '')
    {
        $this->module_name = $module;
    }

    /**
     * Module Content to be set.
     *
     * @return void
     */
    public function setContent($content = array())
    {
        $this->content_info = $content;
    }
    
    /**
     * Controller Object to be set.
     *
     * @return void
     */
    public function setController($ctrl_obj = '')
    {
        $this->ctrl_object = $ctrl_obj;
    }
    
    /**
     * Wrap the text.
     *
     * @param string $path
     * 
     * @return string
     */
    private function wrapWithQuotes($data = '')
    {
        $data = preg_replace('/"(.+)"/', '""$1""', $data);
        return sprintf('"%s"', $data);
    }
    

    /**
     * Output to be rendered here.
     *
     * @return void
     */
    public function output($headers = array(), $data = array(), $widths = array(), $aligns = array())
    {
        //$output = chr(239) . chr(187) . chr(191);
        if (is_object($this->ctrl_object) && method_exists($this->ctrl_object, "citCSVWriteTableCallback")) {
            $callback_res = $this->ctrl_object->citCSVWriteTableCallback($this, $headers, $data, $widths, $aligns);
            if ($callback_res == 2) {
                return;
            }
            $this->data = array_merge(array($headers), $data);
        }
        if (method_exists($this->CI->general, "citCSVWriteTableCallback")) {
            $callback_res = $this->CI->general->citCSVWriteTableCallback($this, $headers, $data, $widths, $aligns);
            if ($callback_res == 2) {
                return;
            }
            $this->data = array_merge(array($headers), $data);
        }
        
        $file_name = $this->name;
        $file_name = str_replace(" ", "-", $file_name);
        $file_name = preg_replace('/[^A-Za-z0-9@.-_]/', '', $file_name);
        $file_name = $file_name . '-' . time() . '.csv';
        $temp_path = $this->CI->config->item('admin_upload_temp_path');
        if (!is_dir($temp_path)) {
            mkdir($temp_path, 0777);
            chmod($temp_path, 0777);
        }
        $file_path = $temp_path . $file_name;

        $output_data = '';
        foreach ($this->data as $row) {
            $quoted_data = array_map(array('CSV_Writer', 'wrapWithQuotes'), $row);
            $output_data .= sprintf("%s\n", implode($this->deliminator, $quoted_data));
        }
        
        $fp = fopen($file_path, 'w+');
        fwrite($fp, $output_data);
        fclose($fp);

        $this->download($file_path);
        unlink($file_path);
    }

    /**
     * Download the CSV file
     *
     * @param string $path
     * 
     * @return void
     */
    public function download($path = '')
    {
        $mimetype = get_mime_by_extension($path);
        if (ob_get_length() > 0) {
            ob_end_clean();
        }
        ob_start();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Cache-Control: private", FALSE);
        header('Content-Disposition: attachment; filename=' . $this->name . '.csv');
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($path));
        if ($mimetype) {
            header("Content-Type: " . $mimetype);
        }
        flush();
        readfile($path);
    }

    /**
     * Sets proper attachment for the CSV output.
     *
     * @param string $name
     * 
     * @return void
     */
    public function headers($name)
    {
        $this->name = $name;
//        header('Content-Type: application/csv; charset=utf-8');
//        header("Content-disposition: attachment; filename={$name}.csv");
    }
}
