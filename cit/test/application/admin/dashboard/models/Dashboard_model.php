<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Dashboard Model
 *
 * @category admin
 *
 * @package dashboard
 *
 * @subpackage models
 *
 * @module Dashboard
 *
 * @class Dashboard_model.php
 *
 * @path application\admin\dashboard\models\Dashboard_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 08.04.2022
 */

class Dashboard_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $rec_per_page;

    /**
     * __construct method is used to set model preferences while model object initialization.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->table_name = "mod_admin_dashboard";
        $this->table_alias = "mad";
        $this->primary_key = "iDashBoardId";
        $this->primary_alias = "mad_dash_board_id";
        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '')
    {
        if (is_numeric($where))
        {
            $this->db->where($this->primary_key, $where);
        }
        else
        {
            $this->db->where($where, FALSE, FALSE);
        }
        return $this->db->update($this->table_name, $data);
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "")
    {
        if (is_numeric($where))
        {
            $this->db->where($this->primary_key, $where);
        }
        else
        {
            $this->db->where($where, FALSE, FALSE);
        }
        return $this->db->delete($this->table_name);
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No")
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        else
        {
            $this->db->select($this->table_alias.".*");
        }
        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif ($extra_cond != "")
        {
            if (is_numeric($extra_cond))
            {
                $this->db->where($this->table_alias.".".$this->primary_key, $extra_cond);
            }
            else
            {
                $this->db->where($extra_cond, FALSE, FALSE);
            }
        }
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $list_data_obj = $this->db->get();
        return (is_object($list_data_obj) ? $list_data_obj->result_array() :
        array()
    );
}

/**
     * product_list method is used to return data related product_list dashboard block
     * @created Mdseraj Khan | 08.04.2022
     * @modified Mdseraj Khan | 08.04.2022
     * @param array $search_filters settings_params are used for filter data.
     * @return array $assoc_data returns response of dashboard block.
     */
public function product_list($search_filters = array(), $params_arr = array())
{
    $assoc_data = array();

    $mod_config = array();
    $col_config = array(
        "p_product_name" => array(
            "name" => "p_product_name",
            "label" => $this->lang->line('DASHBOARD_P_PRODUCT_NAME'),
            "display_query" => "ws.p_product_name",
            "show_in" => "Grid",
        ),
        "c_category_name" => array(
            "name" => "c_category_name",
            "label" => $this->lang->line('DASHBOARD_C_CATEGORY_NAME'),
            "display_query" => "ws.c_category_name",
            "show_in" => "Grid",
        ),
        "b_brand" => array(
            "name" => "b_brand",
            "label" => $this->lang->line('DASHBOARD_B_BRAND'),
            "display_query" => "ws.b_brand",
            "show_in" => "Grid",
        ),
        "p_price" => array(
            "name" => "p_price",
            "label" => $this->lang->line('DASHBOARD_P_PRICE'),
            "display_query" => "ws.p_price",
            "show_in" => "Grid",
        ),
        "p_retail_price" => array(
            "name" => "p_retail_price",
            "label" => $this->lang->line('DASHBOARD_P_RETAIL_PRICE'),
            "display_query" => "ws.p_retail_price",
            "show_in" => "Grid",
        ),
        "pi_image" => array(
            "name" => "pi_image",
            "label" => $this->lang->line('DASHBOARD_PI_IMAGE'),
            "display_query" => "ws.pi_image",
            "php_func" => "getImage",
            "show_in" => "Grid",
        )
    );
    $col_names = array(
        array(
            "name" => "p_product_name",
            "label" => $this->lang->line('DASHBOARD_P_PRODUCT_NAME')
        ),
        array(
            "name" => "c_category_name",
            "label" => $this->lang->line('DASHBOARD_C_CATEGORY_NAME')
        ),
        array(
            "name" => "b_brand",
            "label" => $this->lang->line('DASHBOARD_B_BRAND')
        ),
        array(
            "name" => "p_price",
            "label" => $this->lang->line('DASHBOARD_P_PRICE')
        ),
        array(
            "name" => "p_retail_price",
            "label" => $this->lang->line('DASHBOARD_P_RETAIL_PRICE')
        ),
        array(
            "name" => "pi_image",
            "label" => $this->lang->line('DASHBOARD_PI_IMAGE')
        )
    );
    $col_model = array(
        array(
            "name" => "p_product_name",
            "index" => "p_product_name",
            "label" => $this->lang->line('DASHBOARD_P_PRODUCT_NAME'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "c_category_name",
            "index" => "c_category_name",
            "label" => $this->lang->line('DASHBOARD_C_CATEGORY_NAME'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "b_brand",
            "index" => "b_brand",
            "label" => $this->lang->line('DASHBOARD_B_BRAND'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "p_price",
            "index" => "p_price",
            "label" => $this->lang->line('DASHBOARD_P_PRICE'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "p_retail_price",
            "index" => "p_retail_price",
            "label" => $this->lang->line('DASHBOARD_P_RETAIL_PRICE'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "pi_image",
            "index" => "pi_image",
            "label" => $this->lang->line('DASHBOARD_PI_IMAGE'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        )
    );

    $data_model = $link_model = $data_rec = array();

    $api_data = $this->listing->getDashboardAPIData("get_product_list", $params_arr);
    $data_rec = $api_data["data"];
    if (is_array($data_rec) && count($data_rec) > 0)
    {
        foreach ((array) $data_rec as $m => $n)
        {
            $temp_rec = array();
            $data_arr = $data_rec[$m];
            $id = (is_array($data_arr) && array_key_exists("unique_id", $data_arr)) ? $data_arr["unique_id"] : $m;
            $temp_rec["id"] = $id;
            foreach ((array) $col_model as $i => $j)
            {
                $name = $col_model[$i]["name"];
                $value = $data_arr[$name];
                list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid", $m);
                $temp_rec[$name] = $parse_data;
                if ($col_config[$name]["edit_link"] == "Yes")
                {
                    $link_model[$id][$name] = $final_data;
                }
            }
            $data_model[$m] = $temp_rec;
        }
    }
    $assoc_data["colNames"] = $col_names;
    $assoc_data["colModel"] = $col_model;
    $assoc_data["dataModel"] = $data_model;
    $assoc_data["linkModel"] = $link_model;
    $assoc_data["hidePaging"] = "No";
    $assoc_data["rowNumber"] = "5";
    $assoc_data["frozenCols"] = "No";
    $assoc_data["searchMode"] = "Yes";
    $assoc_data["autoUpdate"] = "Yes";
    $assoc_data["dateFilter"] = "";
    $assoc_data["filterField"] = "";
    $assoc_data["filterValue"] = "";
    $assoc_data["dbID"] = "1";
    $assoc_data["gridID"] = "dblist2_1";
    $assoc_data["pagerID"] = "dbpager2_1";

    return $assoc_data;
}

/**
     * user_details method is used to return data related user_details dashboard block
     * @created Mdseraj Khan | 08.04.2022
     * @modified Mdseraj Khan | 08.04.2022
     * @param array $search_filters settings_params are used for filter data.
     * @return array $assoc_data returns response of dashboard block.
     */
public function user_details($search_filters = array(), $params_arr = array())
{
    $assoc_data = array();

    $mod_config = array();
    $col_config = array(
        "tb_vName" => array(
            "name" => "tb_vName",
            "label" => $this->lang->line('DASHBOARD_NAME'),
            "display_query" => "tb.vName",
            "show_in" => "Grid",
        ),
        "tb_vUserName" => array(
            "name" => "tb_vUserName",
            "label" => $this->lang->line('DASHBOARD_USER_NAME'),
            "display_query" => "tb.vUserName",
            "show_in" => "Grid",
        ),
        "tb_vEmail" => array(
            "name" => "tb_vEmail",
            "label" => $this->lang->line('DASHBOARD_EMAIL'),
            "display_query" => "tb.vEmail",
            "show_in" => "Grid",
        ),
        "tb_vPhonenumber" => array(
            "name" => "tb_vPhonenumber",
            "label" => $this->lang->line('DASHBOARD_PHONENUMBER'),
            "display_query" => "tb.vPhonenumber",
            "show_in" => "Grid",
        ),
        "tb_dLastAccess" => array(
            "name" => "tb_dLastAccess",
            "label" => $this->lang->line('DASHBOARD_LAST_ACCESS'),
            "display_query" => "tb.dLastAccess",
            "show_in" => "Grid",
        )
    );
    $col_names = array(
        array(
            "name" => "tb_vName",
            "label" => $this->lang->line('DASHBOARD_NAME')
        ),
        array(
            "name" => "tb_vUserName",
            "label" => $this->lang->line('DASHBOARD_USER_NAME')
        ),
        array(
            "name" => "tb_vEmail",
            "label" => $this->lang->line('DASHBOARD_EMAIL')
        ),
        array(
            "name" => "tb_vPhonenumber",
            "label" => $this->lang->line('DASHBOARD_PHONENUMBER')
        ),
        array(
            "name" => "tb_dLastAccess",
            "label" => $this->lang->line('DASHBOARD_LAST_ACCESS')
        )
    );
    $col_model = array(
        array(
            "name" => "tb_vName",
            "index" => "tb_vName",
            "label" => $this->lang->line('DASHBOARD_NAME'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "tb_vUserName",
            "index" => "tb_vUserName",
            "label" => $this->lang->line('DASHBOARD_USER_NAME'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "tb_vEmail",
            "index" => "tb_vEmail",
            "label" => $this->lang->line('DASHBOARD_EMAIL'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "tb_vPhonenumber",
            "index" => "tb_vPhonenumber",
            "label" => $this->lang->line('DASHBOARD_PHONENUMBER'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "text",
        ),
        array(
            "name" => "tb_dLastAccess",
            "index" => "tb_dLastAccess",
            "label" => $this->lang->line('DASHBOARD_LAST_ACCESS'),
            "labelClass" => "header-align-left",
            "resizable" => FALSE,
            "width" => "100",
            "search" => FALSE,
            "sortable" => TRUE,
            "align" => "left",
            "sorttype" => "date",
        )
    );

    $data_model = $link_model = $data_rec = array();

    $select_fields = array();
    $select_fields[] = array(
        "field" => "tb.iAdminId AS iAdminId",
    );
    $select_fields[] = array(
        "field" => "tb.vName AS tb_vName",
    );
    $select_fields[] = array(
        "field" => "tb.vUserName AS tb_vUserName",
    );
    $select_fields[] = array(
        "field" => "tb.vEmail AS tb_vEmail",
    );
    $select_fields[] = array(
        "field" => "tb.vPhonenumber AS tb_vPhonenumber",
    );
    $select_fields[] = array(
        "field" => "tb.dLastAccess AS tb_dLastAccess",
    );

    $this->listing->addSelectFields($select_fields);
    $this->db->from("mod_admin AS tb");
    $this->general->getPhysicalRecordWhere("mod_admin", "tb", "AR");
    $filter_sql = $this->filter->applyDashBoardFilter($search_filters, $col_config);
    if ($filter_sql != "")
    {
        $this->db->where("(".$filter_sql.")", FALSE, FALSE);
    }

    $this->db->limit(5);
    $data_obj = $this->db->get();
    $data_rec = is_object($data_obj) ? $data_obj->result_array() : array();
    //echo $this->db->last_query();
    if (is_array($data_rec) && count($data_rec) > 0)
    {
        foreach ((array) $data_rec as $m => $n)
        {
            $temp_rec = array();
            $data_arr = $data_rec[$m];
            $id = $data_arr["iAdminId"];
            $temp_rec["id"] = $id;
            foreach ((array) $col_model as $i => $j)
            {
                $name = $col_model[$i]["name"];
                $value = $data_arr[$name];
                list($parse_data, $final_data) = $this->listing->formatDashboardData($value, $id, $data_arr, $col_config[$name], $mod_config, "Grid", $m);
                $temp_rec[$name] = $parse_data;
                if ($col_config[$name]["edit_link"] == "Yes")
                {
                    $link_model[$id][$name] = $final_data;
                }
            }
            $data_model[$m] = $temp_rec;
        }
    }
    $assoc_data["colNames"] = $col_names;
    $assoc_data["colModel"] = $col_model;
    $assoc_data["dataModel"] = $data_model;
    $assoc_data["linkModel"] = $link_model;
    $assoc_data["hidePaging"] = "No";
    $assoc_data["rowNumber"] = "5";
    $assoc_data["frozenCols"] = "No";
    $assoc_data["searchMode"] = "No";
    $assoc_data["autoUpdate"] = "No";
    $assoc_data["dateFilter"] = "";
    $assoc_data["filterField"] = "";
    $assoc_data["filterValue"] = "";
    $assoc_data["dbID"] = "2";
    $assoc_data["gridID"] = "dblist2_2";
    $assoc_data["pagerID"] = "dbpager2_2";

    return $assoc_data;
}
}
