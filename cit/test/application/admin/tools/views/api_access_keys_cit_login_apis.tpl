<%assign var=api value=$data['maa_request_limit']|json_decode:1%>
<%assign var=params value=$api['attempts']['params']%>

<label class="form-label span3"></label>
<div class="form-right-div api-div">
    <table class="api-table" >
        <thead>
            <tr>
                <th width="45%">API Name</th>
                <th width="45%">API Parameter</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody id="clone_tbody">
            <%foreach name=params from=$params key=key item=val%>
                <%assign var=inc_no value=$smarty.foreach.params.iteration%>
                <tr id="clone_tr_<%$inc_no%>">
                    <td>
                        <select name="login_api[]" id="login_api_<%$inc_no%>" title="Login API" aria-chosen-valid="Yes" class="chosen-select frm-size-medium attempts-login-api" data-placeholder="Please select Login API" style="display: none;">
                            <option value="">Please Select API</option>
                            <%foreach from=$data['api_list'] key=k item=v%>
                                <%if $key eq $k%>
                                    <option value="<%$k%>" selected><%$v['name']%></option>
                                <%else%>
                                    <option value="<%$k%>"><%$v['name']%></option>
                                <%/if%>
                            <%/foreach%>
                        </select>
                    </td>
                    <td>
                        <select name="api_keys[]" id="api_keys_<%$inc_no%>" title="API Parameter" aria-chosen-valid="Yes" class="chosen-select frm-size-medium attempts-api-keys" data-placeholder="Please select Parameter" style="display: none;">
                            <option value="">Please Select Parameter</option>
                            <%foreach from=$data['api_list'][$key]['keys'] key=k1 item=v1%>
                                <%if $val eq $v1%>
                                    <option value="<%$v1%>" selected><%$v1%></option>
                                <%else%>
                                    <option value="<%$v1%>"><%$v1%></option>
                                <%/if%>
                            <%/foreach%>
                            
                        </select>
                    </td>
                    <td id="clone_td_<%$inc_no%>">
                        <%if $inc_no eq 1%>
                            <div class="btn attempts-api-clone"><span class="fa fa-plus"></span></div>
                        <%else%>
                            <div class="btn attempts-api-delete"><span class="fa fa-trash-o"></span></div>
                        <%/if%>
                    </td>
                </tr>
            <%/foreach%>
            
        </tbody>
    </table>
</div>

<%javascript%>
    var api_list = JSON.parse('<%$data['api_json']%>');
    var inc_no = $('#clone_tbody').find("tr").length;
    $(document).on('change', '.attempts-login-api', function()
    {
        var id_arr = this.id.split('_');
        var id = id_arr['2']
        var params = api_list[this.value];
        $('#api_keys_'+id).empty();
        $('#api_keys_'+id).append('<option value="0">Please Select Parameter</option>')
        jQuery.each( params['keys'], function( i, val ) 
        {
            $('#api_keys_'+id).append('<option value="'+val+'">'+val+'</option>')
        });
        $('#api_keys_'+id).trigger('chosen:updated');
    });
    $(document).on('click', '.attempts-api-clone', function() {
        var clon_obj = $("#clone_tr_1").clone(true);
        clon_obj.find('.chosen-container').remove();
        inc_no++;
        var new_tr_id = "clone_tr_" + parseInt(inc_no);
        clon_obj.attr('id', new_tr_id);
        
        clon_obj.find("select").each(function (index, domEle) {
            $(this).css('display', 'block');
            var new_id = $(this).attr('id').replace(/([0-9]{1,})/i, parseInt(inc_no));
            var new_name = $(this).attr('name').replace(/([0-9]{1,})/i, parseInt(inc_no));
            $(this).removeClass('chosen-done');
            $(this).data('chosen', null);
            $(this).attr('name', new_name);
            $(this).attr('id', new_id);
            $(this).val("");
        });
        
        clon_obj.find("#clone_td_1").each(function (index, domEle) {
            var new_id = $(this).attr('id').replace(/([0-9]{1,})/i, parseInt(inc_no));
            $(this).attr('id', new_id);
            var add = $(this).find(".attempts-api-clone");
            $(add).attr("class", "btn attempts-api-delete");
            var icon = $(this).find("span");
            $(icon).attr("class", "fa fa-trash-o")
        });
        
        $('#clone_tbody').append(clon_obj);
        initializejQueryChosenEvents($('#clone_tr_'+inc_no));
        
    });
      
    $(document).on('click', '.attempts-api-delete', function() {
        if(confirm("Are you sure want to delete?")){
            var tr_id = $(this).closest("tr").remove();
        }
    });
<%/javascript%>