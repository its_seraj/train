<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%$this->css->add_css('multiselect/jquery.multiselect.css')%>
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%>
<div class="headingfix">
    <div class="heading" id="top_heading_fix">
        <h3>
            <div class="screen-title">
                Listing :: API
                
            </div>
        </h3>
        <div class="header-right-drops">
            <div class="frm-back-to">
                <a hijacked="yes" href="<%$mod_enc_url['keys_list']%>" class="backlisting-link" title="Back To API Access Keys Listing">
                    <span class="icon16 minia-icon-arrow-left"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<div id="ajax_content_div" class="ajax-content-div top-frm-tab-spacing box gradient">
    <div id="ajax_qLoverlay"></div>
    <div id="ajax_qLbar"></div>
    
    <div id="scrollable_content" class="scrollable-content top-list-spacing">
        <div class="grid-data-container pad-calc-container">
            <div class="top-list-tab-layout" id="top_list_grid_layout">
                <!-- Module Tabs & Top Detail View -->
                <div class="top-list-tab-layout" id="top_list_grid_layout">
                    <div id="refer_tabs_height" class="module-navigation-tabs">
                        <%include file="api_access_keys_index_tabs.tpl" %>
                    </div>
                </div>
            </div>
            <div id="backup" style="margin-top:46px;">
                <form name="frmbackupsave" id="frmbackupsave" method="post" action="<%$admin_url%><%$mod_enc_url['backup_backup_form_a']%>?<%$extra_hstr%>" style="margin:0px!important">
                    <input type="hidden" name="btype" id="btype" value=""/>
                    <input type='hidden' name='id_arr' id='id_arr' value=""/>
                    <input type="hidden" name="type" id="type" value="<%$extra_hstr%>"/>
                    <input id="iAccesskeyId" type="hidden" value="<%$iAccesskeyId%>" />
                </form>
                <table class="grid-table-view top-list-pager-space" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td id="grid_data_col" class="<%$rl_theme_arr['grid_search_toolbar']%>">
                            <div id="pager2"></div>
                            <table id="list2"></table>
                        </td>
                    </tr>
                </table>
                <div id="apidialog" title="Select API">
                    <div>
                        <%if $message eq 0%>
                            <h2><%$this->lang->line('GENERIC_ALL_API_ARE_SELECTED')%></h2>
                        <%else%>
                            <select name="basic[]" multiple="multiple" class="3col active">
                                <%foreach from=$list key=optgroup item=value%>
                                    <optgroup label="<%ucfirst($optgroup)%>">
                                    <%foreach from=$value key=key item=val%>
                                        <option value="<%$val%>"><%$key%></option>
                                    <%/foreach%>
                                <%/foreach%>
                            </select>
                        <%/if%>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="selAllRows" value="" id="selAllRows">
<%javascript%>
    $.jgrid.no_legacy_api = true, $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_name_json = {}, js_col_model_json = {}, js_data_json = {};
    
    var js_api_add_url = '<%$mod_enc_url['api_add']%>';
    var js_api_list_url = '<%$mod_enc_url['api_list']%>';
    
    el_grid_settings['delete_url'] = admin_url+'<%$mod_enc_url['api_delete']%>?type=<%$extra_hstr%>';
    //el_grid_settings['save_url'] =  admin_url+'<%$mod_enc_url['api_list']%>';
    
    el_grid_settings['permit_del_btn'] = <%$del_access%>;
    el_grid_settings['permit_view_btn'] = '<%$view_access%>';
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';

    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['default_sort'] = 'api_name';
    el_grid_settings['sort_order'] = 'desc';
    
    js_col_name_json = [{
        "label": "API Name",
        "name": "api_name"
    }];
    
    var js_col_model_json = [{
        "name": "api_name",
        "index": "api_name",
        "label": "API Name",
        "labelClass": "header-align-left",
        "align": "left",
        "resizable": true,
        "width": null,
        "sortable": true,
        "search": true,
        "searchoptions": {
            "sopt": ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc', 'nu', 'nn']
	},
        "filterSopt":"bw"
    }];
    
    <%if $apilist%>
        js_data_json = <%$apilist%>;
    <%/if%>;
    
<%/javascript%>

<%$this->js->add_js('admin/admin/js_api_access_keys_list.js')%>
<%$this->js->add_js('admin/multiselect/jquery.multiselect.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%>

<%javascript%>
    Project.modules.api_access_keys.apiDialogBox();
    Project.modules.api_access_keys.showAPIListing();
    Project.modules.api_access_keys.multiSelectAPI();
    
<%/javascript%>