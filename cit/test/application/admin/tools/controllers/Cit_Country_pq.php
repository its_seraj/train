<?php


/**
 * Description of Country PQ Extended Controller
 * 
 * @module Extended Country PQ
 * 
 * @class Cit_Country_pq.php
 * 
 * @path application\admin\tools\controllers\Cit_Country_pq.php
 * 
 * @author CIT Dev Team
 * 
 * @date 06.04.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Country_pq extends Country_pq {
        public function __construct()
{
    parent::__construct();
}
public function before_load($render_arr){
    // $get;
    // $this->parser->parse("country_pq_index_strip.tpl", $get, true);
    // pr($get);exit;
    return $render_arr;
}
}
