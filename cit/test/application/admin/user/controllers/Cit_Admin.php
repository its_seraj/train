<?php


/**
 * Description of Admin Extended Controller
 * 
 * @module Extended Admin
 * 
 * @class Cit_Admin.php
 * 
 * @path application\admin\user\controllers\Cit_Admin.php
 * 
 * @author CIT Dev Team
 * 
 * @date 29.03.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Admin extends Admin {
        public function __construct()
{
    parent::__construct();
    #pr($this->config->item("admin_2fa_authentication"));exit;
}

public function beforeSubmit($mode = '',$id = '', $parID = ''){
    $_POST['ma_phone_code'] = str_replace($_POST['intl_ma_phonenumber'], '', $_POST['ma_phonenumber']);
    $_POST['ma_phonenumber'] = $_POST['intl_ma_phonenumber'];
    unset($_POST['intl_ma_phonenumber']);
    unset($_POST['sys_custom_field_1'] );
    unset($_POST['sys_custom_field_2'] );
    return array("success" => true);
}
}
