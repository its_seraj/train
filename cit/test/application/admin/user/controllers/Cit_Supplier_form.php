<?php


/**
 * Description of Supplier Form Extended Controller
 * 
 * @module Extended Supplier Form
 * 
 * @class Cit_Supplier_form.php
 * 
 * @path application\admin\user\controllers\Cit_Supplier_form.php
 * 
 * @author CIT Dev Team
 * 
 * @date 28.03.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Supplier_form extends Supplier_form {
        public function __construct()
{
    parent::__construct();
}
}
