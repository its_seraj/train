<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container">
    <%include file="supplier_form_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="supplier_form" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="supplier_form" class="frm-module-block frm-view-block frm-thclm-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('SUPPLIER_FORM_SUPPLIER_FORM')%></h4></div>
                                <div class="content two-column-block <%$rl_theme_arr['frm_twclm_label_align']%>">
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_sys_static_field_1"> <div class="form-right-div frm-elements-div form-static-div"><b>Company Information</b></div></div><div class="two-block-view " id="cc_sh_sys_static_field_2"> <div class="form-right-div frm-elements-div form-static-div"><b>Owner Information</b></div></div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_name"> 
                                            <label class="form-label span3"><%$form_config['ma_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_email"> 
                                            <label class="form-label span3"><%$form_config['ma_email']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_email']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_user_name"> 
                                            <label class="form-label span3"><%$form_config['ma_user_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_user_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_password"> 
                                            <label class="form-label span3"><%$form_config['ma_password']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_password']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_dial_code"> 
                                            <label class="form-label span3"><%$form_config['ma_dial_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_dial_code']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_phonenumber"> 
                                            <label class="form-label span3"><%$form_config['ma_phonenumber']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_phonenumber']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_group_id"> 
                                            <label class="form-label span3"><%$form_config['ma_group_id']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_group_id'], $opt_arr['ma_group_id'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_auth_type"> 
                                            <label class="form-label span3"><%$form_config['ma_auth_type']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_auth_type'], $opt_arr['ma_auth_type'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_auth_code"> 
                                            <label class="form-label span3"><%$form_config['ma_auth_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_auth_code']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_o_tpcode"> 
                                            <label class="form-label span3"><%$form_config['ma_o_tpcode']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_o_tpcode']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_email_verified"> 
                                            <label class="form-label span3"><%$form_config['ma_email_verified']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_email_verified'], $opt_arr['ma_email_verified'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_verification_code"> 
                                            <label class="form-label span3"><%$form_config['ma_verification_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['ma_verification_code']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_is_temporary_password"> 
                                            <label class="form-label span3"><%$form_config['ma_is_temporary_password']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_is_temporary_password'], $opt_arr['ma_is_temporary_password'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_password_expired_on"> 
                                            <label class="form-label span3"><%$form_config['ma_password_expired_on']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateDefinedFormat('Y-m-d',$data['ma_password_expired_on'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_login_failed_attempts"> 
                                            <label class="form-label span3"><%$form_config['ma_login_failed_attempts']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_login_failed_attempts'], $opt_arr['ma_login_failed_attempts'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_login_locked_until"> 
                                            <label class="form-label span3"><%$form_config['ma_login_locked_until']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateDefinedFormat('Y-m-d',$data['ma_login_locked_until'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_added_date"> 
                                            <label class="form-label span3"><%$form_config['ma_added_date']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateDefinedFormat('Y-m-d',$data['ma_added_date'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_modified_date"> 
                                            <label class="form-label span3"><%$form_config['ma_modified_date']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateDefinedFormat('Y-m-d',$data['ma_modified_date'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_last_access"> 
                                            <label class="form-label span3"><%$form_config['ma_last_access']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateDefinedFormat('Y-m-d',$data['ma_last_access'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_ma_status"> 
                                            <label class="form-label span3"><%$form_config['ma_status']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_status'], $opt_arr['ma_status'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view tab-focus-element">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
