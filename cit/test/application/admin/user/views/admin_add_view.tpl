<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container<%if $top_detail_view["exists"] eq "1"%> has-detail-view<%/if%>">
    <%include file="admin_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="admin" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
                <!-- Top Detail View Block -->
                <%if $top_detail_view["exists"] eq "1"%>
                    <%$top_detail_view["html"]%>
                <%/if%>
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="admin" class="frm-module-block frm-view-block frm-stand-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <input type="hidden" name="ma_auth_code" id="ma_auth_code" value="<%$data['ma_auth_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_last_access" id="ma_last_access" value="<%$this->general->dateTimeSystemFormat($data['ma_last_access'])%>"  class='ignore-valid '  aria-date-format='<%$this->general->getAdminJSFormats('date_and_time', 'dateFormat')%>'  aria-time-format='<%$this->general->getAdminJSFormats('date_and_time', 'timeFormat')%>'  aria-format-type='datetime' />
                    <input type="hidden" name="ma_phone_code" id="ma_phone_code" value="<%$data['ma_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_o_tpcode" id="ma_o_tpcode" value="<%$data['ma_o_tpcode']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_email_verified" id="ma_email_verified" value="<%$data['ma_email_verified']%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_verification_code" id="ma_verification_code" value="<%$data['ma_verification_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_is_temporary_password" id="ma_is_temporary_password" value="<%$data['ma_is_temporary_password']%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_password_expired_on" id="ma_password_expired_on" value="<%$this->general->dateDefinedFormat('',$data['ma_password_expired_on'])%>"  class='ignore-valid '  aria-format-type='date' />
                    <input type="hidden" name="ma_login_failed_attempts" id="ma_login_failed_attempts" value="<%$data['ma_login_failed_attempts']%>"  class='ignore-valid ' />
                    <input type="hidden" name="ma_login_locked_until" id="ma_login_locked_until" value="<%$this->general->dateDefinedFormat('',$data['ma_login_locked_until'])%>"  class='ignore-valid '  aria-format-type='date' />
                    <input type="hidden" name="ma_added_date" id="ma_added_date" value="<%$this->general->dateDefinedFormat('',$data['ma_added_date'])%>"  class='ignore-valid '  aria-format-type='date' />
                    <input type="hidden" name="ma_modified_date" id="ma_modified_date" value="<%$this->general->dateDefinedFormat('',$data['ma_modified_date'])%>"  class='ignore-valid '  aria-format-type='date' />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('ADMIN_ADMIN')%></h4></div>
                                <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                    <div class="form-row row-fluid " id="cc_sh_ma_name">
                                        <label class="form-label span3">
                                            <%$form_config['ma_name']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span id="cc_md_ma_name">
                                                <span class="frm-data-label"><strong><%$data['ma_name']%></strong></span>
                                            </span>
                                            <span id="cc_md_sys_custom_field_1">
                                                <span class="frm-data-label"><strong><%$data['sys_custom_field_1']%></strong></span>
                                            </span>
                                            <span id="cc_md_sys_custom_field_2">
                                                <span class="frm-data-label"><strong><%$data['sys_custom_field_2']%></strong></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_ma_email">
                                        <label class="form-label span3">
                                            <%$form_config['ma_email']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['ma_email']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_ma_user_name">
                                        <label class="form-label span3">
                                            <%$form_config['ma_user_name']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['ma_user_name']%></strong></span>
                                        </div>
                                    </div>
                                    <%assign var="cb_ma_password" value=$func["ma_password"]%>
                                    <%if $cb_ma_password neq 0%>
                                        <div class="form-row row-fluid " id="cc_sh_ma_password">
                                            <label class="form-label span3">
                                                <%$form_config['ma_password']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div frm-elements-div  <%if cb_ma_password eq 2%>frm-elements-div<%/if%>">
                                                *****
                                            </div>
                                        </div>
                                    <%/if%>
                                    <%assign var="cb_ma_auth_type" value=$func["ma_auth_type"]%>
                                    <%if $cb_ma_auth_type neq 0%>
                                        <div class="form-row row-fluid " id="cc_sh_ma_auth_type">
                                            <label class="form-label span3">
                                                <%$form_config['ma_auth_type']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div frm-elements-div frm-elements-div">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData(explode(",",$data['ma_auth_type']), $opt_arr['ma_auth_type'])%></strong></span>
                                            </div>
                                        </div>
                                    <%/if%>
                                    <div class="form-row row-fluid " id="cc_sh_ma_phonenumber">
                                        <label class="form-label span3">
                                            <%$form_config['ma_phonenumber']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['ma_phonenumber']%></strong></span>
                                        </div>
                                    </div>
                                    <%assign var="cb_ma_group_id" value=$func["ma_group_id"]%>
                                    <%if $cb_ma_group_id neq 0%>
                                        <div class="form-row row-fluid " id="cc_sh_ma_group_id">
                                            <label class="form-label span3">
                                                <%$form_config['ma_group_id']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div frm-elements-div  <%if cb_ma_group_id eq 2%>frm-elements-div<%/if%>">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_group_id'], $opt_arr['ma_group_id'])%></strong></span>
                                            </div>
                                        </div>
                                    <%/if%>
                                    <div class="form-row row-fluid " id="cc_sh_ma_status">
                                        <label class="form-label span3">
                                            <%$form_config['ma_status']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['ma_status'], $opt_arr['ma_status'])%></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%$this->js->add_js("admin/custom/extended_admin.js")%>
<%$this->css->add_css("custom/extended.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
