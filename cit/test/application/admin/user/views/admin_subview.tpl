<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {
        "gv_ma_name": {
            "htmlID": "gv_ma_name",
            "name": "ma_name",
            "label": "Name",
            "label_lang": "<%$this->lang->line('ADMIN_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['ma_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_name']['value']%>",
            "dbval": "<%$list_config['ma_name']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": "<%$this->lang->line('GENERIC_FIRST_NAME')%>"
            },
            "extra_class": ""
        },
        "gv_ma_email": {
            "htmlID": "gv_ma_email",
            "name": "ma_email",
            "label": "Email",
            "label_lang": "<%$this->lang->line('ADMIN_EMAIL')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['ma_email']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_email']['value']%>",
            "dbval": "<%$list_config['ma_email']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "gv_ma_phonenumber": {
            "htmlID": "gv_ma_phonenumber",
            "name": "ma_phonenumber",
            "label": "Phonenumber",
            "label_lang": "<%$this->lang->line('ADMIN_PHONENUMBER')%>",
            "type": "phone_number",
            "ctrl_type": "phone_number",
            "editable": <%if $list_config['ma_phonenumber']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_phonenumber']['value']%>",
            "dbval": "<%$list_config['ma_phonenumber']['dbval']%>",
            "editoptions": {
                "format": "(999) 999-9999"
            }
        },
        "gv_ma_user_name": {
            "htmlID": "gv_ma_user_name",
            "name": "ma_user_name",
            "label": "User Name",
            "label_lang": "<%$this->lang->line('ADMIN_USER_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['ma_user_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_user_name']['value']%>",
            "dbval": "<%$list_config['ma_user_name']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "gv_mgm_group_name": {
            "htmlID": "gv_mgm_group_name",
            "name": "mgm_group_name",
            "label": "Group Name",
            "label_lang": "<%$this->lang->line('ADMIN_GROUP_NAME')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['mgm_group_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['mgm_group_name']['value']%>",
            "dbval": "<%$list_config['mgm_group_name']['dbval']%>",
            "editoptions": {
                "data_placeholder": null
            }
        },
        "gv_ma_last_access": {
            "htmlID": "gv_ma_last_access",
            "name": "ma_last_access",
            "label": "Last Access",
            "label_lang": "<%$this->lang->line('ADMIN_LAST_ACCESS')%>",
            "type": "date_and_time",
            "ctrl_type": "date_and_time",
            "editable": <%if $list_config['ma_last_access']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_last_access']['value']%>",
            "dbval": "<%$list_config['ma_last_access']['dbval']%>",
            "editoptions": {
                "dateFormat": "<%$this->general->getAdminJSFormats('date_and_time', 'dateFormat')%>",
                "timeFormat": "<%$this->general->getAdminJSFormats('date_and_time', 'timeFormat')%>",
                "showSecond": "<%$this->general->getAdminJSFormats('date_and_time', 'showSecond')%>",
                "ampm": "<%$this->general->getAdminJSFormats('date_and_time', 'ampm')%>",
                "minDate": "",
                "maxDate": "",
                "placeholder": ""
            }
        },
        "gv_login_as_btn": {
            "htmlID": "gv_login_as_btn",
            "name": "login_as_btn",
            "label": "Login As",
            "label_lang": "<%$this->lang->line('ADMIN_LOGIN_AS')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['login_as_btn']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['login_as_btn']['value']%>",
            "dbval": "<%$list_config['login_as_btn']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "extra_class": ""
        },
        "gv_ma_status": {
            "htmlID": "gv_ma_status",
            "name": "ma_status",
            "label": "Status",
            "label_lang": "<%$this->lang->line('ADMIN_STATUS')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['ma_status']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_status']['value']%>",
            "dbval": "<%$list_config['ma_status']['dbval']%>",
            "editoptions": {
                "data_placeholder": null
            }
        },
        "gv_extra_fields": {
            "htmlID": "gv_extra_fields",
            "name": "extra_fields",
            "label": "Extra Fields",
            "label_lang": "<%$this->lang->line('ADMIN_EXTRA_FIELDS')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['extra_fields']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['extra_fields']['value']%>",
            "dbval": "<%$list_config['extra_fields']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "extra_class": ""
        },
        "gv_ma_password": {
            "htmlID": "gv_ma_password",
            "name": "ma_password",
            "label": "Password",
            "label_lang": "<%$this->lang->line('ADMIN_PASSWORD')%>",
            "type": "password",
            "ctrl_type": "password",
            "editable": <%if $list_config['ma_password']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['ma_password']['value']%>",
            "dbval": "<%$list_config['ma_password']['dbval']%>",
            "editoptions": {
                "placeholder": ""
            }
        }
    };
    
    initSubGridDetailView();
<%/javascript%>
    

<div class="expand-detail-view">                        
    <table id="<%$subgrid_view_id%>" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td width="12%"><strong><%$this->lang->line('ADMIN_NAME')%>: </strong></td>
            <td width="20%"><%$data['ma_name']%></td>
            <td width="12%"><strong><%$this->lang->line('ADMIN_EMAIL')%>: </strong></td>
            <td width="20%"><%$data['ma_email']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('ADMIN_PHONENUMBER')%>: </strong></td>
            <td width="20%"><%$data['ma_phonenumber']%></td>
            <td width="12%"><strong><%$this->lang->line('ADMIN_USER_NAME')%>: </strong></td>
            <td width="20%"><%$data['ma_user_name']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('ADMIN_GROUP_NAME')%>: </strong></td>
            <td width="20%"><%$data['mgm_group_name']%></td>
            <td width="12%"><strong><%$this->lang->line('ADMIN_LAST_ACCESS')%>: </strong></td>
            <td width="20%"><%$data['ma_last_access']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('ADMIN_LOGIN_AS')%>: </strong></td>
            <td width="20%"><%$data['login_as_btn']%></td>
            <td width="12%"><strong><%$this->lang->line('ADMIN_STATUS')%>: </strong></td>
            <td width="20%"><%$data['ma_status']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('ADMIN_EXTRA_FIELDS')%>: </strong></td>
            <td width="20%"><%$data['extra_fields']%></td>
            <td width="12%"><strong><%$this->lang->line('ADMIN_PASSWORD')%>: </strong></td>
            <td width="20%"><%$data['ma_password']%></td>
        </tr>
         
    </table>
</div>