<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>    
<%$this->js->add_js('admin/admin/js_verify_password.js')%>

<div id="ajax_qLbar"></div>
<div class="popup-frm-setup">
    <div class="popup-left-block" id="popup-left-block">
        <!-- Top Detail View Block -->
        <div class="popup-left-container">
            <div class="popup-left-main-title">
                <div class="popup-left-main-title-icon"><span><i class="<%$icon%>" style="font-size:80px;color: #ffffff;"></i></span></div>
                <span class="popup-left-main-title-content">Unlock Data</span>
            </div>
            <div class="popup-label-box">
                <div class="label-box-row">
                    <span class="label-box-row-title"></span>
                    <em class="label-box-row-content"></em>
                </div>
                <div class="label-box-row">
                    <span class="label-box-row-title"></span>
                    <em class="label-box-row-content"></em>
                </div>
            </div>
        </div>
    </div>   
    <div id="ajax_content_div" class="ajax-content-div top-frm-spacing popup-right-block">
        <div id="ajax_qLoverlay"></div>
        <div id="ajax_qLbar"></div>
        <div id="scrollable_content" class="scrollable-content">
            <div id="verifypassword" class="frm-elem-block frm-stand-view">
                <form name="frmverifyprotected" id="frmverifyprotected" action="<%$action_url%>" method="post"  enctype="multipart/form-data">
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>">
                    <input type="hidden" id="patternLock" name="patternLock" value="<%$is_patternlock%>" > 
                    <input type="hidden" name="<%$csrf['name']%>" value="<%$csrf['hash']%>" />
                    <div class="main-content-block" id="main_content_block">
                        <div style="width:98%" class="frm-block-layout" >
                            <div class="box gradient <%$rl_theme_arr['frm_gener_content_row']%> <%$rl_theme_arr['frm_gener_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_gener_titles_bar']%>"><h4><%$this->lang->line('GENERIC_CHANGE_PASSWORD')%></h4></div>
                                <div class="content <%$rl_theme_arr['frm_gener_label_align']%>">
                                    <div class="form-row row-fluid">
                                        <label class="form-label span3"><%$this->lang->line('GENERIC_PASSWORD')%>  <em>*</em></label> 
                                        <div class="form-right-div">
                                            <input type="password" title="<%$this->lang->line('GENERIC_LOGIN_PASSWORD_ERR')%>" id="vPassword" value="" name="vPassword" class="frm-size-medium" autocomplete="off">
                                        </div>
                                        <div class="error-msg-form" ><label class="error" id="vPasswordErr"></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="popup-right-footer frm-bot-btn <%$rl_theme_arr['frm_gener_action_bar']%> <%$rl_theme_arr['frm_gener_action_btn']%>">
                            <div class="action-btn-align">
                                <input value="Submit" name="ctrladd" type="submit" class='btn btn-info' />&nbsp;&nbsp; 
                                <input value="<%$this->lang->line('GENERIC_DISCARD')%>" name="ctrldiscard" type="button" class='btn' onclick="closeWindow()">
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<%javascript%>
    var frm_key_settings = JSON.parse('<%$frm_enc_info%>');
    var is_ajax_submit = 1;
    
    var verify_pwd_url = '<%$action_url%>';
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 