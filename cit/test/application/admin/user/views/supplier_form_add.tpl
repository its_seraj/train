<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="supplier_form_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="supplier_form" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="supplier_form" class="frm-module-block frm-elem-block frm-thclm-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('SUPPLIER_FORM_SUPPLIER_FORM')%></h4></div>
                                    <div class="content two-column-block tab-focus-parent <%$rl_theme_arr['frm_twclm_label_align']%>">
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_1"> <div class="form-right-div form-static-div"><b>Company Information</b></div></div><div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_2"> <div class="form-right-div form-static-div"><b>Owner Information</b></div></div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_name"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_name']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_name']|@htmlentities%>" name="ma_name" id="ma_name" title="<%$this->lang->line('SUPPLIER_FORM_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_nameErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_email"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_email']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_email']|@htmlentities%>" name="ma_email" id="ma_email" title="<%$this->lang->line('SUPPLIER_FORM_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_emailErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_user_name"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_user_name']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_user_name']|@htmlentities%>" name="ma_user_name" id="ma_user_name" title="<%$this->lang->line('SUPPLIER_FORM_USER_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_user_nameErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_password"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_password']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_password']|@htmlentities%>" name="ma_password" id="ma_password" title="<%$this->lang->line('SUPPLIER_FORM_PASSWORD')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_passwordErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_dial_code"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_dial_code']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_dial_code']|@htmlentities%>" name="ma_dial_code" id="ma_dial_code" title="<%$this->lang->line('SUPPLIER_FORM_DIAL_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_dial_codeErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_phonenumber"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_phonenumber']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_phonenumber']|@htmlentities%>" name="ma_phonenumber" id="ma_phonenumber" title="<%$this->lang->line('SUPPLIER_FORM_PHONENUMBER')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_phonenumberErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_group_id"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_group_id']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_group_id']%>
                                                    <%$this->dropdown->display("ma_group_id","ma_group_id","  title='<%$this->lang->line('SUPPLIER_FORM_GROUP_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_GROUP_ID')%>'  ", "|||", "", $opt_selected,"ma_group_id")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_group_idErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_auth_type"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_auth_type']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_auth_type']%>
                                                    <%$this->dropdown->display("ma_auth_type","ma_auth_type","  title='<%$this->lang->line('SUPPLIER_FORM_AUTH_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_AUTH_TYPE')%>'  ", "|||", "", $opt_selected,"ma_auth_type")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_auth_typeErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_auth_code"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_auth_code']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_auth_code']|@htmlentities%>" name="ma_auth_code" id="ma_auth_code" title="<%$this->lang->line('SUPPLIER_FORM_AUTH_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_auth_codeErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_o_tpcode"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_o_tpcode']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_o_tpcode']|@htmlentities%>" name="ma_o_tpcode" id="ma_o_tpcode" title="<%$this->lang->line('SUPPLIER_FORM_O_TPCODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_o_tpcodeErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_email_verified"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_email_verified']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_email_verified']%>
                                                    <%$this->dropdown->display("ma_email_verified","ma_email_verified","  title='<%$this->lang->line('SUPPLIER_FORM_EMAIL_VERIFIED')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_EMAIL_VERIFIED')%>'  ", "|||", "", $opt_selected,"ma_email_verified")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_email_verifiedErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_verification_code"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_verification_code']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['ma_verification_code']|@htmlentities%>" name="ma_verification_code" id="ma_verification_code" title="<%$this->lang->line('SUPPLIER_FORM_VERIFICATION_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_verification_codeErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_is_temporary_password"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_is_temporary_password']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_is_temporary_password']%>
                                                    <%$this->dropdown->display("ma_is_temporary_password","ma_is_temporary_password","  title='<%$this->lang->line('SUPPLIER_FORM_IS_TEMPORARY_PASSWORD')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_IS_TEMPORARY_PASSWORD')%>'  ", "|||", "", $opt_selected,"ma_is_temporary_password")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_is_temporary_passwordErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_password_expired_on"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_password_expired_on']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  input-append text-append-prepend  ">
                                                    <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['ma_password_expired_on'])%>" placeholder="" name="ma_password_expired_on" id="ma_password_expired_on" title="<%$this->lang->line('SUPPLIER_FORM_PASSWORD_EXPIRED_ON')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                    <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_password_expired_onErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_login_failed_attempts"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_login_failed_attempts']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_login_failed_attempts']%>
                                                    <%$this->dropdown->display("ma_login_failed_attempts","ma_login_failed_attempts","  title='<%$this->lang->line('SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS')%>'  ", "|||", "", $opt_selected,"ma_login_failed_attempts")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_login_failed_attemptsErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_login_locked_until"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_login_locked_until']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  input-append text-append-prepend  ">
                                                    <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['ma_login_locked_until'])%>" placeholder="" name="ma_login_locked_until" id="ma_login_locked_until" title="<%$this->lang->line('SUPPLIER_FORM_LOGIN_LOCKED_UNTIL')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                    <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_login_locked_untilErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_added_date"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_added_date']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  input-append text-append-prepend  ">
                                                    <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['ma_added_date'])%>" placeholder="" name="ma_added_date" id="ma_added_date" title="<%$this->lang->line('SUPPLIER_FORM_ADDED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                    <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_added_dateErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_modified_date"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_modified_date']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  input-append text-append-prepend  ">
                                                    <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['ma_modified_date'])%>" placeholder="" name="ma_modified_date" id="ma_modified_date" title="<%$this->lang->line('SUPPLIER_FORM_MODIFIED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                    <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_modified_dateErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_last_access"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_last_access']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  input-append text-append-prepend  ">
                                                    <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['ma_last_access'])%>" placeholder="" name="ma_last_access" id="ma_last_access" title="<%$this->lang->line('SUPPLIER_FORM_LAST_ACCESS')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                    <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_last_accessErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_ma_status"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['ma_status']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <%assign var="opt_selected" value=$data['ma_status']%>
                                                    <%$this->dropdown->display("ma_status","ma_status","  title='<%$this->lang->line('SUPPLIER_FORM_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_FORM_STATUS')%>'  ", "|||", "", $opt_selected,"ma_status")%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='ma_statusErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="frm-bot-btn <%$rl_theme_arr['frm_twclm_action_bar']%> <%$rl_theme_arr['frm_twclm_action_btn']%> popup-footer">
                                <%if $rl_theme_arr['frm_twclm_ctrls_view'] eq 'No'%>
                                    <%assign var='rm_ctrl_directions' value=true%>
                                <%/if%>
                                <%include file="supplier_form_add_buttons.tpl"%>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/supplier_form_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.supplier_form.callEvents();
<%/javascript%>