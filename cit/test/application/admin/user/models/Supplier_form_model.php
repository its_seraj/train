<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Supplier Form Model
 *
 * @category admin
 *
 * @package user
 *
 * @subpackage models
 *
 * @module Supplier Form
 *
 * @class Supplier_form_model.php
 *
 * @path application\admin\user\models\Supplier_form_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @date 28.03.2022
 */

class Supplier_form_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $insert_id;
    //
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $global_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $print_list;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     * @created Mdseraj Khan | 28.03.2022
     * @modified Mdseraj Khan | 28.03.2022
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->module_name = "supplier_form";
        $this->table_name = "mod_admin";
        $this->table_alias = "ma";
        $this->primary_key = "iAdminId";
        $this->primary_alias = "ma_admin_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "ma_name",
            "ma_email",
            "ma_user_name",
            "ma_password",
            "ma_dial_code",
            "ma_phonenumber",
            "ma_group_id",
            "ma_auth_type",
            "ma_auth_code",
            "ma_o_tpcode",
            "ma_email_verified",
            "ma_verification_code",
            "ma_is_temporary_password",
            "ma_password_expired_on",
            "ma_login_failed_attempts",
            "ma_login_locked_until",
            "ma_added_date",
            "ma_modified_date",
            "ma_last_access",
            "ma_status",
        );
        $this->join_tables = array();
        $this->extra_cond = "";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->switchto_options = array();
        $this->default_filters = array();
        $this->global_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->print_list = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        $this->insert_id = $insert_id;
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No")
    {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No')
        {
            if ($alias == "Yes")
            {
                if (is_array($join['joins']) && count($join['joins']))
                {
                    $join_tbls = '';
                    if ($join['list'] == "Yes")
                    {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '')
                {
                    $set_cond = array();
                    foreach ($data as $key => $val)
                    {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where))
                    {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    }
                    elseif ($where)
                    {
                        $extra_cond = " WHERE ".$where;
                    }
                    else
                    {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                }
                else
                {
                    if (is_numeric($where))
                    {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    }
                    elseif ($where)
                    {
                        $this->db->where($where, FALSE, FALSE);
                    }
                    else
                    {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        }
        else
        {
            if ($alias == "Yes")
            {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins']))
                {
                    if ($join['list'] == "Yes")
                    {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where))
                {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $del_query .= " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE)
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        elseif ($list == TRUE)
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "")
            {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("ma.vName AS ma_name");
            $this->db->select("ma.vEmail AS ma_email");
            $this->db->select("ma.vUserName AS ma_user_name");
            $this->db->select("ma.vPassword AS ma_password");
            $this->db->select("ma.vDialCode AS ma_dial_code");
            $this->db->select("ma.vPhonenumber AS ma_phonenumber");
            $this->db->select("ma.iGroupId AS ma_group_id");
            $this->db->select("ma.eAuthType AS ma_auth_type");
            $this->db->select("ma.vAuthCode AS ma_auth_code");
            $this->db->select("ma.vOTPCode AS ma_o_tpcode");
            $this->db->select("ma.eEmailVerified AS ma_email_verified");
            $this->db->select("ma.vVerificationCode AS ma_verification_code");
            $this->db->select("ma.isTemporaryPassword AS ma_is_temporary_password");
            $this->db->select("ma.dPasswordExpiredOn AS ma_password_expired_on");
            $this->db->select("ma.iLoginFailedAttempts AS ma_login_failed_attempts");
            $this->db->select("ma.dLoginLockedUntil AS ma_login_locked_until");
            $this->db->select("ma.dtAddedDate AS ma_added_date");
            $this->db->select("ma.dtModifiedDate AS ma_modified_date");
            $this->db->select("ma.dLastAccess AS ma_last_access");
            $this->db->select("ma.eStatus AS ma_status");
        }
        else
        {
            $this->db->select("ma.iAdminId AS iAdminId");
            $this->db->select("ma.iAdminId AS ma_admin_id");
            $this->db->select("ma.vName AS ma_name");
            $this->db->select("ma.vEmail AS ma_email");
            $this->db->select("ma.vUserName AS ma_user_name");
            $this->db->select("ma.vPassword AS ma_password");
            $this->db->select("ma.vDialCode AS ma_dial_code");
            $this->db->select("ma.vPhonenumber AS ma_phonenumber");
            $this->db->select("ma.iGroupId AS ma_group_id");
            $this->db->select("ma.eAuthType AS ma_auth_type");
            $this->db->select("ma.vAuthCode AS ma_auth_code");
            $this->db->select("ma.vOTPCode AS ma_o_tpcode");
            $this->db->select("ma.eEmailVerified AS ma_email_verified");
            $this->db->select("ma.vVerificationCode AS ma_verification_code");
            $this->db->select("ma.isTemporaryPassword AS ma_is_temporary_password");
            $this->db->select("ma.dPasswordExpiredOn AS ma_password_expired_on");
            $this->db->select("ma.iLoginFailedAttempts AS ma_login_failed_attempts");
            $this->db->select("ma.dLoginLockedUntil AS ma_login_locked_until");
            $this->db->select("ma.dtAddedDate AS ma_added_date");
            $this->db->select("ma.dtModifiedDate AS ma_modified_date");
            $this->db->select("ma.dLastAccess AS ma_last_access");
            $this->db->select("ma.eStatus AS ma_status");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0)
        {
            $this->listing->addJoinTables($join['joins']);
        }
        else
        {


        }
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif (is_numeric($extra_cond))
        {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        }
        elseif ($extra_cond)
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['global_filters'] = $this->global_filters;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "")
        {
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        }
        else
        {
            $total_records = $this->db->count_all_results();
        }
        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("ma.vName AS ma_name");
        $this->db->select("ma.vEmail AS ma_email");
        $this->db->select("ma.vUserName AS ma_user_name");
        $this->db->select("ma.vPassword AS ma_password");
        $this->db->select("ma.vDialCode AS ma_dial_code");
        $this->db->select("ma.vPhonenumber AS ma_phonenumber");
        $this->db->select("ma.iGroupId AS ma_group_id");
        $this->db->select("ma.eAuthType AS ma_auth_type");
        $this->db->select("ma.vAuthCode AS ma_auth_code");
        $this->db->select("ma.vOTPCode AS ma_o_tpcode");
        $this->db->select("ma.eEmailVerified AS ma_email_verified");
        $this->db->select("ma.vVerificationCode AS ma_verification_code");
        $this->db->select("ma.isTemporaryPassword AS ma_is_temporary_password");
        $this->db->select("ma.dPasswordExpiredOn AS ma_password_expired_on");
        $this->db->select("ma.iLoginFailedAttempts AS ma_login_failed_attempts");
        $this->db->select("ma.dLoginLockedUntil AS ma_login_locked_until");
        $this->db->select("ma.dtAddedDate AS ma_added_date");
        $this->db->select("ma.dtModifiedDate AS ma_modified_date");
        $this->db->select("ma.dLastAccess AS ma_last_access");
        $this->db->select("ma.eStatus AS ma_status");
        if ($sdef == "Yes")
        {
            if (is_array($order_by) && count($order_by) > 0)
            {
                foreach ($order_by as $orK => $orV)
                {
                    $sort_filed = $orV['field'];
                    $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                    $this->db->order_by($sort_filed, $sort_order);
                }
            }
            else
            if (!empty($order_by) && is_string($order_by))
            {
                $this->db->order_by($order_by);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array())
    {
        $page = $config_arr['page'];
        $id = $config_arr['id'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if (is_array($id) && count($id) > 0)
        {
            $this->db->where_in($this->table_alias.".".$this->primary_key, $id);
        }
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['global_filters'] = $this->global_filters;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("ma.vName AS ma_name");
        $this->db->select("ma.vEmail AS ma_email");
        $this->db->select("ma.vUserName AS ma_user_name");
        $this->db->select("ma.vPassword AS ma_password");
        $this->db->select("ma.vDialCode AS ma_dial_code");
        $this->db->select("ma.vPhonenumber AS ma_phonenumber");
        $this->db->select("ma.iGroupId AS ma_group_id");
        $this->db->select("ma.eAuthType AS ma_auth_type");
        $this->db->select("ma.vAuthCode AS ma_auth_code");
        $this->db->select("ma.vOTPCode AS ma_o_tpcode");
        $this->db->select("ma.eEmailVerified AS ma_email_verified");
        $this->db->select("ma.vVerificationCode AS ma_verification_code");
        $this->db->select("ma.isTemporaryPassword AS ma_is_temporary_password");
        $this->db->select("ma.dPasswordExpiredOn AS ma_password_expired_on");
        $this->db->select("ma.iLoginFailedAttempts AS ma_login_failed_attempts");
        $this->db->select("ma.dLoginLockedUntil AS ma_login_locked_until");
        $this->db->select("ma.dtAddedDate AS ma_added_date");
        $this->db->select("ma.dtModifiedDate AS ma_modified_date");
        $this->db->select("ma.dLastAccess AS ma_last_access");
        $this->db->select("ma.eStatus AS ma_status");
        if ($sdef == "Yes")
        {
            if (is_array($order_by) && count($order_by) > 0)
            {
                foreach ($order_by as $orK => $orV)
                {
                    $sort_filed = $orV['field'];
                    $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                    $this->db->order_by($sort_filed, $sort_order);
                }
            }
            else
            if (!empty($order_by) && is_string($order_by))
            {
                $this->db->order_by($order_by);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "")
        {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE)
    {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0)
        {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "")
    {
        $list_config = array(
            "ma_name" => array(
                "name" => "ma_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vName",
                "source_field" => "ma_name",
                "display_query" => "ma.vName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Name",
                "lang_code" => "SUPPLIER_FORM_NAME",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_NAME'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "edit_link" => "Yes",
            ),
            "ma_email" => array(
                "name" => "ma_email",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vEmail",
                "source_field" => "ma_email",
                "display_query" => "ma.vEmail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Email",
                "lang_code" => "SUPPLIER_FORM_EMAIL",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_EMAIL'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_user_name" => array(
                "name" => "ma_user_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vUserName",
                "source_field" => "ma_user_name",
                "display_query" => "ma.vUserName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "User Name",
                "lang_code" => "SUPPLIER_FORM_USER_NAME",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_USER_NAME'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_password" => array(
                "name" => "ma_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPassword",
                "source_field" => "ma_password",
                "display_query" => "ma.vPassword",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Password",
                "lang_code" => "SUPPLIER_FORM_PASSWORD",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PASSWORD'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_dial_code" => array(
                "name" => "ma_dial_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vDialCode",
                "source_field" => "ma_dial_code",
                "display_query" => "ma.vDialCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Dial Code",
                "lang_code" => "SUPPLIER_FORM_DIAL_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_DIAL_CODE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_phonenumber" => array(
                "name" => "ma_phonenumber",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPhonenumber",
                "source_field" => "ma_phonenumber",
                "display_query" => "ma.vPhonenumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Phonenumber",
                "lang_code" => "SUPPLIER_FORM_PHONENUMBER",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PHONENUMBER'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_group_id" => array(
                "name" => "ma_group_id",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iGroupId",
                "source_field" => "ma_group_id",
                "display_query" => "ma.iGroupId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Group Id",
                "lang_code" => "SUPPLIER_FORM_GROUP_ID",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_GROUP_ID'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_auth_type" => array(
                "name" => "ma_auth_type",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eAuthType",
                "source_field" => "ma_auth_type",
                "display_query" => "ma.eAuthType",
                "entry_type" => "Table",
                "data_type" => "set",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Auth Type",
                "lang_code" => "SUPPLIER_FORM_AUTH_TYPE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_AUTH_TYPE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_auth_code" => array(
                "name" => "ma_auth_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vAuthCode",
                "source_field" => "ma_auth_code",
                "display_query" => "ma.vAuthCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Auth Code",
                "lang_code" => "SUPPLIER_FORM_AUTH_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_AUTH_CODE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_o_tpcode" => array(
                "name" => "ma_o_tpcode",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vOTPCode",
                "source_field" => "ma_o_tpcode",
                "display_query" => "ma.vOTPCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "O Tpcode",
                "lang_code" => "SUPPLIER_FORM_O_TPCODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_O_TPCODE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_email_verified" => array(
                "name" => "ma_email_verified",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eEmailVerified",
                "source_field" => "ma_email_verified",
                "display_query" => "ma.eEmailVerified",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Email Verified",
                "lang_code" => "SUPPLIER_FORM_EMAIL_VERIFIED",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_EMAIL_VERIFIED'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("ma_email_verified",
                "Text",
                "No")
            ),
            "ma_verification_code" => array(
                "name" => "ma_verification_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vVerificationCode",
                "source_field" => "ma_verification_code",
                "display_query" => "ma.vVerificationCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Verification Code",
                "lang_code" => "SUPPLIER_FORM_VERIFICATION_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_VERIFICATION_CODE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_is_temporary_password" => array(
                "name" => "ma_is_temporary_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "isTemporaryPassword",
                "source_field" => "ma_is_temporary_password",
                "display_query" => "ma.isTemporaryPassword",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Is Temporary Password",
                "lang_code" => "SUPPLIER_FORM_IS_TEMPORARY_PASSWORD",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_IS_TEMPORARY_PASSWORD'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("ma_is_temporary_password",
                "Text",
                "No")
            ),
            "ma_password_expired_on" => array(
                "name" => "ma_password_expired_on",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dPasswordExpiredOn",
                "source_field" => "ma_password_expired_on",
                "display_query" => "ma.dPasswordExpiredOn",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Password Expired On",
                "lang_code" => "SUPPLIER_FORM_PASSWORD_EXPIRED_ON",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PASSWORD_EXPIRED_ON'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => 'Y-m-d',
            ),
            "ma_login_failed_attempts" => array(
                "name" => "ma_login_failed_attempts",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iLoginFailedAttempts",
                "source_field" => "ma_login_failed_attempts",
                "display_query" => "ma.iLoginFailedAttempts",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Login Failed Attempts",
                "lang_code" => "SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "ma_login_locked_until" => array(
                "name" => "ma_login_locked_until",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dLoginLockedUntil",
                "source_field" => "ma_login_locked_until",
                "display_query" => "ma.dLoginLockedUntil",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Login Locked Until",
                "lang_code" => "SUPPLIER_FORM_LOGIN_LOCKED_UNTIL",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LOGIN_LOCKED_UNTIL'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => 'Y-m-d',
            ),
            "ma_added_date" => array(
                "name" => "ma_added_date",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dtAddedDate",
                "source_field" => "ma_added_date",
                "display_query" => "ma.dtAddedDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Added Date",
                "lang_code" => "SUPPLIER_FORM_ADDED_DATE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_ADDED_DATE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => 'Y-m-d',
            ),
            "ma_modified_date" => array(
                "name" => "ma_modified_date",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dtModifiedDate",
                "source_field" => "ma_modified_date",
                "display_query" => "ma.dtModifiedDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Modified Date",
                "lang_code" => "SUPPLIER_FORM_MODIFIED_DATE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_MODIFIED_DATE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => 'Y-m-d',
            ),
            "ma_last_access" => array(
                "name" => "ma_last_access",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dLastAccess",
                "source_field" => "ma_last_access",
                "display_query" => "ma.dLastAccess",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_in" => "Both",
                "type" => "date",
                "align" => "left",
                "label" => "Last Access",
                "lang_code" => "SUPPLIER_FORM_LAST_ACCESS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LAST_ACCESS'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "format" => 'Y-m-d',
            ),
            "ma_status" => array(
                "name" => "ma_status",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eStatus",
                "source_field" => "ma_status",
                "display_query" => "ma.eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Status",
                "lang_code" => "SUPPLIER_FORM_STATUS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_STATUS'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "default" => $this->filter->getDefaultValue("ma_status",
                "Text",
                "Active")
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $list_config[$name];
        }
        else
        {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "")
    {
        $form_config = array(
            "sys_static_field_1" => array(
                "name" => "sys_static_field_1",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysStaticField_1",
                "entry_type" => "Static",
                "data_type" => "",
                "show_input" => "Both",
                "type" => "",
                "label" => "Company Information",
                "lang_code" => "SUPPLIER_FORM_COMPANY_INFORMATION",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_COMPANY_INFORMATION')
            ),
            "ma_name" => array(
                "name" => "ma_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Name",
                "lang_code" => "SUPPLIER_FORM_NAME",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_NAME')
            ),
            "ma_email" => array(
                "name" => "ma_email",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vEmail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Email",
                "lang_code" => "SUPPLIER_FORM_EMAIL",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_EMAIL')
            ),
            "ma_user_name" => array(
                "name" => "ma_user_name",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vUserName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "User Name",
                "lang_code" => "SUPPLIER_FORM_USER_NAME",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_USER_NAME')
            ),
            "ma_password" => array(
                "name" => "ma_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPassword",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Password",
                "lang_code" => "SUPPLIER_FORM_PASSWORD",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PASSWORD')
            ),
            "ma_dial_code" => array(
                "name" => "ma_dial_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vDialCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Dial Code",
                "lang_code" => "SUPPLIER_FORM_DIAL_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_DIAL_CODE')
            ),
            "ma_phonenumber" => array(
                "name" => "ma_phonenumber",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vPhonenumber",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Phonenumber",
                "lang_code" => "SUPPLIER_FORM_PHONENUMBER",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PHONENUMBER')
            ),
            "ma_group_id" => array(
                "name" => "ma_group_id",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iGroupId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Group Id",
                "lang_code" => "SUPPLIER_FORM_GROUP_ID",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_GROUP_ID')
            ),
            "ma_auth_type" => array(
                "name" => "ma_auth_type",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eAuthType",
                "entry_type" => "Table",
                "data_type" => "set",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Auth Type",
                "lang_code" => "SUPPLIER_FORM_AUTH_TYPE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_AUTH_TYPE')
            ),
            "ma_auth_code" => array(
                "name" => "ma_auth_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vAuthCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Auth Code",
                "lang_code" => "SUPPLIER_FORM_AUTH_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_AUTH_CODE')
            ),
            "ma_o_tpcode" => array(
                "name" => "ma_o_tpcode",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vOTPCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "O Tpcode",
                "lang_code" => "SUPPLIER_FORM_O_TPCODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_O_TPCODE')
            ),
            "ma_email_verified" => array(
                "name" => "ma_email_verified",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eEmailVerified",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Email Verified",
                "lang_code" => "SUPPLIER_FORM_EMAIL_VERIFIED",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_EMAIL_VERIFIED'),
                "default" => $this->filter->getDefaultValue("ma_email_verified",
                "Text",
                "No"),
                "dfapply" => "addOnly",
            ),
            "ma_verification_code" => array(
                "name" => "ma_verification_code",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "vVerificationCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Verification Code",
                "lang_code" => "SUPPLIER_FORM_VERIFICATION_CODE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_VERIFICATION_CODE')
            ),
            "ma_is_temporary_password" => array(
                "name" => "ma_is_temporary_password",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "isTemporaryPassword",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Is Temporary Password",
                "lang_code" => "SUPPLIER_FORM_IS_TEMPORARY_PASSWORD",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_IS_TEMPORARY_PASSWORD'),
                "default" => $this->filter->getDefaultValue("ma_is_temporary_password",
                "Text",
                "No"),
                "dfapply" => "addOnly",
            ),
            "ma_password_expired_on" => array(
                "name" => "ma_password_expired_on",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dPasswordExpiredOn",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_input" => "Both",
                "type" => "date",
                "label" => "Password Expired On",
                "lang_code" => "SUPPLIER_FORM_PASSWORD_EXPIRED_ON",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_PASSWORD_EXPIRED_ON'),
                "format" => 'Y-m-d',
            ),
            "ma_login_failed_attempts" => array(
                "name" => "ma_login_failed_attempts",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "iLoginFailedAttempts",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Login Failed Attempts",
                "lang_code" => "SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LOGIN_FAILED_ATTEMPTS')
            ),
            "ma_login_locked_until" => array(
                "name" => "ma_login_locked_until",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dLoginLockedUntil",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_input" => "Both",
                "type" => "date",
                "label" => "Login Locked Until",
                "lang_code" => "SUPPLIER_FORM_LOGIN_LOCKED_UNTIL",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LOGIN_LOCKED_UNTIL'),
                "format" => 'Y-m-d',
            ),
            "ma_added_date" => array(
                "name" => "ma_added_date",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dtAddedDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_input" => "Both",
                "type" => "date",
                "label" => "Added Date",
                "lang_code" => "SUPPLIER_FORM_ADDED_DATE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_ADDED_DATE'),
                "format" => 'Y-m-d',
            ),
            "ma_modified_date" => array(
                "name" => "ma_modified_date",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dtModifiedDate",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_input" => "Both",
                "type" => "date",
                "label" => "Modified Date",
                "lang_code" => "SUPPLIER_FORM_MODIFIED_DATE",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_MODIFIED_DATE'),
                "format" => 'Y-m-d',
            ),
            "ma_last_access" => array(
                "name" => "ma_last_access",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "dLastAccess",
                "entry_type" => "Table",
                "data_type" => "datetime",
                "show_input" => "Both",
                "type" => "date",
                "label" => "Last Access",
                "lang_code" => "SUPPLIER_FORM_LAST_ACCESS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_LAST_ACCESS'),
                "format" => 'Y-m-d',
            ),
            "ma_status" => array(
                "name" => "ma_status",
                "table_name" => "mod_admin",
                "table_alias" => "ma",
                "field_name" => "eStatus",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Status",
                "lang_code" => "SUPPLIER_FORM_STATUS",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_STATUS'),
                "default" => $this->filter->getDefaultValue("ma_status",
                "Text",
                "Active"),
                "dfapply" => "addOnly",
            ),
            "sys_static_field_2" => array(
                "name" => "sys_static_field_2",
                "table_name" => "",
                "table_alias" => "",
                "field_name" => "sysStaticField_2",
                "entry_type" => "Static",
                "data_type" => "",
                "show_input" => "Both",
                "type" => "",
                "label" => "Owner Information",
                "lang_code" => "SUPPLIER_FORM_OWNER_INFORMATION",
                "label_lang" => $this->lang->line('SUPPLIER_FORM_OWNER_INFORMATION')
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $form_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $form_config[$name];
        }
        else
        {
            $config_arr = $form_config;
        }
        return $config_arr;
    }

    /**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
    public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND')
    {
        $exists = FALSE;
        if (!is_array($field_arr) || count($field_arr) == 0)
        {
            return $exists;
        }
        foreach ((array) $field_arr as $key => $val)
        {
            $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape(trim($field_val[$val]));
        }
        $extra_cond = "(".implode(" ".$con." ", $extra_cond_arr).")";
        if ($mode == "Add")
        {
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        elseif ($mode == "Update")
        {
            $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
            $data = $this->getData($extra_cond, "COUNT(*) AS tot");
            if ($data[0]['tot'] > 0)
            {
                $exists = TRUE;
            }
        }
        return $exists;
    }

    /**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
    public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '')
    {
        $switchto_fields = $this->switchto_fields;
        $switch_data = array();
        if (!is_array($switchto_fields) || count($switchto_fields) == 0)
        {
            if ($type == "count")
            {
                return count($switch_data);
            }
            else
            {
                return $switch_data;
            }
        }
        $fields_arr = array();
        $fields_arr[] = array(
            "field" => $this->table_alias.".".$this->primary_key." AS id",
        );
        $fields_arr[] = array(
            "field" => $this->db->concat($switchto_fields)." AS val",
            "escape" => TRUE,
        );
        if (is_array($this->switchto_options) && count($this->switchto_options) > 0)
        {
            foreach ($this->switchto_options as $option)
            {
                $fields_arr[] = array(
                    "field" => $option,
                    "escape" => TRUE,
                );
            }
        }
        if (trim($this->extra_cond) != "")
        {
            $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
        }
        $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
        #echo $this->db->last_query();
        if ($type == "count")
        {
            return count($switch_data);
        }
        else
        {
            return $switch_data;
        }
    }
}
