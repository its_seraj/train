<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of companies Model
 *
 * @category admin
 *
 * @package master
 *
 * @subpackage models
 *
 * @module companies
 *
 * @class Companies_model.php
 *
 * @path application\admin\master\models\Companies_model.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @date 25.03.2022
 */

class Companies_model extends CI_Model
{
    public $table_name;
    public $table_alias;
    public $primary_key;
    public $primary_alias;
    public $insert_id;
    //
    public $grid_fields;
    public $join_tables;
    public $extra_cond;
    public $groupby_cond;
    public $orderby_cond;
    public $unique_type;
    public $unique_fields;
    public $switchto_fields;
    public $default_filters;
    public $global_filters;
    public $search_config;
    public $relation_modules;
    public $deletion_modules;
    public $print_rec;
    public $print_list;
    public $multi_lingual;
    public $physical_data_remove;
    //
    public $listing_data;
    public $rec_per_page;
    public $message;

    /**
     * __construct method is used to set model preferences while model object initialization.
     * @created Mdseraj Khan | 25.03.2022
     * @modified Mdseraj Khan | 25.03.2022
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->module_name = "companies";
        $this->table_name = "companies";
        $this->table_alias = "c";
        $this->primary_key = "iCompaniesId";
        $this->primary_alias = "c_companies_id";
        $this->physical_data_remove = "Yes";
        $this->grid_fields = array(
            "c_company_name",
            "c_parent_id",
            "c_business_trading_name",
            "c_short_name",
            "c_nature_of_business",
            "c_business_type",
            "c_taxno",
            "c_company_phone_code",
            "c_company_phone",
            "c_company_alt_phone",
            "c_company_email",
            "c_company_website",
        );
        $this->join_tables = array();
        $this->extra_cond = "";
        $this->groupby_cond = array();
        $this->having_cond = "";
        $this->orderby_cond = array();
        $this->unique_type = "OR";
        $this->unique_fields = array();
        $this->switchto_fields = array();
        $this->switchto_options = array();
        $this->default_filters = array();
        $this->global_filters = array();
        $this->search_config = array();
        $this->relation_modules = array();
        $this->deletion_modules = array();
        $this->print_rec = "No";
        $this->print_list = "No";
        $this->multi_lingual = "No";

        $this->rec_per_page = $this->config->item('REC_LIMIT');
    }

    /**
     * insert method is used to insert data records to the database table.
     * @param array $data data array for insert into table.
     * @return numeric $insert_id returns last inserted id.
     */
    public function insert($data = array())
    {
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        $this->insert_id = $insert_id;
        return $insert_id;
    }

    /**
     * update method is used to update data records to the database table.
     * @param array $data data array for update into table.
     * @param string $where where is the query condition for updating.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while updating records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function update($data = array(), $where = '', $alias = "No", $join = "No")
    {
        if ($alias == "Yes")
        {
            if ($join == "Yes")
            {
                $join_tbls = $this->addJoinTables("NR");
            }
            if (trim($join_tbls) != '')
            {
                $set_cond = array();
                foreach ($data as $key => $val)
                {
                    $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                }
                if (is_numeric($where))
                {
                    $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $extra_cond = " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".implode(", ", $set_cond)." ".$extra_cond;
                $res = $this->db->query($update_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->table_alias.".".$this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
            }
        }
        else
        {
            if (is_numeric($where))
            {
                $this->db->where($this->primary_key, $where);
            }
            elseif ($where)
            {
                $this->db->where($where, FALSE, FALSE);
            }
            else
            {
                return FALSE;
            }
            $res = $this->db->update($this->table_name, $data);
        }
        return $res;
    }

    /**
     * delete method is used to delete data records from the database table.
     * @param string $where where is the query condition for deletion.
     * @param string $alias alias is to keep aliases for query or not.
     * @param string $join join is to make joins while deleting records.
     * @return boolean $res returns TRUE or FALSE.
     */
    public function delete($where = "", $alias = "No", $join = "No")
    {
        if ($this->config->item('PHYSICAL_RECORD_DELETE') && $this->physical_data_remove == 'No')
        {
            if ($alias == "Yes")
            {
                if (is_array($join['joins']) && count($join['joins']))
                {
                    $join_tbls = '';
                    if ($join['list'] == "Yes")
                    {
                        $join_tbls = $this->addJoinTables("NR");
                    }
                    $join_tbls .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $join_tbls = $this->addJoinTables("NR");
                }
                $data = $this->general->getPhysicalRecordUpdate($this->table_alias);
                if (trim($join_tbls) != '')
                {
                    $set_cond = array();
                    foreach ($data as $key => $val)
                    {
                        $set_cond[] = $this->db->protect($key)." = ".$this->db->escape($val);
                    }
                    if (is_numeric($where))
                    {
                        $extra_cond = " WHERE ".$this->db->protect($this->table_alias.".".$this->primary_key)." = ".$this->db->escape($where);
                    }
                    elseif ($where)
                    {
                        $extra_cond = " WHERE ".$where;
                    }
                    else
                    {
                        return FALSE;
                    }
                    $update_query = "UPDATE ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias)." ".$join_tbls." SET ".implode(", ", $set_cond)." ".$extra_cond;
                    $res = $this->db->query($update_query);
                }
                else
                {
                    if (is_numeric($where))
                    {
                        $this->db->where($this->table_alias.".".$this->primary_key, $where);
                    }
                    elseif ($where)
                    {
                        $this->db->where($where, FALSE, FALSE);
                    }
                    else
                    {
                        return FALSE;
                    }
                    $res = $this->db->update($this->table_name." AS ".$this->table_alias, $data);
                }
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $data = $this->general->getPhysicalRecordUpdate();
                $res = $this->db->update($this->table_name, $data);
            }
        }
        else
        {
            if ($alias == "Yes")
            {
                $del_query = "DELETE ".$this->db->protect($this->table_alias).".* FROM ".$this->db->protect($this->table_name)." AS ".$this->db->protect($this->table_alias);
                if (is_array($join['joins']) && count($join['joins']))
                {
                    if ($join['list'] == "Yes")
                    {
                        $del_query .= $this->addJoinTables("NR");
                    }
                    $del_query .= ' '.$this->listing->addJoinTables($join['joins'], "NR");
                }
                elseif ($join == "Yes")
                {
                    $del_query .= $this->addJoinTables("NR");
                }
                if (is_numeric($where))
                {
                    $del_query .= " WHERE ".$this->db->protect($this->table_alias).".".$this->db->protect($this->primary_key)." = ".$this->db->escape($where);
                }
                elseif ($where)
                {
                    $del_query .= " WHERE ".$where;
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->query($del_query);
            }
            else
            {
                if (is_numeric($where))
                {
                    $this->db->where($this->primary_key, $where);
                }
                elseif ($where)
                {
                    $this->db->where($where, FALSE, FALSE);
                }
                else
                {
                    return FALSE;
                }
                $res = $this->db->delete($this->table_name);
            }
        }
        return $res;
    }

    /**
     * getData method is used to get data records for this module.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @param string $fields fields are either array or string.
     * @param string $order_by order_by is to append order by condition.
     * @param string $group_by group_by is to append group by condition.
     * @param string $limit limit is to append limit condition.
     * @param string $join join is to make joins with relation tables.
     * @param boolean $having_cond having cond is the query condition for getting conditional data.
     * @param boolean $list list is to differ listing fields or form fields.
     * @return array $data_arr returns data records array.
     */
    public function getData($extra_cond = "", $fields = "", $order_by = "", $group_by = "", $limit = "", $join = "No", $having_cond = '', $list = FALSE)
    {
        if (is_array($fields))
        {
            $this->listing->addSelectFields($fields);
        }
        elseif ($fields != "")
        {
            $this->db->select($fields);
        }
        elseif ($list == TRUE)
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
            if ($this->primary_alias != "")
            {
                $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
            }
            $this->db->select("c.vCompanyName AS c_company_name");
            $this->db->select("c.iParentID AS c_parent_id");
            $this->db->select("c.vBusinessTradingName AS c_business_trading_name");
            $this->db->select("c.vShortName AS c_short_name");
            $this->db->select("c.vNatureOfBusiness AS c_nature_of_business");
            $this->db->select("c.eBusinessType AS c_business_type");
            $this->db->select("c.vTAXNo AS c_taxno");
            $this->db->select("c.vCompanyPhoneCode AS c_company_phone_code");
            $this->db->select("c.vCompanyPhone AS c_company_phone");
            $this->db->select("c.vCompanyAltPhone AS c_company_alt_phone");
            $this->db->select("c.vCompanyEmail AS c_company_email");
            $this->db->select("c.vCompanyWebsite AS c_company_website");
        }
        else
        {
            $this->db->select("c.iCompaniesId AS iCompaniesId");
            $this->db->select("c.iCompaniesId AS c_companies_id");
            $this->db->select("c.vCompanyName AS c_company_name");
            $this->db->select("c.iParentID AS c_parent_id");
            $this->db->select("c.vBusinessTradingName AS c_business_trading_name");
            $this->db->select("c.vShortName AS c_short_name");
            $this->db->select("c.vNatureOfBusiness AS c_nature_of_business");
            $this->db->select("c.eBusinessType AS c_business_type");
            $this->db->select("c.vTAXNo AS c_taxno");
            $this->db->select("c.vCompanyPhoneCode AS c_company_phone_code");
            $this->db->select("c.vCompanyPhone AS c_company_phone");
            $this->db->select("c.vCompanyAltPhoneCode AS c_company_alt_phone_code");
            $this->db->select("c.vCompanyAltPhone AS c_company_alt_phone");
            $this->db->select("c.vCompanyEmail AS c_company_email");
            $this->db->select("c.vCompanyWebsite AS c_company_website");
            $this->db->select("c.vCompanyRegNo AS c_company_reg_no");
            $this->db->select("c.vInsuranceNo AS c_insurance_no");
            $this->db->select("c.vStreet AS c_street");
            $this->db->select("c.iCountryId AS c_country_id");
            $this->db->select("c.iStateId AS c_state_id");
            $this->db->select("c.vCity AS c_city");
            $this->db->select("c.vZipCode AS c_zip_code");
            $this->db->select("c.vOwnerName AS c_owner_name");
            $this->db->select("c.vOwnerEmail AS c_owner_email");
            $this->db->select("c.vOwnerPhone AS c_owner_phone");
            $this->db->select("c.dtAddedDate AS c_added_date");
            $this->db->select("c.iAddedDate AS c_added_date");
            $this->db->select("c.dtUpdatDate AS c_updat_date");
            $this->db->select("c.eStatus AS c_status");
        }

        $this->db->from($this->table_name." AS ".$this->table_alias);
        if (is_array($join) && is_array($join['joins']) && count($join['joins']) > 0)
        {
            $this->listing->addJoinTables($join['joins']);
        }
        else
        {


        }
        if (is_array($extra_cond) && count($extra_cond) > 0)
        {
            $this->listing->addWhereFields($extra_cond);
        }
        elseif (is_numeric($extra_cond))
        {
            $this->db->where($this->table_alias.".".$this->primary_key, intval($extra_cond));
        }
        elseif ($extra_cond)
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if ($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        if ($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if ($limit != "")
        {
            if (is_numeric($limit))
            {
                $this->db->limit($limit);
            }
            else
            {
                list($offset, $limit) = explode(",", $limit);
                $this->db->limit($offset, $limit);
            }
        }
        $data_obj = $this->db->get();
        $data_arr = is_object($data_obj) ? $data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $data_arr;
    }

    /**
     * getListingData method is used to get grid listing data records for this module.
     * @param array $config_arr config_arr for grid listing settigs.
     * @return array $listing_data returns data records array for grid.
     */
    public function getListingData($config_arr = array())
    {
        $page = $config_arr['page'];
        $rows = $config_arr['rows'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $rec_per_page = (intval($rows) > 0) ? intval($rows) : $this->rec_per_page;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->start_cache();
        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['global_filters'] = $this->global_filters;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;
        $filter_config['grid_fields'] = $this->grid_fields;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->stop_cache();
        if ((is_array($group_by) && count($group_by) > 0) || trim($having_cond) != "")
        {
            $total_records_arr = $this->db->get();
            $total_records = is_object($total_records_arr) ? $total_records_arr->num_rows() : 0;
        }
        else
        {
            $total_records = $this->db->count_all_results();
        }
        $total_pages = $this->listing->getTotalPages($total_records, $rec_per_page);

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("c.vCompanyName AS c_company_name");
        $this->db->select("c.iParentID AS c_parent_id");
        $this->db->select("c.vBusinessTradingName AS c_business_trading_name");
        $this->db->select("c.vShortName AS c_short_name");
        $this->db->select("c.vNatureOfBusiness AS c_nature_of_business");
        $this->db->select("c.eBusinessType AS c_business_type");
        $this->db->select("c.vTAXNo AS c_taxno");
        $this->db->select("c.vCompanyPhoneCode AS c_company_phone_code");
        $this->db->select("c.vCompanyPhone AS c_company_phone");
        $this->db->select("c.vCompanyAltPhone AS c_company_alt_phone");
        $this->db->select("c.vCompanyEmail AS c_company_email");
        $this->db->select("c.vCompanyWebsite AS c_company_website");
        if ($sdef == "Yes")
        {
            if (is_array($order_by) && count($order_by) > 0)
            {
                foreach ($order_by as $orK => $orV)
                {
                    $sort_filed = $orV['field'];
                    $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                    $this->db->order_by($sort_filed, $sort_order);
                }
            }
            else
            if (!empty($order_by) && is_string($order_by))
            {
                $this->db->order_by($order_by);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        $limit_offset = $this->listing->getStartIndex($total_records, $page, $rec_per_page);
        $this->db->limit($rec_per_page, $limit_offset);
        $return_data_obj = $this->db->get();
        $return_data = is_object($return_data_obj) ? $return_data_obj->result_array() : array();
        $this->db->flush_cache();
        $listing_data = $this->listing->getDataForJqGrid($return_data, $filter_config, $page, $total_pages, $total_records);
        $this->listing_data = $return_data;
        #echo $this->db->last_query();
        return $listing_data;
    }

    /**
     * getExportData method is used to get grid export data records for this module.
     * @param array $config_arr config_arr for grid export settigs.
     * @return array $export_data returns data records array for export.
     */
    public function getExportData($config_arr = array())
    {
        $page = $config_arr['page'];
        $id = $config_arr['id'];
        $rows = $config_arr['rows'];
        $rowlimit = $config_arr['rowlimit'];
        $sidx = $config_arr['sidx'];
        $sord = $config_arr['sord'];
        $sdef = $config_arr['sdef'];
        $filters = $config_arr['filters'];

        $extra_cond = $config_arr['extra_cond'];
        $group_by = $config_arr['group_by'];
        $having_cond = $config_arr['having_cond'];
        $order_by = $config_arr['order_by'];

        $page = ($page != '') ? $page : 1;
        $extra_cond = ($extra_cond != "") ? $extra_cond : "";

        $this->db->from($this->table_name." AS ".$this->table_alias);
        $this->addJoinTables("AR");
        if (is_array($id) && count($id) > 0)
        {
            $this->db->where_in($this->table_alias.".".$this->primary_key, $id);
        }
        if ($extra_cond != "")
        {
            $this->db->where($extra_cond, FALSE, FALSE);
        }
        $this->general->getPhysicalRecordWhere($this->table_name, $this->table_alias, "AR");
        if (is_array($group_by) && count($group_by) > 0)
        {
            $this->db->group_by($group_by);
        }
        if ($having_cond != "")
        {
            $this->db->having($having_cond, FALSE, FALSE);
        }
        $filter_config = array();
        $filter_config['module_config'] = $config_arr['module_config'];
        $filter_config['list_config'] = $config_arr['list_config'];
        $filter_config['form_config'] = $config_arr['form_config'];
        $filter_config['dropdown_arr'] = $config_arr['dropdown_arr'];
        $filter_config['search_config'] = $this->search_config;
        $filter_config['global_filters'] = $this->global_filters;
        $filter_config['table_name'] = $this->table_name;
        $filter_config['table_alias'] = $this->table_alias;
        $filter_config['primary_key'] = $this->primary_key;

        $filter_main = $this->filter->applyFilter($filters, $filter_config, "Select");
        $filter_left = $this->filter->applyLeftFilter($filters, $filter_config, "Select");
        $filter_range = $this->filter->applyRangeFilter($filters, $filter_config, "Select");
        if ($filter_main != "")
        {
            $this->db->where("(".$filter_main.")", FALSE, FALSE);
        }
        if ($filter_left != "")
        {
            $this->db->where("(".$filter_left.")", FALSE, FALSE);
        }
        if ($filter_range != "")
        {
            $this->db->where("(".$filter_range.")", FALSE, FALSE);
        }

        $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_key);
        if ($this->primary_alias != "")
        {
            $this->db->select($this->table_alias.".".$this->primary_key." AS ".$this->primary_alias);
        }
        $this->db->select("c.vCompanyName AS c_company_name");
        $this->db->select("c.iParentID AS c_parent_id");
        $this->db->select("c.vBusinessTradingName AS c_business_trading_name");
        $this->db->select("c.vShortName AS c_short_name");
        $this->db->select("c.vNatureOfBusiness AS c_nature_of_business");
        $this->db->select("c.eBusinessType AS c_business_type");
        $this->db->select("c.vTAXNo AS c_taxno");
        $this->db->select("c.vCompanyPhoneCode AS c_company_phone_code");
        $this->db->select("c.vCompanyPhone AS c_company_phone");
        $this->db->select("c.vCompanyAltPhone AS c_company_alt_phone");
        $this->db->select("c.vCompanyEmail AS c_company_email");
        $this->db->select("c.vCompanyWebsite AS c_company_website");
        if ($sdef == "Yes")
        {
            if (is_array($order_by) && count($order_by) > 0)
            {
                foreach ($order_by as $orK => $orV)
                {
                    $sort_filed = $orV['field'];
                    $sort_order = (strtolower($orV['order']) == "desc") ? "DESC" : "ASC";
                    $this->db->order_by($sort_filed, $sort_order);
                }
            }
            else
            if (!empty($order_by) && is_string($order_by))
            {
                $this->db->order_by($order_by);
            }
        }
        if ($sidx != "")
        {
            $this->listing->addGridOrderBy($sidx, $sord, $config_arr['list_config']);
        }
        if ($rowlimit != "")
        {
            $offset = $rowlimit;
            $limit = ($rowlimit*$page-$rowlimit);
            $this->db->limit($offset, $limit);
        }
        $export_data_obj = $this->db->get();
        $export_data = is_object($export_data_obj) ? $export_data_obj->result_array() : array();
        #echo $this->db->last_query();
        return $export_data;
    }

    /**
     * addJoinTables method is used to make relation tables joins with main table.
     * @param string $type type is to get active record or join string.
     * @param boolean $allow_tables allow_table is to restrict some set of tables.
     * @return string $ret_joins returns relation tables join string.
     */
    public function addJoinTables($type = 'AR', $allow_tables = FALSE)
    {
        $join_tables = $this->join_tables;
        if (!is_array($join_tables) || count($join_tables) == 0)
        {
            return '';
        }
        $ret_joins = $this->listing->addJoinTables($join_tables, $type, $allow_tables);
        return $ret_joins;
    }

    /**
     * getListConfiguration method is used to get listing configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns listing configuration array.
     */
    public function getListConfiguration($name = "")
    {
        $list_config = array(
            "c_company_name" => array(
                "name" => "c_company_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyName",
                "source_field" => "c_company_name",
                "display_query" => "c.vCompanyName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Name",
                "lang_code" => "COMPANIES_COMPANY_NAME",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_NAME'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
                "edit_link" => "Yes",
            ),
            "c_parent_id" => array(
                "name" => "c_parent_id",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "iParentID",
                "source_field" => "c_parent_id",
                "display_query" => "c.iParentID",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Parent Id",
                "lang_code" => "COMPANIES_PARENT_ID",
                "label_lang" => $this->lang->line('COMPANIES_PARENT_ID'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_business_trading_name" => array(
                "name" => "c_business_trading_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vBusinessTradingName",
                "source_field" => "c_business_trading_name",
                "display_query" => "c.vBusinessTradingName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Business Trading Name",
                "lang_code" => "COMPANIES_BUSINESS_TRADING_NAME",
                "label_lang" => $this->lang->line('COMPANIES_BUSINESS_TRADING_NAME'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_short_name" => array(
                "name" => "c_short_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vShortName",
                "source_field" => "c_short_name",
                "display_query" => "c.vShortName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Short Name",
                "lang_code" => "COMPANIES_SHORT_NAME",
                "label_lang" => $this->lang->line('COMPANIES_SHORT_NAME'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_nature_of_business" => array(
                "name" => "c_nature_of_business",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vNatureOfBusiness",
                "source_field" => "c_nature_of_business",
                "display_query" => "c.vNatureOfBusiness",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Nature Of Business",
                "lang_code" => "COMPANIES_NATURE_OF_BUSINESS",
                "label_lang" => $this->lang->line('COMPANIES_NATURE_OF_BUSINESS'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_business_type" => array(
                "name" => "c_business_type",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "eBusinessType",
                "source_field" => "c_business_type",
                "display_query" => "c.eBusinessType",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_in" => "Both",
                "type" => "dropdown",
                "align" => "center",
                "label" => "Business Type",
                "lang_code" => "COMPANIES_BUSINESS_TYPE",
                "label_lang" => $this->lang->line('COMPANIES_BUSINESS_TYPE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_taxno" => array(
                "name" => "c_taxno",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vTAXNo",
                "source_field" => "c_taxno",
                "display_query" => "c.vTAXNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Taxno",
                "lang_code" => "COMPANIES_TAXNO",
                "label_lang" => $this->lang->line('COMPANIES_TAXNO'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_company_phone_code" => array(
                "name" => "c_company_phone_code",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyPhoneCode",
                "source_field" => "c_company_phone_code",
                "display_query" => "c.vCompanyPhoneCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Phone Code",
                "lang_code" => "COMPANIES_COMPANY_PHONE_CODE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_PHONE_CODE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_company_phone" => array(
                "name" => "c_company_phone",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyPhone",
                "source_field" => "c_company_phone",
                "display_query" => "c.vCompanyPhone",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Phone",
                "lang_code" => "COMPANIES_COMPANY_PHONE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_PHONE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_company_alt_phone" => array(
                "name" => "c_company_alt_phone",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyAltPhone",
                "source_field" => "c_company_alt_phone",
                "display_query" => "c.vCompanyAltPhone",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Alt Phone",
                "lang_code" => "COMPANIES_COMPANY_ALT_PHONE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_ALT_PHONE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_company_email" => array(
                "name" => "c_company_email",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyEmail",
                "source_field" => "c_company_email",
                "display_query" => "c.vCompanyEmail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Email",
                "lang_code" => "COMPANIES_COMPANY_EMAIL",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_EMAIL'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            ),
            "c_company_website" => array(
                "name" => "c_company_website",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyWebsite",
                "source_field" => "c_company_website",
                "display_query" => "c.vCompanyWebsite",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_in" => "Both",
                "type" => "textbox",
                "align" => "left",
                "label" => "Company Website",
                "lang_code" => "COMPANIES_COMPANY_WEBSITE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_WEBSITE'),
                "width" => 50,
                "search" => "Yes",
                "export" => "Yes",
                "sortable" => "Yes",
                "addable" => "No",
                "editable" => "No",
                "viewedit" => "No",
            )
        );

        $config_arr = array();
        if (is_array($name) && count($name) > 0)
        {
            $name_cnt = count($name);
            for ($i = 0; $i < $name_cnt; $i++)
            {
                $config_arr[$name[$i]] = $list_config[$name[$i]];
            }
        }
        elseif ($name != "" && is_string($name))
        {
            $config_arr = $list_config[$name];
        }
        else
        {
            $config_arr = $list_config;
        }
        return $config_arr;
    }

    /**
     * getFormConfiguration method is used to get form configuration array.
     * @param string $name name is to get specific field configuration.
     * @return array $config_arr returns form configuration array.
     */
    public function getFormConfiguration($name = "")
    {
        $form_config = array(
            "c_company_name" => array(
                "name" => "c_company_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Name",
                "lang_code" => "COMPANIES_COMPANY_NAME",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_NAME')
            ),
            "c_parent_id" => array(
                "name" => "c_parent_id",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "iParentID",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Parent Id",
                "lang_code" => "COMPANIES_PARENT_ID",
                "label_lang" => $this->lang->line('COMPANIES_PARENT_ID')
            ),
            "c_business_trading_name" => array(
                "name" => "c_business_trading_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vBusinessTradingName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Business Trading Name",
                "lang_code" => "COMPANIES_BUSINESS_TRADING_NAME",
                "label_lang" => $this->lang->line('COMPANIES_BUSINESS_TRADING_NAME')
            ),
            "c_short_name" => array(
                "name" => "c_short_name",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vShortName",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Short Name",
                "lang_code" => "COMPANIES_SHORT_NAME",
                "label_lang" => $this->lang->line('COMPANIES_SHORT_NAME')
            ),
            "c_nature_of_business" => array(
                "name" => "c_nature_of_business",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vNatureOfBusiness",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Nature Of Business",
                "lang_code" => "COMPANIES_NATURE_OF_BUSINESS",
                "label_lang" => $this->lang->line('COMPANIES_NATURE_OF_BUSINESS')
            ),
            "c_business_type" => array(
                "name" => "c_business_type",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "eBusinessType",
                "entry_type" => "Table",
                "data_type" => "enum",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Business Type",
                "lang_code" => "COMPANIES_BUSINESS_TYPE",
                "label_lang" => $this->lang->line('COMPANIES_BUSINESS_TYPE')
            ),
            "c_taxno" => array(
                "name" => "c_taxno",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vTAXNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Taxno",
                "lang_code" => "COMPANIES_TAXNO",
                "label_lang" => $this->lang->line('COMPANIES_TAXNO')
            ),
            "c_company_phone_code" => array(
                "name" => "c_company_phone_code",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyPhoneCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Phone Code",
                "lang_code" => "COMPANIES_COMPANY_PHONE_CODE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_PHONE_CODE')
            ),
            "c_company_phone" => array(
                "name" => "c_company_phone",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyPhone",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Phone",
                "lang_code" => "COMPANIES_COMPANY_PHONE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_PHONE')
            ),
            "c_company_alt_phone_code" => array(
                "name" => "c_company_alt_phone_code",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyAltPhoneCode",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Alt Phone Code",
                "lang_code" => "COMPANIES_COMPANY_ALT_PHONE_CODE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_ALT_PHONE_CODE')
            ),
            "c_company_alt_phone" => array(
                "name" => "c_company_alt_phone",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyAltPhone",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Alt Phone",
                "lang_code" => "COMPANIES_COMPANY_ALT_PHONE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_ALT_PHONE')
            ),
            "c_company_email" => array(
                "name" => "c_company_email",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyEmail",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Email",
                "lang_code" => "COMPANIES_COMPANY_EMAIL",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_EMAIL')
            ),
            "c_company_website" => array(
                "name" => "c_company_website",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyWebsite",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Website",
                "lang_code" => "COMPANIES_COMPANY_WEBSITE",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_WEBSITE')
            ),
            "c_company_reg_no" => array(
                "name" => "c_company_reg_no",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vCompanyRegNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Company Reg No",
                "lang_code" => "COMPANIES_COMPANY_REG_NO",
                "label_lang" => $this->lang->line('COMPANIES_COMPANY_REG_NO')
            ),
            "c_insurance_no" => array(
                "name" => "c_insurance_no",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vInsuranceNo",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Insurance No",
                "lang_code" => "COMPANIES_INSURANCE_NO",
                "label_lang" => $this->lang->line('COMPANIES_INSURANCE_NO')
            ),
            "c_street" => array(
                "name" => "c_street",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "vStreet",
                "entry_type" => "Table",
                "data_type" => "varchar",
                "show_input" => "Both",
                "type" => "textbox",
                "label" => "Street",
                "lang_code" => "COMPANIES_STREET",
                "label_lang" => $this->lang->line('COMPANIES_STREET')
            ),
            "c_country_id" => array(
                "name" => "c_country_id",
                "table_name" => "companies",
                "table_alias" => "c",
                "field_name" => "iCountryId",
                "entry_type" => "Table",
                "data_type" => "int",
                "show_input" => "Both",
                "type" => "dropdown",
                "label" => "Country Id",
                "lang_code" => "COMPANIES_COUNTRY_ID",
                "label_lang" => $this->lang->line('COMPANIES_COUNTRY_ID'),
                "validation" => array(
                    "rules" => array(
                        "required" => TRUE,
                    ),
                    "messages" => array(
                        "required" => $this->general->replaceDisplayLabel($this->lang->line("GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46"),
                    "#FIELD#",
                    $this->lang->line('COMPANIES_COUNTRY_ID')
                ))
            )
        ), "c_state_id" => array("name" => "c_state_id",
        "table_name" => "companies",
        "table_alias" => "c",
        "field_name" => "iStateId",
        "entry_type" => "Table",
        "data_type" => "int",
        "show_input" => "Both",
        "type" => "dropdown",
        "label" => "State Id",
        "lang_code" => "COMPANIES_STATE_ID",
        "label_lang" => $this->lang->line('COMPANIES_STATE_ID')), "c_city" => array("name" => "c_city",
    "table_name" => "companies",
    "table_alias" => "c",
    "field_name" => "vCity",
    "entry_type" => "Table",
    "data_type" => "varchar",
    "show_input" => "Both",
    "type" => "textbox",
    "label" => "City",
    "lang_code" => "COMPANIES_CITY",
    "label_lang" => $this->lang->line('COMPANIES_CITY')), "c_zip_code" => array("name" => "c_zip_code",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "vZipCode",
"entry_type" => "Table",
"data_type" => "varchar",
"show_input" => "Both",
"type" => "textbox",
"label" => "Zip Code",
"lang_code" => "COMPANIES_ZIP_CODE",
"label_lang" => $this->lang->line('COMPANIES_ZIP_CODE')), "c_owner_name" => array("name" => "c_owner_name",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "vOwnerName",
"entry_type" => "Table",
"data_type" => "varchar",
"show_input" => "Both",
"type" => "textbox",
"label" => "Owner Name",
"lang_code" => "COMPANIES_OWNER_NAME",
"label_lang" => $this->lang->line('COMPANIES_OWNER_NAME')), "c_owner_email" => array("name" => "c_owner_email",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "vOwnerEmail",
"entry_type" => "Table",
"data_type" => "varchar",
"show_input" => "Both",
"type" => "textbox",
"label" => "Owner Email",
"lang_code" => "COMPANIES_OWNER_EMAIL",
"label_lang" => $this->lang->line('COMPANIES_OWNER_EMAIL')), "c_owner_phone" => array("name" => "c_owner_phone",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "vOwnerPhone",
"entry_type" => "Table",
"data_type" => "varchar",
"show_input" => "Both",
"type" => "textbox",
"label" => "Owner Phone",
"lang_code" => "COMPANIES_OWNER_PHONE",
"label_lang" => $this->lang->line('COMPANIES_OWNER_PHONE')), "c_added_date" => array("name" => "c_added_date",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "dtAddedDate",
"entry_type" => "Table",
"data_type" => "datetime",
"show_input" => "Both",
"type" => "date",
"label" => "Added Date",
"lang_code" => "COMPANIES_ADDED_DATE",
"label_lang" => $this->lang->line('COMPANIES_ADDED_DATE'),
"format" => 'Y-m-d'), "c_added_date" => array("name" => "c_added_date",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "iAddedDate",
"entry_type" => "Table",
"data_type" => "int",
"show_input" => "Both",
"type" => "dropdown",
"label" => "Added Date",
"lang_code" => "COMPANIES_ADDED_DATE",
"label_lang" => $this->lang->line('COMPANIES_ADDED_DATE')), "c_updat_date" => array("name" => "c_updat_date",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "dtUpdatDate",
"entry_type" => "Table",
"data_type" => "datetime",
"show_input" => "Both",
"type" => "date",
"label" => "Updat Date",
"lang_code" => "COMPANIES_UPDAT_DATE",
"label_lang" => $this->lang->line('COMPANIES_UPDAT_DATE'),
"format" => 'Y-m-d'), "c_status" => array("name" => "c_status",
"table_name" => "companies",
"table_alias" => "c",
"field_name" => "eStatus",
"entry_type" => "Table",
"data_type" => "enum",
"show_input" => "Both",
"type" => "dropdown",
"label" => "Status",
"lang_code" => "COMPANIES_STATUS",
"label_lang" => $this->lang->line('COMPANIES_STATUS'))
);

$config_arr = array();
if (is_array($name) && count($name) > 0)
{
    $name_cnt = count($name);
    for ($i = 0; $i < $name_cnt; $i++)
    {
        $config_arr[$name[$i]] = $form_config[$name[$i]];
    }
}
elseif ($name != "" && is_string($name))
{
    $config_arr = $form_config[$name];
}
else
{
    $config_arr = $form_config;
}
return $config_arr;
}

/**
     * checkRecordExists method is used to check duplication of records.
     * @param array $field_arr field_arr is having fields to check.
     * @param array $field_val field_val is having values of respective fields.
     * @param numeric $id id is to avoid current records.
     * @param string $mode mode is having either Add or Update.
     * @param string $con con is having either AND or OR.
     * @return boolean $exists returns either TRUE of FALSE.
     */
public function checkRecordExists($field_arr = array(), $field_val = array(), $id = '', $mode = '', $con = 'AND')
{
    $exists = FALSE;
    if (!is_array($field_arr) || count($field_arr) == 0)
    {
        return $exists;
    }
    foreach ((array) $field_arr as $key => $val)
    {
        $extra_cond_arr[] = $this->db->protect($this->table_alias.".".$field_arr[$key])." =  ".$this->db->escape(trim($field_val[$val]));
    }
    $extra_cond = "(".implode(" ".$con." ", $extra_cond_arr).")";
    if ($mode == "Add")
    {
        $data = $this->getData($extra_cond, "COUNT(*) AS tot");
        if ($data[0]['tot'] > 0)
        {
            $exists = TRUE;
        }
    }
    elseif ($mode == "Update")
    {
        $extra_cond = $this->db->protect($this->table_alias.".".$this->primary_key)." <> ".$this->db->escape($id)." AND ".$extra_cond;
        $data = $this->getData($extra_cond, "COUNT(*) AS tot");
        if ($data[0]['tot'] > 0)
        {
            $exists = TRUE;
        }
    }
    return $exists;
}

/**
     * getSwitchTo method is used to get switch to dropdown array.
     * @param string $extra_cond extra_cond is the query condition for getting filtered data.
     * @return array $switch_data returns data records array.
     */
public function getSwitchTo($extra_cond = '', $type = 'records', $limit = '')
{
    $switchto_fields = $this->switchto_fields;
    $switch_data = array();
    if (!is_array($switchto_fields) || count($switchto_fields) == 0)
    {
        if ($type == "count")
        {
            return count($switch_data);
        }
        else
        {
            return $switch_data;
        }
    }
    $fields_arr = array();
    $fields_arr[] = array(
        "field" => $this->table_alias.".".$this->primary_key." AS id",
    );
    $fields_arr[] = array(
        "field" => $this->db->concat($switchto_fields)." AS val",
        "escape" => TRUE,
    );
    if (is_array($this->switchto_options) && count($this->switchto_options) > 0)
    {
        foreach ($this->switchto_options as $option)
        {
            $fields_arr[] = array(
                "field" => $option,
                "escape" => TRUE,
            );
        }
    }
    if (trim($this->extra_cond) != "")
    {
        $extra_cond = (trim($extra_cond) != "") ? $extra_cond." AND ".$this->extra_cond : $this->extra_cond;
    }
    $switch_data = $this->getData($extra_cond, $fields_arr, "val ASC", "", $limit, "Yes");
    #echo $this->db->last_query();
    if ($type == "count")
    {
        return count($switch_data);
    }
    else
    {
        return $switch_data;
    }
}
}
