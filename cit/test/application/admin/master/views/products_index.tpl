<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-list-container">
    <%include file="products_index_strip.tpl"%>
    <!-- List View HTML Block -->
    <div id="layout_view_list2" class="perm-elem-hide">                        
        <div class="item-list">  
            <div class="thumbnail">{%pi_image%}</div>  
            <div class="item-info-mian">  
                <div class="item-info">  
                    <h3>{%p_product_name%}</h3>  
                    <div class="meta">{%p_description%}</div>  
                </div>  
                <div class="sale-info">  
                    <div class="sale-price">{%p_price%}</div>  
                    <div class="sale-status">{%p_status%}</div>    
                    <div class="sale-rating">{%rm_rate%}</div>    
                </div>  
            </div>  
        </div>  
        <style>  
        </style>
    </div>                
    <!-- Grid View HTML Block -->
    <div id="layout_grid_list2" class="perm-elem-hide">                        
        <div class="item-grid-main">  
            <repeat>  
                <div class="item-grid">  
                    <div class="thumbnail">{%pi_image%}</div>  
                    <div class="item-info">  
                        <h3><div class="item-title">{%p_product_name%}</div></h3>  
                    </div>  
                    <div class="sale-info">  
                        <div class="sale-price">{%p_price%}</div>  
                        <div class="sale-status">{%p_status%}</div>  
                        <div class="sale-rating">{%rm_rate%}</div>    
                    </div>  
                </div>  
            </repeat>  
        </div>  
        <style>  
            .cart-button  
            {  
                padding: 3px 6px;  
            }  
            .thumbnail  
            {  
                border: 1px solid #F6F6F6;  
                border-radius: 2px;  
                box-shadow: 0 1px 3px rgba(0,0,0,0.055);  
                display: inline-block;  
                line-height: 20px;  
                margin: 0 10px;  
                moz-border-radius: 4px;  
                moz-box-shadow: 0 1px 3px rgba(0,0,0,0.055);  
                moz-transition: all .2s ease-in-out;  
                o-transition: all .2s ease-in-out;  
                padding: 4px;  
                transition: all .2s ease-in-out;  
                vertical-align: top;  
                webkit-border-radius: 4px;  
                webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.055);  
                webkit-transition: all .2s ease-in-out;  
                width: 100px;  
            }  
            .item-info  
            {  
                border-right: dotted 1px #ccc;  
                display: inline-block;  
                margin-right: 2%;  
                min-height: 110px;  
                padding-right: 2%;  
                vertical-align: top;  
                width: 78%;  
            }  
            .sale-info  
            {  
                display: inline-block;  
                margin-top: 15px;  
            }  
            .sale-price  
            {  
                color: #D9534F;  
                font-size: 22px;  
                margin-bottom: 10px;  
                text-align: center;  
            }  
            .sale-status  
            {  
                color: #34D508;  
                text-align: center;  
            }  
            .item-grid-main  
            {  
                padding: 10px;  
            }  
            .item-grid  
            {  
                border: solid 1px #ccc;  
                border-radius: 5px;  
                box-sizing: border-box;  
                display: inline-block;  
                margin-bottom: 10px;  
                margin-right: 10px;  
                border-radius: 2px;  
                padding: 10px;  
                text-align: center;  
                width: 250px;  
                height:245px;  
                float:left;  
            }  
            .item-grid:hover  
            {  
                background: #f8f8f8;  
            }  
            .item-grid > .thumbnail  
            {  
                margin: 0;  
            }  
            .item-grid > .item-info  
            {  
                border: none;  
                margin: 0;  
                min-height: 30px;  
                padding: 10px 0;  
                text-align: center;  
                width: 100%;  
            }  
            .item-grid > .item-info .item-title  
            {  
                width: 100%;  
                display: inline-block;  
                overflow: hidden;  
                text-overflow: ellipsis;  
                white-space: nowrap;  
            }  
            .item-grid-main .item-grid:nth-of-type(5n)  
            {  
                margin-right:0;  
            }  
        </style>
    </div>
    <div class="<%$module_name%>" data-list-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content top-list-spacing">
                <div class="grid-data-container pad-calc-container">
                    <div class="top-list-tab-layout" id="top_list_grid_layout">
                    </div>
                    <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="left-search-td">
                                <span class="side-btn left-show <%$rl_theme_arr['grid_left_search']%>" id="grid_search_btn">
                                    <a class="side-menu-hide" href="javascript://" title="Hide Left Search Panel"><span class="icomoon-icon-arrow-left-7"></span></a>
                                </span>
                                <div id="left_search_panel" class="left-search-panel <%$rl_theme_arr['grid_left_search']%>">
                                    <!-- Global Search Textbox -->
                                    <div class="left-search-box"> 
                                        <input type="text" name="input_left_search" id="input_left_search" class="input-left-search" />
                                    </div>
                                    <div id="left_search_items">
                                        <%include file="products_search.tpl" %>
                                    </div>
                                </div>
                            </td>
                            <!-- Module Listing Block -->
                            <td id="grid_data_col" class="<%$rl_theme_arr['grid_search_toolbar']%>">
                                <div id="pager2"></div>
                                <table id="list2"></table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" name="selAllRows" value="" id="selAllRows" />
    </div>
</div>
<!-- Module Listing Javascript -->
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module'] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';

    el_grid_settings['index_page_url'] = '<%$mod_enc_url["index"]%>';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['print_url'] =  admin_url+'<%$mod_enc_url["print_listing"]%>?<%$extra_qstr%>';
        
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = $.parseJSON('<%$hide_admin_rec|@json_encode%>');;
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '1';
    el_grid_settings['hide_del_btn'] = '1';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['show_saved_search'] = 'No';
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = '<%$capabilities.hide_multi_select%>';
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    
    el_grid_settings['popup_add_form'] = 'Yes';
    el_grid_settings['popup_edit_form'] = 'Yes';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$edit_access%>';
    el_grid_settings['permit_view_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    el_grid_settings['permit_print_btn'] = '<%$print_access%>';
        
    el_grid_settings['serial_number'] = 'No';
    el_grid_settings['group_search'] = '';
    el_grid_settings['default_sort'] = 'pi_image';
    el_grid_settings['sort_order'] = 'asc';
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['auto_refresh'] = 'No';
    el_grid_settings['lazy_loading'] = 'No';
    el_grid_settings['print_rec'] = 'No';
    el_grid_settings['print_list'] = 'No';
    
    el_grid_settings['subgrid'] = '<%$capabilities.subgrid%>';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['rating_allow'] = 'Yes';
    el_grid_settings['global_filter'] = 'No';
    
    el_grid_settings['search_slug'] = '<%$search_slug%>';
    el_grid_settings['search_list'] = $.parseJSON('<%$search_preferences|@json_encode%>');
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [];
    el_grid_settings['buttons_grp'] = [];
    el_grid_settings['callbacks'] = {
        "after_data_load": "after_load_data"
    };
    el_grid_settings['message_arr'] = {
        "delete_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD')%>",
        "delete_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
        "status_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD_TO__C35STATUS_C35')%>",
        "status_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO__C35STATUS_C35_THIS_RECORDS_C63')%>",
    };
    
    js_col_name_json = [{
        "name": "pi_image",
        "label": "<%$list_config['pi_image']['label_lang']%>"
    },
    {
        "name": "p_product_name",
        "label": "<%$list_config['p_product_name']['label_lang']%>"
    },
    {
        "name": "b_brand",
        "label": "<%$list_config['b_brand']['label_lang']%>"
    },
    {
        "name": "m_name",
        "label": "<%$list_config['m_name']['label_lang']%>"
    },
    {
        "name": "c_category_name",
        "label": "<%$list_config['c_category_name']['label_lang']%>"
    },
    {
        "name": "p_price",
        "label": "<%$list_config['p_price']['label_lang']%>"
    },
    {
        "name": "p_retail_price",
        "label": "<%$list_config['p_retail_price']['label_lang']%>"
    },
    {
        "name": "rm_rate",
        "label": "<%$list_config['rm_rate']['label_lang']%>"
    },
    {
        "name": "p_status",
        "label": "<%$list_config['p_status']['label_lang']%>"
    },
    {
        "name": "p_description",
        "label": "<%$list_config['p_description']['label_lang']%>"
    }];
    
    js_col_model_json = [{
        "name": "pi_image",
        "index": "pi_image",
        "label": "<%$list_config['pi_image']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['pi_image']['width']%>",
        "search": <%if $list_config['pi_image']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['pi_image']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['pi_image']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['pi_image']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['pi_image']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['pi_image']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['pi_image']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": null,
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['pi_image']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": null,
            "placeholder": null,
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['pi_image']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "p_product_name",
        "index": "p_product_name",
        "label": "<%$list_config['p_product_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['p_product_name']['width']%>",
        "search": <%if $list_config['p_product_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['p_product_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['p_product_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['p_product_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['p_product_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['p_product_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['p_product_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRODUCT_NAME)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_product_name",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['p_product_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_product_name",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['p_product_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "b_brand",
        "index": "b_brand",
        "label": "<%$list_config['b_brand']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['b_brand']['width']%>",
        "search": <%if $list_config['b_brand']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['b_brand']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['b_brand']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['b_brand']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['b_brand']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['b_brand']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['b_brand']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_BRAND)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_brand_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['b_brand']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["b_brand"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=b_brand&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["b_brand"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["b_brand"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['b_brand']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["b_brand"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_brand_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=b_brand&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['b_brand']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["b_brand"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_BRAND')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['b_brand']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "m_name",
        "index": "m_name",
        "label": "<%$list_config['m_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['m_name']['width']%>",
        "search": <%if $list_config['m_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['m_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['m_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['m_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['m_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['m_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['m_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_MANUFACTURER_ID)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_manufacturer_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['m_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["m_name"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=m_name&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["m_name"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["m_name"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['m_name']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["m_name"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_manufacturer_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=m_name&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['m_name']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["m_name"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_MANUFACTURE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['m_name']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "c_category_name",
        "index": "c_category_name",
        "label": "<%$list_config['c_category_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_category_name']['width']%>",
        "search": <%if $list_config['c_category_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_category_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_category_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_category_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_category_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_category_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_category_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_CATEGORY)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_category_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['c_category_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["c_category_name"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_category_name&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["c_category_name"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["c_category_name"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['c_category_name']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_category_name"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_category_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_category_name&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['c_category_name']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_category_name"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_CATEGORY_NAME')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['c_category_name']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "p_price",
        "index": "p_price",
        "label": "<%$list_config['p_price']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['p_price']['width']%>",
        "search": <%if $list_config['p_price']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['p_price']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['p_price']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['p_price']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['p_price']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['p_price']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['p_price']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRICE)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_price",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['p_price']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_price",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['p_price']['default']%>",
        "filterSopt": "eq"
    },
    {
        "name": "p_retail_price",
        "index": "p_retail_price",
        "label": "<%$list_config['p_retail_price']['label_lang']%>",
        "labelClass": "header-align-right",
        "resizable": true,
        "width": "<%$list_config['p_retail_price']['width']%>",
        "search": <%if $list_config['p_retail_price']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['p_retail_price']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['p_retail_price']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['p_retail_price']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['p_retail_price']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['p_retail_price']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['p_retail_price']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "right",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_RETAIL_PRICE)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_retail_price",
                "autocomplete": "off"
            },
            "sopt": numSearchOpts,
            "searchhidden": <%if $list_config['p_retail_price']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_retail_price",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['p_retail_price']['default']%>",
        "filterSopt": "eq"
    },
    {
        "name": "rm_rate",
        "index": "rm_rate",
        "label": "<%$list_config['rm_rate']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['rm_rate']['width']%>",
        "search": <%if $list_config['rm_rate']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['rm_rate']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['rm_rate']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['rm_rate']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['rm_rate']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['rm_rate']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['rm_rate']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "rm_rate",
                "autocomplete": "off",
                "aria-raty-number": "5",
                "aria-raty-half": 1,
                "aria-raty-precision": 1,
                "aria-raty-icons": "stars",
                "aria-raty-hints": "1,2,3,4,5"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['rm_rate']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchRatingMasterEvent
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "rm_rate",
            "class": "inline-edit-row"
        },
        "ctrl_type": "rating_master",
        "default_value": "<%$list_config['rm_rate']['default']%>",
        "filterSopt": "in",
        "ratyallow": true,
        "ratyevents": {
            "raty": {
                "params": {
                    "number": "5",
                    "half": true,
                    "cancel": 0,
                    "readOnly": true,
                    "precision": true,
                    "cancelOff": "cancel-custom-off.png",
                    "cancelOn": "cancel-custom-on.png",
                    "starOff": "star-off.png",
                    "starOn": "star-on.png",
                    "starHalf": "star-half.png"
                },
                "hints": "1,2,3,4,5",
                "icons": "stars"
            }
        },
        "formatter": formatAdminModuleRatingLink,
        "unformat": unformatAdminModuleRatingLink
    },
    {
        "name": "p_status",
        "index": "p_status",
        "label": "<%$list_config['p_status']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['p_status']['width']%>",
        "search": <%if $list_config['p_status']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['p_status']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['p_status']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['p_status']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['p_status']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['p_status']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['p_status']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_STATUS)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_status",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['p_status']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["p_status"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_status&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["p_status"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["p_status"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['p_status']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["p_status"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_status",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_status&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['p_status']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["p_status"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_STATUS')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['p_status']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "p_description",
        "index": "p_description",
        "label": "<%$list_config['p_description']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['p_description']['width']%>",
        "search": <%if $list_config['p_description']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['p_description']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['p_description']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['p_description']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['p_description']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['p_description']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['p_description']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "textarea",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "products",
                "aria-unique-name": "p_description",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['p_description']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "products",
            "aria-unique-name": "p_description",
            "rows": "1",
            "placeholder": "",
            "dataInit": initEditGridElasticEvent,
            "class": "inline-edit-row inline-textarea-edit "
        },
        "ctrl_type": "textarea",
        "default_value": "<%$list_config['p_description']['default']%>",
        "filterSopt": "bw"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%$this->js->add_js("admin/custom/after_load_data.js")%>
<%$this->css->add_css("custom/custom_extended.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 