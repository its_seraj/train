<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="suppliers_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="suppliers" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="suppliers" class="frm-module-block frm-elem-block frm-thclm-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <input type="hidden" name="c_company_phone_code" id="c_company_phone_code" value="<%$data['c_company_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                        <input type="hidden" name="c_company_alt_phone_code" id="c_company_alt_phone_code" value="<%$data['c_company_alt_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                        <input type="hidden" name="c_added_date" id="c_added_date" value="<%$data['c_added_date']%>"  class='ignore-valid ' />
                        <input type="hidden" name="c_updat_date" id="c_updat_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_updat_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                        <input type="hidden" name="c_status" id="c_status" value="<%$data['c_status']%>"  class='ignore-valid ' />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('SUPPLIERS_SUPPLIERS')%></h4></div>
                                    <div class="content two-column-block tab-focus-parent <%$rl_theme_arr['frm_twclm_label_align']%>">
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_1"> <div class="form-right-div form-static-div"><h3 class="inner-title">Company Information</h3></div></div><div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_2"> <div class="form-right-div form-static-div"><h3 class="inner-title">Owner Information</h3></div></div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_c_company_logo"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['c_company_logo']['label_lang']%> <em>*</em> 
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <div  class='btn-uploadify frm-size-full_width' >
                                                        <input type="hidden" value="<%$data['c_company_logo']%>" name="old_c_company_logo" id="old_c_company_logo" />
                                                        <input type="hidden" value="<%$data['c_company_logo']%>" name="c_company_logo" id="c_company_logo"  aria-extensions="gif,png,jpg,jpeg,jpe,bmp,ico" aria-valid-size="<%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB"/>
                                                        <input type="hidden" value="<%$data['c_company_logo']%>" name="temp_c_company_logo" id="temp_c_company_logo"  />
                                                        <div id="upload_drop_zone_c_company_logo" class="upload-drop-zone"></div>
                                                        <div class="uploader upload-src-zone">
                                                            <input type="file" name="uploadify_c_company_logo" id="uploadify_c_company_logo" title="<%$this->lang->line('SUPPLIERS_COMPANY_LOGO')%>" />
                                                            <span class="filename" id="preview_c_company_logo">
                                                                <%if $data['c_company_logo'] neq ''%>
                                                                    <%$data['c_company_logo']%>
                                                                <%else%>
                                                                    <%$this->lang->line('GENERIC_DROP_FILES_HERE_OR_CLICK_TO_UPLOAD')%>
                                                                <%/if%>
                                                            </span>
                                                            <span class="action">Choose File</span>
                                                        </div>
                                                    </div>
                                                    <div class='upload-image-btn'>
                                                        <%$img_html['c_company_logo']%>
                                                    </div>
                                                    <span class="input-comment">
                                                        <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif, png, jpg, jpeg, jpe, bmp, ico.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB."><span class="icomoon-icon-help"></span></a>
                                                    </span>
                                                    <div class='clear upload-progress' id='progress_c_company_logo'>
                                                        <div class='upload-progress-bar progress progress-striped active'>
                                                            <div class='bar' id='practive_c_company_logo'></div>
                                                        </div>
                                                        <div class='upload-cancel-div'><a class='upload-cancel' href='javascript://'>Cancel</a></div>
                                                        <div class='clear'></div>
                                                    </div>
                                                    <div class='clear'></div>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='c_company_logoErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element " id="cc_sh_c_owner_name"> 
                                                <label class="form-label span3  frm-labels-merge">
                                                    <%$form_config['c_owner_name']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div   frm-controls-merge">
                                                    <div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;' id="cc_md_c_owner_name">
                                                        <input type="text" placeholder="<%$this->lang->line('GENERIC_FIRST_NAME')%>" value="<%$data['c_owner_name']|@htmlentities%>" name="c_owner_name" id="c_owner_name" title="<%$this->lang->line('SUPPLIERS_OWNER_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-tiny'  />
                                                    </div>
                                                    <div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;' id="cc_md_sys_custom_field_1">
                                                        <input type="text" placeholder="<%$this->lang->line('GENERIC_MIDDLE_NAME')%>" value="<%$data['sys_custom_field_1']|@htmlentities%>" name="sys_custom_field_1" id="sys_custom_field_1" title="<%$this->lang->line('SUPPLIERS_MIDDLE_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-tiny'  style='margin-right: 10px;'  />
                                                    </div>
                                                    <div class="frm-merge-ctrl-block frm-merge-ctrl-20" style='width:20%!important;' id="cc_md_sys_custom_field_2">
                                                        <input type="text" placeholder="<%$this->lang->line('GENERIC_LAST_NAME')%>" value="<%$data['sys_custom_field_2']|@htmlentities%>" name="sys_custom_field_2" id="sys_custom_field_2" title="<%$this->lang->line('SUPPLIERS_LAST_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-tiny'  />
                                                    </div>
                                                </div>
                                                <div class="error-msg-form  frm-errormsg-merge"><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='c_owner_nameErr'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_1Err'></label></div><div class="frm-merge-eror-block frm-merge-ctrl-20" style='width:20%!important;'><label class='error' id='sys_custom_field_2Err'></label></div></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_c_company_name"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['c_company_name']['label_lang']%> <em>*</em> 
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['c_company_name']|@htmlentities%>" name="c_company_name" id="c_company_name" title="<%$this->lang->line('SUPPLIERS_COMPANY_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-full_width'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='c_company_nameErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element " id="cc_sh_sys_custom_field_3"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['sys_custom_field_3']['label_lang']%>
                                                </label> 
                                                <div class="form-right-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%> ">
                                                    <%if $mode eq "Update"%>
                                                        <input type="hidden" class="ignore-valid" name="sys_custom_field_3" id="sys_custom_field_3" value="<%$data['sys_custom_field_3']|@htmlentities%>" />
                                                        <span class="frm-data-label">
                                                            <strong>
                                                                <%if $data['sys_custom_field_3'] neq ""%>
                                                                    <%$data['sys_custom_field_3']%>
                                                                <%else%>
                                                                <%/if%>
                                                            </strong>
                                                        </span>
                                                    <%else%>
                                                        <input type="text" placeholder="" value="<%$data['sys_custom_field_3']|@htmlentities%>" name="sys_custom_field_3" id="sys_custom_field_3" title="<%$this->lang->line('SUPPLIERS_FULL_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  readonly='readonly'  />
                                                    <%/if%>
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='sys_custom_field_3Err'></label></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element " id="cc_sh_c_short_name"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['c_short_name']['label_lang']%> <em>*</em> 
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['c_short_name']|@htmlentities%>" name="c_short_name" id="c_short_name" title="<%$this->lang->line('SUPPLIERS_SHORT_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-full_width'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='c_short_nameErr'></label></div>
                                            </div>
                                            <div class="two-block-view tab-focus-element " id="cc_sh_c_owner_email"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['c_owner_email']['label_lang']%> <em>*</em> 
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['c_owner_email']|@htmlentities%>" name="c_owner_email" id="c_owner_email" title="<%$this->lang->line('SUPPLIERS_OWNER_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='c_owner_emailErr'></label></div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid tab-focus-child">
                                            <div class="two-block-view tab-focus-element company_short_name" id="cc_sh_c_company_code"> 
                                                <label class="form-label span3 ">
                                                    <%$form_config['c_company_code']['label_lang']%> <em>*</em> 
                                                </label> 
                                                <div class="form-right-div  ">
                                                    <input type="text" placeholder="" value="<%$data['c_company_code']|@htmlentities%>" name="c_company_code" id="c_company_code" title="<%$this->lang->line('SUPPLIERS_COMPANY_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-full_width'  />
                                                </div>
                                                <div class="error-msg-form "><label class='error' id='c_company_codeErr'></label></div>
                                                </div><div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_3"> <div class="form-right-div form-static-div"><h3 class="inner-title">Contact Information</h3></div></div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                <div class="two-block-view tab-focus-element " id="cc_sh_c_company_email"> 
                                                    <label class="form-label span3 ">
                                                        <%$form_config['c_company_email']['label_lang']%> <em>*</em> 
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <input type="text" placeholder="" value="<%$data['c_company_email']|@htmlentities%>" name="c_company_email" id="c_company_email" title="<%$this->lang->line('SUPPLIERS_COMPANY_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='c_company_emailErr'></label></div>
                                                </div>
                                                <div class="two-block-view tab-focus-element " id="cc_sh_c_owner_phone"> 
                                                    <label class="form-label span3 ">
                                                        <%$form_config['c_owner_phone']['label_lang']%> <em>*</em> 
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <input type="hidden" value="<%$data['c_owner_phone']%>" name="c_owner_phone" id="c_owner_phone" />
                                                        <input type="text" value="<%$data['c_owner_phone']%>" name="intl_c_owner_phone" id="intl_c_owner_phone" title="<%$this->lang->line('SUPPLIERS_OWNER_PHONE')%>"  data-ctrl-type='phone_number'  class='frm-phone-number frm-size-full_width'  style='width:auto;' />
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='c_owner_phoneErr'></label></div>
                                                </div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                <div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_7"> <div class="form-right-div form-static-div"><h3 class="inner-title">Company Contact Information</h3></div></div><div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_5"> <div class="form-right-div form-static-div"><h3 class="inner-title">Commission Information</h3></div></div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                <div class="two-block-view tab-focus-element " id="cc_sh_c_company_phone"> 
                                                    <label class="form-label span3 ">
                                                        <%$form_config['c_company_phone']['label_lang']%> <em>*</em> 
                                                    </label> 
                                                    <div class="form-right-div  ">
                                                        <input type="hidden" value="<%$data['c_company_phone']%>" name="c_company_phone" id="c_company_phone" />
                                                        <input type="text" value="<%$data['c_company_phone']%>" name="intl_c_company_phone" id="intl_c_company_phone" title="<%$this->lang->line('SUPPLIERS_COMPANY_PHONE')%>"  data-ctrl-type='phone_number'  class='frm-phone-number frm-size-full_width'  style='width:auto;' />
                                                    </div>
                                                    <div class="error-msg-form "><label class='error' id='c_company_phoneErr'></label></div>
                                                    </div><div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_4"> <div class="form-right-div form-static-div"><h3 class="inner-title">Other Information</h3></div></div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_company_alt_phone"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_company_alt_phone']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="hidden" value="<%$data['c_company_alt_phone']%>" name="c_company_alt_phone" id="c_company_alt_phone" />
                                                            <input type="text" value="<%$data['c_company_alt_phone']%>" name="intl_c_company_alt_phone" id="intl_c_company_alt_phone" title="<%$this->lang->line('SUPPLIERS_COMPANY_ALT_PHONE')%>"  data-ctrl-type='phone_number'  class='frm-phone-number frm-size-full_width'  style='width:auto;' />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_company_alt_phoneErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_6"> <div class="form-right-div form-static-div"><h3 class="inner-title">Company Address Information</h3></div></div><div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_street"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_street']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_street']|@htmlentities%>" name="c_street" id="c_street" title="<%$this->lang->line('SUPPLIERS_STREET')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_streetErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_country_id"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_country_id']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <%assign var="opt_selected" value=$data['c_country_id']%>
                                                            <%$this->dropdown->display("c_country_id","c_country_id","  title='<%$this->lang->line('SUPPLIERS_COUNTRY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_COUNTRY_ID')%>'  ", "|||", "", $opt_selected,"c_country_id")%>
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_country_idErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_state_id"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_state_id']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <%assign var="opt_selected" value=$data['c_state_id']%>
                                                            <%$this->dropdown->display("c_state_id","c_state_id","  title='<%$this->lang->line('SUPPLIERS_STATE_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-full_width'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_STATE_ID')%>'  ", "|||", "", $opt_selected,"c_state_id")%>
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_state_idErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_city"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_city']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_city']|@htmlentities%>" name="c_city" id="c_city" title="<%$this->lang->line('SUPPLIERS_CITY')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_cityErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_zip_code"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_zip_code']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_zip_code']|@htmlentities%>" name="c_zip_code" id="c_zip_code" title="<%$this->lang->line('SUPPLIERS_ZIP_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_zip_codeErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_sys_static_field_8"> <div class="form-right-div form-static-div"><h3 class="inner-title">Company Order Information</h3></div></div><div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_nature_of_business"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_nature_of_business']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_nature_of_business']|@htmlentities%>" name="c_nature_of_business" id="c_nature_of_business" title="<%$this->lang->line('SUPPLIERS_NATURE_OF_BUSINESS')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_nature_of_businessErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_company_reg_no"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_company_reg_no']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_company_reg_no']|@htmlentities%>" name="c_company_reg_no" id="c_company_reg_no" title="<%$this->lang->line('SUPPLIERS_COMPANY_REG_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_company_reg_noErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_taxno"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_taxno']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_taxno']|@htmlentities%>" name="c_taxno" id="c_taxno" title="<%$this->lang->line('SUPPLIERS_TAXNO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_taxnoErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_insurance_no"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_insurance_no']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_insurance_no']|@htmlentities%>" name="c_insurance_no" id="c_insurance_no" title="<%$this->lang->line('SUPPLIERS_INSURANCE_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_insurance_noErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_business_trading_name"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_business_trading_name']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_business_trading_name']|@htmlentities%>" name="c_business_trading_name" id="c_business_trading_name" title="<%$this->lang->line('SUPPLIERS_BUSINESS_TRADING_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_business_trading_nameErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_business_type"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_business_type']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <%assign var="opt_selected" value=$data['c_business_type']%>
                                                            <%$this->dropdown->display("c_business_type","c_business_type","  title='<%$this->lang->line('SUPPLIERS_BUSINESS_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_BUSINESS_TYPE')%>'  ", "|||", "", $opt_selected,"c_business_type")%>
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_business_typeErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_parent_id"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_parent_id']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <%assign var="opt_selected" value=$data['c_parent_id']%>
                                                            <%$this->dropdown->display("c_parent_id","c_parent_id","  title='<%$this->lang->line('SUPPLIERS_PARENT_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_PARENT_ID')%>'  ", "|||", "", $opt_selected,"c_parent_id")%>
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_parent_idErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                                <div class="column-view-parent form-row row-fluid tab-focus-child">
                                                    <div class="two-block-view tab-focus-element " id="cc_sh_c_company_website"> 
                                                        <label class="form-label span3 ">
                                                            <%$form_config['c_company_website']['label_lang']%>
                                                        </label> 
                                                        <div class="form-right-div  ">
                                                            <input type="text" placeholder="" value="<%$data['c_company_website']|@htmlentities%>" name="c_company_website" id="c_company_website" title="<%$this->lang->line('SUPPLIERS_COMPANY_WEBSITE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                                        </div>
                                                        <div class="error-msg-form "><label class='error' id='c_company_websiteErr'></label></div>
                                                    </div>
                                                    <div class="two-block-view tab-focus-element">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="frm-bot-btn <%$rl_theme_arr['frm_twclm_action_bar']%> <%$rl_theme_arr['frm_twclm_action_btn']%> popup-footer">
                                        <%if $rl_theme_arr['frm_twclm_ctrls_view'] eq 'No'%>
                                            <%assign var='rm_ctrl_directions' value=true%>
                                        <%/if%>
                                        <%include file="suppliers_add_buttons.tpl"%>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/suppliers_add_js.js')%>

<%$this->js->add_js("admin/custom/extended.js")%>
<%$this->css->add_css("custom/Suppliers_extended.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.suppliers.callEvents();
<%/javascript%>