<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-list-container">
    <%include file="suppliers_index_strip.tpl"%>
    <div class="<%$module_name%>" data-list-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing box gradient">
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content top-list-spacing">
                <div class="grid-data-container pad-calc-container">
                    <div class="top-list-tab-layout" id="top_list_grid_layout">
                    </div>
                    <table class="grid-table-view " width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <!-- Module Listing Block -->
                            <td id="grid_data_col" class="<%$rl_theme_arr['grid_search_toolbar']%>">
                                <div id="pager2"></div>
                                <table id="list2"></table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" name="selAllRows" value="" id="selAllRows" />
    </div>
</div>
<!-- Module Listing Javascript -->
<%javascript%>
    $.jgrid.no_legacy_api = true; $.jgrid.useJSON = true;
    var el_grid_settings = {}, js_col_model_json = {}, js_col_name_json = {}; 
                    
    el_grid_settings['module_name'] = '<%$module_name%>';
    el_grid_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_grid_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_grid_settings['enc_location'] = '<%$enc_loc_module%>';
    el_grid_settings['par_module'] = '<%$this->general->getAdminEncodeURL($parMod)%>';
    el_grid_settings['par_data'] = '<%$this->general->getAdminEncodeURL($parID)%>';
    el_grid_settings['par_field'] = '<%$parField%>';
    el_grid_settings['par_type'] = 'parent';

    el_grid_settings['index_page_url'] = '<%$mod_enc_url["index"]%>';
    el_grid_settings['add_page_url'] = '<%$mod_enc_url["add"]%>'; 
    el_grid_settings['edit_page_url'] =  admin_url+'<%$mod_enc_url["inline_edit_action"]%>?<%$extra_qstr%>';
    el_grid_settings['listing_url'] = admin_url+'<%$mod_enc_url["listing"]%>?<%$extra_qstr%>';
    el_grid_settings['export_url'] =  admin_url+'<%$mod_enc_url["export"]%>?<%$extra_qstr%>';
    el_grid_settings['print_url'] =  admin_url+'<%$mod_enc_url["print_listing"]%>?<%$extra_qstr%>';
        
    el_grid_settings['search_refresh_url'] = admin_url+'<%$mod_enc_url["get_left_search_content"]%>?<%$extra_qstr%>';
    el_grid_settings['search_autocomp_url'] = admin_url+'<%$mod_enc_url["get_search_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['auto_complete_url'] = admin_url+'<%$mod_enc_url["get_token_auto_complete"]%>?<%$extra_qstr%>';
    el_grid_settings['subgrid_listing_url'] =  admin_url+'<%$mod_enc_url["get_subgrid_block"]%>?<%$extra_qstr%>';
    el_grid_settings['jparent_switchto_url'] = admin_url+'<%$parent_switch_cit["url"]%>?<%$extra_qstr%>';
    
    el_grid_settings['admin_rec_arr'] = $.parseJSON('<%$hide_admin_rec|@json_encode%>');;
    el_grid_settings['status_arr'] = $.parseJSON('<%$status_array|@json_encode%>');
    el_grid_settings['status_lang_arr'] = $.parseJSON('<%$status_label|@json_encode%>');
                
    el_grid_settings['hide_add_btn'] = '1';
    el_grid_settings['hide_del_btn'] = '1';
    el_grid_settings['hide_status_btn'] = '1';
    el_grid_settings['hide_export_btn'] = '1';
    el_grid_settings['hide_columns_btn'] = 'No';
    
    el_grid_settings['show_saved_search'] = 'No';
    el_grid_settings['hide_advance_search'] = 'No';
    el_grid_settings['hide_search_tool'] = 'No';
    el_grid_settings['hide_multi_select'] = '<%$capabilities.hide_multi_select%>';
    el_grid_settings['hide_paging_btn'] = 'No';
    el_grid_settings['hide_refresh_btn'] = 'No';
    
    el_grid_settings['popup_add_form'] = 'No';
    el_grid_settings['popup_edit_form'] = 'No';
    el_grid_settings['popup_add_size'] = ['75%', '75%'];
    el_grid_settings['popup_edit_size'] = ['75%', '75%'];
    
    el_grid_settings['permit_add_btn'] = '<%$add_access%>';
    el_grid_settings['permit_del_btn'] = '<%$del_access%>';
    el_grid_settings['permit_edit_btn'] = '<%$edit_access%>';
    el_grid_settings['permit_view_btn'] = '<%$view_access%>';
    el_grid_settings['permit_expo_btn'] = '<%$expo_access%>';
    el_grid_settings['permit_print_btn'] = '<%$print_access%>';
        
    el_grid_settings['serial_number'] = 'No';
    el_grid_settings['group_search'] = '';
    el_grid_settings['default_sort'] = 'c_company_name';
    el_grid_settings['sort_order'] = 'asc';
    el_grid_settings['footer_row'] = 'No';
    el_grid_settings['grouping'] = 'No';
    el_grid_settings['group_attr'] = {};
    
    el_grid_settings['inline_add'] = 'No';
    el_grid_settings['rec_position'] = 'Top';
    el_grid_settings['auto_width'] = 'Yes';
    el_grid_settings['auto_refresh'] = 'No';
    el_grid_settings['lazy_loading'] = 'No';
    el_grid_settings['print_rec'] = 'No';
    el_grid_settings['print_list'] = 'No';
    
    el_grid_settings['subgrid'] = '<%$capabilities.subgrid%>';
    el_grid_settings['colgrid'] = 'No';
    el_grid_settings['listview'] = 'list';
    el_grid_settings['rating_allow'] = 'No';
    el_grid_settings['global_filter'] = 'No';
    
    el_grid_settings['search_slug'] = '<%$search_slug%>';
    el_grid_settings['search_list'] = $.parseJSON('<%$search_preferences|@json_encode%>');
    el_grid_settings['filters_arr'] = $.parseJSON('<%$default_filters|@json_encode%>');
    el_grid_settings['top_filter'] = [];
    el_grid_settings['buttons_arr'] = [];
    el_grid_settings['buttons_grp'] = [];
    el_grid_settings['callbacks'] = [];
    el_grid_settings['message_arr'] = {
        "delete_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD')%>",
        "delete_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
        "status_alert" : "<%$this->general->processMessageLabel('ACTION_PLEASE_SELECT_ANY_RECORD_TO__C35STATUS_C35')%>",
        "status_popup" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO__C35STATUS_C35_THIS_RECORDS_C63')%>",
    };
    
    js_col_name_json = [{
        "name": "c_company_name",
        "label": "<%$list_config['c_company_name']['label_lang']%>"
    },
    {
        "name": "c_parent_id",
        "label": "<%$list_config['c_parent_id']['label_lang']%>"
    },
    {
        "name": "c_business_trading_name",
        "label": "<%$list_config['c_business_trading_name']['label_lang']%>"
    },
    {
        "name": "c_short_name",
        "label": "<%$list_config['c_short_name']['label_lang']%>"
    },
    {
        "name": "c_nature_of_business",
        "label": "<%$list_config['c_nature_of_business']['label_lang']%>"
    },
    {
        "name": "c_business_type",
        "label": "<%$list_config['c_business_type']['label_lang']%>"
    },
    {
        "name": "c_taxno",
        "label": "<%$list_config['c_taxno']['label_lang']%>"
    },
    {
        "name": "c_company_phone_code",
        "label": "<%$list_config['c_company_phone_code']['label_lang']%>"
    },
    {
        "name": "c_company_phone",
        "label": "<%$list_config['c_company_phone']['label_lang']%>"
    },
    {
        "name": "c_company_alt_phone_code",
        "label": "<%$list_config['c_company_alt_phone_code']['label_lang']%>"
    },
    {
        "name": "c_company_alt_phone",
        "label": "<%$list_config['c_company_alt_phone']['label_lang']%>"
    },
    {
        "name": "c_company_email",
        "label": "<%$list_config['c_company_email']['label_lang']%>"
    },
    {
        "name": "c_company_website",
        "label": "<%$list_config['c_company_website']['label_lang']%>"
    },
    {
        "name": "c_company_reg_no",
        "label": "<%$list_config['c_company_reg_no']['label_lang']%>"
    },
    {
        "name": "c_insurance_no",
        "label": "<%$list_config['c_insurance_no']['label_lang']%>"
    },
    {
        "name": "c_street",
        "label": "<%$list_config['c_street']['label_lang']%>"
    },
    {
        "name": "c_city",
        "label": "<%$list_config['c_city']['label_lang']%>"
    },
    {
        "name": "c_zip_code",
        "label": "<%$list_config['c_zip_code']['label_lang']%>"
    },
    {
        "name": "c_owner_name",
        "label": "<%$list_config['c_owner_name']['label_lang']%>"
    },
    {
        "name": "c_owner_email",
        "label": "<%$list_config['c_owner_email']['label_lang']%>"
    },
    {
        "name": "c_owner_phone",
        "label": "<%$list_config['c_owner_phone']['label_lang']%>"
    },
    {
        "name": "c_added_date",
        "label": "<%$list_config['c_added_date']['label_lang']%>"
    },
    {
        "name": "c_added_date",
        "label": "<%$list_config['c_added_date']['label_lang']%>"
    },
    {
        "name": "c_updat_date",
        "label": "<%$list_config['c_updat_date']['label_lang']%>"
    },
    {
        "name": "c_status",
        "label": "<%$list_config['c_status']['label_lang']%>"
    },
    {
        "name": "mc_country",
        "label": "<%$list_config['mc_country']['label_lang']%>"
    },
    {
        "name": "ms_state",
        "label": "<%$list_config['ms_state']['label_lang']%>"
    }];
    
    js_col_model_json = [{
        "name": "c_company_name",
        "index": "c_company_name",
        "label": "<%$list_config['c_company_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_name']['width']%>",
        "search": <%if $list_config['c_company_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_COMPANY_NAME)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_name",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_name",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_name']['default']%>",
        "filterSopt": "bw",
        "formatter": formatAdminModuleEditLink,
        "unformat": unformatAdminModuleEditLink
    },
    {
        "name": "c_parent_id",
        "index": "c_parent_id",
        "label": "<%$list_config['c_parent_id']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['c_parent_id']['width']%>",
        "search": <%if $list_config['c_parent_id']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_parent_id']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_parent_id']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_parent_id']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_parent_id']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_parent_id']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_parent_id']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_parent_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['c_parent_id']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["c_parent_id"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_parent_id&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["c_parent_id"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["c_parent_id"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['c_parent_id']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_parent_id"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_parent_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_parent_id&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['c_parent_id']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_parent_id"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_PARENT_ID')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['c_parent_id']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "c_business_trading_name",
        "index": "c_business_trading_name",
        "label": "<%$list_config['c_business_trading_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_business_trading_name']['width']%>",
        "search": <%if $list_config['c_business_trading_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_business_trading_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_business_trading_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_business_trading_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_business_trading_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_business_trading_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_business_trading_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_business_trading_name",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_business_trading_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_business_trading_name",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_business_trading_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_short_name",
        "index": "c_short_name",
        "label": "<%$list_config['c_short_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_short_name']['width']%>",
        "search": <%if $list_config['c_short_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_short_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_short_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_short_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_short_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_short_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_short_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_SHORT_NAME)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_short_name",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_short_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_short_name",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_short_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_nature_of_business",
        "index": "c_nature_of_business",
        "label": "<%$list_config['c_nature_of_business']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_nature_of_business']['width']%>",
        "search": <%if $list_config['c_nature_of_business']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_nature_of_business']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_nature_of_business']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_nature_of_business']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_nature_of_business']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_nature_of_business']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_nature_of_business']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_nature_of_business",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_nature_of_business']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_nature_of_business",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_nature_of_business']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_business_type",
        "index": "c_business_type",
        "label": "<%$list_config['c_business_type']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['c_business_type']['width']%>",
        "search": <%if $list_config['c_business_type']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_business_type']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_business_type']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_business_type']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_business_type']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_business_type']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_business_type']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_business_type",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['c_business_type']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["c_business_type"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_business_type&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["c_business_type"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["c_business_type"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['c_business_type']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_business_type"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_business_type",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_business_type&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['c_business_type']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_business_type"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_BUSINESS_TYPE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['c_business_type']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "c_taxno",
        "index": "c_taxno",
        "label": "<%$list_config['c_taxno']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_taxno']['width']%>",
        "search": <%if $list_config['c_taxno']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_taxno']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_taxno']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_taxno']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_taxno']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_taxno']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_taxno']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_taxno",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_taxno']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_taxno",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_taxno']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_company_phone_code",
        "index": "c_company_phone_code",
        "label": "<%$list_config['c_company_phone_code']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_phone_code']['width']%>",
        "search": <%if $list_config['c_company_phone_code']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_phone_code']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_phone_code']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_phone_code']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_phone_code']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_phone_code']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_phone_code']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_phone_code",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_phone_code']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_phone_code",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_phone_code']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_company_phone",
        "index": "c_company_phone",
        "label": "<%$list_config['c_company_phone']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_phone']['width']%>",
        "search": <%if $list_config['c_company_phone']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_phone']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_phone']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_phone']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_phone']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_phone']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_phone']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_COMPANY_PHONE)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_phone",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_phone']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_phone",
            "aria-phone-format": "<%$this->general->getAdminPHPFormats('phone')%>",
            "dataInit": initEditGridMaskingEvent,
            "class": "inline-edit-row phoneNumber"
        },
        "ctrl_type": "phone_number",
        "default_value": "<%$list_config['c_company_phone']['default']%>",
        "filterSopt": "cn"
    },
    {
        "name": "c_company_alt_phone_code",
        "index": "c_company_alt_phone_code",
        "label": "<%$list_config['c_company_alt_phone_code']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_alt_phone_code']['width']%>",
        "search": <%if $list_config['c_company_alt_phone_code']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_alt_phone_code']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_alt_phone_code']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_alt_phone_code']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_alt_phone_code']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_alt_phone_code']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_alt_phone_code']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_alt_phone_code",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_alt_phone_code']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_alt_phone_code",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_alt_phone_code']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_company_alt_phone",
        "index": "c_company_alt_phone",
        "label": "<%$list_config['c_company_alt_phone']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_alt_phone']['width']%>",
        "search": <%if $list_config['c_company_alt_phone']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_alt_phone']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_alt_phone']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_alt_phone']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_alt_phone']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_alt_phone']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_alt_phone']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_alt_phone",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_alt_phone']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_alt_phone",
            "aria-phone-format": "<%$this->general->getAdminPHPFormats('phone')%>",
            "dataInit": initEditGridMaskingEvent,
            "class": "inline-edit-row phoneNumber"
        },
        "ctrl_type": "phone_number",
        "default_value": "<%$list_config['c_company_alt_phone']['default']%>",
        "filterSopt": "cn"
    },
    {
        "name": "c_company_email",
        "index": "c_company_email",
        "label": "<%$list_config['c_company_email']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_email']['width']%>",
        "search": <%if $list_config['c_company_email']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_email']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_email']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_email']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_email']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_email']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_email']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "email": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_COMPANY_EMAIL)
                },
                "email": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_COMPANY_EMAIL)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_email",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_email']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_email",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_email']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_company_website",
        "index": "c_company_website",
        "label": "<%$list_config['c_company_website']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_website']['width']%>",
        "search": <%if $list_config['c_company_website']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_website']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_website']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_website']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_website']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_website']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_website']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_website",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_website']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_website",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_website']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_company_reg_no",
        "index": "c_company_reg_no",
        "label": "<%$list_config['c_company_reg_no']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_company_reg_no']['width']%>",
        "search": <%if $list_config['c_company_reg_no']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_company_reg_no']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_company_reg_no']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_company_reg_no']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_company_reg_no']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_company_reg_no']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_company_reg_no']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_company_reg_no",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_company_reg_no']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_company_reg_no",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_company_reg_no']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_insurance_no",
        "index": "c_insurance_no",
        "label": "<%$list_config['c_insurance_no']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_insurance_no']['width']%>",
        "search": <%if $list_config['c_insurance_no']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_insurance_no']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_insurance_no']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_insurance_no']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_insurance_no']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_insurance_no']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_insurance_no']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_insurance_no",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_insurance_no']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_insurance_no",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_insurance_no']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_street",
        "index": "c_street",
        "label": "<%$list_config['c_street']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_street']['width']%>",
        "search": <%if $list_config['c_street']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_street']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_street']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_street']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_street']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_street']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_street']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_street",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_street']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_street",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_street']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_city",
        "index": "c_city",
        "label": "<%$list_config['c_city']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_city']['width']%>",
        "search": <%if $list_config['c_city']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_city']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_city']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_city']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_city']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_city']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_city']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_city",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_city']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_city",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_city']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_zip_code",
        "index": "c_zip_code",
        "label": "<%$list_config['c_zip_code']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_zip_code']['width']%>",
        "search": <%if $list_config['c_zip_code']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_zip_code']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_zip_code']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_zip_code']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_zip_code']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_zip_code']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_zip_code']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_zip_code",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_zip_code']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_zip_code",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_zip_code']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_owner_name",
        "index": "c_owner_name",
        "label": "<%$list_config['c_owner_name']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_owner_name']['width']%>",
        "search": <%if $list_config['c_owner_name']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_owner_name']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_owner_name']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_owner_name']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_owner_name']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_owner_name']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_owner_name']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_owner_name",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_owner_name']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_owner_name",
            "placeholder": "<%$this->lang->line('GENERIC_FIRST_NAME')%>",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_owner_name']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_owner_email",
        "index": "c_owner_email",
        "label": "<%$list_config['c_owner_email']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_owner_email']['width']%>",
        "search": <%if $list_config['c_owner_email']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_owner_email']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_owner_email']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_owner_email']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_owner_email']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_owner_email']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_owner_email']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "email": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_OWNER_EMAIL)
                },
                "email": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_VALID_EMAIL_ADDRESS_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_OWNER_EMAIL)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_owner_email",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_owner_email']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_owner_email",
            "placeholder": "",
            "class": "inline-edit-row "
        },
        "ctrl_type": "textbox",
        "default_value": "<%$list_config['c_owner_email']['default']%>",
        "filterSopt": "bw"
    },
    {
        "name": "c_owner_phone",
        "index": "c_owner_phone",
        "label": "<%$list_config['c_owner_phone']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_owner_phone']['width']%>",
        "search": <%if $list_config['c_owner_phone']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_owner_phone']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_owner_phone']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_owner_phone']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_owner_phone']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_owner_phone']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_owner_phone']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "required": true,
            "infoArr": {
                "required": {
                    "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.SUPPLIERS_OWNER_PHONE)
                }
            }
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_owner_phone",
                "autocomplete": "off"
            },
            "sopt": strSearchOpts,
            "searchhidden": <%if $list_config['c_owner_phone']['search'] eq 'Yes' %>true<%else%>false<%/if%>
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_owner_phone",
            "aria-phone-format": "<%$this->general->getAdminPHPFormats('phone')%>",
            "dataInit": initEditGridMaskingEvent,
            "class": "inline-edit-row phoneNumber"
        },
        "ctrl_type": "phone_number",
        "default_value": "<%$list_config['c_owner_phone']['default']%>",
        "filterSopt": "cn"
    },
    {
        "name": "c_added_date",
        "index": "c_added_date",
        "label": "<%$list_config['c_added_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_added_date']['width']%>",
        "search": <%if $list_config['c_added_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_added_date']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_added_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_added_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_added_date']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_added_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_added_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_added_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "YYYY-MM-DD"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['c_added_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_added_date",
            "aria-date-format": "yy-mm-dd",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['c_added_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "c_added_date",
        "index": "c_added_date",
        "label": "<%$list_config['c_added_date']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['c_added_date']['width']%>",
        "search": <%if $list_config['c_added_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_added_date']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_added_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_added_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_added_date']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_added_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_added_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_added_date",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['c_added_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["c_added_date"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_added_date&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["c_added_date"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["c_added_date"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['c_added_date']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_added_date"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_added_date",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_added_date&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['c_added_date']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_added_date"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_ADDED_DATE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['c_added_date']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "c_updat_date",
        "index": "c_updat_date",
        "label": "<%$list_config['c_updat_date']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['c_updat_date']['width']%>",
        "search": <%if $list_config['c_updat_date']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_updat_date']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_updat_date']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_updat_date']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_updat_date']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_updat_date']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_updat_date']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "text",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_updat_date",
                "autocomplete": "off",
                "class": "search-inline-date",
                "aria-date-format": "YYYY-MM-DD"
            },
            "sopt": dateSearchOpts,
            "searchhidden": <%if $list_config['c_updat_date']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataInit": initSearchGridDateRangePicker
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_updat_date",
            "aria-date-format": "yy-mm-dd",
            "aria-min": "",
            "aria-max": "",
            "placeholder": "",
            "class": "inline-edit-row inline-date-edit date-picker-icon dateOnly"
        },
        "ctrl_type": "date",
        "default_value": "<%$list_config['c_updat_date']['default']%>",
        "filterSopt": "bt"
    },
    {
        "name": "c_status",
        "index": "c_status",
        "label": "<%$list_config['c_status']['label_lang']%>",
        "labelClass": "header-align-center",
        "resizable": true,
        "width": "<%$list_config['c_status']['width']%>",
        "search": <%if $list_config['c_status']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['c_status']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['c_status']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['c_status']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['c_status']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['c_status']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['c_status']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "center",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_status",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['c_status']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["c_status"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_status&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["c_status"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["c_status"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['c_status']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_status"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_status",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_status&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['c_status']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["c_status"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_STATUS')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['c_status']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "mc_country",
        "index": "mc_country",
        "label": "<%$list_config['mc_country']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['mc_country']['width']%>",
        "search": <%if $list_config['mc_country']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['mc_country']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['mc_country']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['mc_country']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['mc_country']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['mc_country']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['mc_country']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_country_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['mc_country']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["mc_country"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mc_country&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["mc_country"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["mc_country"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['mc_country']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mc_country"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_country_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=mc_country&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['mc_country']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["mc_country"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_COUNTRY')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['mc_country']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    },
    {
        "name": "ms_state",
        "index": "ms_state",
        "label": "<%$list_config['ms_state']['label_lang']%>",
        "labelClass": "header-align-left",
        "resizable": true,
        "width": "<%$list_config['ms_state']['width']%>",
        "search": <%if $list_config['ms_state']['search'] eq 'No' %>false<%else%>true<%/if%>,
        "export": <%if $list_config['ms_state']['export'] eq 'No' %>false<%else%>true<%/if%>,
        "sortable": <%if $list_config['ms_state']['sortable'] eq 'No' %>false<%else%>true<%/if%>,
        "hidden": <%if $list_config['ms_state']['hidden'] eq 'Yes' %>true<%else%>false<%/if%>,
        "hideme": <%if $list_config['ms_state']['hideme'] eq 'Yes' %>true<%else%>false<%/if%>,
        "addable": <%if $list_config['ms_state']['addable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "editable": <%if $list_config['ms_state']['editable'] eq 'Yes' %>true<%else%>false<%/if%>,
        "align": "left",
        "edittype": "select",
        "editrules": {
            "infoArr": []
        },
        "searchoptions": {
            "attr": {
                "aria-grid-id": el_tpl_settings.main_grid_id,
                "aria-module-name": "suppliers",
                "aria-unique-name": "c_state_id",
                "autocomplete": "off",
                "data-placeholder": " ",
                "class": "search-chosen-select",
                "multiple": "multiple"
            },
            "sopt": intSearchOpts,
            "searchhidden": <%if $list_config['ms_state']['search'] eq 'Yes' %>true<%else%>false<%/if%>,
            "dataUrl": <%if $count_arr["ms_state"]["json"] eq "Yes" %>false<%else%>'<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=ms_state&mode=<%$mod_enc_mode["Search"]%>&rformat=html<%$extra_qstr%>'<%/if%>,
            "value": <%if $count_arr["ms_state"]["json"] eq "Yes" %>$.parseJSON('<%$count_arr["ms_state"]["data"]|@addslashes%>')<%else%>null<%/if%>,
            "dataInit": <%if $count_arr['ms_state']['ajax'] eq 'Yes' %>initSearchGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["ms_state"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
            "multiple": true
        },
        "editoptions": {
            "aria-grid-id": el_tpl_settings.main_grid_id,
            "aria-module-name": "suppliers",
            "aria-unique-name": "c_state_id",
            "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=ms_state&mode=<%$mod_enc_mode["Update"]%>&rformat=html<%$extra_qstr%>',
            "dataInit": <%if $count_arr['ms_state']['ajax'] eq 'Yes' %>initEditGridAjaxChosenEvent<%else%>initGridChosenEvent<%/if%>,
            "ajaxCall": '<%if $count_arr["ms_state"] eq "Yes" %>ajax-call<%/if%>',
            "data-placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIERS_STATE')%>",
            "class": "inline-edit-row chosen-select"
        },
        "ctrl_type": "dropdown",
        "default_value": "<%$list_config['ms_state']['default']%>",
        "filterSopt": "in",
        "stype": "select"
    }];
         
    initMainGridListing();
    createTooltipHeading();
    callSwitchToParent();
<%/javascript%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 