<%section name=i loop=1%>
    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>" data-row-index="<%$row_index%>">
        <td class="row-num-child" data-col-type="serial">
            <span class="row-num-span"><%$row_index%></span>
            <input type="hidden" name="child[product_specification][id][<%$row_index%>]" id="child_product_specification_id_<%$row_index%>" value="<%$child_id%>" />
            <input type="hidden" name="child[product_specification][enc_id][<%$row_index%>]" id="child_product_specification_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
            <input type="hidden" name="child[product_specification][ps_product_id][<%$row_index%>]" id="child_product_specification_ps_product_id_<%$row_index%>" value="<%$child_data[i]['ps_product_id']%>"  class='ignore-valid ' />
        </td>
        <td data-col-type="column" data-col-name="ps_title">
            <div class=" " id="ch_product_specification_cc_sh_ps_title_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['ps_title']|@htmlentities%>" name="child[product_specification][ps_title][<%$row_index%>]" id="child_product_specification_ps_title_<%$row_index%>" title="<%$child_conf_arr['form_config']['ps_title']['label_lang']%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_product_specification_ps_title_<%$row_index%>Err'></label>
            </div>
        </td>
        <td data-col-type="column" data-col-name="ps_value">
            <div class=" " id="ch_product_specification_cc_sh_ps_value_<%$row_index%>">
                <input type="text" placeholder="" value="<%$child_data[i]['ps_value']|@htmlentities%>" name="child[product_specification][ps_value][<%$row_index%>]" id="child_product_specification_ps_value_<%$row_index%>" title="<%$child_conf_arr['form_config']['ps_value']['label_lang']%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />  
            </div>
            <div>
                <label class='error' id='child_product_specification_ps_value_<%$row_index%>Err'></label>
            </div>
        </td>
        <td data-col-type="column" data-col-name="color_picker">
            <div class=" " id="ch_product_specification_cc_sh_color_picker_<%$row_index%>">
                <div class="input-append text-append-prepend">
                    <input type="text" value="<%$child_data[i]['color_picker']%>" name="child[product_specification][color_picker][<%$row_index%>]" id="child_product_specification_color_picker_<%$row_index%>" title="<%$child_conf_arr['form_config']['color_picker']['label_lang']%>"  data-ctrl-type='color_picker'  color_preview='No'  class='frm-size-medium'  maxlength='7'  />
                    <span id="child_product_specification_color_picker_<%$row_index%>_span" class="color-append-class add-on text-addon">
                        <i id="child_product_specification_color_picker_<%$row_index%>_preview" style="background-color:<%$child_data[i]['color_picker']%>"></i>
                    </span>
                </div>
            </div>
            <div>
                <label class='error' id='child_product_specification_color_picker_<%$row_index%>Err'></label>
            </div>
        </td>
        <%if $child_access_arr["actions"] eq 1%>
            <td align="center" data-col-type="actions">
                <div class="controls center">
                    <%if $child_access_arr["save"] eq 1%>
                        <%if $mode eq "Update"%>
                            <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip action-child-module-save" title="<%$this->lang->line('GENERIC_SAVE')%>">
                                <span class="icon14 icomoon-icon-disk"></span>
                            </a>
                        <%/if%>
                        <%/if%><%if $child_access_arr["delete"] eq 1%>
                        <%if $recMode eq "Update"%>
                            <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                <span class="icon16 icomoon-icon-remove"></span>
                            </a>
                        <%else%>
                            <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                <span class="icon16 icomoon-icon-remove"></span>
                            </a>
                        <%/if%>
                    <%/if%>
                </div>
            </td>
        <%/if%>
    </tr>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        <%javascript%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        setTimeout(function(){
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
    }, 500);
    <%/javascript%>
<%/if%>
<%/section%>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.products.childEvents("product_specification", "#tr_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>