<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container">
    <%include file="products_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="products" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="products" class="frm-module-block frm-view-block frm-stand-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <input type="hidden" name="p_added_date" id="p_added_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['p_added_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="p_added_by" id="p_added_by" value="<%$data['p_added_by']%>"  class='ignore-valid ' />
                    <input type="hidden" name="p_updated_date" id="p_updated_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['p_updated_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="p_updated_by" id="p_updated_by" value="<%$data['p_updated_by']%>"  class='ignore-valid ' />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('PRODUCTS_PRODUCTS')%></h4></div>
                                <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                    <div class="form-row row-fluid " id="cc_sh_p_product_name">
                                        <label class="form-label span3">
                                            <%$form_config['p_product_name']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['p_product_name']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_category_id">
                                        <label class="form-label span3">
                                            <%$form_config['p_category_id']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['p_category_id'], $opt_arr['p_category_id'])%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_description">
                                        <label class="form-label span3">
                                            <%$form_config['p_description']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['p_description']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_price">
                                        <label class="form-label span3">
                                            <%$form_config['p_price']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['p_price']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_retail_price">
                                        <label class="form-label span3">
                                            <%$form_config['p_retail_price']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$data['p_retail_price']%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_manufacturer_id">
                                        <label class="form-label span3">
                                            <%$form_config['p_manufacturer_id']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['p_manufacturer_id'], $opt_arr['p_manufacturer_id'])%></strong></span>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_brand_id">
                                        <label class="form-label span3">
                                            <%$form_config['p_brand_id']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['p_brand_id'], $opt_arr['p_brand_id'])%></strong></span>
                                        </div>
                                    </div>
                                    <%assign var="child_module_name" value="product_image"%>
                                    <%assign var="child_module_name" value="product_image"%>
                                    <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                    <%assign var="child_func" value=$child_assoc_func[$child_module_name]%>
                                    <%assign var="child_elem" value=$child_assoc_elem[$child_module_name]%>
                                    <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                    <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                    <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                    <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                    <%assign var="child_access_arr" value=$child_assoc_access[$child_module_name]%>
                                    <%if $child_conf_arr["recMode"] eq "Update"%>
                                        <%assign var="child_cnt" value=$child_data|@count%>
                                        <%assign var="recMode" value="Update"%>
                                    <%else%>
                                        <%assign var="child_cnt" value="0"%>
                                        <%assign var="recMode" value="Add"%>
                                    <%/if%>
                                    <%if $child_cnt gt 0%>
                                        <%assign var="child_ord" value=$child_cnt%>
                                    <%else%>
                                        <%assign var="child_ord" value="1"%>
                                    <%/if%>
                                    <%assign var="child_merge_data" value=[]%>
                                    <div class="form-row row-fluid form-inline-child" id="child_module_<%$child_module_name%>">
                                        <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                        <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="No" />
                                        <input type="hidden" name="childModuleViewMode[<%$child_module_name%>]" id="childModuleViewMode_<%$child_module_name%>" value="Yes" />
                                        <%section name=i loop=$child_ord%>
                                            <%assign var="row_index" value=$smarty.section.i.index%>
                                            <%assign var="child_id" value=$child_data[i]['iProductImageId']%>
                                            <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                            <%assign var="child_id_temp" value=[$child_data[i]['pi_image']]%>
                                            <%assign var="child_merge_data" value=$child_merge_data|@array_merge:$child_id_temp%>
                                            <input type="hidden" name="child[product_image][id][<%$row_index%>]" id="child_product_image_id_<%$row_index%>" value="<%$child_id%>" />
                                            <input type="hidden" name="child[product_image][enc_id][<%$row_index%>]" id="child_product_image_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                            <input type="hidden" name="child[product_image][pi_product_id][<%$row_index%>]" id="child_product_image_pi_product_id_<%$row_index%>" value="<%$child_data[i]['pi_product_id']%>"  class='ignore-valid ' />
                                        <%/section%>
                                        <label class="form-label span3 inline-module-label"><%$this->lang->line('PRODUCTS_PRODUCT_IMAGE')%><em> * </em></label>
                                        <div class="form-right-div form-inline-child "  id="child_module_rel_<%$child_module_name%>">
                                            <div id="upload_multi_file_product_image" class="upload-multi-file frm-size-medium clear" aria-required="true" aria-module="product_image" aria-field="pi_image">
                                                <%assign var="is_images_exists" value=0%>
                                                <%section name=j loop=$child_cnt%>
                                                    <%assign var="row_index" value=$smarty.section.j.index%>
                                                    <%if $child_img_html[$row_index]["pi_image"] neq ""%>
                                                        <%assign var="is_images_exists" value=1%>
                                                        <div class="row-upload-file" id="upload_row_product_image_<%$row_index%>">
                                                            <input type="hidden" value="<%$child_data[j]['pi_image']%>" name="child[product_image][old_pi_image][<%$row_index%>]" id="child_product_image_old_pi_image_<%$row_index%>" />
                                                            <input type="hidden" value="<%$child_data[j]['pi_image']%>" name="child[product_image][pi_image][<%$row_index%>]" id="child_product_image_pi_image_<%$row_index%>" class="ignore-valid" aria-extensions="gif,png,jpg,jpeg,jpe,bmp,ico" aria-valid-size="<%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB"/>
                                                            <div class="">
                                                                <%$child_img_html[$row_index]["pi_image"]%>
                                                            </div>
                                                        </div>
                                                    <%/if%>
                                                <%/section%>
                                                <%if $is_images_exists eq 0%>
                                                    <input type="hidden" value="" name="child[product_image][pi_image][0]" id="child_product_image_pi_image_0" class="_upload_req_file"/>
                                                <%/if%>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <%assign var="child_module_name" value="product_specification"%>
                                    <div class="box form-child-table" id="child_module_product_specification">
                                        <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                        <%assign var="child_func" value=$child_assoc_func[$child_module_name]%>
                                        <%assign var="child_elem" value=$child_assoc_elem[$child_module_name]%>
                                        <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                        <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                        <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                        <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                        <%assign var="child_access_arr" value=$child_assoc_access[$child_module_name]%>
                                        <%if $child_conf_arr["recMode"] eq "Update"%>
                                            <%assign var="child_cnt" value=$child_data|@count%>
                                            <%assign var="recMode" value="Update"%>
                                        <%else%>
                                            <%if $child_data|@count gt 0%>
                                                <%assign var="child_cnt" value=$child_data|@count%>
                                            <%else%>
                                                <%assign var="child_cnt" value="1"%>
                                            <%/if%>
                                            <%assign var="recMode" value="Add"%>
                                        <%/if%>
                                        <%assign var="popup" value=$child_conf_arr["popup"]%>
                                        <div class="title">
                                            <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                            <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="No" />
                                            <input type="hidden" name="childModuleViewMode[<%$child_module_name%>]" id="childModuleViewMode_<%$child_module_name%>" value="Yes" />
                                            <h4>
                                                <span class="icon12 icomoon-icon-equalizer-2"></span><span><%$this->lang->line('PRODUCTS_PRODUCT_SPECIFICATION')%></span>
                                            </h4>
                                            <a href="javascript://" class="minimize" style="display: none;"></a>
                                        </div>
                                        <div class="content noPad form-table-row" style="display: block;">
                                            <table class="responsive table table-bordered child-module-table" id="tbl_child_module_<%$child_module_name%>">
                                                <thead class="child-module-head">
                                                    <tr>
                                                        <th width="3%" data-col-type="serial">#</th>
                                                        <th width="30%" data-col-type="column" data-col-name="ps_title">
                                                            <%$child_conf_arr["form_config"]["ps_title"]["label_lang"]%>
                                                            <%if $smarty.get.mode neq "View"%>
                                                            <%/if%>
                                                        </th>
                                                        <th width="30%" data-col-type="column" data-col-name="ps_value">
                                                            <%$child_conf_arr["form_config"]["ps_value"]["label_lang"]%>
                                                            <%if $smarty.get.mode neq "View"%>
                                                            <%/if%>
                                                        </th>
                                                        <th width="30%" data-col-type="column" data-col-name="color_picker">
                                                            <%$child_conf_arr["form_config"]["color_picker"]["label_lang"]%>
                                                            <%if $smarty.get.mode neq "View"%>
                                                            <%/if%>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="add_child_module_<%$child_module_name%>" class="child-module-body">
                                                    <tr class="ch-mod-firstrow">
                                                        <td width="3%" data-col-type="serial"></td> 
                                                        <td width="30%" data-col-type="column" data-col-name="ps_title"></td>
                                                        <td width="30%" data-col-type="column" data-col-name="ps_value"></td>
                                                        <td width="30%" data-col-type="column" data-col-name="color_picker"></td>
                                                    </tr>
                                                    <%section name=i loop=$child_cnt%>
                                                        <%assign var="row_index" value=$smarty.section.i.index%>
                                                        <%assign var="row_number" value=$smarty.section.i.iteration%>
                                                        <%assign var="child_id" value=$child_data[i]['iProductSpecificationId']%>
                                                        <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                                        <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>" data-row-index="<%$row_index%>">
                                                            <td class="row-num-child" data-col-type="serial">
                                                                <span class="row-num-span"><%$row_number%></span>
                                                                <input type="hidden" name="child[product_specification][id][<%$row_index%>]" id="child_product_specification_id_<%$row_index%>" value="<%$child_id%>" />
                                                                <input type="hidden" name="child[product_specification][enc_id][<%$row_index%>]" id="child_product_specification_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                                                <input type="hidden" name="child[product_specification][ps_product_id][<%$row_index%>]" id="child_product_specification_ps_product_id_<%$row_index%>" value="<%$child_data[i]['ps_product_id']%>"  class='ignore-valid ' />
                                                            </td>
                                                            <td data-col-type="column" data-col-name="ps_title">
                                                                <div class=" " id="ch_product_specification_cc_sh_ps_title_<%$row_index%>">
                                                                    <span class="frm-data-label"><strong><%$child_data[i]['ps_title']%></strong></span>
                                                                </div>
                                                            </td>
                                                            <td data-col-type="column" data-col-name="ps_value">
                                                                <div class=" " id="ch_product_specification_cc_sh_ps_value_<%$row_index%>">
                                                                    <span class="frm-data-label"><strong><%$child_data[i]['ps_value']%></strong></span>
                                                                </div>
                                                            </td>
                                                            <td data-col-type="column" data-col-name="color_picker">
                                                                <div class=" " id="ch_product_specification_cc_sh_color_picker_<%$row_index%>">
                                                                    <span class="frm-data-label"><strong><%$child_data[i]['color_picker']%></strong></span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <%/section%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_status">
                                        <label class="form-label span3">
                                            <%$form_config['p_status']['label_lang']%>
                                        </label> 
                                        <div class="form-right-div frm-elements-div ">
                                            <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['p_status'], $opt_arr['p_status'])%></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%$this->js->add_js("admin/custom/extended.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
