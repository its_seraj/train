<div class="headingfix">
    <!-- Top Header Block -->
    <div class="heading" id="top_heading_fix">
        <!-- Top Strip Title Block -->
        <h3>
            <div class="screen-title">
                <%$this->lang->line('GENERIC_LISTING')%> :: <%$this->lang->line('SUPPLIER_COMPANY_SUPPLIER_COMPANY')%>
            </div>        
        </h3>
        <!-- Top Strip Dropdown Block -->
        <div class="header-right-drops">
            
            
        </div>
    </div>
</div>    