<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_subview_settings = {}, view_js_col_model_json = {}, view_token_input_assign = {}, view_token_pre_populates = {};
      
    el_subview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_subview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_subview_settings['view_id'] = '<%$subgrid_view_id%>';
    el_subview_settings['edit_id'] = '<%$enc_view_primary_id%>';
    el_subview_settings['module_name'] = '<%$module_name%>';
            
    el_subview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>?&oper=edit&<%$extra_qstr%>';
    el_subview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>?<%$extra_qstr%>';
    el_subview_settings['permit_edit_btn'] = '<%$edit_access%>';

    view_js_col_model_json =  {
        "gv_pi_image": {
            "htmlID": "gv_pi_image",
            "name": "pi_image",
            "label": "Image",
            "label_lang": "<%$this->lang->line('PRODUCTS_IMAGE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['pi_image']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['pi_image']['value']%>",
            "dbval": "<%$list_config['pi_image']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "extra_class": ""
        },
        "gv_p_product_name": {
            "htmlID": "gv_p_product_name",
            "name": "p_product_name",
            "label": "Product Name",
            "label_lang": "<%$this->lang->line('PRODUCTS_PRODUCT_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_product_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_product_name']['value']%>",
            "dbval": "<%$list_config['p_product_name']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRODUCT_NAME)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "gv_b_brand": {
            "htmlID": "gv_b_brand",
            "name": "b_brand",
            "label": "Brand",
            "label_lang": "<%$this->lang->line('PRODUCTS_BRAND')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['b_brand']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['b_brand']['value']%>",
            "dbval": "<%$list_config['b_brand']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_BRAND)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['b_brand']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=b_brand&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_brand_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_BRAND')%>"
            }
        },
        "gv_m_name": {
            "htmlID": "gv_m_name",
            "name": "m_name",
            "label": "Manufacture",
            "label_lang": "<%$this->lang->line('PRODUCTS_MANUFACTURE')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['m_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['m_name']['value']%>",
            "dbval": "<%$list_config['m_name']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_MANUFACTURER_ID)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['m_name']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=m_name&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_manufacturer_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_MANUFACTURE')%>"
            }
        },
        "gv_c_category_name": {
            "htmlID": "gv_c_category_name",
            "name": "c_category_name",
            "label": "Category Name",
            "label_lang": "<%$this->lang->line('PRODUCTS_CATEGORY_NAME')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['c_category_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['c_category_name']['value']%>",
            "dbval": "<%$list_config['c_category_name']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_CATEGORY)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['c_category_name']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=c_category_name&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_category_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_CATEGORY_NAME')%>"
            }
        },
        "gv_p_price": {
            "htmlID": "gv_p_price",
            "name": "p_price",
            "label": "Price",
            "label_lang": "<%$this->lang->line('PRODUCTS_PRICE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_price']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_price']['value']%>",
            "dbval": "<%$list_config['p_price']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRICE)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "gv_p_retail_price": {
            "htmlID": "gv_p_retail_price",
            "name": "p_retail_price",
            "label": "Retail Price",
            "label_lang": "<%$this->lang->line('PRODUCTS_RETAIL_PRICE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_retail_price']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_retail_price']['value']%>",
            "dbval": "<%$list_config['p_retail_price']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_RETAIL_PRICE)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "gv_rm_rate": {
            "htmlID": "gv_rm_rate",
            "name": "rm_rate",
            "label": "Rate",
            "label_lang": "<%$this->lang->line('PRODUCTS_RATE')%>",
            "type": "rating_master",
            "ctrl_type": "rating_master",
            "editable": <%if $list_config['rm_rate']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['rm_rate']['value']%>",
            "dbval": "<%$list_config['rm_rate']['dbval']%>",
            "editoptions": {
                "raty": {
                    "params": {
                        "number": "5",
                        "cancel": "false",
                        "half": "true",
                        "targetKeep": "true",
                        "precision": "true",
                        "cancelOff": "cancel-custom-off.png",
                        "cancelOn": "cancel-custom-on.png",
                        "starOff": "star-off.png",
                        "starOn": "star-on.png",
                        "starHalf": "star-half.png"
                    },
                    "hints": "1,2,3,4,5",
                    "icons": "stars"
                }
            }
        },
        "gv_p_status": {
            "htmlID": "gv_p_status",
            "name": "p_status",
            "label": "Status",
            "label_lang": "<%$this->lang->line('PRODUCTS_STATUS')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['p_status']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_status']['value']%>",
            "dbval": "<%$list_config['p_status']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_STATUS)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr['p_status']['ajax'] eq 'Yes' %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_status&id=<%$enc_view_primary_id%>&par_field=<%$exp_par_field%>&par_data=<%$exp_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_status",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_STATUS')%>"
            }
        }
    };
    
    initSubGridDetailView();
<%/javascript%>
    

<div class="expand-detail-view">                        
    <table id="<%$subgrid_view_id%>" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_IMAGE')%>: </strong></td>
            <td width="20%"><%$data['pi_image']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_PRODUCT_NAME')%>: </strong></td>
            <td width="20%"><%$data['p_product_name']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_BRAND')%>: </strong></td>
            <td width="20%"><%$data['b_brand']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_MANUFACTURE')%>: </strong></td>
            <td width="20%"><%$data['m_name']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_CATEGORY_NAME')%>: </strong></td>
            <td width="20%"><%$data['c_category_name']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_PRICE')%>: </strong></td>
            <td width="20%"><%$data['p_price']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_RETAIL_PRICE')%>: </strong></td>
            <td width="20%"><%$data['p_retail_price']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_RATE')%>: </strong></td>
            <td width="20%"><%$data['rm_rate']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_STATUS')%>: </strong></td>
            <td width="20%"><%$data['p_status']%></td>
        </tr>
         
    </table>
</div>