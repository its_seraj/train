<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="products_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="products" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="products" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <input type="hidden" name="p_added_date" id="p_added_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['p_added_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                        <input type="hidden" name="p_added_by" id="p_added_by" value="<%$data['p_added_by']%>"  class='ignore-valid ' />
                        <input type="hidden" name="p_updated_date" id="p_updated_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['p_updated_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                        <input type="hidden" name="p_updated_by" id="p_updated_by" value="<%$data['p_updated_by']%>"  class='ignore-valid ' />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('PRODUCTS_PRODUCTS')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_p_product_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_product_name']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['p_product_name']|@htmlentities%>" name="p_product_name" id="p_product_name" title="<%$this->lang->line('PRODUCTS_PRODUCT_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_product_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_category_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_category_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['p_category_id']%>
                                                <%$this->dropdown->display("p_category_id","p_category_id","  title='<%$this->lang->line('PRODUCTS_CATEGORY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_CATEGORY')%>'  ", "|||", "", $opt_selected,"p_category_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_category_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_description">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_description']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <textarea placeholder=""  name="p_description" id="p_description" title="<%$this->lang->line('PRODUCTS_DESCRIPTION')%>"  data-ctrl-type='textarea'  class='elastic frm-size-medium'  ><%$data['p_description']%></textarea>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_descriptionErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_price">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_price']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['p_price']|@htmlentities%>" name="p_price" id="p_price" title="<%$this->lang->line('PRODUCTS_PRICE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_priceErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_retail_price">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_retail_price']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['p_retail_price']|@htmlentities%>" name="p_retail_price" id="p_retail_price" title="<%$this->lang->line('PRODUCTS_RETAIL_PRICE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_retail_priceErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_manufacturer_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_manufacturer_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['p_manufacturer_id']%>
                                                <%$this->dropdown->display("p_manufacturer_id","p_manufacturer_id","  title='<%$this->lang->line('PRODUCTS_MANUFACTURER_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_MANUFACTURER_ID')%>'  ", "|||", "", $opt_selected,"p_manufacturer_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_manufacturer_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_p_brand_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['p_brand_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['p_brand_id']%>
                                                <%$this->dropdown->display("p_brand_id","p_brand_id","  title='<%$this->lang->line('PRODUCTS_BRAND')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_BRAND')%>'  ", "|||", "", $opt_selected,"p_brand_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='p_brand_idErr'></label></div>
                                        </div>
                                        <%assign var="child_module_name" value="product_image"%>
                                        <%assign var="child_module_name" value="product_image"%>
                                        <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                        <%assign var="child_func" value=$child_assoc_func[$child_module_name]%>
                                        <%assign var="child_elem" value=$child_assoc_elem[$child_module_name]%>
                                        <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                        <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                        <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                        <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                        <%assign var="child_access_arr" value=$child_assoc_access[$child_module_name]%>
                                        <%if $child_conf_arr["recMode"] eq "Update"%>
                                            <%assign var="child_cnt" value=$child_data|@count%>
                                            <%assign var="recMode" value="Update"%>
                                        <%else%>
                                            <%assign var="child_cnt" value="0"%>
                                            <%assign var="recMode" value="Add"%>
                                        <%/if%>
                                        <%if $child_cnt gt 0%>
                                            <%assign var="child_ord" value=$child_cnt%>
                                        <%else%>
                                            <%assign var="child_ord" value="1"%>
                                        <%/if%>
                                        <%assign var="child_merge_data" value=[]%>
                                        <div class="form-row row-fluid form-inline-child" id="child_module_<%$child_module_name%>">
                                            <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                            <input type="hidden" name="childModuleSingle[<%$child_module_name%>]" id="childModuleSingle_<%$child_module_name%>" value="pi_image"/>
                                            <input type="hidden" name="childModuleFileDesc[<%$child_module_name%>]" id="childModuleFileDesc_<%$child_module_name%>" value=""/>
                                            <input type="hidden" name="childModuleParField[<%$child_module_name%>]" id="childModuleParField_<%$child_module_name%>" value="iProductId"/>
                                            <input type="hidden" name="childModuleParData[<%$child_module_name%>]" id="childModuleParData_<%$child_module_name%>" value="<%$this->general->getAdminEncodeURL($data['iProductId'])%>"/>
                                            <input type="hidden" name="childModuleLayout[<%$child_module_name%>]" id="childModuleLayout_<%$child_module_name%>" value="Single"/>
                                            <input type="hidden" name="childModuleType[<%$child_module_name%>]" id="childModuleType_<%$child_module_name%>" value="Single"/>
                                            <input type="hidden" name="childModuleCnt[<%$child_module_name%>]" id="childModuleCnt_<%$child_module_name%>" value="<%$child_cnt%>" />
                                            <input type="hidden" name="childModuleInc[<%$child_module_name%>]" id="childModuleInc_<%$child_module_name%>" value="<%$child_cnt%>" />
                                            <input type="hidden" name="childModulePopup[<%$child_module_name%>]" id="childModulePopup_<%$child_module_name%>" value="No" />
                                            <input type="hidden" name="childModuleUploadURL[<%$child_module_name%>]" id="childModuleUploadURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['upload_form_file']%>" />
                                            <input type="hidden" name="childModuleChosenURL[<%$child_module_name%>]" id="childModuleChosenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_chosen_auto_complete']%>" />
                                            <input type="hidden" name="childModuleParentURL[<%$child_module_name%>]" id="childModuleParentURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['parent_source_options']%>" />
                                            <input type="hidden" name="childModuleTokenURL[<%$child_module_name%>]" id="childModuleTokenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_token_auto_complete']%>" />
                                            <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="Yes" />
                                            <input type="hidden" name="childModuleViewMode[<%$child_module_name%>]" id="childModuleViewMode_<%$child_module_name%>" value="No" />
                                            <%section name=i loop=$child_ord%>
                                                <%assign var="row_index" value=$smarty.section.i.index%>
                                                <%assign var="child_id" value=$child_data[i]['iProductImageId']%>
                                                <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                                <%assign var="child_id_temp" value=[$child_data[i]['pi_image']]%>
                                                <%assign var="child_merge_data" value=$child_merge_data|@array_merge:$child_id_temp%>
                                                <input type="hidden" name="child[product_image][id][<%$row_index%>]" id="child_product_image_id_<%$row_index%>" value="<%$child_id%>" />
                                                <input type="hidden" name="child[product_image][enc_id][<%$row_index%>]" id="child_product_image_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                                <input type="hidden" name="child[product_image][pi_product_id][<%$row_index%>]" id="child_product_image_pi_product_id_<%$row_index%>" value="<%$child_data[i]['pi_product_id']%>"  class='ignore-valid ' />
                                            <%/section%>
                                            <label class="form-label span3 inline-module-label"><%$this->lang->line('PRODUCTS_PRODUCT_IMAGE')%><em> * </em></label>
                                            <div class="form-right-div form-inline-child "  id="child_module_rel_<%$child_module_name%>">
                                                <div  class='btn-uploadify frm-size-medium' >
                                                    <div id="upload_drop_zone_child_product_image_pi_image_0" class="upload-drop-zone"></div>
                                                    <div class="uploader upload-src-zone">
                                                        <input type="file" name="uploadify_child[product_image][pi_image][0]" id="uploadify_child_product_image_pi_image_0" title="<%$child_conf_arr['form_config']['pi_image']['label_lang']%>" multiple=true />
                                                        <span class="filename" id="preview_child_product_image_pi_image_0"><%$this->lang->line('GENERIC_DROP_FILES_HERE_OR_CLICK_TO_UPLOAD')%></span>
                                                        <span class="action">Choose File</span>
                                                    </div>
                                                </div>
                                                <span class="file-viewer">
                                                    <a href="javascript://" class="viewer-anchor tipR" style="text-decoration:none;" data-viewer-target="upload_multi_file_product_image" data-viewer-loop="row-upload-file" data-viewer-ext="gif,png,jpg,jpeg,jpe,bmp,ico">
                                                        <span class="icon24 minia-icon-eye viewer-icon" aria-hidden="true"></span>
                                                    </a>
                                                </span>
                                                <span class="input-comment">
                                                    <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif, png, jpg, jpeg, jpe, bmp, ico.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB."><span class="icomoon-icon-help"></span></a>
                                                </span>
                                                <div class='clear upload-progress' id='progress_child_product_image_pi_image_0' >
                                                    <div class='upload-progress-bar progress progress-striped active'>
                                                        <div class='bar' id='practive_child_product_image_pi_image_0'></div>
                                                    </div>
                                                    <div class='upload-cancel-div'><a class='upload-cancel' href='javascript://'>Cancel</a></div>
                                                    <div class='clear'></div>
                                                </div>
                                                <div id="upload_multi_file_product_image" class="upload-multi-file frm-size-medium clear" aria-required="true" aria-module="product_image" aria-field="pi_image">
                                                    <%assign var="is_images_exists" value=0%>
                                                    <%section name=j loop=$child_cnt%>
                                                        <%assign var="row_index" value=$smarty.section.j.index%>
                                                        <%if $child_img_html[$row_index]["pi_image"] neq ""%>
                                                            <%assign var="is_images_exists" value=1%>
                                                            <div class="row-upload-file" id="upload_row_product_image_<%$row_index%>">
                                                                <input type="hidden" value="<%$child_data[j]['pi_image']%>" name="child[product_image][old_pi_image][<%$row_index%>]" id="child_product_image_old_pi_image_<%$row_index%>" />
                                                                <input type="hidden" value="<%$child_data[j]['pi_image']%>" name="child[product_image][pi_image][<%$row_index%>]" id="child_product_image_pi_image_<%$row_index%>" class="ignore-valid" aria-extensions="gif,png,jpg,jpeg,jpe,bmp,ico" aria-valid-size="<%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB"/>
                                                                <div class="">
                                                                    <%$child_img_html[$row_index]["pi_image"]%>
                                                                </div>
                                                            </div>
                                                        <%/if%>
                                                    <%/section%>
                                                    <%if $is_images_exists eq 0%>
                                                        <input type="hidden" value="" name="child[product_image][pi_image][0]" id="child_product_image_pi_image_0" class="_upload_req_file"/>
                                                    <%/if%>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="error-msg-form inline-module-error">
                                                <label class='error' id='child_product_image_pi_image_0Err'></label>
                                            </div>
                                        </div>
                                        <%assign var="child_module_name" value="product_specification"%>
                                        <div class="box form-child-table" id="child_module_product_specification">
                                            <%assign var="child_data" value=$child_assoc_data[$child_module_name]%>
                                            <%assign var="child_func" value=$child_assoc_func[$child_module_name]%>
                                            <%assign var="child_elem" value=$child_assoc_elem[$child_module_name]%>
                                            <%assign var="child_conf_arr" value=$child_assoc_conf[$child_module_name]%>
                                            <%assign var="child_opt_arr" value=$child_assoc_opt[$child_module_name]%>
                                            <%assign var="child_img_html" value=$child_assoc_img[$child_module_name]%>
                                            <%assign var="child_auto_arr" value=$child_assoc_auto[$child_module_name]%>
                                            <%assign var="child_access_arr" value=$child_assoc_access[$child_module_name]%>
                                            <%if $child_conf_arr["recMode"] eq "Update"%>
                                                <%assign var="child_cnt" value=$child_data|@count%>
                                                <%assign var="recMode" value="Update"%>
                                            <%else%>
                                                <%if $child_data|@count gt 0%>
                                                    <%assign var="child_cnt" value=$child_data|@count%>
                                                <%else%>
                                                    <%assign var="child_cnt" value="1"%>
                                                <%/if%>
                                                <%assign var="recMode" value="Add"%>
                                            <%/if%>
                                            <%assign var="popup" value=$child_conf_arr["popup"]%>
                                            <div class="title">
                                                <input type="hidden" name="childModule[]" id="childModule_<%$child_module_name%>" value="<%$child_module_name%>" />
                                                <input type="hidden" name="childModuleParField[<%$child_module_name%>]" id="childModuleParField_<%$child_module_name%>" value="iProductId"/>
                                                <input type="hidden" name="childModuleParData[<%$child_module_name%>]" id="childModuleParData_<%$child_module_name%>" value="<%$this->general->getAdminEncodeURL($data['iProductId'])%>"/>
                                                <input type="hidden" name="childModuleLayout[<%$child_module_name%>]" id="childModuleLayout_<%$child_module_name%>" value="Column"/>
                                                <input type="hidden" name="childModuleType[<%$child_module_name%>]" id="childModuleType_<%$child_module_name%>" value="Table"/>
                                                <input type="hidden" name="childModuleCnt[<%$child_module_name%>]" id="childModuleCnt_<%$child_module_name%>" value="<%$child_cnt%>" />
                                                <input type="hidden" name="childModuleInc[<%$child_module_name%>]" id="childModuleInc_<%$child_module_name%>" value="<%$child_cnt%>" />
                                                <input type="hidden" name="childModulePopup[<%$child_module_name%>]" id="childModulePopup_<%$child_module_name%>" value="<%$popup%>" />
                                                <input type="hidden" name="childModuleUploadURL[<%$child_module_name%>]" id="childModuleUploadURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['upload_form_file']%>" />
                                                <input type="hidden" name="childModuleChosenURL[<%$child_module_name%>]" id="childModuleChosenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_chosen_auto_complete']%>" />
                                                <input type="hidden" name="childModuleParentURL[<%$child_module_name%>]" id="childModuleParentURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['parent_source_options']%>" />
                                                <input type="hidden" name="childModuleTokenURL[<%$child_module_name%>]" id="childModuleTokenURL_<%$child_module_name%>" value="<%$child_conf_arr['mod_enc_url']['get_token_auto_complete']%>" />
                                                <input type="hidden" name="childModuleShowHide[<%$child_module_name%>]" id="childModuleShowHide_<%$child_module_name%>" value="Yes" />
                                                <input type="hidden" name="childModuleViewMode[<%$child_module_name%>]" id="childModuleViewMode_<%$child_module_name%>" value="No" />
                                                <h4>
                                                    <span class="icon12 icomoon-icon-equalizer-2"></span><span><%$this->lang->line('PRODUCTS_PRODUCT_SPECIFICATION')%></span>
                                                    <span style="display:none;margin-left:32%" id="ajax_loader_childModule_<%$child_module_name%>"><i class="fa fa-refresh fa-spin-light fa-2x fa-fw"></i></span>
                                                    <%if $child_access_arr["add"] eq 1%>
                                                        <%assign var="_ch_label_add_text" value=$this->lang->line('GENERIC_ADD_NEW')%>
                                                        <%assign var="_ch_label_add_icon" value="icon12 icomoon-icon-plus-2"%>
                                                        <%if isset($child_access_arr["labels"]["add"]["text"])%>
                                                            <%assign var="_ch_label_add_text" value=$child_access_arr["labels"]["add"]["text"]%>
                                                        <%/if%>
                                                        <%if isset($child_access_arr["labels"]["add"]["icon"])%>
                                                            <%assign var="_ch_label_add_icon" value=$child_access_arr["labels"]["add"]["icon"]%>
                                                        <%/if%>
                                                        <div class="box-addmore right">
                                                            <a class="btn btn-success" href="javascript://" onclick="getChildModuleAjaxTable('<%$mod_enc_url.child_data_add%>','<%$child_module_name%>', '<%$mode%>')" title="<%$_ch_label_add_text%>">
                                                                <span class="<%$_ch_label_add_icon%>"></span>
                                                                <strong><%$_ch_label_add_text%></strong>
                                                            </a>
                                                        </div>
                                                    <%/if%>
                                                </h4>
                                                <a href="javascript://" class="minimize" style="display: none;"></a>
                                            </div>
                                            <div class="content noPad form-table-row" style="display: block;">
                                                <table class="responsive table table-bordered child-module-table" id="tbl_child_module_<%$child_module_name%>">
                                                    <thead class="child-module-head">
                                                        <tr>
                                                            <th width="3%" data-col-type="serial">#</th>
                                                            <th width="30%" data-col-type="column" data-col-name="ps_title">
                                                                <%$child_conf_arr["form_config"]["ps_title"]["label_lang"]%>
                                                                <%if $smarty.get.mode neq "View"%>
                                                                <%/if%>
                                                            </th>
                                                            <th width="30%" data-col-type="column" data-col-name="ps_value">
                                                                <%$child_conf_arr["form_config"]["ps_value"]["label_lang"]%>
                                                                <%if $smarty.get.mode neq "View"%>
                                                                <%/if%>
                                                            </th>
                                                            <th width="30%" data-col-type="column" data-col-name="color_picker">
                                                                <%$child_conf_arr["form_config"]["color_picker"]["label_lang"]%>
                                                                <%if $smarty.get.mode neq "View"%>
                                                                <%/if%>
                                                            </th>
                                                            <%if $child_access_arr["actions"] eq 1%>
                                                                <th width="7%" data-col-type="actions">
                                                                    <%assign var="_ch_label_actions_text" value=$this->lang->line("GENERIC_ACTIONS")%>
                                                                    <%if isset($child_access_arr["labels"]["actions"]["text"])%>
                                                                        <%assign var="_ch_label_actions_text" value=$child_access_arr["labels"]["actions"]["text"]%>
                                                                    <%/if%>
                                                                    <div align="center"><%$_ch_label_actions_text%></div>
                                                                </th>
                                                            <%/if%>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="add_child_module_<%$child_module_name%>" class="child-module-body">
                                                        <tr class="ch-mod-firstrow">
                                                            <td width="3%" data-col-type="serial"></td> 
                                                            <td width="30%" data-col-type="column" data-col-name="ps_title"></td>
                                                            <td width="30%" data-col-type="column" data-col-name="ps_value"></td>
                                                            <td width="30%" data-col-type="column" data-col-name="color_picker"></td> <td width="7%" data-col-type="actions"></td>
                                                        </tr>
                                                        <%section name=i loop=$child_cnt%>
                                                            <%assign var="row_index" value=$smarty.section.i.index%>
                                                            <%assign var="row_number" value=$smarty.section.i.iteration%>
                                                            <%assign var="child_id" value=$child_data[i]['iProductSpecificationId']%>
                                                            <%assign var="enc_child_id" value=$this->general->getAdminEncodeURL($child_id)%>
                                                            <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>" data-row-index="<%$row_index%>">
                                                                <td class="row-num-child" data-col-type="serial">
                                                                    <span class="row-num-span"><%$row_number%></span>
                                                                    <input type="hidden" name="child[product_specification][id][<%$row_index%>]" id="child_product_specification_id_<%$row_index%>" value="<%$child_id%>" />
                                                                    <input type="hidden" name="child[product_specification][enc_id][<%$row_index%>]" id="child_product_specification_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
                                                                    <input type="hidden" name="child[product_specification][ps_product_id][<%$row_index%>]" id="child_product_specification_ps_product_id_<%$row_index%>" value="<%$child_data[i]['ps_product_id']%>"  class='ignore-valid ' />
                                                                </td>
                                                                <td data-col-type="column" data-col-name="ps_title">
                                                                    <div class=" " id="ch_product_specification_cc_sh_ps_title_<%$row_index%>">
                                                                        <input type="text" placeholder="" value="<%$child_data[i]['ps_title']|@htmlentities%>" name="child[product_specification][ps_title][<%$row_index%>]" id="child_product_specification_ps_title_<%$row_index%>" title="<%$child_conf_arr['form_config']['ps_title']['label_lang']%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />  
                                                                    </div>
                                                                    <div>
                                                                        <label class='error' id='child_product_specification_ps_title_<%$row_index%>Err'></label>
                                                                    </div>
                                                                </td>
                                                                <td data-col-type="column" data-col-name="ps_value">
                                                                    <div class=" " id="ch_product_specification_cc_sh_ps_value_<%$row_index%>">
                                                                        <input type="text" placeholder="" value="<%$child_data[i]['ps_value']|@htmlentities%>" name="child[product_specification][ps_value][<%$row_index%>]" id="child_product_specification_ps_value_<%$row_index%>" title="<%$child_conf_arr['form_config']['ps_value']['label_lang']%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />  
                                                                    </div>
                                                                    <div>
                                                                        <label class='error' id='child_product_specification_ps_value_<%$row_index%>Err'></label>
                                                                    </div>
                                                                </td>
                                                                <td data-col-type="column" data-col-name="color_picker">
                                                                    <div class=" " id="ch_product_specification_cc_sh_color_picker_<%$row_index%>">
                                                                        <div class="input-append text-append-prepend">
                                                                            <input type="text" value="<%$child_data[i]['color_picker']%>" name="child[product_specification][color_picker][<%$row_index%>]" id="child_product_specification_color_picker_<%$row_index%>" title="<%$child_conf_arr['form_config']['color_picker']['label_lang']%>"  data-ctrl-type='color_picker'  color_preview='No'  class='frm-size-medium'  maxlength='7'  />
                                                                            <span id="child_product_specification_color_picker_<%$row_index%>_span" class="color-append-class add-on text-addon">
                                                                                <i id="child_product_specification_color_picker_<%$row_index%>_preview" style="background-color:<%$child_data[i]['color_picker']%>"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label class='error' id='child_product_specification_color_picker_<%$row_index%>Err'></label>
                                                                    </div>
                                                                </td>
                                                                <%if $child_access_arr["actions"] eq 1%>
                                                                    <td align="center" data-col-type="actions">
                                                                        <div class="controls center">
                                                                            <%if $child_access_arr["save"] eq 1%>
                                                                                <%if $mode eq "Update"%>
                                                                                    <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip action-child-module-save" title="<%$this->lang->line('GENERIC_SAVE')%>">
                                                                                        <span class="icon14 icomoon-icon-disk"></span>
                                                                                    </a>
                                                                                <%/if%>
                                                                                <%/if%><%if $child_access_arr["delete"] eq 1%>
                                                                                <%if $recMode eq "Update"%>
                                                                                    <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                                        <span class="icon16 icomoon-icon-remove"></span>
                                                                                    </a>
                                                                                <%else%>
                                                                                    <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                                                                        <span class="icon16 icomoon-icon-remove"></span>
                                                                                    </a>
                                                                                <%/if%>
                                                                            <%/if%>
                                                                        </div>
                                                                    </td>
                                                                <%/if%>
                                                            </tr>
                                                            <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
                                                                <%javascript%>
                                                                var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
                                                                setTimeout(function(){
                                                                <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
                                                                    if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
                                                                    $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
                                                                    dataType: "json",
                                                                    type: "POST",
                                                                    url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
                                                                    },{
                                                                    loadingImg: admin_image_url+"chosen-loading.gif"
                                                                    });
                                                                }
                                                            <%/foreach%>
                                                            }, 500);
                                                            <%/javascript%>
                                                        <%/if%>
                                                    <%/section%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-row row-fluid " id="cc_sh_p_status">
                                        <label class="form-label span3 ">
                                            <%$form_config['p_status']['label_lang']%> <em>*</em> 
                                        </label> 
                                        <div class="form-right-div  ">
                                            <%assign var="opt_selected" value=$data['p_status']%>
                                            <%$this->dropdown->display("p_status","p_status","  title='<%$this->lang->line('PRODUCTS_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_STATUS')%>'  ", "|||", "", $opt_selected,"p_status")%>
                                        </div>
                                        <div class="error-msg-form "><label class='error' id='p_statusErr'></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                            <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                <%assign var='rm_ctrl_directions' value=true%>
                            <%/if%>
                            <%include file="products_add_buttons.tpl"%>
                        </div>
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {
        "product_image": {
            "pi_product_id": {
                "type": "dropdown",
                "vUniqueName": "pi_product_id",
                "editrules": {
                    "required": true,
                    "infoArr": {
                        "required": {
                            "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCT_IMAGE_PRODUCT)
                        }
                    }
                }
            },
            "pi_image": {
                "type": "file",
                "vUniqueName": "pi_image",
                "editrules": {
                    "required": true,
                    "infoArr": {
                        "required": {
                            "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCT_IMAGE_IMAGE)
                        }
                    }
                }
            }
        },
        "product_specification": {
            "ps_title": {
                "type": "textbox",
                "vUniqueName": "ps_title",
                "editrules": {
                    "infoArr": []
                }
            },
            "ps_value": {
                "type": "textbox",
                "vUniqueName": "ps_value",
                "editrules": {
                    "infoArr": []
                }
            },
            "ps_product_id": {
                "type": "dropdown",
                "vUniqueName": "ps_product_id",
                "editrules": {
                    "infoArr": []
                }
            },
            "color_picker": {
                "type": "color_picker",
                "vUniqueName": "color_picker",
                "editrules": {
                    "infoArr": []
                }
            }
        }
    };
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
        "child_messages" : {
            "delete_message" : {
        "product_image" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
        "product_specification" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
            }
        }
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/products_add_js.js')%>

<%$this->js->add_js("admin/custom/extended.js")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.products.callEvents();
<%/javascript%>