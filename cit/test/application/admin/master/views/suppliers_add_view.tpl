<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container">
    <%include file="suppliers_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="suppliers" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="suppliers" class="frm-module-block frm-view-block frm-thclm-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <input type="hidden" name="c_company_phone_code" id="c_company_phone_code" value="<%$data['c_company_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="c_company_alt_phone_code" id="c_company_alt_phone_code" value="<%$data['c_company_alt_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="c_added_date" id="c_added_date" value="<%$data['c_added_date']%>"  class='ignore-valid ' />
                    <input type="hidden" name="c_updat_date" id="c_updat_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_updat_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="c_status" id="c_status" value="<%$data['c_status']%>"  class='ignore-valid ' />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('SUPPLIERS_SUPPLIERS')%></h4></div>
                                <div class="content two-column-block <%$rl_theme_arr['frm_twclm_label_align']%>">
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_sys_static_field_1"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Company Information</h3></div></div><div class="two-block-view " id="cc_sh_sys_static_field_2"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Owner Information</h3></div></div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_c_company_logo"> 
                                            <label class="form-label span3"><%$form_config['c_company_logo']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <%$img_html['c_company_logo']%>
                                            </div>
                                        </div>
                                        <div class="two-block-view " id="cc_sh_c_owner_name"> 
                                            <label class="form-label span3"><%$form_config['c_owner_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span id="cc_md_c_owner_name">
                                                    <span class="frm-data-label"><strong><%$data['c_owner_name']%></strong></span>
                                                </span>
                                                <span id="cc_md_sys_custom_field_1">
                                                    <span class="frm-data-label"><strong><%$data['sys_custom_field_1']%></strong></span>
                                                </span>
                                                <span id="cc_md_sys_custom_field_2">
                                                    <span class="frm-data-label"><strong><%$data['sys_custom_field_2']%></strong></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_c_company_name"> 
                                            <label class="form-label span3"><%$form_config['c_company_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['c_company_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view " id="cc_sh_sys_custom_field_3"> 
                                            <label class="form-label span3"><%$form_config['sys_custom_field_3']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  <%if $mode eq 'Update'%>frm-elements-div<%/if%>" style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['sys_custom_field_3']%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view " id="cc_sh_c_short_name"> 
                                            <label class="form-label span3"><%$form_config['c_short_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['c_short_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="two-block-view " id="cc_sh_c_owner_email"> 
                                            <label class="form-label span3"><%$form_config['c_owner_email']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['c_owner_email']%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="two-block-view company_short_name" id="cc_sh_c_company_code"> 
                                            <label class="form-label span3"><%$form_config['c_company_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['c_company_code']%></strong></span>
                                            </div>
                                            </div><div class="two-block-view " id="cc_sh_sys_static_field_3"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Contact Information</h3></div></div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid">
                                            <div class="two-block-view " id="cc_sh_c_company_email"> 
                                                <label class="form-label span3"><%$form_config['c_company_email']['label_lang']%></label>
                                                <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                    <span class="frm-data-label"><strong><%$data['c_company_email']%></strong></span>
                                                </div>
                                            </div>
                                            <div class="two-block-view " id="cc_sh_c_owner_phone"> 
                                                <label class="form-label span3"><%$form_config['c_owner_phone']['label_lang']%></label>
                                                <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                    <span class="frm-data-label"><strong><%$data['c_owner_phone']%></strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid">
                                            <div class="two-block-view " id="cc_sh_sys_static_field_7"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Company Contact Information</h3></div></div><div class="two-block-view " id="cc_sh_sys_static_field_5"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Commission Information</h3></div></div>
                                        </div>
                                        <div class="column-view-parent form-row row-fluid">
                                            <div class="two-block-view " id="cc_sh_c_company_phone"> 
                                                <label class="form-label span3"><%$form_config['c_company_phone']['label_lang']%></label>
                                                <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                    <span class="frm-data-label"><strong><%$data['c_company_phone']%></strong></span>
                                                </div>
                                                </div><div class="two-block-view " id="cc_sh_sys_static_field_4"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Other Information</h3></div></div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_company_alt_phone"> 
                                                    <label class="form-label span3"><%$form_config['c_company_alt_phone']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_company_alt_phone']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_sys_static_field_6"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Company Address Information</h3></div></div><div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_street"> 
                                                    <label class="form-label span3"><%$form_config['c_street']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_street']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_country_id"> 
                                                    <label class="form-label span3"><%$form_config['c_country_id']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['c_country_id'], $opt_arr['c_country_id'])%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_state_id"> 
                                                    <label class="form-label span3"><%$form_config['c_state_id']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['c_state_id'], $opt_arr['c_state_id'])%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_city"> 
                                                    <label class="form-label span3"><%$form_config['c_city']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_city']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_zip_code"> 
                                                    <label class="form-label span3"><%$form_config['c_zip_code']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_zip_code']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_sys_static_field_8"> <div class="form-right-div frm-elements-div form-static-div"><h3 class="inner-title">Company Order Information</h3></div></div><div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_nature_of_business"> 
                                                    <label class="form-label span3"><%$form_config['c_nature_of_business']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_nature_of_business']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_company_reg_no"> 
                                                    <label class="form-label span3"><%$form_config['c_company_reg_no']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_company_reg_no']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_taxno"> 
                                                    <label class="form-label span3"><%$form_config['c_taxno']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_taxno']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_insurance_no"> 
                                                    <label class="form-label span3"><%$form_config['c_insurance_no']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_insurance_no']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_business_trading_name"> 
                                                    <label class="form-label span3"><%$form_config['c_business_trading_name']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_business_trading_name']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_business_type"> 
                                                    <label class="form-label span3"><%$form_config['c_business_type']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['c_business_type'], $opt_arr['c_business_type'])%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_parent_id"> 
                                                    <label class="form-label span3"><%$form_config['c_parent_id']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['c_parent_id'], $opt_arr['c_parent_id'])%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                            <div class="column-view-parent form-row row-fluid">
                                                <div class="two-block-view " id="cc_sh_c_company_website"> 
                                                    <label class="form-label span3"><%$form_config['c_company_website']['label_lang']%></label>
                                                    <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                        <span class="frm-data-label"><strong><%$data['c_company_website']%></strong></span>
                                                    </div>
                                                </div>
                                                <div class="two-block-view tab-focus-element">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%$this->js->add_js("admin/custom/extended.js")%>
<%$this->css->add_css("custom/Suppliers_extended.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
