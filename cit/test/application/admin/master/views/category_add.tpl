<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="category_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="category" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="category" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('CATEGORY_CATEGORY')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_c_category_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_category_name']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_category_name']|@htmlentities%>" name="c_category_name" id="c_category_name" title="<%$this->lang->line('CATEGORY_CATEGORY_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium apply-text-upper_case'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_category_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_category_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_category_code']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_category_code']|@htmlentities%>" name="c_category_code" id="c_category_code" title="<%$this->lang->line('CATEGORY_CATEGORY_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_category_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_image">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_image']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <div  class='btn-uploadify frm-size-medium' >
                                                    <input type="hidden" value="<%$data['c_image']%>" name="old_c_image" id="old_c_image" />
                                                    <input type="hidden" value="<%$data['c_image']%>" name="c_image" id="c_image"  aria-extensions="gif,png,jpg,jpeg" aria-valid-size="<%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB"/>
                                                    <input type="hidden" value="<%$data['c_image']%>" name="temp_c_image" id="temp_c_image"  />
                                                    <div id="upload_drop_zone_c_image" class="upload-drop-zone"></div>
                                                    <div class="uploader upload-src-zone">
                                                        <input type="file" name="uploadify_c_image" id="uploadify_c_image" title="<%$this->lang->line('CATEGORY_IMAGE')%>" />
                                                        <span class="filename" id="preview_c_image">
                                                            <%if $data['c_image'] neq ''%>
                                                                <%$data['c_image']%>
                                                            <%else%>
                                                                <%$this->lang->line('GENERIC_DROP_FILES_HERE_OR_CLICK_TO_UPLOAD')%>
                                                            <%/if%>
                                                        </span>
                                                        <span class="action">Choose File</span>
                                                    </div>
                                                </div>
                                                <div class='upload-image-btn'>
                                                    <%$img_html['c_image']%>
                                                </div>
                                                <span class="input-comment">
                                                    <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif, png, jpg, jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB."><span class="icomoon-icon-help"></span></a>
                                                </span>
                                                <div class='clear upload-progress' id='progress_c_image'>
                                                    <div class='upload-progress-bar progress progress-striped active'>
                                                        <div class='bar' id='practive_c_image'></div>
                                                    </div>
                                                    <div class='upload-cancel-div'><a class='upload-cancel' href='javascript://'>Cancel</a></div>
                                                    <div class='clear'></div>
                                                </div>
                                                <div class='clear'></div>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_imageErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_parent_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_parent_id']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_parent_id']%>
                                                <%$this->dropdown->display("c_parent_id","c_parent_id","  title='<%$this->lang->line('CATEGORY_PARENT_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORY_PARENT_ID')%>'  ", "|||", "", $opt_selected,"c_parent_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_parent_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_added_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_added_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_added_date'])%>" placeholder="" name="c_added_date" id="c_added_date" title="<%$this->lang->line('CATEGORY_ADDED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_added_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_added_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_added_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_added_date']%>
                                                <%$this->dropdown->display("c_added_date","c_added_date","  title='<%$this->lang->line('CATEGORY_ADDED_DATE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORY_ADDED_DATE')%>'  ", "|||", "", $opt_selected,"c_added_date")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_added_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_updat_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_updat_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_updat_date'])%>" placeholder="" name="c_updat_date" id="c_updat_date" title="<%$this->lang->line('CATEGORY_UPDAT_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_updat_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_company_id']%>
                                                <%$this->dropdown->display("c_company_id","c_company_id","  title='<%$this->lang->line('CATEGORY_COMPANY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORY_COMPANY_ID')%>'  ", "|||", "", $opt_selected,"c_company_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_status">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_status']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_status']%>
                                                <%$this->dropdown->display("c_status","c_status","  title='<%$this->lang->line('CATEGORY_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'CATEGORY_STATUS')%>'  ", "|||", "", $opt_selected,"c_status")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_statusErr'></label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                                <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                    <%assign var='rm_ctrl_directions' value=true%>
                                <%/if%>
                                <%include file="category_add_buttons.tpl"%>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/category_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.category.callEvents();
<%/javascript%>