<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="companies_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="companies" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="companies" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('COMPANIES_COMPANIES')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_c_company_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_name']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_name']|@htmlentities%>" name="c_company_name" id="c_company_name" title="<%$this->lang->line('COMPANIES_COMPANY_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_parent_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_parent_id']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_parent_id']%>
                                                <%$this->dropdown->display("c_parent_id","c_parent_id","  title='<%$this->lang->line('COMPANIES_PARENT_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_PARENT_ID')%>'  ", "|||", "", $opt_selected,"c_parent_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_parent_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_business_trading_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_business_trading_name']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_business_trading_name']|@htmlentities%>" name="c_business_trading_name" id="c_business_trading_name" title="<%$this->lang->line('COMPANIES_BUSINESS_TRADING_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_business_trading_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_short_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_short_name']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_short_name']|@htmlentities%>" name="c_short_name" id="c_short_name" title="<%$this->lang->line('COMPANIES_SHORT_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_short_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_nature_of_business">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_nature_of_business']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_nature_of_business']|@htmlentities%>" name="c_nature_of_business" id="c_nature_of_business" title="<%$this->lang->line('COMPANIES_NATURE_OF_BUSINESS')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_nature_of_businessErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_business_type">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_business_type']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_business_type']%>
                                                <%$this->dropdown->display("c_business_type","c_business_type","  title='<%$this->lang->line('COMPANIES_BUSINESS_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_BUSINESS_TYPE')%>'  ", "|||", "", $opt_selected,"c_business_type")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_business_typeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_taxno">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_taxno']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_taxno']|@htmlentities%>" name="c_taxno" id="c_taxno" title="<%$this->lang->line('COMPANIES_TAXNO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_taxnoErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_phone_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_phone_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_phone_code']|@htmlentities%>" name="c_company_phone_code" id="c_company_phone_code" title="<%$this->lang->line('COMPANIES_COMPANY_PHONE_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_phone_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_phone">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_phone']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_phone']|@htmlentities%>" name="c_company_phone" id="c_company_phone" title="<%$this->lang->line('COMPANIES_COMPANY_PHONE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_phoneErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_alt_phone_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_alt_phone_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_alt_phone_code']|@htmlentities%>" name="c_company_alt_phone_code" id="c_company_alt_phone_code" title="<%$this->lang->line('COMPANIES_COMPANY_ALT_PHONE_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_alt_phone_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_alt_phone">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_alt_phone']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_alt_phone']|@htmlentities%>" name="c_company_alt_phone" id="c_company_alt_phone" title="<%$this->lang->line('COMPANIES_COMPANY_ALT_PHONE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_alt_phoneErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_email">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_email']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_email']|@htmlentities%>" name="c_company_email" id="c_company_email" title="<%$this->lang->line('COMPANIES_COMPANY_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_emailErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_website">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_website']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_website']|@htmlentities%>" name="c_company_website" id="c_company_website" title="<%$this->lang->line('COMPANIES_COMPANY_WEBSITE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_websiteErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_company_reg_no">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_company_reg_no']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_company_reg_no']|@htmlentities%>" name="c_company_reg_no" id="c_company_reg_no" title="<%$this->lang->line('COMPANIES_COMPANY_REG_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_company_reg_noErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_insurance_no">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_insurance_no']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_insurance_no']|@htmlentities%>" name="c_insurance_no" id="c_insurance_no" title="<%$this->lang->line('COMPANIES_INSURANCE_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_insurance_noErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_street">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_street']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_street']|@htmlentities%>" name="c_street" id="c_street" title="<%$this->lang->line('COMPANIES_STREET')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_streetErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_country_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_country_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_country_id']%>
                                                <%$this->dropdown->display("c_country_id","c_country_id","  title='<%$this->lang->line('COMPANIES_COUNTRY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_COUNTRY_ID')%>'  ", "|||", "", $opt_selected,"c_country_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_country_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_state_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_state_id']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_state_id']%>
                                                <%$this->dropdown->display("c_state_id","c_state_id","  title='<%$this->lang->line('COMPANIES_STATE_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_STATE_ID')%>'  ", "|||", "", $opt_selected,"c_state_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_state_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_city">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_city']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_city']|@htmlentities%>" name="c_city" id="c_city" title="<%$this->lang->line('COMPANIES_CITY')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_cityErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_zip_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_zip_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_zip_code']|@htmlentities%>" name="c_zip_code" id="c_zip_code" title="<%$this->lang->line('COMPANIES_ZIP_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_zip_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_owner_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_owner_name']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_owner_name']|@htmlentities%>" name="c_owner_name" id="c_owner_name" title="<%$this->lang->line('COMPANIES_OWNER_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_owner_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_owner_email">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_owner_email']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_owner_email']|@htmlentities%>" name="c_owner_email" id="c_owner_email" title="<%$this->lang->line('COMPANIES_OWNER_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_owner_emailErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_owner_phone">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_owner_phone']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['c_owner_phone']|@htmlentities%>" name="c_owner_phone" id="c_owner_phone" title="<%$this->lang->line('COMPANIES_OWNER_PHONE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_owner_phoneErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_added_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_added_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_added_date'])%>" placeholder="" name="c_added_date" id="c_added_date" title="<%$this->lang->line('COMPANIES_ADDED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_added_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_added_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_added_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_added_date']%>
                                                <%$this->dropdown->display("c_added_date","c_added_date","  title='<%$this->lang->line('COMPANIES_ADDED_DATE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_ADDED_DATE')%>'  ", "|||", "", $opt_selected,"c_added_date")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_added_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_updat_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_updat_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['c_updat_date'])%>" placeholder="" name="c_updat_date" id="c_updat_date" title="<%$this->lang->line('COMPANIES_UPDAT_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_updat_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_c_status">
                                            <label class="form-label span3 ">
                                                <%$form_config['c_status']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['c_status']%>
                                                <%$this->dropdown->display("c_status","c_status","  title='<%$this->lang->line('COMPANIES_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'COMPANIES_STATUS')%>'  ", "|||", "", $opt_selected,"c_status")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='c_statusErr'></label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                                <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                    <%assign var='rm_ctrl_directions' value=true%>
                                <%/if%>
                                <%include file="companies_add_buttons.tpl"%>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/companies_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.companies.callEvents();
<%/javascript%>