<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-form-container">
    <%include file="supplier_company_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="supplier_company" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <div id="supplier_company" class="frm-module-block frm-elem-block frm-stand-view">
                    <!-- Module Form Block -->
                    <form name="frmaddupdate" id="frmaddupdate" action="<%$admin_url%><%$mod_enc_url['add_action']%>?<%$extra_qstr%>" method="post"  enctype="multipart/form-data">
                        <!-- Form Hidden Fields Unit -->
                        <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                        <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                        <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                        <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                        <input type="hidden" id="draft_uniq_id" name="draft_uniq_id" value="<%$draft_uniq_id%>" />
                        <input type="hidden" id="extra_hstr" name="extra_hstr" value="<%$extra_hstr%>" />
                        <!-- Form Dispaly Fields Unit -->
                        <div class="main-content-block " id="main_content_block">
                            <div style="width:98%" class="frm-block-layout pad-calc-container">
                                <div class="box gradient <%$rl_theme_arr['frm_stand_content_row']%> <%$rl_theme_arr['frm_stand_border_view']%>">
                                    <div class="title <%$rl_theme_arr['frm_stand_titles_bar']%>"><h4><%$this->lang->line('SUPPLIER_COMPANY_SUPPLIER_COMPANY')%></h4></div>
                                    <div class="content <%$rl_theme_arr['frm_stand_label_align']%>">
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_name']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_name']|@htmlentities%>" name="scd_company_name" id="scd_company_name" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_code']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_code']|@htmlentities%>" name="scd_company_code" id="scd_company_code" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_nature_of_business">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_nature_of_business']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_nature_of_business']|@htmlentities%>" name="scd_nature_of_business" id="scd_nature_of_business" title="<%$this->lang->line('SUPPLIER_COMPANY_NATURE_OF_BUSINESS')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_nature_of_businessErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_admin_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_admin_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_admin_id']%>
                                                <%$this->dropdown->display("scd_admin_id","scd_admin_id","  title='<%$this->lang->line('SUPPLIER_COMPANY_ADMIN_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_ADMIN_ID')%>'  ", "|||", "", $opt_selected,"scd_admin_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_admin_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_vendor_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_vendor_code']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_vendor_code']|@htmlentities%>" name="scd_vendor_code" id="scd_vendor_code" title="<%$this->lang->line('SUPPLIER_COMPANY_VENDOR_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_vendor_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_phone_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_phone_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_phone_code']|@htmlentities%>" name="scd_company_phone_code" id="scd_company_phone_code" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_PHONE_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_phone_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_phone">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_phone']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_phone']|@htmlentities%>" name="scd_company_phone" id="scd_company_phone" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_PHONE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_phoneErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_alt_phone_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_alt_phone_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_alt_phone_code']|@htmlentities%>" name="scd_company_alt_phone_code" id="scd_company_alt_phone_code" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_ALT_PHONE_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_alt_phone_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_alt_phone">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_alt_phone']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_alt_phone']|@htmlentities%>" name="scd_company_alt_phone" id="scd_company_alt_phone" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_ALT_PHONE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_alt_phoneErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_taxno">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_taxno']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_taxno']|@htmlentities%>" name="scd_taxno" id="scd_taxno" title="<%$this->lang->line('SUPPLIER_COMPANY_TAXNO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_taxnoErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_reg_no">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_reg_no']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_reg_no']|@htmlentities%>" name="scd_company_reg_no" id="scd_company_reg_no" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_REG_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_reg_noErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_insurance_no">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_insurance_no']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_insurance_no']|@htmlentities%>" name="scd_insurance_no" id="scd_insurance_no" title="<%$this->lang->line('SUPPLIER_COMPANY_INSURANCE_NO')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_insurance_noErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_commission_type">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_commission_type']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_commission_type']%>
                                                <%$this->dropdown->display("scd_commission_type","scd_commission_type","  title='<%$this->lang->line('SUPPLIER_COMPANY_COMMISSION_TYPE')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_COMMISSION_TYPE')%>'  ", "|||", "", $opt_selected,"scd_commission_type")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_commission_typeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_commission_value">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_commission_value']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_commission_value']|@htmlentities%>" name="scd_commission_value" id="scd_commission_value" title="<%$this->lang->line('SUPPLIER_COMPANY_COMMISSION_VALUE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_commission_valueErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_email">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_email']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_email']|@htmlentities%>" name="scd_company_email" id="scd_company_email" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_EMAIL')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_emailErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_address">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_address']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <textarea placeholder=""  name="scd_address" id="scd_address" title="<%$this->lang->line('SUPPLIER_COMPANY_ADDRESS')%>"  data-ctrl-type='textarea'  class='elastic frm-size-medium'  ><%$data['scd_address']%></textarea>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_addressErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_country_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_country_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_country_id']%>
                                                <%$this->dropdown->display("scd_country_id","scd_country_id","  title='<%$this->lang->line('SUPPLIER_COMPANY_COUNTRY_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_COUNTRY_ID')%>'  ", "|||", "", $opt_selected,"scd_country_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_country_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_state_id">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_state_id']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_state_id']%>
                                                <%$this->dropdown->display("scd_state_id","scd_state_id","  title='<%$this->lang->line('SUPPLIER_COMPANY_STATE_ID')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_STATE_ID')%>'  ", "|||", "", $opt_selected,"scd_state_id")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_state_idErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_city">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_city']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_city']|@htmlentities%>" name="scd_city" id="scd_city" title="<%$this->lang->line('SUPPLIER_COMPANY_CITY')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_cityErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_zip_code">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_zip_code']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_zip_code']|@htmlentities%>" name="scd_zip_code" id="scd_zip_code" title="<%$this->lang->line('SUPPLIER_COMPANY_ZIP_CODE')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_zip_codeErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_incorporation_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_incorporation_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['scd_incorporation_date'])%>" placeholder="" name="scd_incorporation_date" id="scd_incorporation_date" title="<%$this->lang->line('SUPPLIER_COMPANY_INCORPORATION_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_incorporation_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_logo">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_logo']['label_lang']%> <em>*</em> 
                                            </label> 
                                            <div class="form-right-div  ">
                                                <div  class='btn-uploadify frm-size-medium' >
                                                    <input type="hidden" value="<%$data['scd_company_logo']%>" name="old_scd_company_logo" id="old_scd_company_logo" />
                                                    <input type="hidden" value="<%$data['scd_company_logo']%>" name="scd_company_logo" id="scd_company_logo"  aria-extensions="gif,png,jpg,jpeg" aria-valid-size="<%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB"/>
                                                    <input type="hidden" value="<%$data['scd_company_logo']%>" name="temp_scd_company_logo" id="temp_scd_company_logo"  />
                                                    <div id="upload_drop_zone_scd_company_logo" class="upload-drop-zone"></div>
                                                    <div class="uploader upload-src-zone">
                                                        <input type="file" name="uploadify_scd_company_logo" id="uploadify_scd_company_logo" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_LOGO')%>" />
                                                        <span class="filename" id="preview_scd_company_logo">
                                                            <%if $data['scd_company_logo'] neq ''%>
                                                                <%$data['scd_company_logo']%>
                                                            <%else%>
                                                                <%$this->lang->line('GENERIC_DROP_FILES_HERE_OR_CLICK_TO_UPLOAD')%>
                                                            <%/if%>
                                                        </span>
                                                        <span class="action">Choose File</span>
                                                    </div>
                                                </div>
                                                <div class='upload-image-btn'>
                                                    <%$img_html['scd_company_logo']%>
                                                </div>
                                                <span class="input-comment">
                                                    <a href="javascript://" style="text-decoration: none;" class="tipR" title="<%$this->lang->line('GENERIC_VALID_EXTENSIONS')%> : gif, png, jpg, jpeg.<br><%$this->lang->line('GENERIC_VALID_SIZE')%> : <%$this->lang->line('GENERIC_LESS_THAN')%> (<) 100 MB."><span class="icomoon-icon-help"></span></a>
                                                </span>
                                                <div class='clear upload-progress' id='progress_scd_company_logo'>
                                                    <div class='upload-progress-bar progress progress-striped active'>
                                                        <div class='bar' id='practive_scd_company_logo'></div>
                                                    </div>
                                                    <div class='upload-cancel-div'><a class='upload-cancel' href='javascript://'>Cancel</a></div>
                                                    <div class='clear'></div>
                                                </div>
                                                <div class='clear'></div>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_logoErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_company_short_name">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_company_short_name']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <input type="text" placeholder="" value="<%$data['scd_company_short_name']|@htmlentities%>" name="scd_company_short_name" id="scd_company_short_name" title="<%$this->lang->line('SUPPLIER_COMPANY_COMPANY_SHORT_NAME')%>"  data-ctrl-type='textbox'  class='frm-size-medium'  />
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_company_short_nameErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_added_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_added_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['scd_added_date'])%>" placeholder="" name="scd_added_date" id="scd_added_date" title="<%$this->lang->line('SUPPLIER_COMPANY_ADDED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_added_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_updated_date">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_updated_date']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  input-append text-append-prepend  ">
                                                <input type="text" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['scd_updated_date'])%>" placeholder="" name="scd_updated_date" id="scd_updated_date" title="<%$this->lang->line('SUPPLIER_COMPANY_UPDATED_DATE')%>"  data-ctrl-type='date'  class='frm-datepicker ctrl-append-prepend frm-size-medium'  aria-date-format='yy-mm-dd'  aria-format-type='date'  />
                                                <span class='add-on text-addon date-append-class icomoon-icon-calendar'></span>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_updated_dateErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_added_by">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_added_by']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_added_by']%>
                                                <%$this->dropdown->display("scd_added_by","scd_added_by","  title='<%$this->lang->line('SUPPLIER_COMPANY_ADDED_BY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_ADDED_BY')%>'  ", "|||", "", $opt_selected,"scd_added_by")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_added_byErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_updated_by">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_updated_by']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_updated_by']%>
                                                <%$this->dropdown->display("scd_updated_by","scd_updated_by","  title='<%$this->lang->line('SUPPLIER_COMPANY_UPDATED_BY')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_UPDATED_BY')%>'  ", "|||", "", $opt_selected,"scd_updated_by")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_updated_byErr'></label></div>
                                        </div>
                                        <div class="form-row row-fluid " id="cc_sh_scd_status">
                                            <label class="form-label span3 ">
                                                <%$form_config['scd_status']['label_lang']%>
                                            </label> 
                                            <div class="form-right-div  ">
                                                <%assign var="opt_selected" value=$data['scd_status']%>
                                                <%$this->dropdown->display("scd_status","scd_status","  title='<%$this->lang->line('SUPPLIER_COMPANY_STATUS')%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'SUPPLIER_COMPANY_STATUS')%>'  ", "|||", "", $opt_selected,"scd_status")%>
                                            </div>
                                            <div class="error-msg-form "><label class='error' id='scd_statusErr'></label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="frm-bot-btn <%$rl_theme_arr['frm_stand_action_bar']%> <%$rl_theme_arr['frm_stand_action_btn']%> popup-footer">
                                <%if $rl_theme_arr['frm_stand_ctrls_view'] eq 'No'%>
                                    <%assign var='rm_ctrl_directions' value=true%>
                                <%/if%>
                                <%include file="supplier_company_add_buttons.tpl"%>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Module Form Javascript -->
<%javascript%>
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];
    
    google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
    child_rules_arr = {};
            
    <%if $auto_arr|@is_array && $auto_arr|@count gt 0%>
        setTimeout(function(){
            <%foreach name=i from=$auto_arr item=v key=k%>
                if($("#<%$k%>").is("select")){
                    $("#<%$k%>").ajaxChosen({
                        dataType: "json",
                        type: "POST",
                        url: el_form_settings.get_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$mode]%>&id=<%$enc_id%>"
                        },{
                        loadingImg: admin_image_url+"chosen-loading.gif"
                    });
                }
            <%/foreach%>
        }, 500);
    <%/if%>        
    el_form_settings['jajax_submit_func'] = '';
    el_form_settings['jajax_submit_back'] = '';
    el_form_settings['jajax_action_url'] = '<%$admin_url%><%$mod_enc_url["add_action"]%>?<%$extra_qstr%>';
    el_form_settings['save_as_draft'] = 'No';
    el_form_settings['multi_lingual_trans'] = 'Yes';
    el_form_settings['buttons_arr'] = [];
    el_form_settings['message_arr'] = {
        "delete_message" : "<%$this->general->processMessageLabel('ACTION_ARE_YOU_SURE_WANT_TO_DELETE_THIS_RECORD_C63')%>",
    };
    
    
    callSwitchToSelf();
<%/javascript%>
<%$this->js->add_js('admin/supplier_company_add_js.js')%>

<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 
<%javascript%>
    Project.modules.supplier_company.callEvents();
<%/javascript%>