<ul class="nav nav-tabs module-tab-container">
    <li <%if $module_name eq "products"%> class="active" <%/if%>>
        <a class="tab-item item-products" 
        <%if $mode eq "Add" && $module_name eq "products"%>
            title="<%$this->lang->line('GENERIC_ADD')%> <%$this->lang->line('PRODUCTS_PRODUCTS')%>"
        <%else%>
            title="<%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCTS_PRODUCTS')%>"
        <%/if%>
        <%if $module_name eq "products"%> 
            href="javascript://"
        <%else%> 
            href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('master/products/add')%>|mode|<%$mod_enc_mode['Update']%>|id|<%$this->general->getAdminEncodeURL($parID)%>" 
        <%/if%>
        >
        <%if $mode eq "Add" && $module_name eq "products"%>
            <%$this->lang->line('GENERIC_ADD')%> <%$this->lang->line('PRODUCTS_PRODUCTS')%>
        <%else%>
            <%$this->lang->line('GENERIC_EDIT')%> <%$this->lang->line('PRODUCTS_PRODUCTS')%>
        <%/if%>
    </a>
</li>
<li <%if $module_name eq "product_specification"%> class="active" <%/if%>>
    <a class="tab-item item-product_specification"  title="<%$this->lang->line('PRODUCTS_PRODUCT_SPECIFICATION')%> <%$this->lang->line('GENERIC_LIST')%>" 
        <%if $module_name eq "product_specification"%> 
            href="javascript://"
        <%elseif $module_name eq "products"%> 
            <%if $mode eq "Update"%>
                href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('master/product_specification/index')%>|parMod|<%$this->general->getAdminEncodeURL('products')%>|parID|<%$this->general->getAdminEncodeURL($data['iProductId'])%>"
            <%else%>
                href="javascript://" aria-disabled="true" 
            <%/if%>                    
        <%else%> 
            href="<%$admin_url%>#<%$this->general->getAdminEncodeURL('master/product_specification/index')%>|parMod|<%$this->general->getAdminEncodeURL('products')%>|parID|<%$this->general->getAdminEncodeURL($parID)%>" 
        <%/if%>
        >
        <%$this->lang->line('PRODUCTS_PRODUCT_SPECIFICATION')%>
    </a>
</li>
</ul>            