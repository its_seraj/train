<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->clean_js()%>
<%/if%>
<div class="module-view-container">
    <%include file="supplier_management_form_add_strip.tpl"%>
    <div class="<%$module_name%>" data-form-name="<%$module_name%>">
        <div id="ajax_content_div" class="ajax-content-div top-frm-spacing" >
            <input type="hidden" id="projmod" name="projmod" value="supplier_management_form" />
            <!-- Page Loader -->
            <div id="ajax_qLoverlay"></div>
            <div id="ajax_qLbar"></div>
            <!-- Module Tabs & Top Detail View -->
            <div class="top-frm-tab-layout" id="top_frm_tab_layout">
            </div>
            <!-- Middle Content -->
            <div id="scrollable_content" class="scrollable-content popup-content top-block-spacing ">
                <!-- Module View Block -->
                <div id="supplier_management_form" class="frm-module-block frm-view-block frm-thclm-view">
                    <!-- Form Hidden Fields Unit -->
                    <input type="hidden" id="id" name="id" value="<%$enc_id%>" />
                    <input type="hidden" id="mode" name="mode" value="<%$mod_enc_mode[$mode]%>" />
                    <input type="hidden" id="ctrl_flow" name="ctrl_flow" value="<%$ctrl_flow%>" />
                    <input type="hidden" id="ctrl_prev_id" name="ctrl_prev_id" value="<%$next_prev_records['prev']['id']%>" />
                    <input type="hidden" id="ctrl_next_id" name="ctrl_next_id" value="<%$next_prev_records['next']['id']%>" />
                    <input type="hidden" name="scd_company_alt_phone_code" id="scd_company_alt_phone_code" value="<%$data['scd_company_alt_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_company_phone_code" id="scd_company_phone_code" value="<%$data['scd_company_phone_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_supplier_id" id="scd_supplier_id" value="<%$data['scd_supplier_id']%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_vendor_code" id="scd_vendor_code" value="<%$data['scd_vendor_code']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_commission_type" id="scd_commission_type" value="<%$data['scd_commission_type']%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_commission_value" id="scd_commission_value" value="<%$data['scd_commission_value']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_added_date" id="scd_added_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['scd_added_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="scd_updated_date" id="scd_updated_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['scd_updated_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="scd_added_by" id="scd_added_by" value="<%$data['scd_added_by']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_updated_by" id="scd_updated_by" value="<%$data['scd_updated_by']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="scd_status" id="scd_status" value="<%$data['scd_status']%>"  class='ignore-valid ' />
                    <input type="hidden" name="sd_status" id="sd_status" value="<%$data['sd_status']%>"  class='ignore-valid ' />
                    <input type="hidden" name="sd_added_date" id="sd_added_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['sd_added_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="sd_added_by" id="sd_added_by" value="<%$data['sd_added_by']|@htmlentities%>"  class='ignore-valid ' />
                    <input type="hidden" name="sd_updated_date" id="sd_updated_date" value="<%$this->general->dateDefinedFormat('Y-m-d',$data['sd_updated_date'])%>"  class='ignore-valid '  aria-date-format='yy-mm-dd'  aria-format-type='date' />
                    <input type="hidden" name="sd_updated_by" id="sd_updated_by" value="<%$data['sd_updated_by']|@htmlentities%>"  class='ignore-valid ' />
                    <!-- Form Display Fields Unit -->
                    <div class="main-content-block " id="main_content_block">
                        <div style="width:98%;" class="frm-block-layout pad-calc-container">
                            <div class="box gradient <%$rl_theme_arr['frm_twclm_content_row']%> <%$rl_theme_arr['frm_twclm_border_view']%>">
                                <div class="title <%$rl_theme_arr['frm_twclm_titles_bar']%>"><h4><%$this->lang->line('SUPPLIERS_SUPPLIERS')%></h4></div>
                                
                                <div class="content three-column-block <%$rl_theme_arr['frm_twclm_label_align']%>">
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_sys_static_field_1"> 
                                            <div class="form-right-div frm-elements-div form-static-div">
                                                <div style="position:absolute;"><h2 ><b>Company Information</b></h2><hr style=" border: 1px solid #ffd700; margin:0px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_scd_company_logo"> 
                                            <label class="form-label span3"><%$form_config['c_company_logo']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <%$img_html['c_company_logo']%>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_company_name"> 
                                            <label class="form-label span3"><%$form_config['scd_company_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_company_code"> 
                                            <label class="form-label span3"><%$form_config['scd_company_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_code']%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        
                                        <div class="three-block-view " id="cc_sh_scd_company_short_name"> 
                                            <label class="form-label span3"><%$form_config['scd_company_short_name']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_short_name']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_company_email"> 
                                            <label class="form-label span3"><%$form_config['scd_company_email']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_email']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_incorporation_date"> 
                                            <label class="form-label span3"><%$form_config['scd_incorporation_date']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->dateSystemFormat($data['scd_incorporation_date'])%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_scd_company_phone"> 
                                        <label class="form-label span3"><%$form_config['scd_company_phone']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_phone']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_company_alt_phone"> 
                                            <label class="form-label span3"><%$form_config['scd_company_alt_phone']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_alt_phone']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_country_id"> 
                                            <label class="form-label span3"><%$form_config['scd_country_id']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['scd_country_id'], $opt_arr['scd_country_id'])%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_scd_state_id"> 
                                            <label class="form-label span3"><%$form_config['scd_state_id']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->displayKeyValueData($data['scd_state_id'], $opt_arr['scd_state_id'])%></strong></span>
                                            </div>
                                        </div>three
                                        <div class="three-block-view " id="cc_sh_scd_city"> 
                                            <label class="form-label span3"><%$form_config['scd_city']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_city']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_zip_code"> 
                                            <label class="form-label span3"><%$form_config['scd_zip_code']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_zip_code']%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_scd_address"> 
                                            <label class="form-label span3"><%$form_config['scd_address']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$this->general->processQuotes($data['scd_address'])%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_settlement_duration"> 
                                            <label class="form-label span3"><%$form_config['scd_settlement_duration']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div  input-append text-append-prepend" style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_settlement_duration']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_nature_of_business"> 
                                            <label class="form-label span3"><%$form_config['scd_nature_of_business']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_nature_of_business']%></strong></span>
                                            </div>
                                        </div>three
                                    </div>
                                  
                                    <div class="column-view-parent form-row row-fluid">
                                        <div class="three-block-view " id="cc_sh_scd_company_reg_no"> 
                                            <label class="form-label span3"><%$form_config['scd_company_reg_no']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_company_reg_no']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_insurance_no"> 
                                            <label class="form-label span3"><%$form_config['scd_insurance_no']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_insurance_no']%></strong></span>
                                            </div>
                                        </div>
                                        <div class="three-block-view " id="cc_sh_scd_taxno"> 
                                            <label class="form-label span3"><%$form_config['scd_taxno']['label_lang']%></label>
                                            <div class="form-right-div frm-elements-div " style="width:60%!important">
                                                <span class="frm-data-label"><strong><%$data['scd_taxno']%></strong></span>
                                            </div>
                                        </div>
                                    </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%javascript%>        
            
    var el_form_settings = {}, elements_uni_arr = {}, child_rules_arr = {}, google_map_json = {}, pre_cond_code_arr = [];
    el_form_settings['module_name'] = '<%$module_name%>'; 
    el_form_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_form_settings['extra_qstr'] = '<%$extra_qstr%>';
    el_form_settings['upload_form_file_url'] = admin_url+"<%$mod_enc_url['upload_form_file']%>?<%$extra_qstr%>";
    el_form_settings['get_chosen_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_chosen_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['token_auto_complete_url'] = admin_url+"<%$mod_enc_url['get_token_auto_complete']%>?<%$extra_qstr%>";
    el_form_settings['tab_wise_block_url'] = admin_url+"<%$mod_enc_url['get_tab_wise_block']%>?<%$extra_qstr%>";
    el_form_settings['parent_source_options_url'] = "<%$mod_enc_url['parent_source_options']%>?<%$extra_qstr%>";
    el_form_settings['jself_switchto_url'] =  admin_url+'<%$switch_cit["url"]%>';
    el_form_settings['callbacks'] = [];

    callSwitchToSelf();
<%/javascript%>

<%$this->css->add_css("custom/extended_form.css")%>
<%if $this->input->is_ajax_request()%>
    <%$this->js->js_src()%>
<%/if%> 
<%if $this->input->is_ajax_request()%>
    <%$this->css->css_src()%>
<%/if%> 