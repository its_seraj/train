<%section name=i loop=1%>
    <tr id="tr_child_row_<%$child_module_name%>_<%$row_index%>" data-row-index="<%$row_index%>">
        <td class="row-num-child" data-col-type="serial">
            <span class="row-num-span"><%$row_index%></span>
            <input type="hidden" name="child[rating][id][<%$row_index%>]" id="child_rating_id_<%$row_index%>" value="<%$child_id%>" />
            <input type="hidden" name="child[rating][enc_id][<%$row_index%>]" id="child_rating_enc_id_<%$row_index%>" value="<%$enc_child_id%>" />
            <input type="hidden" name="child[rating][rm_product_id][<%$row_index%>]" id="child_rating_rm_product_id_<%$row_index%>" value="<%$child_data[i]['rm_product_id']%>"  class='ignore-valid ' />
        </td>
        <td data-col-type="column" data-col-name="rm_user_id">
            <div class=" " id="ch_rating_cc_sh_rm_user_id_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['rm_user_id']%>
                <%$this->dropdown->display("child_rating_rm_user_id_<%$row_index%>","child[rating][rm_user_id][<%$row_index%>]","  title='<%$child_conf_arr['form_config']['rm_user_id']['label_lang']%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RATING_USER_ID')%>'  ","|||","",$opt_selected,"child_rating_rm_user_id_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_rating_rm_user_id_<%$row_index%>Err'></label>
            </div>
        </td>
        <td data-col-type="column" data-col-name="rm_rate">
            <div class=" <%if $recMode eq 'Update'%>frm-elements-div<%/if%> " id="ch_rating_cc_sh_rm_rate_<%$row_index%>">
                <input aria-raty-clear="true" aria-raty-readonly="true" type="hidden" value="<%$child_data[i]['rm_rate']%>" name="child[rating][rm_rate][<%$row_index%>]" id="child_rating_rm_rate_<%$row_index%>" title="<%$child_conf_arr['form_config']['rm_rate']['label_lang']%>"  />
                <span id='rating_child_rating_rm_rate_<%$row_index%>' class='rating-icons-block'></span>  
            </div>
            <div>
                <label class='error' id='child_rating_rm_rate_<%$row_index%>Err'></label>
            </div>
        </td>
        <td data-col-type="column" data-col-name="rm_status">
            <div class=" " id="ch_rating_cc_sh_rm_status_<%$row_index%>">
                <%assign var="opt_selected" value=$child_data[i]['rm_status']%>
                <%$this->dropdown->display("child_rating_rm_status_<%$row_index%>","child[rating][rm_status][<%$row_index%>]","  title='<%$child_conf_arr['form_config']['rm_status']['label_lang']%>'  aria-chosen-valid='Yes'  class='chosen-select frm-size-medium'  data-placeholder='<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'RATING_STATUS')%>'  ","|||","",$opt_selected,"child_rating_rm_status_$row_index")%>  
            </div>
            <div>
                <label class='error' id='child_rating_rm_status_<%$row_index%>Err'></label>
            </div>
        </td>
        <%if $child_access_arr["actions"] eq 1%>
            <td align="center" data-col-type="actions">
                <div class="controls center">
                    <%if $child_access_arr["save"] eq 1%>
                        <%if $mode eq "Update"%>
                            <a onclick="saveChildModuleSingleData('<%$mod_enc_url.child_data_save%>', '<%$mod_enc_url.child_data_add%>', '<%$child_module_name%>', '<%$recMode%>', '<%$row_index%>', '<%$enc_child_id%>', 'No', '<%$mode%>')" href="javascript://" class="tip action-child-module-save" title="<%$this->lang->line('GENERIC_SAVE')%>">
                                <span class="icon14 icomoon-icon-disk"></span>
                            </a>
                        <%/if%>
                        <%/if%><%if $child_access_arr["delete"] eq 1%>
                        <%if $recMode eq "Update"%>
                            <a onclick="deleteChildModuleSingleData('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>','<%$enc_child_id%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                <span class="icon16 icomoon-icon-remove"></span>
                            </a>
                        <%else%>
                            <a onclick="deleteChildModuleRow('<%$mod_enc_url.child_data_delete%>','<%$child_module_name%>', '<%$row_index%>')" href="javascript://" class="tip action-child-module-delete" title="<%$this->lang->line('GENERIC_DELETE')%>" >
                                <span class="icon16 icomoon-icon-remove"></span>
                            </a>
                        <%/if%>
                    <%/if%>
                </div>
            </td>
        <%/if%>
    </tr>
    <%if $child_auto_arr[$row_index]|@is_array && $child_auto_arr[$row_index]|@count gt 0%>
        <%javascript%>
        var $child_chosen_auto_complete_url = admin_url+""+$("#childModuleChosenURL_<%$child_module_name%>").val()+"?";
        setTimeout(function(){
        <%foreach name=i from=$child_auto_arr[$row_index] item=v key=k%>
            if($("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").is("select")){
            $("#child_<%$child_module_name%>_<%$k%>_<%$row_index%>").ajaxChosen({
            dataType: "json",
            type: "POST",
            url: $child_chosen_auto_complete_url+"&unique_name=<%$k%>&mode=<%$mod_enc_mode[$recMode]%>&id=<%$enc_child_id%>"
            },{
            loadingImg: admin_image_url+"chosen-loading.gif"
            });
        }
    <%/foreach%>
    }, 500);
    <%/javascript%>
<%/if%>
<%/section%>
<%javascript%>
var google_map_json = $.parseJSON('<%$google_map_arr|@json_encode%>');
function initChildRenderJSScript(){
Project.modules.products.childEvents("rating", "#tr_child_row_<%$child_module_name%>_<%$row_index%>");
callGoogleMapEvents();
}
<%/javascript%>