<%javascript%>
    $.fn.editable.defaults.mode = 'inline', $.fn.editable.defaults.clear = false;
    var el_topview_settings = {}, detail_view_colmodel_json = {}, detail_token_input_assign = {}, detail_token_pre_populates = {};
              
    el_topview_settings['extra_hstr'] = '<%$extra_hstr%>';
    el_topview_settings['extra_qstr'] = '<%$extra_qstr%>';
                    
    el_topview_settings['layout_view_id'] = '<%$extra_hstr%>';
    el_topview_settings['edit_id'] = '<%$enc_detail_primary_id%>';
    el_topview_settings['module_name'] = '<%$module_name%>';
                    
                    
    el_topview_settings['edit_page_url'] = admin_url+'<%$mod_enc_url["inline_edit_action"]%>'+'?&oper=edit&<%$extra_qstr%>';
    el_topview_settings['ajax_data_url'] = admin_url+'<%$mod_enc_url["get_chosen_auto_complete"]%>'+'?<%$extra_qstr%>';
    el_topview_settings['permit_edit_btn'] = '<%$edit_access%>';
    
    detail_view_colmodel_json = {
        "dv_p_product_name": {
            "htmlID": "dv_p_product_name",
            "name": "p_product_name",
            "label": "Product Name",
            "label_lang": "<%$this->lang->line('PRODUCTS_PRODUCT_NAME')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_product_name']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_product_name']['value']%>",
            "dbval": "<%$list_config['p_product_name']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRODUCT_NAME)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "dv_p_category_id": {
            "htmlID": "dv_p_category_id",
            "name": "p_category_id",
            "label": "Category Id",
            "label_lang": "<%$this->lang->line('PRODUCTS_CATEGORY_ID')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['p_category_id']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_category_id']['value']%>",
            "dbval": "<%$list_config['p_category_id']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_CATEGORY)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr["p_category_id"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_category_id&id=<%$enc_detail_primary_id%>&par_field=<%$top_par_field%>&par_data=<%$top_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_category_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_CATEGORY_ID')%>"
            }
        },
        "dv_p_brand_id": {
            "htmlID": "dv_p_brand_id",
            "name": "p_brand_id",
            "label": "Brand Id",
            "label_lang": "<%$this->lang->line('PRODUCTS_BRAND_ID')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['p_brand_id']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_brand_id']['value']%>",
            "dbval": "<%$list_config['p_brand_id']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_BRAND)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr["p_brand_id"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_brand_id&id=<%$enc_detail_primary_id%>&par_field=<%$top_par_field%>&par_data=<%$top_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_brand_id",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_BRAND_ID')%>"
            }
        },
        "dv_p_price": {
            "htmlID": "dv_p_price",
            "name": "p_price",
            "label": "Price",
            "label_lang": "<%$this->lang->line('PRODUCTS_PRICE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_price']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_price']['value']%>",
            "dbval": "<%$list_config['p_price']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_PRICE)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "dv_p_retail_price": {
            "htmlID": "dv_p_retail_price",
            "name": "p_retail_price",
            "label": "Retail Price",
            "label_lang": "<%$this->lang->line('PRODUCTS_RETAIL_PRICE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['p_retail_price']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_retail_price']['value']%>",
            "dbval": "<%$list_config['p_retail_price']['dbval']%>",
            "edittype": "text",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_RETAIL_PRICE)
                    }
                }
            },
            "editoptions": {
                "text_case": "",
                "placeholder": ""
            },
            "extra_class": ""
        },
        "dv_p_status": {
            "htmlID": "dv_p_status",
            "name": "p_status",
            "label": "Status",
            "label_lang": "<%$this->lang->line('PRODUCTS_STATUS')%>",
            "type": "dropdown",
            "ctrl_type": "dropdown",
            "editable": <%if $list_config['p_status']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['p_status']['value']%>",
            "dbval": "<%$list_config['p_status']['dbval']%>",
            "edittype": "select",
            "editrules": {
                "required": true,
                "infoArr": {
                    "required": {
                        "message": ci_js_validation_message(js_lang_label.GENERIC_PLEASE_ENTER_A_VALUE_FOR_THE__C35FIELD_C35_FIELD_C46 ,"#FIELD#",js_lang_label.PRODUCTS_STATUS)
                    }
                }
            },
            "editoptions": {
                "ajaxCall": '<%if $count_arr["p_status"]["ajax"] eq "Yes" %>ajax-call<%/if%>',
                "dataUrl": '<%$admin_url%><%$mod_enc_url["get_list_options"]%>?alias_name=p_status&id=<%$enc_detail_primary_id%>&par_field=<%$top_par_field%>&par_data=<%$top_par_id%>&mode=<%$mod_enc_mode["Update"]%>&rformat=json<%$extra_qstr%>',
                "rel": "p_status",
                "data_placeholder": "<%$this->general->parseLabelMessage('GENERIC_PLEASE_SELECT__C35FIELD_C35' ,'#FIELD#', 'PRODUCTS_STATUS')%>"
            }
        },
        "dv_pi_image": {
            "htmlID": "dv_pi_image",
            "name": "pi_image",
            "label": "Image",
            "label_lang": "<%$this->lang->line('PRODUCTS_IMAGE')%>",
            "type": "textbox",
            "ctrl_type": "textbox",
            "editable": <%if $list_config['pi_image']['viewedit'] eq 'Yes' %>true<%else%>false<%/if%>,
            "value": "<%$list_config['pi_image']['value']%>",
            "dbval": "<%$list_config['pi_image']['dbval']%>",
            "editoptions": {
                "text_case": "",
                "placeholder": null
            },
            "extra_class": ""
        }
    };

    initDetailViewEditable();
<%/javascript%>


<div id="div_main_top_detail_view" class="div-main-top-detail-view" style="<%if $_toggle_flag eq '1' %>display:none;<%/if%>">
    <table id="<%$detail_layout_view_id%>" class="jqgrid-subview" width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_PRODUCT_NAME')%>: </strong></td>
            <td width="20%"><%$data['p_product_name']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_CATEGORY_ID')%>: </strong></td>
            <td width="20%"><%$data['p_category_id']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_BRAND_ID')%>: </strong></td>
            <td width="20%"><%$data['p_brand_id']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_PRICE')%>: </strong></td>
            <td width="20%"><%$data['p_price']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_RETAIL_PRICE')%>: </strong></td>
            <td width="20%"><%$data['p_retail_price']%></td>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_STATUS')%>: </strong></td>
            <td width="20%"><%$data['p_status']%></td>
        </tr>
        <tr>
            <td width="12%"><strong><%$this->lang->line('PRODUCTS_IMAGE')%>: </strong></td>
            <td width="20%"><%$data['pi_image']%></td>
        </tr>
         
    </table>
</div>