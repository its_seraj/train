<?php


/**
 * Description of Category New Extended Controller
 * 
 * @module Extended Category New
 * 
 * @class Cit_Category_new.php
 * 
 * @path application\admin\master\controllers\Cit_Category_new.php
 * 
 * @author CIT Dev Team
 * 
 * @date 04.04.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Category_new extends Category_new {
        public function __construct()
{
    parent::__construct();
}

public function listing_before_load($render_arr){
    // pr($render_arr);exit;
    return $render_arr;
}

public function listing_before_fetch($data_config){
    // pr($data_config);exit; // in ajax
    return $data_config;
}

public function listing_after_process($id, $row_data, $data_arr, $return_array){
    // pr($return_array);exit; // in ajax
}

public function form_after_fetch($data, $mode, $id){
    // pr($data);exit;
}
}
