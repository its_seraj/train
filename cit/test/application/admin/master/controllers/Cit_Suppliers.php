<?php


/**
 * Description of Suppliers Extended Controller
 * 
 * @module Extended Suppliers
 * 
 * @class Cit_Suppliers.php
 * 
 * @path application\admin\master\controllers\Cit_Suppliers.php
 * 
 * @author CIT Dev Team
 * 
 * @date 04.04.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Suppliers extends Suppliers {
        public function __construct()
{
    parent::__construct();
}
}
