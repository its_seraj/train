<?php


/**
 * Description of Promo Code Extended Controller
 * 
 * @module Extended Promo Code
 * 
 * @class Cit_Promo_code.php
 * 
 * @path application\admin\master\controllers\Cit_Promo_code.php
 * 
 * @author CIT Dev Team
 * 
 * @date 01.04.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Promo_code extends Promo_code {
        public function __construct()
{
    parent::__construct();
}

public function before_save(){
    $start = $this->input->post('p_start_date');
    $end = $this->input->post('p_expire_date');
    $return;
    $date_diff = date_diff(date_create($end), date_create($start));
    pr($start.', '.$end.', '.$date_diff->invert);exit;
    if(!$date_diff.invert){
        $return['success'] = false;
        $return['message'] = "End Date must be greater than the Start Date !";
    }else{
        if($date_diff->days < 10){
            $return['success'] = true;
        }
        else{
            $return['success'] = false;
            $return['message'] = "Promo end Date must be less than 10 Days from start Date !";
            
        }
    }
    
    return $return;
}
}
