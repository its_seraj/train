<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Description of Products Controller
 *
 * @category admin
 *
 * @package master
 *
 * @subpackage controllers
 *
 * @module Products
 *
 * @class Products.php
 *
 * @path application\admin\master\controllers\Products.php
 *
 * @version 4.4
 *
 * @author CIT Dev Team
 *
 * @since 01.04.2022
 */

class Products extends Cit_Controller
{
    /**
     * __construct method is used to set controller preferences while controller object initialization.
     * @created Mdseraj Khan | 25.03.2022
     * @modified Mdseraj Khan | 01.04.2022
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('listing');
        $this->load->library('filter');
        $this->load->library('dropdown');
        $this->load->model('products_model');
        $this->folder_name = "master";
        $this->module_name = "products";
        $this->module_heading = $this->lang->line('PRODUCTS_PRODUCTS');
        $this->mod_enc_url = $this->general->getGeneralEncryptList($this->folder_name, $this->module_name);
        $this->mod_enc_mode = $this->general->getCustomEncryptMode(TRUE);
        $this->_request_params();
        $this->module_config = array(
            'module_name' => $this->module_name,
            'folder_name' => $this->folder_name,
            'mod_enc_url' => $this->mod_enc_url,
            'mod_enc_mode' => $this->mod_enc_mode,
            'delete' => "No",
            'xeditable' => "No",
            'top_detail' => "No",
            "multi_lingual" => "No",
            "print_layouts" => array(),
            "workflow_modes" => array(),
            "skip_validation" => false,
            "physical_data_remove" => "Yes",
            "list_record_callback" => "",
        );
        $this->dropdown_arr = array(
            "p_category_id" => array(
                "type" => "table",
                "table_name" => "category",
                "field_key" => "iCategoryId",
                "field_val" => array(
                    $this->db->protect("vCategoryName")
                ),
                "order_by" => "vCategoryName asc",
                "default" => "Yes",
            ),
            "p_manufacturer_id" => array(
                "type" => "table",
                "table_name" => "manufacturer",
                "field_key" => "iManufacturerId",
                "field_val" => array(
                    $this->db->protect("vName")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
            ),
            "p_brand_id" => array(
                "type" => "table",
                "table_name" => "brand",
                "field_key" => "iBrandId",
                "field_val" => array(
                    $this->db->protect("vBrand")
                ),
                "order_by" => "vBrand asc",
                "default" => "Yes",
            ),
            "p_added_by" => array(
                "type" => "table",
                "table_name" => "mod_admin",
                "field_key" => "iAdminId",
                "field_val" => array(
                    $this->db->protect("vName")
                ),
                "order_by" => "val asc",
                "default" => "Yes",
            ),
            "p_updated_by" => array(
                "type" => "phpfn",
                "function" => "",
                "default" => "Yes",
            ),
            "p_status" => array(
                "type" => "enum",
                "default" => "Yes",
                "values" => array(
                    array(
                        'id' => 'Active',
                        'val' => $this->lang->line('PRODUCTS_ACTIVE')
                    ),
                    array(
                        'id' => 'Inactive',
                        'val' => $this->lang->line('PRODUCTS_INACTIVE')
                    )
                )
            )
        );
        $this->parMod = $this->params_arr["parMod"];
        $this->parID = $this->params_arr["parID"];
        $this->parRefer = array();
        $this->expRefer = array();

        $this->topRefer = array();
        $this->dropdown_limit = $this->config->item('ADMIN_DROPDOWN_LIMIT');
        $this->search_combo_limit = $this->config->item('ADMIN_SEARCH_COMBO_LIMIT');
        $this->switchto_limit = $this->config->item('ADMIN_SWITCH_DROPDOWN_LIMIT');
        $this->response_arr = array();
        $this->count_arr = array();
    }

    /**
     * _request_params method is used to set post/get/request params.
     */
    public function _request_params()
    {
        $this->get_arr = is_array($this->input->get(NULL, TRUE)) ? $this->input->get(NULL, TRUE) : array();
        $this->post_arr = is_array($this->input->post(NULL, TRUE)) ? $this->input->post(NULL, TRUE) : array();
        $this->params_arr = array_merge($this->get_arr, $this->post_arr);
        return $this->params_arr;
    }

    /**
     * index method is used to intialize grid listing page.
     */
    public function index()
    {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        try
        {
            if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
            {
                $access_list = array(
                    "products_list",
                    "products_view",
                    "products_add",
                    "products_update",
                    "products_delete",
                    "products_export",
                    "products_print",
                );
                list($list_access, $view_access, $add_access, $edit_access, $del_access, $expo_access, $print_access) = $this->filter->checkAccessCapability($access_list, TRUE);
            }
            else
            {
                $access_list = array(
                    "List",
                    "View",
                    "Add",
                    "Update",
                    "Delete",
                    "Export",
                    "Print",
                );
                list($list_access, $view_access, $add_access, $edit_access, $del_access, $expo_access, $print_access) = $this->filter->getModuleWiseAccess("products", $access_list, TRUE, TRUE);
            }
            if (!$list_access)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
            }
            $enc_loc_module = $this->general->getMD5EncryptString("ListPrefer", "products");

            $status_array = array(
                'Active',
                'Inactive',
            );
            $status_label = array(
                'js_lang_label.PRODUCTS_ACTIVE',
                'js_lang_label.PRODUCTS_INACTIVE',
            );

            $list_config = $this->products_model->getListConfiguration();
            $this->processConfiguration($list_config, $add_access, $edit_access, TRUE);
            $this->general->trackModuleNavigation("Module", "List", "Viewed", $this->mod_enc_url["index"], "products");

            $search_arr = $this->getLeftSearchContent();
            if (method_exists($this->filter, "setListFieldCapability"))
            {
                $this->filter->setListFieldCapability($list_config);
            }

            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();
            $render_arr = array(

                "search_arr" => $search_arr,
                'list_config' => $list_config,
                'count_arr' => $this->count_arr,
                'enc_loc_module' => $enc_loc_module,
                'status_array' => $status_array,
                'status_label' => $status_label,
                'view_access' => $view_access,
                'add_access' => $add_access,
                'edit_access' => $edit_access,
                'del_access' => $del_access,
                'expo_access' => $expo_access,
                'print_access' => $print_access,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'module_heading' => $this->module_heading,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
                "capabilities" => array(
                    "hide_multi_select" => "No",
                    "subgrid" => "No",
                ),
                'default_filters' => $this->products_model->default_filters,
            );
            $this->smarty->assign($render_arr);
            if (!empty($render_arr['overwrite_view']))
            {
                $this->loadView($render_arr['overwrite_view']);
            }
            else
            {
                $this->loadView("products_index");
            }
        }
        catch(Exception $e)
        {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * listing method is used to load listing data records in json format.
     */
    public function listing()
    {
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rows = $params_arr['rows'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        $filters = $params_arr['filters'];
        if (!trim($sidx) && !trim($sord))
        {
            $sdef = 'Yes';
        }
        $filters = json_decode($filters, TRUE);
        $list_config = $this->products_model->getListConfiguration();
        $form_config = $this->products_model->getFormConfiguration();
        $extra_cond = $this->products_model->extra_cond;
        $groupby_cond = $this->products_model->groupby_cond;
        $having_cond = $this->products_model->having_cond;
        $orderby_cond = $this->products_model->orderby_cond;

        $data_config = array();
        $data_config['page'] = $page;
        $data_config['rows'] = $rows;
        $data_config['sidx'] = $sidx;
        $data_config['sord'] = $sord;
        $data_config['sdef'] = $sdef;
        $data_config['filters'] = $filters;
        $data_config['module_config'] = $this->module_config;
        $data_config['list_config'] = $list_config;
        $data_config['form_config'] = $form_config;
        $data_config['dropdown_arr'] = $this->dropdown_arr;
        $data_config['extra_cond'] = $extra_cond;
        $data_config['group_by'] = $groupby_cond;
        $data_config['having_cond'] = $having_cond;
        $data_config['order_by'] = $orderby_cond;

        $data_recs = $this->products_model->getListingData($data_config);
        $data_recs['no_records_msg'] = $this->general->processMessageLabel('ACTION_NO_PRODUCTS_DATA_FOUND_C46_C46_C33');

        echo json_encode($data_recs);
        $this->skip_template_view();
    }

    /**
     * export method is used to export listing data records in csv or pdf formats.
     */
    public function export()
    {
        if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
        {
            $this->filter->checkAccessCapability("products_export");
        }
        else
        {
            $this->filter->getModuleWiseAccess("products", "Export", TRUE);
        }
        $params_arr = $this->params_arr;
        $page = $params_arr['page'];
        $rowlimit = $params_arr['rowlimit'];
        $sidx = $params_arr['sidx'];
        $sord = $params_arr['sord'];
        $sdef = $params_arr['sdef'];
        if (!trim($sidx) && !trim($sord))
        {
            $sdef = 'Yes';
        }
        $selected = $params_arr['selected'];
        $id = explode(",", $params_arr['id']);
        $export_type = $params_arr['export_type'];
        $export_mode = $params_arr['export_mode'];
        $filters = $params_arr['filters'];
        $filters = json_decode(base64_decode($filters), TRUE);
        $fields = json_decode(base64_decode($params_arr['fields']), TRUE);
        $list_config = $this->products_model->getListConfiguration();
        $form_config = $this->products_model->getFormConfiguration();
        $table_name = $this->products_model->table_name;
        $table_alias = $this->products_modeltable_alias;
        $primary_key = $this->products_model->primary_key;
        $extra_cond = $this->products_model->extra_cond;
        $groupby_cond = $this->products_model->groupby_cond;
        $having_cond = $this->products_model->having_cond;
        $orderby_cond = $this->products_model->orderby_cond;
        if (method_exists($this->filter, "setListFieldCapability"))
        {
            $this->filter->setListFieldCapability($list_config);
        }
        $export_config = array();
        if ($selected == "true")
        {
            $export_config['id'] = $id;
        }
        $export_config['page'] = $page;
        $export_config['rowlimit'] = $rowlimit;
        $export_config['sidx'] = $sidx;
        $export_config['sord'] = $sord;
        $export_config['sdef'] = $sdef;
        $export_config['filters'] = $filters;
        $export_config['export_mode'] = $export_mode;
        $export_config['module_config'] = $this->module_config;
        $export_config['list_config'] = $list_config;
        $export_config['form_config'] = $form_config;
        $export_config['dropdown_arr'] = $this->dropdown_arr;
        $export_config['table_name'] = $table_name;
        $export_config['table_alias'] = $table_alias;
        $export_config['primary_key'] = $primary_key;
        $export_config['extra_cond'] = $extra_cond;
        $export_config['group_by'] = $groupby_cond;
        $export_config['having_cond'] = $having_cond;
        $export_config['order_by'] = $orderby_cond;

        $db_recs = $this->products_model->getExportData($export_config);
        $db_recs = $this->listing->getDataForList($db_recs, $export_config, "GExport", array());
        if (!is_array($db_recs) || count($db_recs) == 0)
        {
            $this->session->set_flashdata('failure', $this->general->processMessageLabel('GENERIC_GRID_NO_RECORDS_TO_PROCESS'));
            redirect($this->config->item("admin_url")."#".$this->mod_enc_url['index'].$extra_hstr);
        }

        require_once ($this->config->item('third_party').'Csv_export.php');
        require_once ($this->config->item('third_party').'Pdf_export.php');

        $tot_fields_arr = array_keys($db_recs[0]);
        if ($export_mode == "all" && is_array($tot_fields_arr))
        {
            if (($pr_key = array_search($primary_key, $tot_fields_arr)) !== FALSE)
            {
                unset($tot_fields_arr[$pr_key]);
            }
            $fields = array();
            if ($this->config->item("DISABLE_LIST_EXPORT_ALL"))
            {
                foreach ((array) $list_config as $key => $val)
                {
                    if (isset($val['export']) && $val['export'] == "Yes")
                    {
                        if (isset($val['hidecm']))
                        {
                            if (in_array($val['hidecm'], array("condition", "capability", "permanent")) && $val['hidden'] == "Yes")
                            {
                                continue;
                            }
                            if ($val['hideme'] == "Yes")
                            {
                                continue;
                            }
                        }
                        $fields[] = $key;
                    }
                }
            }
            else
            {
                $fields = array_values($tot_fields_arr);
            }
        }

        $misc_info = array();
        $misc_info['fields'] = $fields;
        $misc_info['heading'] = $this->module_heading;
        $misc_info['filename'] = "products_records_".count($db_recs);
        $misc_info['pdf_unit'] = PDF_UNIT;
        $misc_info['pdf_page_format'] = PDF_PAGE_FORMAT;
        $misc_info['pdf_page_orientation'] = (!empty($params_arr['orientation'])) ? $params_arr['orientation'] : PDF_PAGE_ORIENTATION;
        $misc_info['pdf_content_before_table'] = '';
        $misc_info['pdf_content_after_table'] = '';
        $misc_info['pdf_header_style'] = '';

        $fields = $misc_info['fields'];
        $heading = $misc_info['heading'];
        $filename = $misc_info['filename'];
        $numberOfColumns = count($fields);

        $columns = $aligns = $widths = $data = array();
        //Table headers info
        for ($i = 0; $i < $numberOfColumns; $i++)
        {
            $size = 10;
            $position = '';
            if (array_key_exists($fields[$i], $list_config))
            {
                $label = $list_config[$fields[$i]]['label_lang'];
                $position = $list_config[$fields[$i]]['align'];
                $size = $list_config[$fields[$i]]['width'];
            }
            elseif (array_key_exists($fields[$i], $form_config))
            {
                $label = $form_config[$fields[$i]]['label_lang'];
            }
            else
            {
                $label = $fields[$i];
            }
            $columns[] = $label;
            $aligns[] = in_array($position, array('right', 'center')) ? $position : "left";
            $widths[] = $size;
        }
        if ($export_type == 'pdf')
        {
            $pdf_style = "TCPDF";
            //Table data info
            $db_rec_cnt = count($db_recs);
            for ($i = 0; $i < $db_rec_cnt; $i++)
            {
                foreach ((array) $db_recs[$i] as $key => $val)
                {
                    if (is_array($fields) && in_array($key, $fields))
                    {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "pdf", $pdf_style);
                    }
                }
            }

            $pdf = new PDF_Export($misc_info['pdf_page_orientation'], $misc_info['pdf_unit'], $misc_info['pdf_page_format'], TRUE, 'UTF-8', FALSE);
            if (method_exists($pdf, "setModule"))
            {
                $pdf->setModule("products");
            }
            if (method_exists($pdf, "setContent"))
            {
                $pdf->setContent($misc_info);
            }
            if (method_exists($pdf, "setController"))
            {
                $pdf->setController($this);
            }
            $pdf->initialize($heading);
            $pdf->writeGridTable($columns, $data, $widths, $aligns);
            $pdf->Output($filename.".pdf", 'D');
        }
        elseif ($export_type == 'csv' || $export_type == 'xls')
        {
            //Table data info
            $db_recs_cnt = count($db_recs);
            for ($i = 0; $i < $db_recs_cnt; $i++)
            {
                foreach ((array) $db_recs[$i] as $key => $val)
                {
                    if (is_array($fields) && in_array($key, $fields))
                    {
                        $data[$i][$key] = $this->listing->dataForExportMode($val, "csv");
                    }
                }
            }
            $export_array = array_merge(array($columns), $data);
            if (is_file($this->config->item('third_party').'Xls_export.php'))
            {
                require_once ($this->config->item('third_party').'Xls_export.php');
            }
            else
            {
                $export_type == "csv";
            }
            if ($export_type == "xls" && class_exists("XLS_Writer"))
            {
                $xls = new XLS_Writer($export_array);
                if (method_exists($xls, "setModule"))
                {
                    $xls->setModule("products");
                }
                if (method_exists($xls, "setContent"))
                {
                    $xls->setContent($misc_info);
                }
                if (method_exists($xls, "setController"))
                {
                    $xls->setController($this);
                }
                $xls->headers($filename);
                $xls->initialize($heading);
                $xls->output($columns, $data, $widths, $aligns);
            }
            else
            {
                $csv = new CSV_Writer($export_array);
                if (method_exists($csv, "setModule"))
                {
                    $csv->setModule("products");
                }
                if (method_exists($csv, "setContent"))
                {
                    $csv->setContent($misc_info);
                }
                if (method_exists($csv, "setController"))
                {
                    $csv->setController($this);
                }
                $csv->headers($filename);
                $csv->output($columns, $data, $widths, $aligns);
            }
        }
        $this->skip_template_view();
    }

    /**
     * add method is used to add or update data records.
     */
    public function add()
    {
        $params_arr = $this->params_arr;
        $extra_qstr = $extra_hstr = '';
        $hideCtrl = $params_arr['hideCtrl'];
        $showDetail = $params_arr['showDetail'];
        $mode = (in_array($params_arr['mode'], array("Update", "View"))) ? "Update" : "Add";
        $viewMode = ($params_arr['mode'] == "View") ? TRUE : FALSE;
        $id = $params_arr['id'];
        $enc_id = $this->general->getAdminEncodeURL($id);
        try
        {
            $extra_cond = $this->products_model->extra_cond;
            if ($mode == "Update")
            {
                if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
                {
                    $access_list = array(
                        "products_list",
                        "products_view",
                        "products_update",
                        "products_delete",
                        "products_print",
                    );
                    list($list_access, $view_access, $edit_access, $del_access, $print_access) = $this->filter->checkAccessCapability($access_list, TRUE);
                }
                else
                {
                    $access_list = array(
                        "List",
                        "View",
                        "Update",
                        "Delete",
                        "Print",
                    );
                    list($list_access, $view_access, $edit_access, $del_access, $print_access) = $this->filter->getModuleWiseAccess("products", $access_list, TRUE, TRUE);
                }
                if (!$edit_access && !$view_access)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_VIEW_THIS_PAGE_C46_C46_C33'));
                }
            }
            else
            {
                if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
                {
                    $access_list = array(
                        "products_list",
                        "products_add",
                    );
                    list($list_access, $add_access) = $this->filter->checkAccessCapability($access_list, TRUE);
                }
                else
                {
                    $access_list = array(
                        "List",
                        "Add",
                    );
                    list($list_access, $add_access) = $this->filter->getModuleWiseAccess("products", $access_list, TRUE, TRUE);
                }
                if (!$add_access)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }

            $data = $orgi = $func = $elem = array();
            if ($mode == 'Update')
            {
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowEdit", "products"), $this->session->userdata('iAdminId'));
                $data_arr = $this->products_model->getData(intval($id));
                $data = $orgi = $data_arr[0];
                if ((!is_array($data) || count($data) == 0) && $params_arr['rmPopup'] != "true")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }

                $switch_arr = $switch_combo = $switch_cit = array();

                $recName = $switch_combo[$id];
                $switch_enc_combo = $this->filter->getSwitchEncryptRec($switch_combo, $switch_arr);
                $this->dropdown->combo("array", "vSwitchPage", $switch_enc_combo, $enc_id, FALSE, "key_value", $switch_arr);
                $next_prev_records = $this->filter->getNextPrevRecords($id, $switch_arr);

                $this->general->trackModuleNavigation("Module", "Form", "Viewed", $this->mod_enc_url["add"], "products", $recName);
            }
            else
            {
                $recName = '';
                $ctrl_flow = $this->ci_local->read($this->general->getMD5EncryptString("FlowAdd", "products"), $this->session->userdata('iAdminId'));
                $this->general->trackModuleNavigation("Module", "Form", "Viewed", $this->mod_enc_url["add"], "products");
            }

            $opt_arr = $img_html = $auto_arr = $config_arr = array();
            if ($mode == "Update")
            {

                $main_data = $this->products_model->getData(intval($id), array('p.iProductId'));
            }
            else
            {
                $main_data = array();
            }
            $relation_module = $this->products_model->relation_modules["product_image"];
            $this->load->module("master/product_image");
            $this->load->model("master/product_image_model");
            $child_config = array();
            $child_config["parent_module"] = "products";
            $child_config["child_module"] = "product_image";
            if ($mode == "Update")
            {
                $extra_cond = $this->db->protect("pi.iProductId")." = ".$this->db->escape($main_data[0]["iProductId"]);
                if ($relation_module["extra_cond"] != "")
                {
                    $extra_cond .= " AND ".$relation_module["extra_cond"];
                }
                $parent_join_arr = array(
                    "joins" => array(
                        array(
                            "table_name" => "product",
                            "table_alias" => "p",
                            "field_name" => "iProductId",
                            "rel_table_name" => "product_image",
                            "rel_table_alias" => "pi",
                            "rel_field_name" => "iProductId",
                            "join_type" => "left",
                        )
                    )
                );
                $child_data = $this->product_image_model->getData($extra_cond, "", "", "pi.iProductImageId", "", $parent_join_arr);
                $child_config["mode"] = (is_array($child_data) && count($child_data) > 0) ? "Update" : "Add";
                $child_config["data"] = $child_data;
                $child_config["parent_data"] = $data;
            }
            else
            {
                $child_config["mode"] = "Add";
                $child_config["data"] = array();
            }
            $child_config["mode"] = (is_array($child_config["data"]) && count($child_config["data"]) > 0) ? "Update" : "Add";
            $child_config["multi_file"] = "Yes";
            $child_config["perform"] = array(
                "data",
                "options",
                "config",
            );

            $module_arr = $this->product_image->getRelationModule($child_config);
            $module_arr["config_arr"]["popup"] = $relation_module["popup"];
            $module_arr["config_arr"]["recMode"] = $child_config["mode"];
            $module_arr["config_arr"]["form_config"] = $module_arr["form_config"];
            $child_assoc_data["product_image"] = $module_arr["data"];
            $child_assoc_func["product_image"] = $module_arr["func"];
            $child_assoc_elem["product_image"] = $module_arr["elem"];
            $child_assoc_conf["product_image"] = $module_arr["config_arr"];
            $child_assoc_opt["product_image"] = $module_arr["opt_arr"];
            $child_assoc_img["product_image"] = $module_arr["img_html"];
            $child_assoc_auto["product_image"] = $module_arr["auto_arr"];
            $child_assoc_status["product_image"] = $module_arr["status"];
            $child_assoc_access["product_image"] = array(
                "add" => 1,
                "save" => 1,
                "delete" => 1,
                "actions" => 1,
                "labels" => array()
            );

            $relation_module = $this->products_model->relation_modules["product_specification"];
            $this->load->module("master/product_specification");
            $this->load->model("master/product_specification_model");
            $child_config = array();
            $child_config["parent_module"] = "products";
            $child_config["child_module"] = "product_specification";
            if ($mode == "Update")
            {
                $extra_cond = $this->db->protect("ps.iProductId")." = ".$this->db->escape($main_data[0]["iProductId"]);
                if ($relation_module["extra_cond"] != "")
                {
                    $extra_cond .= " AND ".$relation_module["extra_cond"];
                }
                $parent_join_arr = array(
                    "joins" => array(
                        array(
                            "table_name" => "product",
                            "table_alias" => "p",
                            "field_name" => "iProductId",
                            "rel_table_name" => "product_specification",
                            "rel_table_alias" => "ps",
                            "rel_field_name" => "iProductId",
                            "join_type" => "left",
                        )
                    )
                );
                $child_data = $this->product_specification_model->getData($extra_cond, "", "", "ps.iProductSpecificationId", "", $parent_join_arr);
                $child_config["mode"] = (is_array($child_data) && count($child_data) > 0) ? "Update" : "Add";
                $child_config["data"] = $child_data;
                $child_config["parent_data"] = $data;
            }
            else
            {
                $child_config["mode"] = "Add";
                $child_config["data"] = array();
            }
            $child_config["perform"] = array(
                "data",
                "options",
                "config",
            );

            $module_arr = $this->product_specification->getRelationModule($child_config);
            $module_arr["config_arr"]["popup"] = $relation_module["popup"];
            $module_arr["config_arr"]["recMode"] = $child_config["mode"];
            $module_arr["config_arr"]["form_config"] = $module_arr["form_config"];
            $child_assoc_data["product_specification"] = $module_arr["data"];
            $child_assoc_func["product_specification"] = $module_arr["func"];
            $child_assoc_elem["product_specification"] = $module_arr["elem"];
            $child_assoc_conf["product_specification"] = $module_arr["config_arr"];
            $child_assoc_opt["product_specification"] = $module_arr["opt_arr"];
            $child_assoc_img["product_specification"] = $module_arr["img_html"];
            $child_assoc_auto["product_specification"] = $module_arr["auto_arr"];
            $child_assoc_status["product_specification"] = $module_arr["status"];
            $child_assoc_access["product_specification"] = array(
                "add" => 1,
                "save" => 1,
                "delete" => 1,
                "actions" => 1,
                "labels" => array()
            );

            $form_config = $this->products_model->getFormConfiguration($config_arr);
            if (is_array($form_config) && count($form_config) > 0)
            {
                foreach ($form_config as $key => $val)
                {
                    if ($params_arr['rmPopup'] == "true" && $params_arr[$key] != "")
                    {
                        $data[$key] = $params_arr[$key];
                    }
                    elseif ($val["dfapply"] != "")
                    {
                        $val['default'] = (substr($val['default'], 0, 6) == "copy::") ? $orgi[substr($val['default'], 6)] : $val['default'];
                        if ($val["dfapply"] == "forceApply" || $val["entry_type"] == "Custom")
                        {
                            $data[$key] = $val['default'];
                        }
                        elseif ($val["dfapply"] == "addOnly")
                        {
                            if ($mode == "Add")
                            {
                                $data[$key] = $val['default'];
                            }
                        }
                        elseif ($val["dfapply"] == "everyUpdate")
                        {
                            if ($mode == "Update" && $params_arr['mode'] != 'View')
                            {
                                $data[$key] = $val['default'];
                            }
                        }
                        else
                        {
                            $data[$key] = (trim($data[$key]) != "") ? $data[$key] : $val['default'];
                        }
                    }
                    if ($val['encrypt'] == "Yes" && $mode == "Update")
                    {
                        $data[$key] = $this->general->decryptDataMethod($data[$key], $val["enctype"]);
                    }
                    if ($val['function'] != "")
                    {
                        $fnctype = $val['functype'];
                        $phpfunc = $val['function'];
                        $tmpdata = '';
                        if (substr($phpfunc, 0, 12) == 'controller::' && substr($phpfunc, 12) !== FALSE)
                        {
                            $phpfunc = substr($phpfunc, 12);
                            if (method_exists($this, $phpfunc))
                            {
                                $tmpdata = $this->$phpfunc($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                            }
                        }
                        elseif (substr($phpfunc, 0, 7) == 'model::' && substr($phpfunc, 7) !== FALSE)
                        {
                            $phpfunc = substr($phpfunc, 7);
                            if (method_exists($this->products_model, $phpfunc))
                            {
                                $tmpdata = $this->products_model->$phpfunc($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                            }
                        }
                        elseif (method_exists($this->general, $phpfunc))
                        {
                            $tmpdata = $this->general->$phpfunc($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                        }
                        if ($fnctype == "input")
                        {
                            $elem[$key] = $tmpdata;
                        }
                        elseif ($fnctype == "status")
                        {
                            $func[$key] = $tmpdata;
                        }
                        else
                        {
                            $data[$key] = $tmpdata;
                        }
                    }
                    if ($val['field_status'] != "")
                    {
                        $status_type = $val['field_status'];
                        $fd_callback = $val['field_callback'];
                        if ($status_type == "capability" && $fd_callback != "")
                        {
                            $func[$key] = $this->filter->getFormFieldCapability($key, $this->module_name, $mode);
                        }
                        elseif ($status_type == "function")
                        {
                            $fd_status = 0;
                            if (substr($fd_callback, 0, 12) == 'controller::' && substr($fd_callback, 12) !== FALSE)
                            {
                                $fd_callback = substr($fd_callback, 12);
                                if (method_exists($this, $fd_callback))
                                {
                                    $fd_status = $this->$fd_callback($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                                }
                            }
                            elseif (substr($fd_callback, 0, 7) == 'model::' && substr($fd_callback, 7) !== FALSE)
                            {
                                $fd_callback = substr($fd_callback, 7);
                                if (method_exists($this->products_model, $fd_callback))
                                {
                                    $fd_status = $this->products_model->$fd_callback($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                                }
                            }
                            elseif (method_exists($this->general, $fd_callback))
                            {
                                $fd_status = $this->general->$fd_callback($mode, $data[$key], $data, $id, $key, $key, $this->module_name);
                            }
                            $func[$key] = $fd_status;
                        }
                    }
                    $source_field = $val['name'];
                    $combo_config = $this->dropdown_arr[$source_field];
                    if (is_array($combo_config) && count($combo_config) > 0)
                    {
                        if ($combo_config['auto'] == "Yes")
                        {
                            $combo_count = $this->getSourceOptions($source_field, $mode, $id, $data, '', 'count');
                            if ($combo_count[0]['tot'] > $this->dropdown_limit)
                            {
                                $auto_arr[$source_field] = "Yes";
                            }
                        }
                        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data);
                        $final_arr = $this->filter->makeArrayDropdown($combo_arr);
                        if ($combo_config['opt_group'] == "Yes")
                        {
                            $display_arr = $this->filter->makeOPTDropdown($combo_arr);
                        }
                        else
                        {
                            $display_arr = $final_arr;
                        }
                        $this->dropdown->combo("array", $source_field, $display_arr, $data[$key]);
                        $opt_arr[$source_field] = $final_arr;
                    }
                    if ($val['file_upload'] == 'Yes')
                    {
                        $del_file = ($edit_access && $viewMode != TRUE && $func[$key] != 2) ? TRUE : FALSE;
                        $val['htmlID'] = $val['name'];
                        $img_html[$key] = $this->listing->parseFormFile($data[$key], $id, $data, $val, $this->module_config, "Form", $del_file);
                    }
                }
            }
            if (method_exists($this->filter, "setFormFieldCapability"))
            {
                $this->filter->setFormFieldCapability($func, $this->module_name, $mode);
            }
            $extra_qstr .= $this->general->getRequestURLParams();
            $extra_hstr .= $this->general->getRequestHASHParams();

            /** access controls <<< **/
            $controls_allow = $prev_link_allow = $next_link_allow = $update_allow = $delete_allow = $backlink_allow = $switchto_allow = $discard_allow = $tabing_allow = TRUE;
            if ($mode == "Update")
            {
                if (!$del_access || $this->module_config["delete"] == "Yes")
                {
                    $delete_allow = FALSE;
                }
            }
            if (is_array($switch_combo) && count($switch_combo) > 0)
            {
                $prev_link_allow = ($next_prev_records['prev']['id'] != '') ? TRUE : FALSE;
                $next_link_allow = ($next_prev_records['next']['id'] != '') ? TRUE : FALSE;
            }
            else
            {
                $prev_link_allow = $next_link_allow = $switchto_allow = FALSE;
            }
            if (!$list_access)
            {
                $backlink_allow = $discard_allow = FALSE;
            }
            if ($hideCtrl == "true")
            {
                $controls_allow = $prev_link_allow = $next_link_allow = $delete_allow = $backlink_allow = $switchto_allow = $tabing_allow = FALSE;
                $ctrl_flow = "Stay";
            }
            /** access controls >>> **/
            $render_arr = array(

                "child_assoc_data" => $child_assoc_data,
                "child_assoc_func" => $child_assoc_func,
                "child_assoc_elem" => $child_assoc_elem,
                "child_assoc_conf" => $child_assoc_conf,
                "child_assoc_opt" => $child_assoc_opt,
                "child_assoc_img" => $child_assoc_img,
                "child_assoc_auto" => $child_assoc_auto,
                "child_assoc_status" => $child_assoc_status,
                "child_assoc_access" => $child_assoc_access,

                "edit_access" => $edit_access,
                "print_access" => $print_access,
                'controls_allow' => $controls_allow,
                'prev_link_allow' => $prev_link_allow,
                'next_link_allow' => $next_link_allow,
                'update_allow' => $update_allow,
                'delete_allow' => $delete_allow,
                'backlink_allow' => $backlink_allow,
                'switchto_allow' => $switchto_allow,
                'discard_allow' => $discard_allow,
                'tabing_allow' => $tabing_allow,
                'enc_id' => $enc_id,
                'id' => $id,
                'mode' => $mode,
                'data' => $data,
                'func' => $func,
                'elem' => $elem,
                'recName' => $recName,
                "opt_arr" => $opt_arr,
                "img_html" => $img_html,
                "auto_arr" => $auto_arr,
                'ctrl_flow' => $ctrl_flow,
                'switch_cit' => $switch_cit,
                "switch_arr" => $switch_arr,
                'switch_combo' => $switch_combo,
                'next_prev_records' => $next_prev_records,
                "form_config" => $form_config,
                'folder_name' => $this->folder_name,
                'module_name' => $this->module_name,
                'mod_enc_url' => $this->mod_enc_url,
                'mod_enc_mode' => $this->mod_enc_mode,
                'extra_qstr' => $extra_qstr,
                'extra_hstr' => $extra_hstr,
                'capabilities' => array()
            );
            $this->smarty->assign($render_arr);
            if (!empty($render_arr['overwrite_view']))
            {
                $this->loadView($render_arr['overwrite_view']);
            }
            else
            {
                if ($mode == "Update")
                {
                    if ($edit_access && $viewMode != TRUE)
                    {
                        $this->loadView("products_add");
                    }
                    else
                    {
                        $this->loadView("products_add_view");
                    }
                }
                else
                {
                    $this->loadView("products_add");
                }
            }
        }
        catch(Exception $e)
        {
            $render_arr['err_message'] = $e->getMessage();
            $this->smarty->assign($render_arr);
            $this->loadView($this->config->item('ADMIN_FORBIDDEN_TEMPLATE'));
        }
    }

    /**
     * addAction method is used to save data, which is posted through form.
     */
    public function addAction()
    {
        $params_arr = $this->params_arr;
        $mode = ($params_arr['mode'] == "Update") ? "Update" : "Add";
        $id = $params_arr['id'];
        try
        {
            $ret_arr = array();
            if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
            {
                if ($mode == "Update")
                {
                    $add_edit_access = $this->filter->checkAccessCapability("products_update", TRUE);
                }
                else
                {
                    $add_edit_access = $this->filter->checkAccessCapability("products_add", TRUE);
                }
            }
            else
            {
                $add_edit_access = $this->filter->getModuleWiseAccess("products", $mode, TRUE, TRUE);
            }
            if (!$add_edit_access)
            {
                if ($mode == "Update")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_ADD_THESE_DETAILS_C46_C46_C33'));
                }
            }

            $form_config = $this->products_model->getFormConfiguration();
            $params_arr = $this->_request_params();

            $p_product_name = $params_arr["p_product_name"];
            $p_category_id = $params_arr["p_category_id"];
            $p_description = $params_arr["p_description"];
            $p_price = $params_arr["p_price"];
            $p_retail_price = $params_arr["p_retail_price"];
            $p_manufacturer_id = $params_arr["p_manufacturer_id"];
            $p_brand_id = $params_arr["p_brand_id"];
            $p_added_date = $params_arr["p_added_date"];
            $p_added_by = $params_arr["p_added_by"];
            $p_updated_date = $params_arr["p_updated_date"];
            $p_updated_by = $params_arr["p_updated_by"];
            $p_status = $params_arr["p_status"];

            $data = $save_data_arr = $file_data = array();
            $data["vProductName"] = $p_product_name;
            $data["iCategoryId"] = $p_category_id;
            $data["tDescription"] = $p_description;
            $data["fPrice"] = $p_price;
            $data["fRetailPrice"] = $p_retail_price;
            $data["iManufacturerId"] = $p_manufacturer_id;
            $data["iBrandId"] = $p_brand_id;
            $data["dtAddedDate"] = $this->filter->formatActionData($p_added_date, $form_config["p_added_date"]);
            $data["iAddedBy"] = $p_added_by;
            $data["dtUpdatedDate"] = $this->filter->formatActionData($p_updated_date, $form_config["p_updated_date"]);
            $data["iUpdatedBy"] = $p_updated_by;
            $data["eStatus"] = $p_status;

            $save_data_arr["p_product_name"] = $data["vProductName"];
            $save_data_arr["p_category_id"] = $data["iCategoryId"];
            $save_data_arr["p_description"] = $data["tDescription"];
            $save_data_arr["p_price"] = $data["fPrice"];
            $save_data_arr["p_retail_price"] = $data["fRetailPrice"];
            $save_data_arr["p_manufacturer_id"] = $data["iManufacturerId"];
            $save_data_arr["p_brand_id"] = $data["iBrandId"];
            $save_data_arr["p_added_date"] = $data["dtAddedDate"];
            $save_data_arr["p_added_by"] = $data["iAddedBy"];
            $save_data_arr["p_updated_date"] = $data["dtUpdatedDate"];
            $save_data_arr["p_updated_by"] = $data["iUpdatedBy"];
            $save_data_arr["p_status"] = $data["eStatus"];
            if ($mode == 'Add')
            {
                $id = $this->products_model->insert($data);
                if (intval($id) > 0)
                {
                    $save_data_arr["iProductId"] = $data["iProductId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                }
                $track_cond = $this->db->protect("p.iProductId")." = ".$this->db->escape($id);
                $switch_combo = $this->products_model->getSwitchTo($track_cond);
                $recName = $switch_combo[0]["val"];
                $this->general->trackModuleNavigation("Module", "Form", "Added", $this->mod_enc_url["add"], "products", $recName, "mode|".$this->general->getAdminEncodeURL("Update")."|id|".$this->general->getAdminEncodeURL($id));
            }
            elseif ($mode == 'Update')
            {
                $res = $this->products_model->update($data, intval($id));
                if (intval($res) > 0)
                {
                    $save_data_arr["iProductId"] = $data["iProductId"] = $id;
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                }
                else
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                }
                $track_cond = $this->db->protect("p.iProductId")." = ".$this->db->escape($id);
                $switch_combo = $this->products_model->getSwitchTo($track_cond);
                $recName = $switch_combo[0]["val"];
                $this->general->trackModuleNavigation("Module", "Form", "Modified", $this->mod_enc_url["add"], "products", $recName, "mode|".$this->general->getAdminEncodeURL("Update")."|id|".$this->general->getAdminEncodeURL($id));
            }
            $ret_arr['id'] = $id;
            $ret_arr['mode'] = $mode;
            $ret_arr['message'] = $msg;
            $ret_arr['success'] = 1;

            $params_arr = $this->_request_params();

            $childModuleArr = $params_arr["childModule"];
            $childPostArr = $params_arr["child"];
            if (is_array($childModuleArr) && count($childModuleArr) > 0)
            {
                $main_data = $this->products_model->getData(intval($id), array('p.iProductId'));
                foreach ((array) $childModuleArr as $mdKey => $mdVal)
                {
                    $child_module = $mdVal;
                    if ($params_arr["childModuleShowHide"][$child_module] == "No" || $params_arr["childModuleViewMode"][$child_module] == "Yes")
                    {
                        continue;
                    }
                    $child_module_post = $childPostArr[$child_module];
                    $child_id_arr = is_array($child_module_post["id"]) ? $child_module_post["id"] : array();
                    switch ($child_module)
                    {
                        case "product_image":
                            $relation_module = $this->products_model->relation_modules["product_image"];

                            $child_unique_name = $params_arr["childModuleSingle"][$child_module];
                            $child_unique_arr = $child_module_post[$child_unique_name];
                            $child_del_ids = array();
                            if (is_array($child_unique_arr) && count($child_unique_arr) > 0)
                            {
                                foreach ((array) $child_unique_arr as $chKey => $chVal)
                                {
                                    if (trim($chVal) == "")
                                    {
                                        continue;
                                    }
                                    if (is_array($child_module_post) && count($child_module_post) > 0)
                                    {
                                        foreach ($child_module_post as $elKey => $elVal)
                                        {
                                            if (isset($elVal[$chKey]))
                                            {
                                                $child_module_post[$elKey][0] = $elVal[$chKey];
                                            }
                                        }
                                    }
                                    $child_module_post[$child_unique_name][0] = $chVal;
                                    $child_save_arr["child_module"] = $child_module;
                                    $child_save_arr["index"] = 0;
                                    $child_save_arr["id"] = $child_id_arr[$chKey];
                                    $child_save_arr["data"] = $child_module_post;
                                    $child_save_arr["main_data"] = $main_data;
                                    $child_res = $this->childDataSave($child_save_arr, true);
                                    $child_del_ids[] = $child_res["id"];
                                }
                            }
                            $extra_del = $this->db->protect("pi.iProductId")." = ".$this->db->escape($main_data[0]["iProductId"])." AND ".$this->db->protect("pi.iProductImageId")." NOT IN ('".implode("','", $child_del_ids)."')";
                            if ($relation_module["extra_cond"] != "")
                            {
                                $extra_del .= " AND ".$relation_module["extra_cond"];
                            }
                            $child_del_arr["child_module"] = $child_module;
                            $child_del_arr["extra_cond"] = $extra_del;
                            $res = $this->childDataDelete($child_del_arr, true);
                            break;
                        case "product_specification":
                            $relation_module = $this->products_model->relation_modules["product_specification"];

                            $extra_del = $this->db->protect("ps.iProductId")." = ".$this->db->escape($main_data[0]["iProductId"])." AND ".$this->db->protect("ps.iProductSpecificationId")." NOT IN ('".implode("','", $child_id_arr)."')";
                            if ($relation_module["extra_cond"] != "")
                            {
                                $extra_del .= " AND ".$relation_module["extra_cond"];
                            }
                            $child_del_arr["child_module"] = $child_module;
                            $child_del_arr["extra_cond"] = $extra_del;
                            $res = $this->childDataDelete($child_del_arr, true);
                            foreach ((array) $child_id_arr as $chKey => $chVal)
                            {
                                $child_save_arr["child_module"] = $child_module;
                                $child_save_arr["index"] = $chKey;
                                $child_save_arr["id"] = $chVal;
                                $child_save_arr["data"] = $child_module_post;
                                $child_save_arr["main_data"] = $main_data;
                                $child_res = $this->childDataSave($child_save_arr, true);
                                if (!$child_res["success"])
                                {
                                    $ret_arr["message"] = $child_res["message"];
                                    $ret_arr["success"] = 0;
                                }
                            }
                            break;
                        }
                    }
                }
        }
        catch(Exception $e)
        {
            $ret_arr["message"] = $e->getMessage();
            $ret_arr["success"] = 0;
        }
        $ret_arr['mod_enc_url']['add'] = $this->mod_enc_url['add'];
        $ret_arr['mod_enc_url']['index'] = $this->mod_enc_url['index'];
        $ret_arr['red_type'] = 'List';
        $this->filter->getPageFlowURL($ret_arr, $this->module_config, $params_arr, $id, $data);

        $this->response_arr = $ret_arr;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * inlineEditAction method is used to save inline editing data records, status field updation,
     * delete records either from grid listing or update form, saving inline adding records from grid
     */
    public function inlineEditAction()
    {
        $params_arr = $this->params_arr;
        $operartor = $params_arr['oper'];
        $all_row_selected = $params_arr['AllRowSelected'];
        $primary_ids = explode(",", $params_arr['id']);
        $primary_ids = count($primary_ids) > 1 ? $primary_ids : $primary_ids[0];
        $filters = $params_arr['filters'];
        $filters = json_decode($filters, TRUE);
        $extra_cond = '';
        $search_mode = $search_join = $search_alias = 'No';
        if ($all_row_selected == "true" && in_array($operartor, array("del", "status")))
        {
            $module_where = $this->products_model->extra_cond;
            if ($module_where != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$module_where.")" : $module_where;
            }
            $search_mode = ($operartor == "del") ? "Delete" : "Update";
            $search_join = $search_alias = "Yes";
            $config_arr['module_name'] = $this->module_name;
            $config_arr['list_config'] = $this->products_model->getListConfiguration();
            $config_arr['form_config'] = $this->products_model->getFormConfiguration();
            $config_arr['table_name'] = $this->products_model->table_name;
            $config_arr['table_alias'] = $this->products_model->table_alias;
            $filter_main = $this->filter->applyFilter($filters, $config_arr, $search_mode);
            $filter_left = $this->filter->applyLeftFilter($filters, $config_arr, $search_mode);
            $filter_range = $this->filter->applyRangeFilter($filters, $config_arr, $search_mode);
            if ($filter_main != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_main.")" : $filter_main;
            }
            if ($filter_left != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_left.")" : $filter_left;
            }
            if ($filter_range != "")
            {
                $extra_cond .= ($extra_cond != "") ? " AND (".$filter_range.")" : $filter_range;
            }
        }
        if ($search_alias == "Yes")
        {
            $primary_field = $this->products_model->table_alias.".".$this->products_model->primary_key;
        }
        else
        {
            $primary_field = $this->products_model->primary_key;
        }
        if (is_array($primary_ids))
        {
            $pk_condition = $this->db->protect($primary_field)." IN ('".implode("','", $primary_ids)."')";
        }
        elseif (intval($primary_ids) > 0)
        {
            $pk_condition = $this->db->protect($primary_field)." = ".$this->db->escape($primary_ids);
        }
        else
        {
            $pk_condition = FALSE;
        }
        if ($all_row_selected == "true" && in_array($operartor, array("del", "status")))
        {
            $pk_condition = $this->db->protect($primary_field)." > 0";
        }
        if ($pk_condition)
        {
            $extra_cond .= ($extra_cond != "") ? " AND (".$pk_condition.")" : $pk_condition;
        }
        $data_arr = $save_data_arr = array();
        try
        {
            switch ($operartor)
            {
                case 'del':
                    $mode = "Delete";
                    if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
                    {
                        $del_access = $this->filter->checkAccessCapability("products_delete", TRUE);
                    }
                    else
                    {
                        $del_access = $this->filter->getModuleWiseAccess("products", "Delete", TRUE, TRUE);
                    }
                    if (!$del_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_DELETE_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $params_arr = $this->_request_params();

                    $success = $this->products_model->delete($extra_cond, $search_alias, $search_join);
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');
                    break;
                case 'edit':
                    $mode = "Update";
                    if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
                    {
                        $edit_access = $this->filter->checkAccessCapability("products_update", TRUE);
                    }
                    else
                    {
                        $edit_access = $this->filter->getModuleWiseAccess("products", "Update", TRUE, TRUE);
                    }
                    if (!$edit_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    $post_name = $params_arr['name'];
                    $post_val = is_array($params_arr['value']) ? implode(",", $params_arr['value']) : $params_arr['value'];

                    $list_config = $this->products_model->getListConfiguration($post_name);
                    $form_config = $this->products_model->getFormConfiguration($list_config['source_field']);
                    if (!is_array($form_config) || count($form_config) == 0)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if (in_array($form_config['type'], array("date", "date_and_time", "time", 'phone_number')))
                    {
                        $post_val = $this->filter->formatActionData($post_val, $form_config);
                    }
                    if ($form_config["encrypt"] == "Yes")
                    {
                        $post_val = $this->general->encryptDataMethod($post_val, $form_config["enctype"]);
                    }
                    $field_name = $form_config['field_name'];
                    $unique_name = $form_config['name'];

                    $data_arr[$field_name] = $post_val;
                    $success = $this->products_model->update($data_arr, intval($primary_ids));
                    $message = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                    }
                    break;
                case 'status':
                    $mode = "Status";
                    if ($this->config->item("ENABLE_ROLES_CAPABILITIES"))
                    {
                        $edit_access = $this->filter->checkAccessCapability("products_update", TRUE);
                    }
                    else
                    {
                        $edit_access = $this->filter->getModuleWiseAccess("products", "Update", TRUE, TRUE);
                    }
                    if (!$edit_access)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_YOU_ARE_NOT_AUTHORIZED_TO_MODIFY_THESE_DETAILS_C46_C46_C33'));
                    }
                    if ($search_mode == "No" && $pk_condition == FALSE)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $status_field = "eStatus";
                    if ($status_field == "")
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
                    }
                    if ($search_mode == "Yes" || $search_alias == "Yes")
                    {
                        $field_name = $this->products_model->table_alias.".eStatus";
                    }
                    else
                    {
                        $field_name = $status_field;
                    }
                    $data_arr[$field_name] = $params_arr['status'];
                    $success = $this->products_model->update($data_arr, $extra_cond, $search_alias, $search_join);
                    if (!$success)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_MODIFYING_THESE_RECORDS_C46_C46_C33'));
                    }
                    $message = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_MODIFIED_SUCCESSFULLY_C46_C46_C33');
                    break;
            }
            $ret_arr['success'] = "true";
            $ret_arr['message'] = $message;
        }
        catch(Exception $e)
        {
            $ret_arr["success"] = "false";
            $ret_arr["message"] = $e->getMessage();
        }
        $this->response_arr = $ret_arr;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * processConfiguration method is used to process add and edit permissions for grid intialization
     */
    protected function processConfiguration(&$list_config = array(), $isAdd = TRUE, $isEdit = TRUE, $runCombo = FALSE)
    {
        if (!is_array($list_config) || count($list_config) == 0)
        {
            return $list_config;
        }
        $count_arr = array();
        foreach ((array) $list_config as $key => $val)
        {
            if (!$isAdd)
            {
                $list_config[$key]["addable"] = "No";
            }
            if (!$isEdit)
            {
                $list_config[$key]["editable"] = "No";
            }

            $source_field = $val['source_field'];
            $dropdown_arr = $this->dropdown_arr[$source_field];
            if (is_array($dropdown_arr) && in_array($val['type'], array("dropdown", "radio_buttons", "checkboxes", "multi_select_dropdown")))
            {
                $count_arr[$key]['ajax'] = "No";
                $count_arr[$key]['json'] = "No";
                $count_arr[$key]['data'] = array();
                $combo_arr = FALSE;
                if ($dropdown_arr['auto'] == "Yes")
                {
                    $combo_arr = $this->getSourceOptions($source_field, "Search", '', array(), '', 'count');
                    if ($combo_arr[0]['tot'] > $this->dropdown_limit)
                    {
                        $count_arr[$key]['ajax'] = "Yes";
                    }
                }
                if ($runCombo == TRUE)
                {
                    if (in_array($dropdown_arr['type'], array("enum", "phpfn")))
                    {
                        $data_arr = $this->getSourceOptions($source_field, "Search");
                        $json_arr = $this->filter->makeArrayDropdown($data_arr);
                        $count_arr[$key]['json'] = "Yes";
                        $count_arr[$key]['data'] = json_encode($json_arr);
                    }
                    else
                    {
                        if ($dropdown_arr['opt_group'] != "Yes")
                        {
                            if ($combo_arr == FALSE)
                            {
                                $combo_arr = $this->getSourceOptions($source_field, "Search", '', array(), '', 'count');
                            }
                            if ($combo_arr[0]['tot'] < $this->search_combo_limit)
                            {
                                $data_arr = $this->getSourceOptions($source_field, "Search");
                                $json_arr = $this->filter->makeArrayDropdown($data_arr);
                                $count_arr[$key]['json'] = "Yes";
                                $count_arr[$key]['data'] = json_encode($json_arr);
                            }
                        }
                    }
                }
            }
        }
        $this->count_arr = $count_arr;
        return $list_config;
    }

    /**
     * getSourceOptions method is used to get data array of enum, table, token or php function input types
     * @param string $name unique name of form configuration field.
     * @param string $mode mode for add or update form.
     * @param string $id update record id of add or update form.
     * @param array $data data array of add or update record.
     * @param string $extra extra query condition for searching data array.
     * @param string $rtype type for getting either records list or records count.
     * @return array $data_arr returns data records array
     */
    public function getSourceOptions($name = '', $mode = 'Add', $id = '', $data = array(), $extra = '', $rtype = 'records')
    {
        $combo_config = $this->dropdown_arr[$name];
        $data_arr = array();
        if (!is_array($combo_config) || count($combo_config) == 0)
        {
            return $data_arr;
        }
        $type = $combo_config['type'];
        switch ($type)
        {
            case 'enum':
                $data_arr = is_array($combo_config['values']) ? $combo_config['values'] : array();
                break;
            case 'token':
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                    {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    }
                    elseif ($mode == "Add")
                    {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                break;
            case 'table':
                if ($combo_config['auto'] == "Yes" && $mode == "Update")
                {
                    if (!empty($data[$name]))
                    {
                        $selected_rec = (is_array($data[$name])) ? $data[$name] : explode(",", $data[$name]);
                        $selected_rec = $this->general->escape_str($selected_rec);
                        $selected_str = implode(",", $selected_rec);
                        $combo_config['order_by'] = "FIELD(".$combo_config['field_key'].", ".$selected_str.") DESC, ".$combo_config['order_by'];
                    }
                }
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                {
                    $source_field = $combo_config['source_field'];
                    $target_field = $combo_config['target_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                    {
                        $parent_src = (is_array($data[$source_field])) ? $data[$source_field] : explode(",", $data[$source_field]);
                        $extra_cond = $this->db->protect($target_field)." IN ('".implode("','", $parent_src)."')";
                    }
                    elseif ($mode == "Add")
                    {
                        $extra_cond = $this->db->protect($target_field)." = ''";
                    }
                    $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                }
                if ($combo_config['parent_child'] == "Yes" && $combo_config['nlevel_child'] == "Yes")
                {
                    $combo_config['main_table'] = $this->products_model->table_name;
                    $data_arr = $this->filter->getTreeLevelDropdown($combo_config, $id, $extra, $rtype);
                }
                else
                {
                    if ($combo_config['parent_child'] == "Yes" && $combo_config['parent_field'] != "")
                    {
                        $parent_field = $combo_config['parent_field'];
                        $extra_cond = "(".$this->db->protect($parent_field)." = '0' OR ".$this->db->protect($parent_field)." = '' OR ".$this->db->protect($parent_field)." IS NULL )";
                        if ($mode == "Update" || ($mode == "Search" && $id > 0))
                        {
                            $extra_cond .= " AND ".$this->db->protect($combo_config['field_key'])." <> ".$this->db->escape($id);
                        }
                        $extra = (trim($extra) != "") ? $extra." AND ".$extra_cond : $extra_cond;
                    }
                    $data_arr = $this->filter->getTableLevelDropdown($combo_config, $id, $extra, $rtype);
                }
                break;
            case 'phpfn':
                $phpfunc = $combo_config['function'];
                $parent_src = '';
                if ($combo_config['parent_src'] == "Yes" && in_array($mode, array("Add", "Update", "Auto")))
                {
                    $source_field = $combo_config['source_field'];
                    if (in_array($mode, array("Update", "Auto")) || $data[$source_field] != "")
                    {
                        $parent_src = $data[$source_field];
                    }
                }
                if (substr($phpfunc, 0, 12) == 'controller::' && substr($phpfunc, 12) !== FALSE)
                {
                    $phpfunc = substr($phpfunc, 12);
                    if (method_exists($this, $phpfunc))
                    {
                        $data_arr = $this->$phpfunc($data[$name], $mode, $id, $data, $parent_src, $this->term);
                    }
                }
                elseif (substr($phpfunc, 0, 7) == 'model::' && substr($phpfunc, 7) !== FALSE)
                {
                    $phpfunc = substr($phpfunc, 7);
                    if (method_exists($this->products_model, $phpfunc))
                    {
                        $data_arr = $this->products_model->$phpfunc($data[$name], $mode, $id, $data, $parent_src, $this->term);
                    }
                }
                elseif (method_exists($this->general, $phpfunc))
                {
                    $data_arr = $this->general->$phpfunc($data[$name], $mode, $id, $data, $parent_src, $this->term);
                }
                break;
        }
        return $data_arr;
    }

    /**
     * getSelfSwitchToPrint method is used to provide autocomplete for switchto dropdown, which is called through form.
     */
    public function getSelfSwitchTo()
    {
        $params_arr = $this->params_arr;

        $term = strtolower($params_arr['data']['q']);

        $switchto_fields = $this->products_model->switchto_fields;
        $extra_cond = $this->products_model->extra_cond;

        $concat_fields = $this->db->concat_cast($switchto_fields);
        $search_cond = "(LOWER(".$concat_fields.") LIKE '".$this->db->escape_like_str($term)."%' OR LOWER(".$concat_fields.") LIKE '%".$this->db->escape_like_str($term)."%')";
        $extra_cond = ($extra_cond == "") ? $search_cond : $extra_cond." AND ".$search_cond;

        $switch_arr = $this->products_model->getSwitchTo($extra_cond);
        $html_arr = $this->filter->getChosenAutoJSON($switch_arr, array(), FALSE, "auto");

        $json_array['q'] = $term;
        $json_array['results'] = $html_arr;
        $html_str = json_encode($json_array);

        echo $html_str;
        $this->skip_template_view();
    }

    /**
     * getListOptions method is used to get  dropdown values searching or inline editing in grid listing (select options in html or json string)
     */
    public function getListOptions()
    {
        $params_arr = $this->params_arr;
        $alias_name = $params_arr['alias_name'];
        $rformat = $params_arr['rformat'];
        $id = $params_arr['id'];
        $mode = ($params_arr['mode'] == "Search") ? "Search" : (($params_arr['mode'] == "Update") ? "Update" : "Add");
        $config_arr = $this->products_model->getListConfiguration($alias_name);
        $source_field = $config_arr['source_field'];
        $combo_config = $this->dropdown_arr[$source_field];
        $data_arr = array();
        if ($mode == "Update")
        {
            $data_arr = $this->products_model->getData(intval($id));
        }
        $combo_arr = $this->getSourceOptions($source_field, $mode, $id, $data_arr[0]);
        if ($rformat == "json")
        {
            $html_str = $this->filter->getChosenAutoJSON($combo_arr, $combo_config, TRUE, "grid");
        }
        else
        {
            if ($combo_config['opt_group'] == "Yes")
            {
                $combo_arr = $this->filter->makeOPTDropdown($combo_arr);
            }
            else
            {
                $combo_arr = $this->filter->makeArrayDropdown($combo_arr);
            }
            $this->dropdown->combo("array", $source_field, $combo_arr, $id);
            $top_option = (in_array($mode, array("Add", "Update")) && $combo_config['default'] == 'Yes') ? "|||" : '';
            $html_str = $this->dropdown->display($source_field, $source_field, ' multiple=true ', $top_option);
        }
        echo $html_str;
        $this->skip_template_view();
    }

    /**
     * getSearchAutoComplete method is used to get dataset values for left search panel in grid listing (array values in json string)
     */
    public function getSearchAutoComplete()
    {
        $params_arr = $this->params_arr;
        $alias_name = $params_arr['alias_name'];
        $this->term = strtolower($params_arr['q']);
        $combo_arr = $this->getLeftSearchContent('single', $alias_name, $this->term);
        $records = $combo_arr[$alias_name]['records'];
        $token_array = array();
        $records_cnt = count($records);
        for ($i = 0, $j = 0; $i < $records_cnt; $i++)
        {
            if ($records[$i]['val'] == "")
            {
                continue;
            }
            $token_array[$j]["id"] = $records[$i]['id'];
            $token_array[$j]["val"] = utf8_encode($records[$i]['val']);
            if ($combo_arr[$alias_name]["count"] == "No")
            {
                $token_array[$j]["count"] = "";
            }
            else
            {
                $token_array[$j]["count"] = $records[$i]['tot'];
            }
            $j++;
        }
        $html_str = json_encode($token_array);
        echo $html_str;
        $this->skip_template_view();
    }
    /**
     * getLeftSearchContent method is used to get grid left search template or search results in left search
     * @param string $type type for getting either search template string or individual search.
     * @param string $alias_name alias name of grid list fields for individual search.
     * @param string $term term for searching specified results for individual search.
     * @return array $search_arr returns search data array for individual search
     */
    public function getLeftSearchContent($type = 'all', $alias_name = array(), $term = '')
    {
        $params_arr = $this->params_arr;
        $search_config = $this->products_model->search_config;
        if ($type == "single")
        {
            $search_arr[$alias_name] = $search_config[$alias_name];
        }
        else
        {
            $search_arr = $search_config;
        }
        $where_cond = $this->products_model->extra_cond;
        if (is_array($search_arr) && count($search_arr) > 0)
        {
            $ls_limit = $this->config->item('ADMIN_GRID_SEARCH_LS');
            $ld_limit = $this->config->item('ADMIN_GRID_SEARCH_LD');
            if (empty($ld_limit))
            {
                $ld_limit = 100;
            }
            foreach ($search_arr as $key => $val)
            {
                $alias_name = $val['name'];
                $config_arr = $this->products_model->getListConfiguration($alias_name);
                $form_config = $this->products_model->getFormConfiguration($config_arr['source_field']);
                if (!is_array($config_arr) || count($config_arr) == 0)
                {
                    continue;
                }
                $display_query = $config_arr['display_query'];
                $display_query_pro = $this->db->protect($display_query);
                $sqlfunc_query = $this->filter->parseSQLFunctionVars($config_arr['sql_func'], $display_query);
                $sqlfunc_query_pro = $this->db->protect($sqlfunc_query);

                $source_field = $config_arr['source_field'];
                $field_type = $config_arr['type'];
                $limit = (in_array($val['order'], array("DD_A", "DD_D"))) ? $ld_limit : $ls_limit;
                $data_arr = $fields_arr = array();
                if ($val["range"] == "Yes")
                {
                    $range_assoc = array();
                    $range_values = $val['values'];
                    if (is_array($range_values) && count($range_values) > 0)
                    {
                        foreach ($range_values as $rkey => $rval)
                        {
                            $min_val = $rval['min'];
                            $max_val = $rval['max'];
                            if (!is_numeric($min_val) && !is_numeric($max_val))
                            {
                                continue;
                            }
                            if ($min_val == "")
                            {
                                $case_query = " ".$display_query_pro." <= ".$this->db->escape($max_val);
                            }
                            else
                            if ($max_val == "")
                            {
                                $case_query = " ".$display_query_pro." <= ".$this->db->escape($min_val);
                            }
                            else
                            {
                                $case_query = " ".$display_query_pro." >= ".$this->db->escape($min_val)." AND ".$display_query_pro." <= ".$this->db->escape($max_val);
                            }
                            $range_values[$rkey]["label"] = $min_val." - ".$max_val;
                            if ($rkey == 0)
                            {
                                if ($min_val == "" && is_numeric($max_val))
                                {
                                    $case_query = " ".$display_query_pro." <= ".$this->db->escape($max_val);
                                    $range_values[$rkey]['level'] = "below";
                                    $range_values[$rkey]["label"] = $max_val." ".$this->general->processMessageLabel('GENERIC_AND_BELOW');
                                }
                            }
                            elseif ($rkey == count($range_values)-1)
                            {
                                if ($max_val == "" && is_numeric($min_val))
                                {
                                    $case_query = " ".$display_query_pro." >= ".$this->db->escape($min_val);
                                    $range_values[$rkey]['level'] = "above";
                                    $range_values[$rkey]["label"] = $min_val." ".$this->general->processMessageLabel('GENERIC_AND_ABOVE');
                                }
                            }
                            $fields_arr[] = array(
                                "field" => "SUM(CASE WHEN ".$case_query." THEN 1 ELSE 0 END) AS tot_".$rkey,
                                "escape" => TRUE,
                            );
                            $range_assoc["tot_".$rkey] = $range_values[$rkey];
                        }
                        if (is_array($fields_arr) && count($fields_arr) > 0)
                        {
                            $limit_fields = array();
                            $limit_fields[] = array(
                                "field" => "MIN(".$display_query_pro.") AS min",
                                "escape" => TRUE,
                            );
                            $limit_fields[] = array(
                                "field" => "MAX(".$display_query_pro.") as max",
                                "escape" => TRUE,
                            );
                            $range_limit = $this->products_model->getData($where_cond, $limit_fields, "", "", "", 'Yes');
                            $range_arr = $this->products_model->getData($where_cond, $fields_arr, "", "", "", 'Yes');
                            if (is_array($range_arr[0]) && count($range_arr[0]) > 0)
                            {
                                $rc = 0;
                                foreach ($range_arr[0] as $drkey => $drval)
                                {
                                    $temp_range['min'] = $range_assoc[$drkey]['min'];
                                    $temp_range['max'] = $range_assoc[$drkey]['max'];
                                    $temp_range['level'] = $range_assoc[$drkey]['level'];
                                    $temp_range['tot'] = $drval;
                                    $data_arr[$rc] = $temp_range;
                                    $rc++;
                                }
                            }
                        }
                    }
                    $search_arr[$key]['records'] = $data_arr;
                    $search_arr[$key]['values'] = $range_values;
                    $search_arr[$key]['range_min'] = intval($range_limit[0]['min']);
                    $search_arr[$key]['range_max'] = intval($range_limit[0]['max']);
                }
                else
                {
                    $search_tbl_arr = $this->dropdown_arr[$source_field];
                    if ($type == "all" && in_array($val['order'], array("TEXT", "AUTO")))
                    {
                        $data_arr = array();
                    }
                    else
                    {
                        $list_arr = $combo_arr = $custom_arr = array();
                        if (in_array($search_tbl_arr['type'], array("enum", "phpfn")))
                        {
                            $combo_arr = $this->getSourceOptions($source_field, "Search", '', array(), '', '');
                            if (is_array($combo_arr) && count($combo_arr) > 0)
                            {
                                $combo_arr_cnt = count($combo_arr);
                                for ($i = 0; $i < $combo_arr_cnt; $i++)
                                {
                                    if ($type == "single")
                                    {
                                        $match_str = $combo_arr[$i]["val"];
                                        $reg_exp = '/\b'.$term.'[a-zA-Z0-9_]*\b/i';
                                        if (preg_match($reg_exp, $match_str, $matches))
                                        {
                                            $list_arr[] = $combo_arr[$i];
                                            if (count($custom_arr) < $limit)
                                            {
                                                $custom_arr[] = $combo_arr[$i]["id"];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $list_arr[] = $combo_arr[$i];
                                    }
                                }
                            }
                        }
                        if ($type == "single")
                        {
                            if (in_array($search_tbl_arr['type'], array("enum", "phpfn")))
                            {
                                $extra_cond = $sqlfunc_query." IN ('".implode("','", $custom_arr)."')";
                            }
                            else
                            {
                                $extra_cond = "(LOWER(".$display_query.") LIKE '".$this->db->escape_like_str($term)."%' OR LOWER(".$display_query.") LIKE '%".$this->db->escape_like_str($term)."%')";
                            }
                        }
                        else
                        {
                            $extra_cond = '';
                        }
                        if ($where_cond != '')
                        {
                            $extra_cond = ($extra_cond != '') ? $extra_cond." AND ".$where_cond : $where_cond;
                        }
                        $group_by = $display_query;
                        if ($val['key'] != "")
                        {
                            $fields_arr[] = array(
                                "field" => $val['key']." AS id",
                            );
                            $fields_arr[] = array(
                                "field" => $display_query." AS val",
                            );
                            $group_by .= ",".$val['key'];
                        }
                        else
                        {
                            $fields_arr[] = array(
                                "field" => $sqlfunc_query." AS id",
                            );
                            $fields_arr[] = array(
                                "field" => $display_query." AS val",
                            );
                        }
                        $fields_arr[] = array(
                            "field" => "COUNT(*) AS tot",
                            "escape" => TRUE,
                        );
                        if ($val['order'] == "DD_A")
                        {
                            $order_by = "val asc";
                        }
                        elseif ($val['order'] == "DD_D")
                        {
                            $order_by = "val desc";
                        }
                        elseif ($val['order'] == "desc")
                        {
                            $order_by = "tot desc";
                        }
                        else
                        {
                            $order_by = "tot asc";
                        }
                        $data_arr = $this->products_model->getData($extra_cond, $fields_arr, $order_by, $group_by, $limit, 'Yes');
                    }

                    $search_arr[$key]['records'] = $data_arr;
                    $search_arr[$key]['dropdown'] = "No";
                    $search_arr[$key]['auto'] = "No";
                    if (in_array($val['order'], array("DD_A", "DD_D")))
                    {
                        $search_arr[$key]['dropdown'] = "Yes";
                    }
                    elseif (!in_array($val['order'], array("TEXT", "AUTO")))
                    {
                        $isSetExt = (is_array($search_tbl_arr)) ? TRUE : FALSE;
                        $isRefReq = (is_array($data_arr) && count($data_arr) < $limit) ? TRUE : FALSE;

                        $search_arr[$key]['auto'] = (!$isRefReq) ? "Yes" : "No";
                        if ($isSetExt && $isRefReq)
                        {
                            if (!in_array($search_tbl_arr['type'], array("enum", "phpfn")))
                            {
                                if ($type == "single")
                                {
                                    $concat_fields = $this->db->concat_cast($search_tbl_arr['field_val']);
                                    $search_cond = "(LOWER(".$concat_fields.") LIKE '".$this->db->escape_like_str($term)."%' OR LOWER(".$concat_fields.") LIKE '%".$this->db->escape_like_str($term)."%')";
                                }
                                else
                                {
                                    $search_cond = "";
                                }
                                $list_arr = $this->getSourceOptions($source_field, "Search", '', array(), $search_cond, '');
                            }

                            $exist_arr = $this->filter->makeArrayDropdown($data_arr);
                            $refer_arr = $this->filter->makeArrayDropdown($list_arr);
                            $arr_diff = array_diff_key($refer_arr, $exist_arr);
                            if (is_array($arr_diff) && count($arr_diff) > 0)
                            {
                                foreach ((array) $arr_diff as $key_diff => $val_diff)
                                {
                                    if (count($search_arr[$key]['records']) >= $limit)
                                    {
                                        $search_arr[$key]['auto'] = "Yes";
                                        break;
                                    }
                                    $search_arr[$key]['records'][] = array(
                                        "id" => $key_diff,
                                        'val' => $val_diff,
                                        'tot' => 0,
                                    );
                                }
                            }
                        }
                    }
                    if ($search_tbl_arr['type'] == "enum")
                    {
                        $temp_data_arr = $search_arr[$key]['records'];
                        $temp_data_arr = (is_array($temp_data_arr) && count($temp_data_arr) > 0) ? $temp_data_arr : array();
                        foreach ($temp_data_arr as $dKey => $dVal)
                        {
                            $temp_data_arr[$dKey]['val'] = $this->listing->getEnumDisplayVal($search_tbl_arr['values'], $dVal['id']);
                        }
                        $search_arr[$key]['records'] = $temp_data_arr;
                    }
                    elseif (in_array($field_type, array("date", "date_and_time", "time")))
                    {
                        $temp_data_arr = $search_arr[$key]['records'];
                        $temp_data_arr = (is_array($temp_data_arr) && count($temp_data_arr) > 0) ? $temp_data_arr : array();
                        foreach ($temp_data_arr as $dKey => $dVal)
                        {
                            $temp_data_arr[$dKey]['val'] = $this->listing->formatListingData($dVal['val'], $dVal['id'], $temp_data_arr[$dKey], $config_arr, $form_config, array(), "MGrid");
                        }
                        $search_arr[$key]['records'] = $temp_data_arr;
                    }
                    if (in_array($val['order'], array("TEXT", "AUTO")))
                    {
                        $search_arr[$key]['auto'] = "Yes";
                    }
                }
                if ($val['php_func'] != '')
                {
                    $phpfunc = $val['php_func'];
                    if (substr($phpfunc, 0, 12) == 'controller::' && substr($phpfunc, 12) !== FALSE)
                    {
                        $phpfunc = substr($phpfunc, 12);
                        if (method_exists($this, $phpfunc))
                        {
                            $search_arr[$key] = $this->$phpfunc($search_arr[$key]);
                        }
                    }
                    elseif (substr($phpfunc, 0, 7) == 'model::' && substr($phpfunc, 7) !== FALSE)
                    {
                        $phpfunc = substr($phpfunc, 7);
                        if (method_exists($this->products_model, $phpfunc))
                        {
                            $search_arr[$key] = $this->products_model->$phpfunc($search_arr[$key]);
                        }
                    }
                    elseif (method_exists($this->general, $phpfunc))
                    {
                        $search_arr[$key] = $this->general->$phpfunc($search_arr[$key]);
                    }
                }
            }
        }
        else
        {
            $search_arr = array();
        }
        if ($params_arr['tempalte'] == "Yes")
        {
            $render_arr = array(
                "search_arr" => $search_arr,
            );
            $html_str = $parse_html = $this->parser->parse("products_search", $render_arr, TRUE);
            echo $html_str;
            $this->skip_template_view();
        }
        else
        {
            return $search_arr;
        }
    }
    /**
     * downloadListFile method is used to download grid listing files
     */
    public function downloadListFile()
    {
        $params_arr = $this->params_arr;
        $alias_name = $params_arr['alias_name'];
        $folder = $params_arr['folder'];
        $id = $params_arr['id'];
        $config_arr = $this->products_model->getListConfiguration($alias_name);
        if (is_array($config_arr) && count($config_arr) > 0)
        {
            $fields = $config_arr['display_query']." AS ".$alias_name;
            $data_arr = $this->products_model->getData(intval($id), $fields, '', '', '', 'Yes');
            $file_name = $data_arr[0][$alias_name];
            $this->listing->downloadFiles("List", $config_arr, $file_name, $folder);
        }
        $this->skip_template_view();
    }
    /**
     * uploadFormFile method is used to upload files or images through add or update form
     */
    public function uploadFormFile()
    {
        $this->load->library('upload');
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $id = $params_arr['id'];
        $old_file = $params_arr['oldFile'];
        $type = $params_arr['type'];
        $upload_files = $_FILES['Filedata'];
        list($file_name, $extension) = $this->general->get_file_attributes($upload_files['name']);
        $this->general->createUploadFolderIfNotExists('__temp');
        $config_arr = $this->products_model->getFormConfiguration($unique_name);

        $temp_folder_path = $this->config->item('admin_upload_temp_path');
        $temp_folder_url = $this->config->item('admin_upload_temp_url');
        try
        {
            if ($type == 'webcam')
            {
                $img_pic = $params_arr['newFile'];
                $res = $this->general->imageupload_base64($temp_folder_path, $img_pic);
                if ($res[0] == FALSE)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
                }
                $file_name = $res[0];
            }
            else
            {
                $file_size = ($config_arr['file_size']) ? $config_arr['file_size'] : $this->config->item('ADMIN_MAX_UPLOAD_FILE_SIZE');
                $file_format = ($config_arr['file_format']) ? $config_arr['file_format'] : $this->config->item('IMAGE_EXTENSION_ARR');
                if ($config_arr['file_folder'] == "")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_PLEASE_SPECIFY_THE_UPLOAD_FOLDER_NAME_C46_C46_C33'));
                }
                if ($upload_files['name'] == "")
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_UPLOAD_FILE_NOT_FOUND_C46_C46_C33'));
                }
                if (!$this->general->validateFileFormat($file_format, $file_name))
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FILE_TYPE_IS_NOT_ACCEPTABLE'));
                }
                if (!$this->general->validateFileSize($file_size, $upload_files['size']))
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FILE_SIZE_NOT_A_VALID_ONE_C46_C46_C33'));
                }
                $upload_config = array(
                    'upload_path' => $temp_folder_path,
                    'allowed_types' => '*',
                    'max_size' => $file_size,
                    'file_name' => $file_name,
                    'remove_space' => TRUE,
                    'overwrite' => FALSE,
                );
                $this->upload->initialize($upload_config);
                if (!$this->upload->do_upload('Filedata'))
                {
                    $upload_error = $this->upload->display_errors('', '');
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
                }
                $file_info = $this->upload->data();
                $file_name = $file_info['file_name'];
            }
            if (!$file_name)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPLOADING_C46_C46_C33'));
            }
            if (substr($upload_files['type'], 0, 5) == 'image')
            {
                $check_img_dimensions = 0;
                if (!empty($config_arr['min_dimension']))
                {
                    $tmp_min_arr = explode('X', strtoupper($config_arr['min_dimension']));
                    $img_min_width = $tmp_min_arr[0];
                    $img_min_height = $tmp_min_arr[1];
                    $check_img_dimensions = ($img_min_width > 0 && $img_min_height > 0) ? 1 : 0;
                }
                if (!empty($config_arr['max_dimension']))
                {
                    $tmp_max_arr = explode('X', strtoupper($config_arr['max_dimension']));
                    $img_max_width = $tmp_max_arr[0];
                    $img_max_height = $tmp_max_arr[1];
                    $check_img_dimensions = ($img_max_width > 0 && $img_max_height > 0) ? 1 : 0;
                }
                if ($check_img_dimensions)
                {
                    $img_arr = getimagesize($temp_folder_url.$file_name);
                    $img_width = $img_arr[0];
                    $img_height = $img_arr[1];
                    if ($img_width > 0)
                    {
                        if ($img_min_width > 0 && $img_width < $img_min_width)
                        {
                            throw new Exception($this->general->processMessageLabel('IMAGE_MIN_WIDTH_SHOULD_BE').$img_min_width);
                        }
                        if ($img_max_width > 0 && $img_width > $img_max_width)
                        {
                            throw new Exception($this->general->processMessageLabel('IMAGE_MAX_WIDTH_SHOULD_BE').$img_max_width);
                        }
                    }
                    if ($img_height > 0)
                    {
                        if ($img_min_height > 0 && $img_height < $img_min_height)
                        {
                            throw new Exception($this->general->processMessageLabel('IMAGE_MIN_HEIGHT_SHOULD_BE').$img_min_height);
                        }
                        if ($img_max_height > 0 && $img_height > $img_max_height)
                        {
                            throw new Exception($this->general->processMessageLabel('IMAGE_MAX_HEIGHT_SHOULD_BE').$img_max_height);
                        }
                    }
                }
            }
            $image_valid_ext = explode('.', $file_name);
            $ret_arr['fileURL'] = $temp_folder_url.$file_name;
            if (in_array(strtolower(end($image_valid_ext)), $this->config->item('IMAGE_EXTENSION_ARR')))
            {
                $ret_arr['width'] = ($config_arr['file_width']) ? $config_arr['file_width'] : $this->config->item('ADMIN_DEFAULT_IMAGE_WIDTH');
                $ret_arr['height'] = ($config_arr['file_height']) ? $config_arr['file_height'] : $this->config->item('ADMIN_DEFAULT_IMAGE_HEIGHT');
                $ret_arr['imgURL'] = $this->general->resize_image($temp_folder_url.$file_name, $ret_arr['width'], $ret_arr['height']);
                $ret_arr['resized'] = 1;
                $icon_class = 'fa-file-image-o';
            }
            else
            {
                $icon_class = 'fa-file-text-o';
                $ret_arr['resized'] = 0;
            }
            $icon_file = $file_name;
            if (function_exists("get_file_icon_class"))
            {
                $icon_class = get_file_icon_class($file_name);
                $icon_file = format_uploaded_file($file_name);
            }
            $ret_arr['iconclass'] = $icon_class;
            $ret_arr['iconfile'] = $icon_file;
            $ret_arr['filename'] = $icon_file;
            if (is_file($temp_folder_path.$old_file) && $old_file != '')
            {
                unlink($temp_folder_path.$old_file);
            }

            $ret_arr['success'] = 1;
            $ret_arr['message'] = $this->general->processMessageLabel('ACTION_FILE_UPLOADED_SUCCESSFULLY_C46_C46_C33');
            $ret_arr['uploadfile'] = $file_name;
            $ret_arr['oldfile'] = $file_name;
        }
        catch(Exception $e)
        {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $e->getMessage();
        }
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }
    /**
     * downloadFormFile method is used to download add or update form files
     */
    public function downloadFormFile()
    {
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $folder = $params_arr['folder'];
        $id = $params_arr['id'];
        $config_arr = $this->products_model->getFormConfiguration($unique_name);
        try
        {
            if (!is_array($config_arr) || count($config_arr) == 0)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
            }
            if ($config_arr['entry_type'] == "Custom")
            {
                $file_name = $params_arr['file'];
            }
            else
            {
                $fields = $config_arr['table_alias'].".".$config_arr['field_name']." AS ".$unique_name;
                $data_arr = $this->products_model->getData(intval($id), $fields, '', '', '', 'Yes');
                if (!is_array($data_arr) || count($data_arr) == 0)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }
                $file_name = $data_arr[0][$unique_name];
            }
            $this->listing->downloadFiles("Form", $config_arr, $file_name, $folder);
        }
        catch(Exception $e)
        {
            $ret_arr['success'] = 0;
            $ret_arr['message'] = $e->getMessage();
            echo json_encode($ret_arr);
        }
        $this->skip_template_view();
    }
    /**
     * deleteFormFile method is used to delete add or update form files
     */
    public function deleteFormFile()
    {
        $params_arr = $this->params_arr;
        $unique_name = $params_arr['unique_name'];
        $folder = $params_arr['folder'];
        $id = $params_arr['id'];
        $config_arr = $this->products_model->getFormConfiguration($unique_name);
        try
        {
            if (!is_array($config_arr) || count($config_arr) == 0)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_FORM_CONFIGURING_NOT_DONE_C46_C46_C33'));
            }
            if ($config_arr['entry_type'] == "Custom")
            {
                //custom fields does not have delete from DB.
                $file_name = $params_arr['file'];
            }
            else
            {
                $field_name = $config_arr['field_name'];
                $fields = $config_arr['table_alias'].".".$field_name." AS ".$unique_name;
                $data_arr = $this->products_model->getData(intval($id), $fields, '', '', '', 'Yes');
                if (!is_array($data_arr) || count($data_arr) == 0)
                {
                    throw new Exception();
                }
                $fields = $config_arr['table_alias'].'.'.$field_name.' AS '.$unique_name;
                $data_arr = $this->products_model->getData(intval($id), $fields, '', '', '', 'Yes');
                if (!is_array($data_arr) || count($data_arr) == 0)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }
                $fields = $config_arr['table_alias'].'.'.$field_name.' AS '.$unique_name;
                $data_arr = $this->products_model->getData(intval($id), $fields, '', '', '', 'Yes');
                if (!is_array($data_arr) || count($data_arr) == 0)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_RECORDS_WHICH_YOU_ARE_TRYING_TO_ACCESS_ARE_NOT_AVAILABLE_C46_C46_C33'));
                }
                $update_arr[$field_name] = '';
                $res = $this->products_model->update($update_arr, intval($id));
                if (!$res)
                {
                    throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_FILE_DELETION_C46_C46_C33'));
                }
                $file_name = $data_arr[0][$unique_name];
            }
            $res = $this->listing->deleteFiles($config_arr, $file_name, $folder, "Form");
            if (!$res)
            {
                throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_FILE_DELETION_C46_C46_C33'));
            }
            $success = 1;
            $message = $this->general->processMessageLabel('ACTION_FILE_DELETED_SUCCESSFULLY_C46_C46_C33');
        }
        catch(Exception $e)
        {
            $success = 0;
            $message = $e->getMessage();
        }
        $ret_arr['success'] = $success;
        $ret_arr['message'] = $message;
        echo json_encode($ret_arr);
        $this->skip_template_view();
    }

    /**
     * childDataAdd method is used to add more inline child module records (relation module records) for add or update form
     */
    public function childDataAdd()
    {

        try
        {
            $params_arr = $this->params_arr;
            $child_module = $params_arr["child_module"];
            $row_index = $params_arr["incNo"];
            $mode = ($params_arr["mode"] == "Update") ? "Update" : "Add";
            $recMode = ($params_arr["recMode"] == "Update") ? "Update" : "Add";
            $rmPopup = ($params_arr["rmPopup"] == "Yes") ? "Yes" : "No";
            $id = $params_arr["id"];
            $child_data = $child_func = $child_elem = array();
            switch ($child_module)
            {
                case "product_image":
                    $this->load->module("master/product_image");
                    $this->load->model("master/product_image_model");
                    $child_config["parent_module"] = "products";
                    $child_config["child_module"] = $child_module;
                    $child_config["mode"] = $recMode;
                    if ($recMode == "Update")
                    {
                        $child_mod_data = $this->product_image_model->getData(intval($id), "", "", "", "", "Yes");
                        $child_config["data"][$row_index] = $child_mod_data[0];
                    }
                    $child_config["row_index"] = $row_index;
                    $child_config["perform"] = array(
                        "data",
                        "options",
                        "config",
                    );
                    $module_arr = $this->product_image->getRelationModule($child_config);
                    $module_arr["config_arr"]["form_config"] = $module_arr["form_config"];
                    $child_data[0] = $module_arr["data"][$row_index];
                    $child_func[0] = $module_arr["func"][$row_index];
                    $child_elem[0] = $module_arr["elem"][$row_index];
                    $child_conf_arr = $module_arr["config_arr"];
                    $child_opt_arr = $module_arr["opt_arr"];
                    $child_img_html = $module_arr["img_html"];
                    $child_auto_arr = $module_arr["auto_arr"];
                    $child_access_arr = array(
                        "add" => 1,
                        "save" => 1,
                        "delete" => 1,
                        "actions" => 1,
                        "labels" => array()
                    );
                    $child_module_add_file = "products_product_image_add_more";
                    break;
                case "product_specification":
                    $this->load->module("master/product_specification");
                    $this->load->model("master/product_specification_model");
                    $child_config["parent_module"] = "products";
                    $child_config["child_module"] = $child_module;
                    $child_config["mode"] = $recMode;
                    if ($recMode == "Update")
                    {
                        $child_mod_data = $this->product_specification_model->getData(intval($id), "", "", "", "", "Yes");
                        $child_config["data"][$row_index] = $child_mod_data[0];
                    }
                    $child_config["row_index"] = $row_index;
                    $child_config["perform"] = array(
                        "data",
                        "options",
                        "config",
                    );
                    $module_arr = $this->product_specification->getRelationModule($child_config);
                    $module_arr["config_arr"]["form_config"] = $module_arr["form_config"];
                    $child_data[0] = $module_arr["data"][$row_index];
                    $child_func[0] = $module_arr["func"][$row_index];
                    $child_elem[0] = $module_arr["elem"][$row_index];
                    $child_conf_arr = $module_arr["config_arr"];
                    $child_opt_arr = $module_arr["opt_arr"];
                    $child_img_html = $module_arr["img_html"];
                    $child_auto_arr = $module_arr["auto_arr"];
                    $child_access_arr = array(
                        "add" => 1,
                        "save" => 1,
                        "delete" => 1,
                        "actions" => 1,
                        "labels" => array()
                    );
                    $child_module_add_file = "products_product_specification_add_more";
                    break;
            }
            $enc_child_id = $this->general->getAdminEncodeURL($id);
            $render_arr = array(
                "mod_enc_url" => $this->mod_enc_url,
                "mod_enc_mode" => $this->mod_enc_mode,
                "child_module_name" => $child_module,
                "row_index" => $row_index,
                "child_data" => $child_data,
                "child_func" => $child_func,
                "child_elem" => $child_elem,
                "child_conf_arr" => $child_conf_arr,
                "child_opt_arr" => $child_opt_arr,
                "child_img_html" => $child_img_html,
                "child_auto_arr" => $child_auto_arr,
                "child_access_arr" => $child_access_arr,
                "recMode" => $recMode,
                "popup" => $rmPopup,
                "mode" => $mode,
                "child_id" => $id,
                "enc_child_id" => $enc_child_id,
            );
            $parse_html = $this->parser->parse($child_module_add_file, $render_arr, true);
            echo $parse_html;
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
            echo $msg;
        }
        $this->skip_template_view();
    }
    /**
     * childDataSave method is used to save inline child module records (relation module records) from add or update form
     * @param array $config_arr config array for saving child module data records.
     * @param boolean $called_func for setting flag to differ function call of url call.
     * @return array $res_arr returns saving data record response
     */
    public function childDataSave($config_arr = array(), $called_func = FALSE)
    {

        try
        {
            $params_arr = $this->params_arr;
            if ($called_func == TRUE)
            {
                $child_module = $config_arr["child_module"];
                $index = $config_arr["index"];
                $id = $config_arr["id"];
                $child_post = $config_arr["data"];
                $main_data = $config_arr["main_data"];
            }
            else
            {
                $child_module = $params_arr["child_module"];
                $index = $params_arr["index"];
                $id = $params_arr["id"];
                $child_post = $params_arr["child"][$child_module];
            }
            switch ($child_module)
            {
                case "product_image":
                    $this->load->module("master/product_image");
                    $this->load->model("master/product_image_model");

                    $child_config["parent_module"] = "products";
                    $child_config["perform"] = array(
                        "config",
                    );
                    $module_arr = $this->product_image->getRelationModule($child_config);
                    $form_config = $module_arr["form_config"];
                    $data_arr = $this->product_image_model->getData(intval($id));
                    $mode = (is_array($data_arr) && count($data_arr) > 0) ? "Update" : "Add";
                    if ($called_func == TRUE)
                    {
                        $params_arr["parID"] = $main_data[0]["iProductId"];
                    }

                    $child_data = $save_data_arr = $file_data = array();
                    $pi_product_id = $child_post["pi_product_id"][$index];
                    $pi_image = $child_post["pi_image"][$index];

                    $child_data["iProductId"] = $pi_product_id;
                    $child_data["vImage"] = $pi_image;

                    $child_data["iProductId"] = $params_arr["parID"];
                    if ($mode == "Add")
                    {
                        $id = $res = $this->product_image_model->insert($child_data);
                        if (intval($id) > 0)
                        {
                            $data["iProductImageId"] = $id;
                            $save_data_arr["iProductImageId"] = $child_data["iProductImageId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                        }
                    }
                    elseif ($mode == "Update")
                    {
                        $res = $this->product_image_model->update($child_data, intval($id));
                        if (intval($res) > 0)
                        {
                            $data["iProductImageId"] = $id;
                            $save_data_arr["iProductImageId"] = $child_data["iProductImageId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                        }
                    }

                    $success = 1;

                    $file_data["pi_image"]["file_name"] = $pi_image;
                    $file_data["pi_image"]["old_file_name"] = $child_post["old_pi_image"];
                    $file_data["pi_image"]["unique_name"] = "pi_image";
                    $file_data["pi_image"]["primary_key"] = "iProductImageId";
                    $file_keep = $form_config["pi_image"]["file_keep"];
                    if ($file_keep != "" && $file_keep != "iProductImageId")
                    {
                        $save_data_arr[$file_keep] = $child_data[$form_config[$file_keep]["field_name"]];
                    }
                    $this->listing->uploadFilesOnSaveForm($file_data, $form_config, $save_data_arr);
                    break;
                case "product_specification":
                    $this->load->module("master/product_specification");
                    $this->load->model("master/product_specification_model");

                    $child_config["parent_module"] = "products";
                    $child_config["perform"] = array(
                        "config",
                    );
                    $module_arr = $this->product_specification->getRelationModule($child_config);
                    $form_config = $module_arr["form_config"];
                    $data_arr = $this->product_specification_model->getData(intval($id));
                    $mode = (is_array($data_arr) && count($data_arr) > 0) ? "Update" : "Add";
                    if ($called_func == TRUE)
                    {
                        $params_arr["parID"] = $main_data[0]["iProductId"];
                    }
                    if (method_exists($this->product_specification, 'before_form_save'))
                    {
                        $event_res = $this->product_specification->before_form_save($mode, $id, $params_arr['parID'], $index, $child_post);
                        if (!$event_res['success'])
                        {
                            $before_error_msg = $this->general->processMessageLabel('ACTION_BEFORE_EVENT_HAS_BEEN_FAILED_C46_C46_C33');
                            $error_msg = ($event_res['message']) ? $event_res['message'] : $before_error_msg;
                            throw new Exception($error_msg);
                        }
                    }

                    $child_data = $save_data_arr = $file_data = array();
                    $ps_title = $child_post["ps_title"][$index];
                    $ps_value = $child_post["ps_value"][$index];
                    $ps_product_id = $child_post["ps_product_id"][$index];

                    $child_data["vTitle"] = $ps_title;
                    $child_data["vValue"] = $ps_value;
                    $child_data["iProductId"] = $ps_product_id;

                    $child_data["iProductId"] = $params_arr["parID"];
                    if ($mode == "Add")
                    {
                        $id = $res = $this->product_specification_model->insert($child_data);
                        if (intval($id) > 0)
                        {
                            $data["iProductSpecificationId"] = $id;
                            $save_data_arr["iProductSpecificationId"] = $child_data["iProductSpecificationId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_ADDED_SUCCESSFULLY_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_ADDING_RECORD_C46_C46_C33'));
                        }
                    }
                    elseif ($mode == "Update")
                    {
                        $res = $this->product_specification_model->update($child_data, intval($id));
                        if (intval($res) > 0)
                        {
                            $data["iProductSpecificationId"] = $id;
                            $save_data_arr["iProductSpecificationId"] = $child_data["iProductSpecificationId"] = $id;
                            $msg = $this->general->processMessageLabel('ACTION_RECORD_SUCCESSFULLY_UPDATED_C46_C46_C33');
                        }
                        else
                        {
                            throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_UPDATING_OF_THIS_RECORD_C46_C46_C33'));
                        }
                    }

                    $success = 1;
                    break;
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
        }
        $res_arr["success"] = $success;
        $res_arr["message"] = $msg;
        if ($called_func == TRUE)
        {
            $res_arr["id"] = $id;

            return $res_arr;
        }
        else
        {
            $enc_id = $this->general->getAdminEncodeURL($id);
            $res_arr["id"] = $enc_id;
            $res_arr["enc_id"] = $enc_id;

            echo json_encode($res_arr);
            $this->skip_template_view();
        }
    }
    /**
     * childDataDelete method is used to delete inline child module records (relation module records) from add or update form
     * @param array $config_arr config array for saving child module data records.
     * @param boolean $called_func for setting flag to differ function call of url call.
     * @return array $res_arr returns delete data record response
     */
    public function childDataDelete($config_arr = array(), $called_func = FALSE)
    {

        try
        {
            $params_arr = $this->params_arr;
            if ($called_func == TRUE)
            {
                $child_module = $config_arr["child_module"];
                $where_cond = $config_arr["extra_cond"];
            }
            else
            {
                $child_module = $params_arr["child_module"];
                $where_cond = $id = intval($params_arr["id"]);
            }
            switch ($child_module)
            {
                case "product_image":
                    $this->load->module("master/product_image");
                    $this->load->model("master/product_image_model");

                    $parent_join_arr = array(
                        "joins" => array(
                            array(
                                "table_name" => "product",
                                "table_alias" => "p",
                                "field_name" => "iProductId",
                                "rel_table_name" => "product_image",
                                "rel_table_alias" => "pi",
                                "rel_field_name" => "iProductId",
                                "join_type" => "left",
                            )
                        )
                    );
                    $res = $this->product_image_model->delete($where_cond, "Yes", $parent_join_arr);
                    if (!$res)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');
                    $success = 1;
                    break;
                case "product_specification":
                    $this->load->module("master/product_specification");
                    $this->load->model("master/product_specification_model");

                    $parent_join_arr = array(
                        "joins" => array(
                            array(
                                "table_name" => "product",
                                "table_alias" => "p",
                                "field_name" => "iProductId",
                                "rel_table_name" => "product_specification",
                                "rel_table_alias" => "ps",
                                "rel_field_name" => "iProductId",
                                "join_type" => "left",
                            )
                        )
                    );
                    $res = $this->product_specification_model->delete($where_cond, "Yes", $parent_join_arr);
                    if (!$res)
                    {
                        throw new Exception($this->general->processMessageLabel('ACTION_FAILURE_IN_DELETION_THIS_RECORD_C46_C46_C33'));
                    }
                    $msg = $this->general->processMessageLabel('ACTION_RECORD_C40S_C41_DELETED_SUCCESSFULLY_C46_C46_C33');
                    $success = 1;
                    break;
            }
        }
        catch(Exception $e)
        {
            $success = 0;
            $msg = $e->getMessage();
        }
        $res_arr["success"] = $success;
        $res_arr["message"] = $msg;
        if ($called_func == TRUE)
        {

            return $res_arr;
        }
        else
        {
            $this->response_arr = $res_arr;

            echo json_encode($res_arr);
            $this->skip_template_view();
        }
    }
}
