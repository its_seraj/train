<?php


/**
 * Description of product_specification Extended Controller
 * 
 * @module Extended product_specification
 * 
 * @class Cit_Product_specification.php
 * 
 * @path application\admin\master\controllers\Cit_Product_specification.php
 * 
 * @author CIT Dev Team
 * 
 * @date 31.03.2022
 */        

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Cit_Product_specification extends Product_specification {
        public function __construct()
{
    parent::__construct();
}

public function before_form_save($mode = '',$id = '', $parID = ''){
    if(!empty($_POST['color_picker'])){
        $_POST['ps_value'] = $_POST['color_picker'];
        unset($_POST['color_picker']);
    }
    pr($_POST);
    $ret_arr['success'] = true;

    return $ret_arr;
}
}
