<!-- <div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title"><%$this->lang->line("FRONT_DASHBOARD")%></div>
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <h3 class="title">Welcome <%$this->session->userdata('vFirstName')%> <%$this->session->userdata('vLastName')%></h3>
        </div>
    </div>
</div> -->

<head>
    <script src="public/js/jquery/jquery.js"></script>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        #grid_json{
            display:  grid;
            grid-template-columns: auto auto auto;
            gap: 1em;
        }
        .card{
            padding: 1em;
            box-shadow: 0 0 12px 0 #ddd;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 8px;
        }
        </style>

</head>
    <div class="container" >

        <div id="grid_json"></div>
    </div>
<script>

    $(function () {

        var data = <%$data%>;
        console.log(data);
        var output = "";
        $.each(data, (i, row) => {
            output += `
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="${row.pi_image}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">${row.p_product_name}</h5>
                    <p class="card-text">Old Price : ${row.p_retail_price}</p>
                    <p class="card-text">New Price : ${row.p_price}</p>
                    <p class="card-text">Category : ${row.c_category_name}</p>
                    <p class="card-text">Brand : ${row.b_brand}</p>
                </div>
            </div>
            `;
        })
        $("#grid_json").html(output);
        
    });
</script>

