<?php  

//Country List Messages
    $lang["country_list"]["finish_country_list_success"] = "Country list found." ;
    $lang["country_list"]["finish_country_list_failure"] = "No country data found." ;

//Country With States Messages
    $lang["country_with_states"]["finish_country_data_success"] = "Country data found." ;
    $lang["country_with_states"]["finish_country_data_failure"] = "No country data found." ;

//Customer Add Messages
    $lang["customer_add"]["last_name_required"] = "Please enter a value for the last_name field." ;
    $lang["customer_add"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["customer_add"]["email_required"] = "Please enter a value for the email field." ;
    $lang["customer_add"]["username_required"] = "Please enter a value for the username field." ;
    $lang["customer_add"]["password_required"] = "Please enter a value for the password field." ;
    $lang["customer_add"]["finish_customer_reg_failure"] = "Customer already exists with this email address" ;
    $lang["customer_add"]["finish_customer_add_failure"] = "Sorry, unable to register." ;
    $lang["customer_add"]["finish_customer_add_success"] = "Customer registration done." ;
    $lang["customer_add"]["finish_customer_reg_failure"] = "This Email/Username is already registered. Please try with different details." ;

//Customer Login Messages
    $lang["customer_login"]["username_required"] = "Please enter a value for the username field." ;
    $lang["customer_login"]["password_required"] = "Please enter a value for the password field." ;
    $lang["customer_login"]["finish_customer_login_success"] = "Customer information found." ;
    $lang["customer_login"]["finish_customer_login_failure"] = "Customer not found. username / password is invalid." ;

//Customer Update Messages
    $lang["customer_update"]["customer_id_required"] = "Please enter a value for the customer_id field." ;
    $lang["customer_update"]["first_name_required"] = "Please enter a value for the first_name field." ;
    $lang["customer_update"]["finish_customer_update_success"] = "Customer profile updated successfully." ;
    $lang["customer_update"]["finish_customer_update_failure"] = "Failure in updating customer profile." ;

//Customer Detail Messages
    $lang["customer_detail"]["customer_id_required"] = "Please enter a value for the customer_id field." ;
    $lang["customer_detail"]["finish_customer_info_success"] = "Custom information found." ;
    $lang["customer_detail"]["finish_customer_info_failure"] = "No customer information found." ;

//Change Password Messages
    $lang["change_password"]["old_password_required"] = "Please enter a value for the old_password field." ;
    $lang["change_password"]["new_password_required"] = "Please enter a value for the new_password field." ;
    $lang["change_password"]["customer_id_required"] = "Please enter a value for the customer_id field." ;
    $lang["change_password"]["finish_customer_pwd_success"] = "Customer password changed." ;
    $lang["change_password"]["finish_customer_pwd_failure"] = "Customer old password does not matched." ;

//Forgot Password Messages
    $lang["forgot_password"]["email_required"] = "Please enter a value for the email field." ;
    $lang["forgot_password"]["finish_customer_pwd_generation"] = "Sorry, we are unable to generate new password. Please contact administrator." ;
    $lang["forgot_password"]["finish_customer_pwd_success"] = "Forgot password email sent to '#mc_email#' successfully. Please check your email." ;
    $lang["forgot_password"]["finish_customer_pwd_failure"] = "No customer exists with this email address." ;

//Get Country List Messages
    $lang["get_country_list"]["mod_country_finish_success"] = "All data has been fetched." ;

//Get Product List Messages
    $lang["get_product_list"]["product_finish_success"] = "Getting all the products name." ;

//Set Product List Messages
    $lang["set_product_list"]["product_finish_success"] = "Data has been inserted successfully." ;

//Category wise Product List Messages
    $lang["category_wise_product_list"]["category_id2_required"] = "Please enter a value for the product_id field." ;
    $lang["category_wise_product_list"]["category_finish_success_1"] = "unsuccess<br>" ;
    $lang["category_wise_product_list"]["category_finish_success"] = "" ;

//Get Admin Messages
    $lang["get_admin"]["id_required"] = "Please enter a value for the id field." ;
    $lang["get_admin"]["mod_admin_finish_success"] = "" ;
    $lang["get_admin"]["mod_admin_finish_success_1"] = "Admin id not found." ;

//Change Password New Messages
    $lang["change_password_new"]["old_password_required"] = "Please enter a value for the old_password field." ;
    $lang["change_password_new"]["new_password_required"] = "Please enter a value for the new_password field." ;
    $lang["change_password_new"]["reapeat_password_required"] = "Please enter a value for the reapeat_password field." ;
    $lang["change_password_new"]["mod_admin_finish_success_1"] = "New Password & Repeat Password must be same" ;
    $lang["change_password_new"]["mod_admin_finish_success_2"] = "Record not exists." ;
    $lang["change_password_new"]["mod_admin_finish_success"] = "Password Change sucessfully." ;

//Change Product Price Messages
    $lang["change_product_price"]["product_id_required"] = "Please enter a value for the product_id field." ;
    $lang["change_product_price"]["price_required"] = "Please enter a value for the price field." ;
    $lang["change_product_price"]["product_finish_success"] = "" ;