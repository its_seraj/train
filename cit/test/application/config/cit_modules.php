<?php

defined('BASEPATH') OR exit('No direct script access allowed');

#####GENERATED_CONFIG_SETTINGS_START#####

$config["admin"] = array(
    "title" => "Admin",
    "table" => "mod_admin",
    "module" => "admin",
    "folder" => "user",
    "add_url" => "user/admin/add",
    "list_url" => "user/admin/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["brands"] = array(
    "title" => "Brands",
    "table" => "brand",
    "module" => "brands",
    "folder" => "master",
    "add_url" => "master/brands/add",
    "list_url" => "master/brands/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["category"] = array(
    "title" => "Category",
    "table" => "category",
    "module" => "category",
    "folder" => "master",
    "add_url" => "master/category/add",
    "list_url" => "master/category/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["category_new"] = array(
    "title" => "Category New",
    "table" => "category",
    "module" => "category_new",
    "folder" => "master",
    "add_url" => "master/category_new/add",
    "list_url" => "master/category_new/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["companies"] = array(
    "title" => "companies",
    "table" => "companies",
    "module" => "companies",
    "folder" => "master",
    "add_url" => "master/companies/add",
    "list_url" => "master/companies/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["country"] = array(
    "title" => "Country",
    "table" => "mod_country",
    "module" => "country",
    "folder" => "tools",
    "add_url" => "tools/country/add",
    "list_url" => "tools/country/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["country_pq"] = array(
    "title" => "Country PQ",
    "table" => "mod_country",
    "module" => "country_pq",
    "folder" => "tools",
    "add_url" => "tools/country_pq/add",
    "list_url" => "tools/country_pq/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["customers"] = array(
    "title" => "Customers",
    "table" => "mod_customer",
    "module" => "customers",
    "folder" => "user",
    "add_url" => "user/customers/add",
    "list_url" => "user/customers/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["group"] = array(
    "title" => "Group",
    "table" => "mod_group_master",
    "module" => "group",
    "folder" => "user",
    "add_url" => "user/group/add",
    "list_url" => "user/group/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["loghistory"] = array(
    "title" => "Log History",
    "table" => "mod_log_history",
    "module" => "loghistory",
    "folder" => "user",
    "add_url" => "user/loghistory/add",
    "list_url" => "user/loghistory/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["manufacture"] = array(
    "title" => "Manufacture",
    "table" => "manufacturer",
    "module" => "manufacture",
    "folder" => "master",
    "add_url" => "master/manufacture/add",
    "list_url" => "master/manufacture/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["product_image"] = array(
    "title" => "Product Image",
    "table" => "product_image",
    "module" => "product_image",
    "folder" => "master",
    "add_url" => "master/product_image/add",
    "list_url" => "master/product_image/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["products"] = array(
    "title" => "Products",
    "table" => "product",
    "module" => "products",
    "folder" => "master",
    "add_url" => "master/products/add",
    "list_url" => "master/products/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["product_specification"] = array(
    "title" => "product_specification",
    "table" => "product_specification",
    "module" => "product_specification",
    "folder" => "master",
    "add_url" => "master/product_specification/add",
    "list_url" => "master/product_specification/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["promo_code"] = array(
    "title" => "Promo Code",
    "table" => "promo",
    "module" => "promo_code",
    "folder" => "master",
    "add_url" => "master/promo_code/add",
    "list_url" => "master/promo_code/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["rating"] = array(
    "title" => "Rating",
    "table" => "rating_master",
    "module" => "rating",
    "folder" => "master",
    "add_url" => "master/rating/add",
    "list_url" => "master/rating/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["state"] = array(
    "title" => "State",
    "table" => "mod_state",
    "module" => "state",
    "folder" => "tools",
    "add_url" => "tools/state/add",
    "list_url" => "tools/state/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["staticpages"] = array(
    "title" => "Static Pages",
    "table" => "mod_page_settings",
    "module" => "staticpages",
    "folder" => "tools",
    "add_url" => "tools/staticpages/add",
    "list_url" => "tools/staticpages/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["supplier_form"] = array(
    "title" => "Supplier Form",
    "table" => "mod_admin",
    "module" => "supplier_form",
    "folder" => "user",
    "add_url" => "user/supplier_form/add",
    "list_url" => "user/supplier_form/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["suppliers"] = array(
    "title" => "Suppliers",
    "table" => "companies",
    "module" => "suppliers",
    "folder" => "master",
    "add_url" => "master/suppliers/add",
    "list_url" => "master/suppliers/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["systememails"] = array(
    "title" => "System Emails",
    "table" => "mod_system_email",
    "module" => "systememails",
    "folder" => "tools",
    "add_url" => "tools/systememails/add",
    "list_url" => "tools/systememails/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["unit"] = array(
    "title" => "unit",
    "table" => "unit_master",
    "module" => "unit",
    "folder" => "master",
    "add_url" => "master/unit/add",
    "list_url" => "master/unit/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);
$config["users"] = array(
    "title" => "users",
    "table" => "users",
    "module" => "users",
    "folder" => "user",
    "add_url" => "user/users/add",
    "list_url" => "user/users/index",
    "csrf_verification" => "no",
    "checksum_validation" => "no",
    "form_data_encryption" => "no",
);#####GENERATED_CONFIG_SETTINGS_END#####
