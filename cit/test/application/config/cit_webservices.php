<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array();

#####GENERATED_CONFIG_SETTINGS_START#####

$config["category_wise_product_list"] = array(
    "title" => "Category wise Product List",
    "folder" => "master",
    "method" => "GET",
    "params" => array(
        "category_id2"
    )
);
$config["change_password"] = array(
    "title" => "Change Password",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id",
        "new_password",
        "old_password"
    )
);
$config["change_password_new"] = array(
    "title" => "Change Password New",
    "folder" => "master",
    "method" => "POST",
    "params" => array(
        "admin_id",
        "device_token",
        "new_password",
        "old_password",
        "reapeat_password"
    )
);
$config["change_product_price"] = array(
    "title" => "Change Product Price",
    "folder" => "master",
    "method" => "POST",
    "params" => array(
        "price",
        "product_id"
    )
);
$config["country_list"] = array(
    "title" => "Country List",
    "folder" => "tools",
    "method" => "GET_POST",
    "params" => array(
    )
);
$config["country_with_states"] = array(
    "title" => "Country With States",
    "folder" => "tools",
    "method" => "GET_POST",
    "params" => array(
        "country_id"
    )
);
$config["customer_add"] = array(
    "title" => "Customer Add",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "email",
        "first_name",
        "last_name",
        "password",
        "profile_image",
        "username"
    )
);
$config["customer_detail"] = array(
    "title" => "Customer Detail",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id"
    )
);
$config["customer_login"] = array(
    "title" => "Customer Login",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "password",
        "username"
    )
);
$config["customer_update"] = array(
    "title" => "Customer Update",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "customer_id",
        "first_name",
        "last_name",
        "profile_image"
    )
);
$config["forgot_password"] = array(
    "title" => "Forgot Password",
    "folder" => "user",
    "method" => "GET_POST",
    "params" => array(
        "email"
    )
);
$config["get_admin"] = array(
    "title" => "Get Admin",
    "folder" => "master",
    "method" => "GET",
    "params" => array(
        "id"
    )
);
$config["get_country_list"] = array(
    "title" => "Get Country List",
    "folder" => "master",
    "method" => "GET",
    "params" => array(
        "country",
        "status"
    )
);
$config["get_product_list"] = array(
    "title" => "Get Product List",
    "folder" => "master",
    "method" => "GET",
    "params" => array(
        "name",
        "price"
    )
);
$config["set_product_list"] = array(
    "title" => "Set Product List",
    "folder" => "master",
    "method" => "POST",
    "params" => array(
        "iCategoryId",
        "vProductName"
    )
);#####GENERATED_CONFIG_SETTINGS_END#####

/* End of file cit_webservices.php */
/* Location: ./application/config/cit_webservices.php */
    
