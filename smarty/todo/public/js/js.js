$(() => {
    function main_load() {
        $.ajax({
            url: "../front/action.php",
            type: "post",
            data: { function: "get_items" },
            dataType: "json",
            success: (data) => {
                render_data(data);
                marked();
            }
        })
    }
    $("#add_item").on("click", () => {
        var item = $("#item_input").val();
        $.ajax({
            url: "../front/action.php",
            type: "post",
            data: { item: item, function: "set_item" },
            success: (data) => {
                if (data == 1) {
                    Swal.fire('Item added successfully!', '', 'success').then((result2) => {
                        $("#item_input").val("");
                        on_change();
                    })
                }
            }
        })
    })

    $("#filter select").on("change", on_change);
    function on_change(){
        var filter1 = $("#filter1").val();
        var filter2 = $("#filter2").val();
        $.ajax({
            url: "../front/action.php",
            type: "post",
            data: { filter1: filter1, filter2: filter2, function: "filter" },
            dataType: "json",
            success: (data) => {
                render_data(data);
                marked();
            }
        })
    }
    
    function render_data(data){
        var output = "";
        $.each(data, (i, item) => {
            output += `
                <ul class="list-group list-group-horizontal rounded-0 mb-2" data-id="${item["id"]}" data-status="${item["isCompleted"]}">
                    <li
                        class="list-group-item d-flex align-items-center ps-0 pe-3 py-1 rounded-0 border-0 bg-transparent">
                        <div class="form-check">
                            <input class="form-check-input me-0" type="checkbox" value=""
                                id="flexCheckChecked3" aria-label="..." />
                        </div>
                    </li>
                    <li
                        class="list-group-item px-3 py-1 d-flex align-items-center flex-grow-1 border-0 bg-transparent">
                        <p class="lead fw-normal mb-0" ` + (item["isCompleted"] == 1 ? `style="text-decoration: line-through;"` : "") + `>${item["item"]}</p>
                    </li>
                    <li class="list-group-item ps-3 pe-0 py-1 rounded-0 border-0 bg-transparent">
                        <div class="text-end text-muted">
                            <a href="#!" class="text-muted" data-mdb-toggle="tooltip"
                                title="Created date">
                                <p class="small mb-0"><i class="fas fa-info-circle me-2"></i>
                                    ${item["date"]}
                                </p>
                            </a>
                        </div>
                    </li>
                </ul>`;
        })
        $("#list_data").html(output);
    }


    function marked() {
        $("#list_data input[type=checkbox]").on("change", (e) => {
            if ($(e.target).is(":checked")) {
                $(e.target).parents("ul").addClass("curr_list");
                var id = $(".curr_list").data("id");
                var status = $(".curr_list").data("status");
                if(status == 0)
                    $(".curr_list").html(`
                        <li class="d-flex align-items-center text-success">
                            <i class="material-icons">check_circle</i>
                            <b>Mark as Completed</b>
                        </li>
                    `);
                else
                    $(".curr_list").html(`
                        <li class="d-flex align-items-center text-danger">
                            <i class="material-icons">dangerous</i>
                            <b>Mark as Uncompleted</b>
                        </li>
                    `);
                $(".curr_list").fadeOut(1500, "linear", () => {
                    $.ajax({
                        url: "../front/action.php",
                        type: "post",
                        data: { id: id, status: status, function: "marked_items" },
                        success: (data) => {
                            if (data == 1) {
                                // main_load();
                                on_change();
                            }
                        }
                    })
                })
            }
        })
    }
    marked();

})