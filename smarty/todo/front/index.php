<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

// NOTE: Smarty has a capital 'S'
require_once('../smarty-lib/Smarty.class.php');
$smarty = new Smarty();

// $smarty->setTemplateDir('/home/hb/Desktop/serajWorkspace/smarty/templates/');
$smarty->setTemplateDir('../templates/');
$smarty->left_delimiter = "<%";
$smarty->right_delimiter = "%>";

// database start here
require_once('./db.php');
// get data
$items;
try{
    $query = "SELECT l.iListId AS id, l.vListItem AS item, date(l.tListTime) AS date, l.bIsCompleted AS isCompleted FROM list AS l WHERE l.bIsCompleted=0 ORDER BY tListTime DESC";
    $sql = $GLOBALS['conn']->query($query);
    $items = $sql->fetchAll(PDO::FETCH_ASSOC);
}catch(Exception $e){
    echo $e->getMessage();
}
// echo "<pre>";print_r($items);exit;
// database end here

$smarty->assign("items", $items);

$smarty->display('index.tpl');
?>