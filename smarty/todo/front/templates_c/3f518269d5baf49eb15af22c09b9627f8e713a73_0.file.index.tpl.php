<?php
/* Smarty version 4.1.0, created on 2022-03-21 18:03:05
  from '/home/hb/Desktop/serajWorkspace/smarty/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.1.0',
  'unifunc' => 'content_623870817364f5_71496280',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3f518269d5baf49eb15af22c09b9627f8e713a73' => 
    array (
      0 => '/home/hb/Desktop/serajWorkspace/smarty/templates/index.tpl',
      1 => 1647865983,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_623870817364f5_71496280 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- jquery cdn -->
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.6.0.min.js"><?php echo '</script'; ?>
>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- JavaScript Bundle with Popper -->
    <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- sweet alert -->
    <?php echo '<script'; ?>
 src="//cdn.jsdelivr.net/npm/sweetalert2@11"><?php echo '</script'; ?>
>
    <!-- custom -->
    <?php echo '<script'; ?>
 src="../public/js/js.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="../public/css/style.css">
</head>

<body>

    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col">
                    <div class="card" id="list1" style="border-radius: .75rem; background-color: #eff1f2;">
                        <div class="card-body py-4 px-4 px-md-5">

                            <!-- <p class="h1 text-center mt-3 mb-4 pb-3 text-primary">
                                <i class="fas fa-check-square me-1"></i>
                                <u>My Todo-s</u>
                            </p> -->

                            <div class="pb-2">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row align-items-center">
                                            <input type="text" class="form-control form-control-lg" id="item_input"
                                                placeholder="Add new..." required>
                                            <a href="#!" class="p-3" data-mdb-toggle="tooltip" title="Set due date">
                                                <!-- <i class="material-icons">event</i> -->
                                            </a>
                                            <div>
                                                <button type="button" id="add_item" class="btn btn-primary">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">

                            <div id="filter" class="d-flex justify-content-end align-items-center mb-4 pt-2 pb-3">
                                <p class=" mb-0 me-2 text-muted">Filter</p>
                                <select id="filter1" class="form-select">
                                    <option value="active">Active</option>
                                    <option value="completed">Completed</option>
                                    <option value="all">All</option>
                                    <!-- <option value="4">Has due date</option> -->
                                </select>
                                <p class=" mb-0 ms-4 me-2 text-muted">Sort</p>
                                <select id="filter2" class="form-select">
                                    <option value="DESC">New First</option>
                                    <option value="ASC">Old First</option>
                                </select>
                                <a href="#!" style="color: #23af89;" data-mdb-toggle="tooltip" title="Ascending"><i
                                        class="fas fa-sort-amount-down-alt ms-2"></i></a>
                            </div>

                            <div id="list_data">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'i', false, 'k');
$_smarty_tpl->tpl_vars['i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->do_else = false;
?>
                                    <ul class="list-group list-group-horizontal rounded-0 mb-2" data-id="<?php echo $_smarty_tpl->tpl_vars['i']->value["id"];?>
" data-status="<?php echo $_smarty_tpl->tpl_vars['i']->value["isCompleted"];?>
">
                                        <li
                                            class="list-group-item d-flex align-items-center ps-0 pe-3 py-1 rounded-0 border-0 bg-transparent">
                                            <div class="form-check">
                                                <input class="form-check-input me-0" type="checkbox" value=""
                                                    id="flexCheckChecked3" aria-label="..." />
                                            </div>
                                        </li>
                                        <li
                                            class="list-group-item px-3 py-1 d-flex align-items-center flex-grow-1 border-0 bg-transparent">
                                            <p class="lead fw-normal mb-0" style="<?php if ($_smarty_tpl->tpl_vars['i']->value['isCompleted'] == 1) {?>text-decoration: line-through<?php }?>"><?php echo $_smarty_tpl->tpl_vars['i']->value["item"];?>
</p>
                                        </li>
                                        <li class="list-group-item ps-3 pe-0 py-1 rounded-0 border-0 bg-transparent">
                                            <div class="text-end text-muted">
                                                <a href="#!" class="text-muted" data-mdb-toggle="tooltip"
                                                    title="Created date">
                                                    <p class="small mb-0"><i class="fas fa-info-circle me-2"></i>
                                                        <?php echo $_smarty_tpl->tpl_vars['i']->value["date"];?>

                                                    </p>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

</html><?php }
}
