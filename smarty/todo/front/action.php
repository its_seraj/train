<?php

// database start here
require_once('./db.php');
// get data
// $items;
// try{
//     $query = "SELECT l.vListItem AS item FROM list AS l ORDER BY tListTime DESC";
//     $sql = $GLOBALS['conn']->query($query);
//     $items = $sql->fetchAll(PDO::FETCH_ASSOC);
// }catch(Exception $e){
//     echo $e->getMessage();
// }
// echo "<pre>";print_r($items);exit;
// database end here

if ($_POST["function"] == "set_item") set_item();
if ($_POST["function"] == "get_items") get_items();
if ($_POST["function"] == "marked_items") marked_items();
if ($_POST["function"] == "filter") filter();

function set_item()
{
    if($_POST["item"] == "") return false;
    try {
        $query = "INSERT INTO list(vListItem) VALUES('{$_POST["item"]}')";
        $sql = $GLOBALS['conn']->query($query);
        echo 1;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function get_items(){
    try{
        $query = "SELECT l.iListId AS id, l.vListItem AS item, date(l.tListTime) AS date, l.bIsCompleted AS isCompleted FROM list AS l WHERE l.bIsCompleted=0 ORDER BY tListTime DESC";
        $sql = $GLOBALS['conn']->query($query);
        echo json_encode($sql->fetchAll(PDO::FETCH_ASSOC));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function marked_items(){
    $id = $_POST["id"];
    $status = $_POST["status"] == 0 ? 1 : 0;
    try{
        $query = "UPDATE list SET bIsCompleted=$status WHERE iListId=$id";
        $sql = $GLOBALS['conn']->query($query);
        echo 1;
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function filter(){
    $filter1 = $_POST["filter1"];
    $filter2 = $_POST["filter2"];

    $query = "";
    if($filter1 == "active")
        $query = "SELECT l.iListId AS id, l.vListItem AS item, date(l.tListTime) AS date, l.bIsCompleted AS isCompleted FROM list AS l WHERE l.bIsCompleted=0 ORDER BY tListTime $filter2";
    elseif($filter1 == "completed")
        $query = "SELECT l.iListId AS id, l.vListItem AS item, date(l.tListTime) AS date, l.bIsCompleted AS isCompleted FROM list AS l WHERE l.bIsCompleted=1 ORDER BY tListTime $filter2";
    else
        $query = "SELECT l.iListId AS id, l.vListItem AS item, date(l.tListTime) AS date, l.bIsCompleted AS isCompleted FROM list AS l ORDER BY tListTime $filter2";

    try{
        $sql = $GLOBALS['conn']->query($query);
        echo json_encode($sql->fetchAll(PDO::FETCH_ASSOC));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}