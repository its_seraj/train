<?php

echo "<pre>";

$list = array(
    [
        "car_name" => "Mazda",
        "code" => "M",
        "buyer" => "JamesT",
        "buy_quantity" => 4
    ],
    [
        "car_name" => "Mazda",
        "code" => "M",
        "buyer" => "steveF",
        "buy_quantity" => 1
    ],
    [
        "car_name" => "Mazda",
        "code" => "M",
        "buyer" => "KirkL",
        "buy_quantity" => 4
    ],
    [
        "car_name" => "Toyota",
        "code" => "U",
        "buyer" => "JamesT",
        "buy_quantity" => 2
    ],
    [
        "car_name" => "Toyota",
        "code" => "U",
        "buyer" => "steveF",
        "buy_quantity" => 0
    ],
    [
        "car_name" => "steveF",
        "code" => "U",
        "buyer" => "KirkL",
        "buy_quantity" => -2
    ],
    [
        "car_name" => "Tesla",
        "code" => "T",
        "buyer" => "Abhi",
        "buy_quantity" => 2
    ],
    [
        "car_name" => "Tesla",
        "code" => "T",
        "buyer" => "Amamn",
        "buy_quantity" => 2
    ],
    [
        "car_name" => "Lambo",
        "code" => "L",
        "buyer" => "Charlotte",
        "buy_quantity" => 2
    ],
    [
        "car_name" => "Lambo",
        "code" => "L",
        "buyer" => "Nikk",
        "buy_quantity" => 1
    ],
    [
        "car_name" => "Lambo",
        "code" => "L",
        "buyer" => "Rami",
        "buy_quantity" => 0
    ]
);

// print_r($list);

/** car's code store in different array  */
$code_list = array();
$output = array();

foreach ($list as $row) {
    if(!array_search($row["code"], array_column($output, "code"))){
    // if(!in_array($row["code"], $code_list)){
        array_push($code_list, $row["code"]);

        array_push($output, array(
            "car_name" => $row["car_name"],
            "code" => $row["code"],
            "buy_quantity" => $row["buy_quantity"]
        ));
    }else{
        $key = array_search($row["code"], array_column($output, "code"));

        // sum buy_quantity
        $output[$key]["buy_quantity"] += $row["buy_quantity"];
    }

}
print_r($output);
