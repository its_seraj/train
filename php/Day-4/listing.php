<?php

// session verification
session_start();
if (!isset($_SESSION["name"])) header("Location: login.php");

include("./data.php");
$newData = array_chunk($data, 10, true);
// echo "<pre>"; print_r($newData);
$totalPage = count($newData);
// Assign Deafult page 1
$_GET["page"] = ($_GET["page"] < 1 || !isset($_GET["page"]) || $_GET["page"] > $totalPage) ? 1 : $_GET["page"];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>
    <!-- jquery -->
    <script src="../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <!-- Google icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        a {
            padding: 10px 1em;
            text-decoration: none;
        }

        a.active {
            background-color: #668cff;
            color: #fff;
        }
        .sort-btn{margin-right: 1em;}
        .logout{position: absolute; bottom: 10px; right: 10px; padding: 1em; padding-bottom: 10px; border-radius: 50%; cursor: pointer;}
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <?php echo $_SESSION["name"]; ?>
                    </a>
                </li>
            </ul>
        </div>

        <form onsubmit="return false" class="form-inline">
            <button class="btn sort-btn">
                    <span class="badge badge-primary">Sort</span>
            </button>
            <input class="form-control mr-sm-2" type="text" id="searchBox" placeholder="Search">
            <button onclick="search()" class="btn btn-light">Search</button>
        </form>
    </nav>
    <div class="container">
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $currPage = $_GET["page"] - 1;
                    foreach ($newData[$currPage] as $key => $user) {
                        echo "
                            <tr>
                                <td>$key</td>
                                <td>{$user['first_name']}</td>
                                <td>{$user['last_name']}</td>
                                <td>{$user['mobile']}</td>
                                <td>{$user['email']}</td>
                            </tr>";
                    }

                    ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php
                for ($i = 1; $i <= $totalPage; $i++) {
                    echo '<a href="listing.php?page=' . $i . '">' . $i . '</a>';
                }
                ?>
            </div>
        </div>
        <button onclick="logout()" class="logout btn btn-outline-primary">
            <i class="material-icons">power_settings_new</i>
            </button>
    </div>
    <script>
        var pages = document.querySelectorAll(".pagination a");
        pages.forEach((e) => {
            e.classList.remove("active");
        })
        pages.forEach((e) => {
            e.addEventListener("click", (e2) => {
                e2.classList.add("active");
            });
        });
        // Logout
        function logout(){
            location.href = "destroy.php";
        }
        // searching
        function search(){
            var key = document.querySelector("#searchBox").value;
            
            $.ajax({
                url: "searching.php",
                data: {key : key},
                success: function(data){
                    console.log(data);
                }
            })
        }
    </script>
</body>

</html>