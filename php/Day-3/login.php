<?php

$email_error = "";
$pass_error = "";
$isValidate = false;
if (isset($_POST["email"]) && isset($_POST["pass"])) {
    if ($_POST["email"] == "") $email_error = "Enter email";
    else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) $email_error = "Enter valid email";
    else $isValidate = true;

    if ($_POST["pass"] == "") $pass_error = "Enter password";
    else $isValidate = true;
}

if ($isValidate) {
    $data = array(
        ["email" => "seraj@gmail.com", "pass" => "ser"],
        ["email" => "aman@gmail.com", "pass" => "aman"],
        ["email" => "prashant@gmail.com", "pass" => "prashant"],
        ["email" => "kshitij@gmail.com", "pass" => "kshitij"]
    );
    foreach ($data as $user) {
        if ($_POST["email"] == $user["email"] && $_POST["pass"] == $user["pass"]) {
            header("Location: listing.php");
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LogIn</title>
    <script src="../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <style>
        *{
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    font-family: 'Lato', sans-serif;
}
        body {
            height: 100vh;
            width: 100vw;
            overflow: hidden;
        }

        .container {
            max-width: 780px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form {
            padding: 5% 10%;
            min-width: 60%;
            border-radius: 8px;
            box-shadow: 0 0 12px 0 #ddd;
        }

        form .head {
            font-size: 1.2rem;
            font-weight: bold;
            color: #007bff;
        }
        .error{
            color: red;
            font-size: 0.8em;
        }
    </style>
</head>

<body>
    <div class="container">
        <form method="post" action="">
            <p class="head">LogIn Form</p>
            <div class="form-group">
                <label for=""></label>
                <input type="text" class="form-control" name="email" id="tel" aria-describedby="helpId" placeholder="Enter Email">
                <div class="error"><?php echo $email_error; ?></div>
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="pass" id="pass" placeholder="Enter Password">
                <div class="error"><?php echo $pass_error; ?></div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>

</html>