<?php

include("./data.php");
$newData = array_chunk($data, 10, true);
// echo "<pre>"; print_r($newData);
$totalPage = count($newData);
// Assign Deafult page 1
$_GET["page"] = ($_GET["page"] < 1 || !isset($_GET["page"] ) || $_GET["page"] > $totalPage )? 1 : $_GET["page"];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <style>
        a {
            padding: 10px 1em;
            text-decoration: none;
        }

        a.active {
            background-color: #668cff;
            color: #fff;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $currPage = $_GET["page"] - 1;
                        foreach ($newData[$currPage] as $key => $user) {
                            echo "
                                <tr>
                                    <td>$key</td>
                                    <td>{$user['first_name']}</td>
                                    <td>{$user['last_name']}</td>
                                    <td>{$user['mobile']}</td>
                                    <td>{$user['email']}</td>
                                </tr>";
                        }

                    ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php
                    for ($i = 1; $i <= $totalPage; $i++) {
                        echo '<a href="listing.php?page=' . $i . '">' .$i. '</a>';
                    }
                ?>
            </div>
        </div>
    </div>
    <script>
        var pages = document.querySelectorAll(".pagination a");
        pages.
        pages.forEach((e) => {
            e.addEventListener("click", () => {
                this.addClass("active");
            });
        });
    </script>
</body>

</html>