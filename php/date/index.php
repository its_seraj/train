<?php

echo "<code>get system timezome : </code>";
echo date_default_timezone_get();
echo "<br>";
echo "<br>";

echo "<code>system date time : </code>";
echo date("Y M d H:i:s");
echo "<br>";
echo "<br>";

date_default_timezone_set("UTC");

echo "<code>UTC date time : </code>";
echo date("Y M d H:i:s");
echo "<br>";
echo "<br>";

echo "<code>current time in seconds : </code>";
$today = time();
echo $today;
echo "<br>";
echo "<br>";

echo "<code>make time and set it to date object : </code>";
$mk = mktime(0,0,0,02,04,2020);
echo date("Y M d H:i:s", $mk);
echo "<br>";
echo "<br>";

echo "<code>string to date time : </code>";
$dt = strtotime("02 april 2010");
echo date("Y M d H:i:s", $dt);

echo "<br>-------------------------------------------------------------------<br>";
echo "<br>------------Date Question by Abhishek.Sr----------------<br>";
// date duration interval
echo '
<form action="index.php" method="get">
    <input type="date" name="input_date" id="input_date">
    <input type="number" name="duration" id="" placeholder="Duration">
    <input type="number" name="interval" id="" placeholder="Interval">
    <input type="submit" value="Submit">
</form>
';
if(isset($_GET)){
    $date = strtotime($_GET["input_date"]);
    for($i = $_GET["duration"]; $i > 0; $i--){
        echo date("d F Y",$date)."<br>";
        $date = strtotime("+{$_GET['interval']} day", $date);
    }
}

echo "<br>-------------------------------------------------------------------<br>";
echo '
<form action="index.php" method="post">
    <input type="text" name="input_date" id="input_date" placeholder="Enter Date">
    <input type="text" name="format" id="format" placeholder="Format">
    <input type="submit" value="Submit">
</form>
';
if(isset($_POST)){
    $date = date_create_from_format("d m Y", date("d m Y", strtotime($_POST["input_date"])));
    echo date_format($date, $_POST["format"]);
}
?>