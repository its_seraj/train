-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 04, 2022 at 07:09 PM
-- Server version: 5.7.36
-- PHP Version: 7.0.33-57+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `vAdminId` varchar(64) NOT NULL,
  `vFirstName` varchar(64) NOT NULL,
  `vLastName` varchar(64) NOT NULL,
  `vEmail` varchar(64) NOT NULL,
  `vPassword` varchar(64) NOT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`vAdminId`, `vFirstName`, `vLastName`, `vEmail`, `vPassword`, `eStatus`) VALUES
('admin', 'admin', 'khan', 'aman.kumar@hiddenbrains.in', '21232f297a57a5a743894a0e4a801fc3', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `iProductId` int(11) NOT NULL,
  `vProductName` varchar(64) NOT NULL,
  `dCostPrice` double(11,2) NOT NULL,
  `dSellingPrice` double(11,2) NOT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`iProductId`, `vProductName`, `dCostPrice`, `dSellingPrice`, `eStatus`) VALUES
(1, 'trimmer', 100.00, 120.00, 'Active'),
(2, 'ball', 80.00, 100.00, 'Active'),
(6, 'Notepad', 21.00, 12.00, 'Active'),
(7, 'Notepad', 21.00, 12.00, 'Active'),
(8, 'trimmers', 342.00, 123.00, 'Active'),
(9, 'trimmers', 342.00, 123.00, 'Active'),
(14, 'Notepad', 123.00, 2.00, 'Active'),
(15, 'sd', 23.00, 1.00, 'Active'),
(16, 'balls', 21.00, 11.00, 'Active'),
(21, 'fdgfd', 32.00, 23.00, 'Active'),
(22, 'xz', 23.00, 2.00, 'Active'),
(23, 'xz', 23.00, 2.00, 'Active'),
(24, 'wqe', 21.00, 123.00, 'Active'),
(25, 'wqe', 21.00, 123.00, 'Active'),
(26, 'galaa', 324.00, 121.00, 'Active'),
(27, 'vgv', 343.00, 54.00, 'Active'),
(28, 'Notepad', 5555.00, 555.00, 'Active'),
(29, 'balls', 1111.00, 111.00, 'Active'),
(30, 'balls', 1111.00, 111.00, 'Active'),
(31, 'aaaa', 111.00, 110.00, 'Active'),
(32, 'ss', 11.00, 22.00, 'Active'),
(33, 'ss', 11.00, 22.00, 'Active'),
(34, 'ssss', 33.00, 33.00, 'Active'),
(45, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(46, 'Hoodie', 9000.00, 700.00, 'Active'),
(47, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(48, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(49, 'Hoodie', 9000.00, 700.00, 'Active'),
(50, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(51, 'balls', 23.00, 2.00, 'Active'),
(52, 'trimmers', 23.00, 23.00, 'Active'),
(53, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(54, 'Hoodie', 9000.00, 700.00, 'Active'),
(55, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(56, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(57, 'Hoodie', 9000.00, 700.00, 'Active'),
(58, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(59, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(60, 'Hoodie', 9000.00, 700.00, 'Active'),
(61, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(62, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(63, 'Hoodie', 9000.00, 700.00, 'Active'),
(64, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(65, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(66, 'Hoodie', 9000.00, 700.00, 'Active'),
(67, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(68, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(69, 'Hoodie', 9000.00, 700.00, 'Active'),
(70, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(71, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(72, 'Hoodie', 9000.00, 700.00, 'Active'),
(73, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(74, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(75, 'Hoodie', 9000.00, 700.00, 'Active'),
(76, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(77, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(78, 'Hoodie', 9000.00, 700.00, 'Active'),
(79, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(80, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(81, 'Hoodie', 9000.00, 700.00, 'Active'),
(82, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(83, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(84, 'Hoodie', 9000.00, 700.00, 'Active'),
(85, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(86, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(87, 'Hoodie', 9000.00, 700.00, 'Active'),
(88, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(89, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(90, 'Hoodie', 9000.00, 700.00, 'Active'),
(91, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(92, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(93, 'Hoodie', 9000.00, 700.00, 'Active'),
(94, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(95, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(96, 'Hoodie', 9000.00, 700.00, 'Active'),
(97, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(98, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(99, 'Hoodie', 9000.00, 700.00, 'Active'),
(100, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(101, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(102, 'Hoodie', 9000.00, 700.00, 'Active'),
(103, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(104, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(105, 'Hoodie', 9000.00, 700.00, 'Active'),
(106, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(107, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(108, 'Hoodie', 9000.00, 700.00, 'Active'),
(109, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(110, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(111, 'Hoodie', 9000.00, 700.00, 'Active'),
(112, 'Hoodie with Logo', 1200.00, 1150.00, 'Active'),
(113, 'V-Neck T-Shirt', 1000.00, 900.00, 'Active'),
(114, 'Hoodie', 9000.00, 700.00, 'Active'),
(115, 'Hoodie with Logo', 1200.00, 1150.00, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `iSliderId` int(11) NOT NULL,
  `vSliderPath` varchar(128) NOT NULL,
  `bIsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(11) NOT NULL,
  `vFirstName` varchar(64) NOT NULL,
  `vLastName` varchar(64) NOT NULL,
  `vEmail` varchar(128) NOT NULL,
  `vProfilePic` varchar(256) NOT NULL,
  `vPassword` varchar(256) NOT NULL,
  `vStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vFirstName`, `vLastName`, `vEmail`, `vProfilePic`, `vPassword`, `vStatus`) VALUES
(1, 'Seraj', 'Khan', 'seraj@mail.com', '', '12278ded4047f1e459c4a83dfa6e78b5', 1),
(4, 'seraj', 'khan', 'mdseraj.khan@hiddenbrains.in', 'mdseraj.khan@hiddenbrains.in147142.png', '9df1c16e82351bd6abc74d230391cd1e', 1),
(7, 'seraj', 'khan', 'mdseraj.khan@hiddenbrains.in1', 'mdseraj.khan@hiddenbrains.in1147142.png', '9df1c16e82351bd6abc74d230391cd1e', 1),
(8, '', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`vAdminId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`iProductId`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`iSliderId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`),
  ADD UNIQUE KEY `vEmail` (`vEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `iProductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `iSliderId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
