<?php

// import database
include("conn.php");

if($_POST["function"] == "login_form") login_form();
if($_POST["function"] == "register_account") register_account();
if($_POST["function"] == "logout") logout();
if ($_POST["function"] == "search") search();

// login verification
function login_form(){
    $data = array();
    parse_str($_POST["formData"], $data);
    
    try{
        $query = "SELECT u.vFirstName AS 'name' FROM users AS u WHERE u.vEmail = '{$data["user_id"]}' AND u.vPassword = MD5('{$data['user_password']}')";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetch(PDO::FETCH_ASSOC);
    }catch(Exception $e){
        // echo $e->getMessage();
    }

    if(!empty($result)){
        session_start();
        $_SESSION["user_id"] = $result["name"];
        $_SESSION["who"] = "user";
        echo 1;
    }
    else echo 0;

}

// register account
function register_account(){
    if(isset($_FILES) && count($_FILES) > 0){
        // echo "<pre>"; print_r($_FILES);
        // print_r(__DIR__.'/profile_pics/'.$_POST["email"].$_FILES["profile_pic"]["name"]);exit;
        $result = move_uploaded_file($_FILES['profile_pic']['tmp_name'], __DIR__.'/profile_pics/'.$_POST["email"].$_FILES["profile_pic"]["name"]);
    }
    try{
        $query = "INSERT INTO users VALUES(NULL, '{$_POST["first_name"]}', '{$_POST['last_name']}', '{$_POST["email"]}', '{$_POST["email"]}{$_FILES["profile_pic"]["name"]}', MD5('{$_POST["password"]}'), 1)";
        $sql = $GLOBALS['conn']->query($query);
        echo 1;
    }catch(Exception $e){
    }

}

// logout function
function logout(){
    session_start();
    session_destroy();
}

// search function
function search(){
    $currPage = $_POST["start"];
    // get data
    $result = '';
    try {
        $query = "SELECT p.vProductName AS product_name, p.dCostPrice AS product_price, p.dSellingPrice AS selling_price 
        FROM product AS p WHERE p.vProductName LIKE '%{$_POST['key']}%' 
        LIMIT {$currPage}, 15";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    // echo "<pre>";print_r($result);
    foreach ($result as $key => $products) {
        echo '
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22285%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20285%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17f4f7e3bf6%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17f4f7e3bf6%22%3E%3Crect%20width%3D%22285%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2298.77603912353516%22%20y%3D%2296.40000019073486%22%3EImage%20cap%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
            <div class="card-body">
                <div class="product_name">'.$products["product_name"].'</div>
                <div class="product_cost">'.$products["product_price"].'</div>
                <div class="product_sell">'.$products["selling_price"].'</div>
            </div>
        </div>
        ';
    }
}

?>