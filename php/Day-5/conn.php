<?php

define('SERVER_NAME','localhost');
define('DB','product');
define('USER_NAME','seraj');
define('PASSWORD','seraj');

$conn = null;

try 
{
    $conn = new PDO("mysql:host=".SERVER_NAME.";dbname=".DB, USER_NAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) 
{
    echo "Connection failed: " . $e->getMessage();
}

?>