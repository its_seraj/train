<?php

// session verification
session_start();
if (!isset($_SESSION["admin_name"]) && $_SESSION["who"] != "admin") header("Location: index.php");

// Assign Deafult page 1
$_GET["page"] = ($_GET["page"] < 1 || !isset($_GET["page"]) || $_GET["page"] > $totalPage) ? 1 : $_GET["page"];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>
    <!-- jquery -->
    <script src="../../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <!-- Google icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        html {
            font-size: 0.8em;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        a {
            padding: 10px 1em;
            text-decoration: none;
        }

        a.active {
            background-color: #668cff;
            color: #fff;
        }

        table th {
            text-align: center;
        }

        table td {
            padding: 0;
            box-shadow: 2px 0 2px 0 #ddd;
        }

        table td:last-child {
            text-align: center;
        }

        table i {
            padding: 0 10px;
            cursor: pointer;
        }

        .sort-btn {
            margin-right: 1em;
        }

        .abs-btn {
            position: fixed;
            bottom: 10px;
            right: 10px;
        }

        .abs-btn button {
            padding: 0.8em;
            padding-bottom: 6px;
            border-radius: 50%;
            background-color: #fff;
            cursor: pointer;
        }

        #bulk-import,
        #crousel-upload {
            margin: 0 10px;
            margin-right: 32px;
            display: flex;
            flex-direction: column;
            text-align: center;
            cursor: pointer;
            color: #fff;
            font-size: 0.6em;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand logo" href="index.php">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <?php echo $_SESSION["admin_name"]; ?>
                    </a>
                </li>
            </ul>
        </div>
        <form id='crouselForm' enctype="multipart/form-data">
            <input id='csv_import' type='file' name='crousel_input' accept=".csv" hidden />
            <div id="crousel-upload"><i class="material-icons">file_download</i><span>Crousel</span></div>
        </form>
        <div id="bulk-import"><i class="material-icons">file_download</i><span>CSV</span></div>
        <form onsubmit="return false" class="form-inline">
            <button class="btn sort-btn">
                <span class="badge badge-primary">Sort</span>
            </button>
            <input onkeyup="search()" class="form-control mr-sm-2" type="text" id="searchBox" placeholder="Search">
        </form>
    </nav>
    <div class="container">
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        <th>Product Price</th>
                        <th>Selling Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="abs-btn">
            <button onclick="location.href = 'insert-product.php'" class="btn btn-outline-primary">
                <i class="material-icons">add_box</i>
            </button>
            <button onclick="location.href='destroy.php'" class="btn btn-outline-primary">
                <i class="material-icons">power_settings_new</i>
            </button>
        </div>
    </div>
    <script>
        // var pages = document.querySelectorAll(".pagination a");
        // pages.forEach((e) => {
        //     e.classList.remove("active");
        // })
        // pages.forEach((e) => {
        //     e.addEventListener("click", (e2) => {
        //         e2.classList.add("active");
        //     });
        // });
        // Logout

        // searching
        var start = 0;
        $(() => {
            // d.animate({
            //     scrollTop: d.prop('scrollHeight')
            // }, 1000);
            $(window).on("scroll", () => {
                if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
                    start += 15;
                    // console.log(start)
                    search();
                }

            })
        })
        $(".btn-search").on("keypress", search());

        function search() {
            var key = document.querySelector("#searchBox").value;

            $.ajax({
                url: "get.php",
                type: "POST",
                data: {
                    key: key,
                    function: "search",
                    start: start
                },
                success: function(data) {
                    $(".table tbody").append(data);
                    $(".btn-update").on("click", (obj) => {
                        location.href = `update-product.php?pid=${$(obj.target).data("value")}`;
                    });
                    // delete product
                    $(".btn-delete").on("click", (obj) => {
                        $.ajax({
                            url: "get.php",
                            type: "POST",
                            data: {
                                pid: $(obj.target).data("value"),
                                function: "delete_product"
                            },
                            success: (data) => {
                                location.reload();
                            }
                        });
                    });
                }
            })
        }
        $("#crousel-upload").on("click", () => {
            $("#crousel_input").click();
        })
        $("#crouselForm").on("change", function(e) {
            var formData = new FormData();
            var files = $('#crousel_input')[0].files;
            formData.append('function', 'crousel_input');
            formData.append('uploadedfile', files[0]);
            // var myFile = $('#csv_import').prop('files');
            // if ($("#csv_import").val() !== "") {
            e.preventDefault();
            // console.log(myFile);
            $.ajax({
                url: "get.php",
                type: 'post',
                contentType: false,
                processData: false,
                cache: false,
                dataType: "json",
                data: formData,
                success: function(data) {
                    if (data["success"] == 1) {
                        $("#importForm").trigger("reset");
                        setTimeout(() => {
                            $("#bulk-import").css("color", "#fff");
                        }, 1000);
                        $("#bulk-import").css("color", "#228B22");
                    }
                }

            });
            // }
        });
    </script>
</body>

</html>