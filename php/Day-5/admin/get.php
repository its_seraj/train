<?php


// DB connection
include("../conn.php");

// calling the functions
if ($_POST["function"] == "get_list") get_list();
if ($_POST["function"] == "search") search();
if ($_POST["function"] == "update_list") update_list();
if ($_POST["function"] == "insert_list") insert_list();
if ($_POST["function"] == "delete_product") delete_product();
if ($_POST["function"] == "csv_import") csv_import();

function get_list(){
    $currPage = $_POST["start"];
    // get data
    $result = '';
    try {
        $query = "SELECT p.iProductId AS product_id, p.vProductName AS product_name, p.dCostPrice AS product_price, p.dSellingPrice AS selling_price FROM product AS p LIMIT {$currPage}, 15";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    // echo "<pre>";print_r($result);
    foreach ($result as $key => $user) {
        echo "
            <tr>
                <td>{$user['product_id']}</td>
                <td>{$user['product_name']}</td>
                <td>{$user['product_price']}</td>
                <td>{$user['selling_price']}</td>
                <td><i data-value='" . $user["product_id"] . "' class='btn-update material-icons'>edit_note</i><i data-value='" . $user["product_id"] . "' class='btn-delete material-icons'>delete</i></td>
            </tr>";
    }
}

function search(){
    $currPage = $_POST["start"];
    // get data
    $result = '';
    try {
        $query = "SELECT p.iProductId AS product_id, p.vProductName AS product_name, p.dCostPrice AS product_price, p.dSellingPrice AS selling_price 
        FROM product AS p WHERE p.vProductName LIKE '%{$_POST['key']}%' LIMIT {$currPage}, 15";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    // echo "<pre>";print_r($result);
    foreach ($result as $key => $user) {
        echo "
            <tr>
                <td>{$user['product_id']}</td>
                <td>{$user['product_name']}</td>
                <td>{$user['product_price']}</td>
                <td>{$user['selling_price']}</td>
                <td><i data-value='" . $user["product_id"] . "' class='btn-update material-icons'>edit_note</i><i data-value='" . $user["product_id"] . "' class='btn-delete material-icons'>delete</i></td>
            </tr>";
    }
}

function update_list()
{
    $data = array();
    parse_str($_POST["form_values"], $data);
    // update data
    try {
        $query = "UPDATE product SET vProductName = '{$data['product_name']}', dCostPrice = {$data['product_price']}, dSellingPrice = {$data['selling_price']}, eStatus = '{$data['status']}' WHERE iProductId = {$data['product_id']}";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function insert_list(){

    $return_arr = [];
    $return_arr["success"] = "1";
    $return_arr["message"] = "Record inserted successfully.";
    $data = array();
    parse_str($_POST["form_values"], $data);
    // Insert data
    try {
        $query = "INSERT INTO product VALUES(NULL, '{$data['product_name']}', {$data['product_price']}, {$data['selling_price']}, '{$data['status']}')";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetch(PDO::FETCH_ASSOC);
        ///

        if (empty($result)) {
            // throw error
            ///throw Exception("Something went wrong while inserting.");
        }
    } catch (Exception $e) {
        // echo $e->getMessage();
        //$return_arr["success"] = 0;
        $return_arr["message"] = $e->getMessage();
    }
    echo json_encode($return_arr);
    exit;
}

function delete_product()
{
    // Delete data
    try {
        $query = "DELETE FROM product WHERE iProductId = {$_POST['pid']}";
        $sql = $GLOBALS['conn']->query($query);
        $result = $sql->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

// Import function for csv
function csv_import()
{
    if (isset($_FILES) && count($_FILES) > 0) {
        $fh = fopen($_FILES["uploadedfile"]["tmp_name"], "r");
        while ($entry = fgetcsv($fh, 1024, ',')) {
            // Output the data in HTML format
            $return_arr = [];
            $return_arr["success"] = "1";
            $return_arr["message"] = "Record inserted successfully.";
            // Insert data
            try {
                $query = "INSERT INTO product VALUES(NULL, '{$entry[0]}', {$entry[1]}, {$entry[2]}, '{$entry[3]}')";
                $sql = $GLOBALS['conn']->query($query);
                $result = $sql->fetch(PDO::FETCH_ASSOC);
                ///

                if (empty($result)) {
                    // throw error
                    ///throw Exception("Something went wrong while inserting.");
                }
            } catch (Exception $e) {
                // echo $e->getMessage();
                //$return_arr["success"] = 0;
                // $return_arr["message"] = $e->getMessage();
            }
        }
        echo json_encode($return_arr);
    }
}
