<?php

// session verification
session_start();
if (isset($_SESSION["admin_name"]) && $_SESSION["who"] == "admin") header("Location: dashboard.php");

// Form validation
$userId_error = "";
$pass_error = "";
$isValidate = false;
if (isset($_POST["user_id"]) && isset($_POST["user_password"])) {
    if ($_POST["user_id"] == "") $userId_error = "Enter User Id";
    else $isValidate = true;

    if ($_POST["user_password"] == "") $pass_error = "Enter password";
    else $isValidate = true;
}

if ($isValidate){
    // DB connection
    include("../conn.php");

    // get data
    $result;
    try{
        $query = "SELECT a.vFirstName AS 'name' FROM admin AS a WHERE a.vAdminId = :user_id AND a.vPassword = MD5(:user_password)";
        $sql = $GLOBALS['conn']->prepare($query);
        $sql->execute([':user_id' => $_POST["user_id"],':user_password' => $_POST['user_password']]);
        $result = $sql->fetch(PDO::FETCH_ASSOC);
    }catch(Exception $e){
        echo $e->getMessage();
    }

    if(!empty($result)){
        $_SESSION["admin_name"] = $result["name"];
        $_SESSION["who"] = "admin";
        header("Location: dashboard.php");
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LogIn</title>
    <script src="../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Lato', sans-serif;
        }

        body {
            height: 100vh;
            width: 100vw;
            overflow: hidden;
            background-color: #222;
        }

        .container {
            max-width: 780px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form {
            padding: 5% 10%;
            min-width: 60%;
            border-radius: 8px;
            box-shadow: 0 0 12px 0 #666;
        }

        form .head {
            font-size: 1.2rem;
            font-weight: bold;
            color: #aaa;
        }

        .error {
            color: red;
            font-size: 0.8em;
        }
    </style>
</head>

<body>
    <div class="container">
        <form method="post" action="index.php">
            <p class="head">Admin LogIn</p>
            <div class="form-group">
                <label for=""></label>
                <input type="text" class="form-control" name="user_id" id="user_id" aria-describedby="helpId" placeholder="User Id">
                <div class="error"><?php echo $userId_error; ?></div>
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Enter Password">
                <div class="error"><?php echo $pass_error; ?></div>
            </div>
            <button type="submit" class="btn btn-primary bg-dark">Submit</button>
        </form>
    </div>
</body>

</html>