<?php

// session verification
session_start();
if (!isset($_SESSION["admin_name"]) && $_SESSION["who"] != "admin") header("Location: index.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>
    <!-- jquery -->
    <script src="../../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <!-- Google icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        a {
            padding: 10px 1em;
            text-decoration: none;
        }

        a.active {
            background-color: #668cff;
            color: #fff;
        }
        form{width: 100%;}

        table th {
            text-align: center;
        }

        table td {
            box-shadow: 2px 0 2px 0 #ddd;
        }

        table td:last-child {
            text-align: center;
        }

        table i {
            padding: 0 10px;
            cursor: pointer;
        }

        .sort-btn {
            margin-right: 1em;
        }

        .logout {
            position: absolute;
            bottom: 10px;
            right: 10px;
            padding: 1em;
            padding-bottom: 10px;
            border-radius: 50%;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand logo" href="index.php">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <?php echo $_SESSION["admin_name"]; ?>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <?php

            // DB connection
            include("../conn.php");

            // get data
            $result;
            try{
                $query = "SELECT p.iProductId AS product_id, p.vProductName AS product_name, p.dCostPrice AS product_price, p.dSellingPrice AS selling_price, p.eStatus AS status FROM product AS p WHERE p.iProductId = :pid";
                $sql = $GLOBALS['conn']->prepare($query);
                $sql->execute([':pid' => $_GET["pid"]]);
                $result = $sql->fetch(PDO::FETCH_ASSOC);
            }catch(Exception $e){
                echo $e->getMessage();
            }
            ?>
            <form id="update_list">
                <div class="form-group">
                  <label for=""></label>
                  <small id="helpId" class="text-muted">Product Id</small>
                  <input type="text" name="product_id" id="" class="form-control" placeholder="" aria-describedby="helpId" value="<?=$result["product_id"]?>" readonly>
                </div>
                <div class="form-group">
                  <label for=""></label>
                  <small id="helpId" class="text-muted">Product Name</small>
                  <input type="text" name="product_name" id="" class="form-control" placeholder="" aria-describedby="helpId" value="<?=$result["product_name"]?>">
                </div>
                <div class="form-group">
                  <label for=""></label>
                  <small id="helpId" class="text-muted">Product Price</small>
                  <input type="number" name="product_price" id="" class="form-control" placeholder="" aria-describedby="helpId" value="<?=$result["product_price"]?>">
                </div>
                <div class="form-group">
                  <label for=""></label>
                  <small id="helpId" class="text-muted">Selling Price</small>
                  <input type="number" name="selling_price" id="" class="form-control" placeholder="" aria-describedby="helpId" value="<?=$result["selling_price"]?>">
                </div>
                <div class="form-group">
                  <label for=""></label>
                  <small id="helpId" class="text-muted">Selling Price</small>
                  <select class="form-control" name="status" id="">
                      <option <?=$result["status"] == "Active" ? "selected" : ""?> value="Active">Active</option>
                      <option <?=$result["status"] == "Inactive" ? "selected" : ""?> value="Inactive">Inactive</option>
                  </select>
                </div>
                <button class="btn btn-outline-primary">Update</button>
            </form>
        </div>
        <button onclick="location.href='destroy.php'" class="logout btn btn-outline-primary">
            <i class="material-icons">power_settings_new</i>
        </button>
    </div>

    <script>
        $(() => {
            $("form#update_list").on("submit", (e) => {
                e.preventDefault();
                var formData = $(e.target).serialize();
                $.ajax({
                    url: "get.php",
                    type: "POST",
                    data: { form_values: formData, function: "update_list" },
                    success: (data) => {
                        // console.log(data);
                        location.reload();
                    }
                })

            });
        })
    </script>
</body>

</html>