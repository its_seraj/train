<?php

// session verification
session_start();
if (!isset($_SESSION["admin_name"]) && $_SESSION["who"] != "admin") header("Location: index.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing</title>
    <!-- jquery -->
    <script src="../../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <!-- Google icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style>
        html {
            font-size: 0.8em;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        a {
            padding: 10px 1em;
            text-decoration: none;
        }

        a.active {
            background-color: #668cff;
            color: #fff;
        }

        .container form {
            width: 100%;
        }

        table th {
            text-align: center;
        }

        table td {
            box-shadow: 2px 0 2px 0 #ddd;
        }

        table td:last-child {
            text-align: center;
        }

        table i {
            padding: 0 10px;
            cursor: pointer;
        }

        .sort-btn {
            margin-right: 1em;
        }

        .abs-btn {
            position: fixed;
            bottom: 10px;
            right: 10px;
        }

        .abs-btn button {
            padding: 0.8em;
            padding-bottom: 6px;
            border-radius: 50%;
            background-color: #fff;
            cursor: pointer;
        }

        .logout {
            position: absolute;
            bottom: 10px;
            right: 10px;
            padding: 1em;
            padding-bottom: 10px;
            border-radius: 50%;
            cursor: pointer;
        }

        #importForm button {
            border: none;
            outline: none;
            background-color: transparent;
        }

        #bulk-import {
            display: flex;
            flex-direction: column;
            text-align: center;
            cursor: pointer;
            color: #fff;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand logo" href="index.php">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <?php echo $_SESSION["admin_name"]; ?>
                    </a>
                </li>
            </ul>
        </div>
        <form id='importForm' enctype="multipart/form-data">
            <input id='csv_import' type='file' name='csv_import' accept=".csv" hidden />
            <div id="bulk-import"><i class="material-icons">upload_file</i><span>Bulk Import</span></div>
        </form>
    </nav>
    <div class="container">
        <div class="row">
            <?php

            // DB connection
            include("../conn.php");

            // get data
            $result;
            try {
                $query = "SELECT p.iProductId AS product_id, p.vProductName AS product_name, p.dCostPrice AS product_price, p.dSellingPrice AS selling_price, p.eStatus AS status FROM product AS p WHERE p.iProductId = :pid";
                $sql = $GLOBALS['conn']->prepare($query);
                $sql->execute([':pid' => $_GET["pid"]]);
                $result = $sql->fetch(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            ?>
            <form id="insert_list">
                <div class="form-group">
                    <label for=""></label>
                    <small id="helpId" class="text-muted">Product Id</small>
                    <input type="text" name="product_id" id="" class="form-control" placeholder="" aria-describedby="helpId" readonly value="NULL" />
                </div>
                <div class="form-group">
                    <label for=""></label>
                    <small id="helpId" class="text-muted">Product Name</small>
                    <input type="text" name="product_name" id="" class="form-control" placeholder="" aria-describedby="helpId" />
                </div>
                <div class="form-group">
                    <label for=""></label>
                    <small id="helpId" class="text-muted">Product Price</small>
                    <input type="number" name="product_price" id="" class="form-control" placeholder="" aria-describedby="helpId" />
                </div>
                <div class="form-group">
                    <label for=""></label>
                    <small id="helpId" class="text-muted">Selling Price</small>
                    <input type="number" name="selling_price" id="" class="form-control" placeholder="" aria-describedby="helpId" />
                </div>
                <div class="form-group">
                    <label for=""></label>
                    <small id="helpId" class="text-muted">Selling Price</small>
                    <select class="form-control" name="status" id="">
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>
                </div>
                <button class="btn btn-outline-primary">Insert Item</button>
            </form>
        </div>
        <button onclick="location.href='destroy.php'" class="logout btn btn-outline-primary">
            <i class="material-icons">power_settings_new</i>
        </button>
        <div class="query-success"></div>
        <div class="query-failed"></div>
    </div>

    <script>
        $(() => {
            $("form#insert_list").on("submit", (e) => {
                e.preventDefault();
                var formData = $(e.target).serialize();
                $.ajax({
                    url: "get.php",
                    type: "POST",
                    data: {
                        form_values: formData,
                        function: "insert_list"
                    },
                    dataType: "JSON",
                    success: (data) => {
                        console.log(data);
                        if (data.success == "1") {
                            $(".query-success").text("Success");
                            $(e.target).trigger("reset");
                        } else {
                            $(".query-failed").text("Failed");
                        }
                    }
                })

            });
            $("#bulk-import").on("click", () => {
                $("#csv_import").click();
            })
            $("#csv_import").on("change", function(e) {
                var formData = new FormData();
                var files = $('#csv_import')[0].files;
                formData.append('function','csv_import');
                formData.append('uploadedfile',files[0]);
                // var myFile = $('#csv_import').prop('files');
                // if ($("#csv_import").val() !== "") {
                    e.preventDefault();
                    // console.log(myFile);
                    $.ajax({
                        url: "get.php",
                        type: 'post',
                        contentType: false,
                        processData: false,
                        cache: false,
                        dataType: "json",
                        data : formData,
                        success: function(data) {
                            if(data["success"] == 1){
                                $("#importForm").trigger("reset");
                                setTimeout(() => {
                                    $("#bulk-import").css("color", "#fff");
                                }, 1000);
                                $("#bulk-import").css("color", "#228B22");
                            }
                        }
                        
                    });
                // }
            });
        })
    </script>
</body>

</html>