<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LogIn</title>
    <script src="../../jquery/jquery-3.6.0.min.js"></script>
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../JavaScript Exam/bootstrap4/css/bootstrap.min.css">
    <script src="../../JavaScript Exam/bootstrap4/js/bootstrap.min.js"></script>
    <!-- Google icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- flickity from a CDN -->
    <link href="https://cdn.jsdelivr.net/npm/flickity@latest/dist/flickity.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/flickity@2.2.1/dist/flickity.pkgd.min.js"></script>
    <!-- custom css -->
    <link rel="stylesheet" href="style.css">
    <script src="js.js"></script>
</head>
<body>
    
<?php

// session verification
session_start();
if (!isset($_SESSION["user_id"]) && $_SESSION["who"] != "user") {
    // Display Login Page
    echo '
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="index.php">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    ';
    echo '<div class="container">';
    echo '<form id="login_form" onsubmit="return false">
            <p class="head">LogIn Form
                <button class="signup-page btn float-right">
                    New <span class="badge badge-primary">SignUp</span>
                </button>
            </p>
            <div class="form-group">
                <label for=""></label>
                <input type="email" class="form-control" name="user_id" id="user_id" aria-describedby="helpId" placeholder="User Id">
                <div class="error"><?php echo $userId_error; ?></div>
            </div>
            <div class="form-group">
                <label for=""></label>
                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Enter Password">
                <div class="error"><?php echo $pass_error; ?></div>
            </div>
            <button type="submit" class="btn btn-primary">Sign In</button>
        </form>';
    echo '</div>';
}else{
    echo '
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">Listing</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_Navbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="material-icons">account_circle</i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">'.$_SESSION["user_id"].'</a>
                </li>
            </ul>
        </div>

        <form onsubmit="return false" class="form-inline">
            <input class="form-control mr-sm-2" type="text" id="searchBox" placeholder="Search">
        </form>
    </nav>
    ';

    // homepage slider
    echo '
    <div class="carousel"><!-- data-flickity="{ options here }" -->
        <div class="carousel-cell">Slide 1
            <img src="" alt="">
        </div>
        <div class="carousel-cell">Slide 2
            <img src="" alt="">
        </div>
        <div class="carousel-cell">Slide 3
            <img src="" alt="">
        </div>
    </div>
    ';

    // main container is here
    echo '
    <div class="container">
        <div class="row"></div>
    </div>
    <div class="pageUp">
        <i class="material-icons" style="color: #fff;">keyboard_arrow_up</i>
    </div>
    <div class="abs-btn">
        <button id="btn-logout" class="btn btn-outline-primary">
            <i class="material-icons">power_settings_new</i>
        </button>
    </div>
    ';

    // logout button
    echo '
    <button class="logout btn btn-outline-primary">
        <i class="material-icons">power_settings_new</i>
    </button>
    ';
}

echo '
</body>
</html>
';
?>
