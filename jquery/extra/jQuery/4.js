$(() => {
    $("button").on("click", () => {
        var row = parseInt($("#row").val());
        var col = parseInt($("#col").val());
        var output = "";
        for(var i = 0; i < row; i++){
            output += '<tr>';
            for(var j = 0; j < col; j++){
                if(i == row-1 && j == col-1) output += '<td style="background-color: ' + $("#color").val() + '"></td>';
                else output += '<td></td>';
            }
            output += '</tr>';
        }

        $("table").append(output);
    })
})