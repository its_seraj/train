$(() => {
    $("button").on("click", () => {
        $("form").html(`
            <input type="number" name="width" id="width" placeholder="Width">
            <input type="number" name="height" id="height" placeholder="Heigth">
            <input type="text" name="border" id="border" placeholder="Border">
            <input type="text" name="text" id="text" placeholder="Text">
            <input type="number" name="left" id="left" placeholder="Left">
            <input type="number" name="top" id="top" placeholder="Top">
        `);

        $("#width").on("change", (obj) => {
            $("div").css("width", $(obj.target).val() + "px");
        })
        $("#height").on("change", (obj) => {
            $("div").css("height", $(obj.target).val() + "px");
        })
        $("#border").on("change", (obj) => {
            $("div").css("border", $(obj.target).val());
        })
        $("#text").on("change", (obj) => {
            $("div").text($(obj.target).val());
        })
        $("#left").on("change", (obj) => {
            $("div").css("left", $(obj.target).val() + "px");
        })
        $("#top").on("change", (obj) => {
            $("div").css("top", $(obj.target).val() + "px");
        })
    })
})