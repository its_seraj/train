$(() => {
    function main_load() {
        $.ajax({
            url: BASE_URL + "index.php/company/getAll/0",
            dataType: "json",
            success: (data) => {
                var output = "";
                $.each(data["data"], (i, item) => {
                    output += `
                <tr>
                    <td>${item["iComapanyId"]}</td>
                    <td>${item["vCompanyName"]}</td>
                    <td>${item["vComapnyDescription"]}</td>
                    <td>
                        <span'>
                            <i class="btn-insert-modal material-icons" data-bs-toggle="modal" data-value='./update.html/${item["iComapanyId"]}' data-bs-target="#updateModal">edit</i>
                        </span>
                        <span>
                            <i id='delete-row' data-value='./delete.html/${item["iComapanyId"]}' class='material-icons'>delete</i>
                        </span>
                    </td>
                </tr>`;
                })
                $("table tbody").html(output);
                $(".container>.pagination").html(data["pagination"]);
                Swal_fire();
                add();
                update();
                search();
                pagination_links();
                upload();
                upload_multiple();
            }
        })
    }
    main_load();

    function Swal_fire() {
        $("i#delete-row").on("click", (e) => {
            Swal.fire({
                title: 'Do you want to delete the row?',
                showCancelButton: true,
                confirmButtonText: 'Confirm',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: BASE_URL + "index.php/" + $(e.target).data("value"),
                        success: (data) => {
                            Swal.fire('Company Deleted!', '', 'success').then((result2) => {
                                // location.reload();
                                // console.log($(e.target).parents());
                                $(e.target).parents("tr").hide();
                                // main_load();
                            })
                        }
                    })
                }
            })
        })
    }
    // Swal_fire();

    // modal call on click
    function update() {
        $('.btn-insert-modal').on('click', function (e) {
            $.ajax({
                url: BASE_URL + "index.php/" + $(e.target).data("value"),
                dataType: "json",
                success: (data) => {
                    $("#company_id").val(data["iComapanyId"]);
                    $("#company_name").val(data["vCompanyName"]);
                    $("#company_description").val(data["vComapnyDescription"]);
                    // $("#status").val(data["bStatus"]);
                    $('#status [value="' + data["bStatus"] + '"]').attr('selected', 'true');
                    // console.log($("#status").val())

                    $("#update_save_changes").on("click", () => {
                        $.ajax({
                            url: BASE_URL + "index.php/" + $(e.target).data("value"),
                            type: "post",
                            data: {
                                "company_id": $("#company_id").val(),
                                "company_name": $("#company_name").val(),
                                "company_description": $("#company_description").val(),
                                "company_status": $("#status").val()
                            },
                            success: (data) => {
                                // console.log($("#status option:selected").val())
                                if (data == 1) {
                                    main_load();
                                    $("[data-bs-dismiss=modal]").trigger({ type: "click" });
                                }
                            }
                        })
                    })
                    $('#updateModal').on('hidden.bs.modal', function () {
                        $("#status option").removeAttr('selected');
                    })
                }
            })
        })
    }

    // add modal
    function add() {
        $('.btn-add-modal').on('click', function (e) {
            $("#add_company_btn").on("click", () => {
                $.ajax({
                    url: BASE_URL + "index.php/add.html",
                    type: "post",
                    data: {
                        "company_name": $("#add_company_name").val(),
                        "company_description": $("#add_company_description").val(),
                        "status": $("#add_status").val()
                    },
                    success: (data) => {
                        if (data == 1) {
                            main_load();
                            $("[data-bs-dismiss=modal]").trigger({ type: "click" });
                            $("#add_modal_form").trigger("reset");
                        }
                    }
                })
            })
        })
    }

    function search() {
        $("#search-btn").on("click", () => {
            var key = $("#search_key").val();
            var status = $("#status").val();
            var sort = $("#sort").val();
            console.log(key + " : " + status + " : " + sort);
            $.ajax({
                url: BASE_URL + "index.php/search.html/" + key + "/" + status + "/" + sort,
                dataType: "json",
                success: (data) => {
                    var output = "";
                    $.each(data, (i, item) => {
                        output += `
                        <tr>
                            <td>${item["iComapanyId"]}</td>
                            <td>${item["vCompanyName"]}</td>
                            <td>${item["vComapnyDescription"]}</td>
                            <td>
                                <span'>
                                    <i class="btn-insert-modal material-icons" data-bs-toggle="modal" data-value='./update.html/${item["iComapanyId"]}' data-bs-target="#updateModal">edit</i>
                                </span>
                                <span>
                                    <i id='delete-row' data-value='./delete.html/${item["iComapanyId"]}' class='material-icons'>delete</i>
                                </span>
                            </td>
                        </tr>`;
                    })
                    $("table tbody").html(output);
                    Swal_fire();
                    add();
                    update();
                }
            })
        })
    }

    // pagination script
    function pagination_links() {
        $(".pagination a").on("click", (e) => {
            e.preventDefault();
            $.ajax({
                url: $(e.target).attr("href"),
                dataType: "json",
                success: (data) => {
                    var output = "";
                    $.each(data["data"], (i, item) => {
                        output += `
                    <tr>
                        <td>${item["iComapanyId"]}</td>
                        <td>${item["vCompanyName"]}</td>
                        <td>${item["vComapnyDescription"]}</td>
                        <td>
                            <span'>
                                <i class="btn-insert-modal material-icons" data-bs-toggle="modal" data-value='./update.html/${item["iComapanyId"]}' data-bs-target="#updateModal">edit</i>
                            </span>
                            <span>
                                <i id='delete-row' data-value='./delete.html/${item["iComapanyId"]}' class='material-icons'>delete</i>
                            </span>
                        </td>
                    </tr>`;
                    })
                    $("table tbody").html(output);
                    $(".container>.pagination").html(data["pagination"]);
                    Swal_fire();
                    update();
                    search();
                    pagination_links();
                }
            })
        })
    }

    // upload image
    function upload() {
        $("form#upload_modal_form").on("submit", (e) => {
            e.preventDefault();

            var form_data = new FormData();
            form_data.append('user_image', $('#user_image')[0].files[0]);

            $.ajax({
                url: BASE_URL + "index.php/upload",
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: (data) => {
                    $("[data-bs-dismiss=modal]").trigger({ type: "click" });
                    $("#upload_modal_form").trigger("reset");
                    Swal.fire(
                        'File Uploaded successfully!',
                        '',
                        'success'
                    )
                }
            })
        })
    }

    function upload_multiple() {
        $("form#upload_multiple").on("submit", (e) => {
            e.preventDefault();

            var form_data = new FormData();
            var files = $('#user_images')[0].files;
            $.each(files, (i, key) => {
                form_data.append('user_image' + i, key);
            })

            $.ajax({
                url: BASE_URL + "index.php/uploads",
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: (data) => {
                    $("[data-bs-dismiss=modal]").trigger({ type: "click" });
                    $("#upload_multiple").trigger("reset");
                    Swal.fire(
                        'File Uploaded successfully!',
                        '',
                        'success'
                    )
                }
            })
        })
    }

    // login verify
    function login() {
        $("form#login_form").on("submit", (e) => {
            e.preventDefault();
            $.ajax({
                url: BASE_URL + "index.php/login_verify",
                type: "post",
                data: { "email": $("#user_email").val(), "password": $("#user_password").val() },
                success: (data) => {
                    if (data == "success") {
                        Swal.fire(
                            'Log in successfully!',
                            '',
                            'success'
                        ).then((result) => {
                            location.reload();
                        });
                    }
                    else location.reload();
                }
            })
        })
    }
    login();

    // signup
    function signup() {
        $("form#user_register").on("submit", (e) => {
            e.preventDefault();
            var formData = new FormData($(e.target)[0]);
            $.ajax({
                url: BASE_URL + "index.php/signup_verify",
                type: "POST",
                enctype: 'multipart/form-data',
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                success: (data) => {
                    if (data == "success") {
                        Swal.fire(
                            'Account Created successfully!',
                            '',
                            'success'
                        ).then((result) => {
                            location.href = BASE_URL + "index.php";
                        });
                    }
                    else location.reload();
                }
            })
        })
    }
    signup();

})