<?php
defined('BASEPATH') or exit("Not found");

class Company extends CI_Controller
{

    public function index()
    {
        if (empty($this->session->userdata('user_email'))) $this->login();
        else {
            $data['template'] = array(
                'company/header',
                'company/nav',
                'company/View_Company_list',
                'company/insert_modal',
                'company/add_modal',
                'company/upload_modal',
                'company/footer'
            );
            $this->load->view('template/main_template', $data);
        }
    }

    // display login page
    public function login()
    {
        if (!empty($this->session->userdata('user_email'))) $this->index();
        else {
            $data['template'] = array(
                'company/header',
                'user/login'
            );
            $this->load->view('template/main_template', $data);
        }
    }

    public function login_verify()
    {
        if (!empty($this->input->post())) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array(
                    'errors' => validation_errors()
                );
                $this->session->set_flashdata($data);
            } else {
                $this->load->model('Model_User');
                // $result = $this->Model_User->login_verify();
                $user = $this->Model_User->login_verify($this->input->post());


                if (!empty($user)) {
                    $user_data = array(
                        'user_email' => $user->email,
                        'user_image' => $user->image
                    );
                    // print_r($user_data);exit;

                    $this->session->set_userdata($user_data);

                    echo "success";
                } else {
                    $this->session->set_flashdata('errors', 'Invalid credentials!');
                }
            }
        }
    }

    // signup controller
    public function signup(){
        if (!empty($this->session->userdata('user_email'))) $this->index();
        else {
            $data['template'] = array(
                'company/header',
                'user/signup'
            );
            $this->load->view('template/main_template', $data);
        }
    }
    public function signup_verify(){
        // print_r($_FILES);exit;
        // upload to the folder
        $this->load->library("uploads");
        $this->uploads->upload_single($_FILES, 'public/upload/images/');
        if (!empty($this->input->post())) {

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array(
                    'errors' => validation_errors()
                );
                $this->session->set_flashdata($data);
            } else {
                $this->load->model('Model_User');
                // append image name in
                $_POST["image_name"] = time()."-".$_FILES["user_image"]["name"];
                $user = $this->Model_User->signup_verify($this->input->post());

                if (!empty($user)) {
                    echo "success";
                } else {
                    $this->session->set_flashdata('errors', 'Invalid credentials!');
                }
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_email');
        // $this->session->sess_destroy();
        $this->session->set_flashdata('message', 'Successfully! Logged Out!');
        redirect(BASE_URL . 'index.php');
    }

    // get all data for for ajax call
    public function getAll($offset)
    {
        $this->load->model('Model_Company');
        $result = $this->Model_Company->getCompanies($offset);

        // Pagination using ci
        $config['base_url'] = BASE_URL . "index.php/page";
        $config['total_rows'] = $this->Model_Company->get_count();
        $config['per_page'] = 5;

        // apply bootstrap on ci pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tagl_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tagl_close'] = '</li>';
        $config['first_tag_open'] = '<li class="page-item disabled">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tagl_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'page-link');

        $this->pagination->initialize($config);

        $pagination_html = $this->pagination->create_links();
        $result = ['data' => $result, 'pagination' => $pagination_html];
        echo json_encode($result);
    }

    // get all data for ajax search
    public function search($key, $status, $sort)
    {
        $this->load->model('Model_Company');
        $result = $this->Model_Company->getSearch(
            array(
                "key" => $key,
                "status" => $status,
                "sort" => $sort
            )
        );
        echo json_encode($result);
    }

    // add company using ajax
    public function add_company()
    {
        if (!empty($this->input->post())) {
            $this->load->model('Model_Company');
            $result = $this->Model_Company->insertCompany($this->input->post());
            echo 1;
        }
    }

    // update company using ajax
    public function update($id)
    {
        if (!empty($this->input->post())) {
            $this->load->model('Model_Company');
            $result = $this->Model_Company->updateCompany($_POST);
            // redirect(BASE_URL."index.php/");
            echo 1;
        } else {
            $this->load->model('Model_Company');
            $result = $this->Model_Company->getCompany($id);
            // $this->load->view('company/View_update', array("data" => $result));
            echo json_encode($result);
        }
    }

    // upload image
    public function upload()
    {
        $this->load->library("uploads");
        $this->uploads->upload_single($_FILES, 'public/upload/images/');
    }
    // multiple file upload
    public function uploads()
    {
        $this->load->library("uploads");
        $this->uploads->upload_multiple($_FILES, 'public/upload/images/');
    }

    // delete company using ajax
    public function delete($id)
    {
        $this->load->model('Model_Company');
        $result = $this->Model_Company->deleteCompany($id);
        echo $id;
    }
}
