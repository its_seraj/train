<div class="container h-100">
    <div class="row justify-content-sm-center h-100">
        <div class="col-xxl-6 col-xl-5 col-lg-5 col-md-7 col-sm-9">
            <!-- flash error data -->
            <?php
            if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-success alert-dismissible d-flex align-items-center fade show" role="alert">
                    <i class="material-icons">check_circle_outline</i>
                    <?php echo $this->session->flashdata('message'); ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                </div>
            <?php } ?>
            <?php
            if ($this->session->flashdata('errors')) { ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <i class="material-icons">error</i>
                    <?php echo $this->session->flashdata('errors'); ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                </div>
            <?php } ?>
            <div class="card shadow-lg">
                <div class="card-body p-5">
                    <h1 class="fs-4 card-title fw-bold">User Login</h1>
                    <form id="login_form" class="needs-validation">
                        <div class="form-group">
                            <label class="mb-2 text-muted" for="user_email">User Email</label>
                            <input type="text" class="form-control" name="user_email" id="user_email" aria-describedby="helpId">
                            <div class="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="mb-2 text-muted" for="user_password">User Password</label>
                            <input type="password" class="form-control" name="user_password" id="user_password">
                            <div class="error"></div>
                        </div>
                        <button type="submit" class="btn btn-primary bg-dark">Submit</button>
                    </form>
                </div>
                <div class="card-footer py-3 border-0">
                    <div class="text-center">
                        Don't have an account? <a href="<?=BASE_URL?>index.php/signup" class="text-dark">Create One</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var BASE_URL = "<?= BASE_URL ?>";
</script>
<script src="<?= BASE_URL ?>public/js/js.js"></script>