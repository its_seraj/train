<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Company</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="add_modal_form">
                    <input type="text" class="form-control" style="margin: 10px 0;" name="add_company_name" id="add_company_name">
                    <input type="text" class="form-control" style="margin: 10px 0;" name="add_company_description" id="add_company_description">
                    <select id="add_status" name="status" class="form-select">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="add_company_btn" class="btn btn-primary">Add Company</button>
            </div>
        </div>
    </div>
</div>