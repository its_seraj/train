<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="upload_modal_form" enctype="multipart/form-data">
                    <div class="mb-3">
                      <label for="user_image" class="form-label"></label>
                      <input class="form-control" type="file" name="user_image" id="user_image">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" form="upload_modal_form" class="btn btn-primary">Upload</button>
            </div>
        </div>
    </div>
</div>

<!-- -----------------Multiple image upload-------------------- -->

<div class="modal fade" id="uploadsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="upload_multiple" enctype="multipart/form-data">
                    <div class="mb-3">
                      <label for="user_image" class="form-label"></label>
                      <input class="form-control" type="file" name="user_images[]" id="user_images" multiple>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" form="upload_multiple" class="btn btn-primary">Upload</button>
            </div>
        </div>
    </div>
</div>