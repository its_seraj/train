<!-- Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Company</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="update_form">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="company_id" id="company_id" aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="company_name">Company Name</label>
                        <input type="text" class="form-control" name="company_name" id="company_name" aria-describedby="helpId">
                        <div class="error"></div>
                    </div>
                    <div class="form-group">
                        <label for="company_description">Description</label>
                        <input type="text" class="form-control" name="company_description" id="company_description">
                        <div class="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Status
                            <select id="status" name="status" class="form-select">
                                <option for="status" value="1">Active</option>
                                <option for="status" value="0">Inactive</option>
                            </select>
                        </label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="update_save_changes" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>