<div class="container">
    <div class="table-title">
        <div class="row">
            <div class="col-sm-6">
                <h2>Manage <b>Companies</b></h2>
            </div>
            <div class="col-sm-6">
                <div class="btn-upload-modal btn btn-success" data-bs-toggle="modal" data-bs-target="#uploadsModal"><i class="material-icons">&#xE147;</i> <span>Upload Multiple Image</span></div>
                <div class="btn-upload-modal btn btn-success" data-bs-toggle="modal" data-bs-target="#uploadModal"><i class="material-icons">&#xE147;</i> <span>Upload Image</span></div>
                <div class="btn-add-modal btn btn-success" data-bs-toggle="modal" data-bs-target="#addModal"><i class="material-icons">&#xE147;</i> <span>Add New Company</span></div>
            </div>
        </div>
    </div>
    <div class="table-title" style="margin-top: 10px; ">
        <div class="row">
            <div class="col-sm-2">
                <h2>Filter : </h2>
            </div>
            <div class="col-sm-3">
                <label for="search_key" class="form-label">Product Name
                    <input type="text" class="form-control" id="search_key" aria-describedby="">
                </label>
            </div>
            <div class="col-sm-2">
                <label for="status" class="form-label">Status
                    <select id="status" class="form-select" aria-label="">
                        <option selected value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </label>
            </div>
            <div class="col-sm-3">
                <label for="sort" class="form-label">Sort By
                    <select id="sort" class="form-select" aria-label="">
                        <option selected value="asc">Company Name ASC</option>
                        <option value="desc">Company Name DESC</option>
                    </select>
                </label>
            </div>
            <div class="col-sm-2">
                <div class="col-sm-12 btn btn-success p-2" id="search-btn">Search</div>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Company ID</th>
                <th>Company Name</th>
                <th>Company Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div class="pagination"></div>
</div>


<script>
    var BASE_URL = "<?= BASE_URL ?>";
    console.log("<?= $this->uri->segment(1) ?>");
</script>
<script src="<?= BASE_URL ?>public/js/js.js"></script>