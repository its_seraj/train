<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?= BASE_URL ?>index.php/">My Dashboard</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= BASE_URL ?>index.php/">Home</a>
            </li>
            <li class="nav-item avatar">
                <a class="nav-link p-0" href="#">
                    <img src="<?= $this->session->userdata("image"); ?>" class="rounded-circle z-depth-0" alt="avatar image" height="35">
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL ?>index.php/logout">LogOut</a>
            </li>
        </ul>
    </div>
</nav>