<?php
defined('BASEPATH') or exit("Not Found");
class Model_Company extends CI_Model{
    public function __construct(){
        // parent::__construct();
    }
    public function getCompanies($offset){
        return $this->db->select("*")->limit(5, $offset)->get("companies")->result_array();
    }
    public function get_count() {
        return $this->db->count_all_results("companies");
    }

    // get company details using companyID
    public function getCompany($id){
        return $this->db->select("*")->where('iComapanyId', $id)->get("companies")->row_array();
    }

    public function getSearch($data){
        return $this->db->select("*")
                ->group_start()
                    ->like('vCompanyName', $data['key'])
                    ->where('bStatus', $data['status'])
                    ->order_by('vCompanyName', $data['sort'])
                ->group_end()
                    ->get("companies")->result_array();
    }

    public function insertCompany($data){
        $data = array(
            "vCompanyName" => $data["company_name"],
            "vComapnyDescription" => $data["company_description"],
            "bStatus" => $data["status"]
        );
        $this->db->insert('companies', $data);
        return $this->db->last_query();
    }

    public function updateCompany($data){
        $this->db->where('iComapanyId', $data["company_id"])
                ->update('companies', array(
                    "vCompanyName" => $data["company_name"], 
                    "vComapnyDescription" => $data["company_description"],
                    "bStatus" => $data["company_status"]
                ));
        return $this->db->last_query();
    }

    public function deleteCompany($id){
        $this->db->where("iComapanyId", $id)->delete('companies');
        return $this->db->last_query();
    }
}



?>