<?php
defined('BASEPATH') or exit("Not Found");
class Model_User extends CI_Model{
    public function __construct(){
        // parent::__construct();
    }

    public function login_verify($data){
        return $this->db->select("vUserEmail as email, vProfilePic as image")
                    ->where("vUserEmail", $data["email"])
                    ->where("vUserPassword", md5($data["password"]))
                    ->get("users")->row();
    }

    // signup form
    public function signup_verify($data){
        $data = array(
            "vUserName" => $data["name"],
            "vUserEmail" => $data["email"],
            "vUserPassword" => md5($data["password"]),
            "vProfilePic" => $data["image_name"]
        );
        $this->db->insert('users', $data);
        return $this->db->last_query();
    }
}

?>