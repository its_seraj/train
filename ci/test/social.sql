-- phpMyAdmin SQL Dump
-- version 4.9.10
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2022 at 05:13 PM
-- Server version: 5.7.36
-- PHP Version: 7.0.33-57+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `iFid` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL,
  `iFriendId` int(11) NOT NULL,
  `bIsFriend` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`iFid`, `iUserId`, `iFriendId`, `bIsFriend`) VALUES
(1, 1, 2, 0),
(2, 2, 1, 1),
(3, 1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `iPostId` int(11) NOT NULL,
  `iUserId` int(11) NOT NULL,
  `vPostPic` varchar(128) NOT NULL,
  `tPostTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vPostDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`iPostId`, `iUserId`, `vPostPic`, `tPostTime`, `vPostDesc`) VALUES
(1, 1, '1648030996-download.jpeg', '2022-03-23 10:23:16', 'Member and a method named signup. Here’s what your class might look like'),
(2, 2, '1648031303-download (1).jpeg', '2022-03-23 10:28:23', 'In your validation config file, you will name your rule group member/signup:'),
(3, 2, '1648031392-images.jpeg', '2022-03-23 10:29:52', 'The Form Validation class supports the use of arrays as field names. Consider this example'),
(4, 4, '1648031397-images.jpeg', '2022-03-23 10:29:57', ''),
(5, 3, '1648031402-images.jpeg', '2022-03-23 10:30:02', ''),
(6, 3, '1648031517-images (1).jpeg', '2022-03-23 10:31:57', 'If you do use an array as a field name, you must use the EXACT array name in the Helper Functions'),
(7, 1, '1648046390-images (1).jpeg', '2022-03-23 14:39:50', 'fdhjsfkdnksaj'),
(8, 1, '1648047019-images (4).jpeg', '2022-03-23 14:50:19', 'sad'),
(9, 2, '1648107688-images.jpeg', '2022-03-24 07:41:28', 'hello world'),
(10, 3, '1648108464-images.jpeg', '2022-03-24 07:54:24', 'Tuesday');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iUserId` int(11) NOT NULL,
  `vUserName` varchar(64) NOT NULL,
  `vUserEmail` varchar(64) NOT NULL,
  `vPassword` varchar(64) NOT NULL,
  `vProfilePic` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iUserId`, `vUserName`, `vUserEmail`, `vPassword`, `vProfilePic`) VALUES
(1, 'Seraj Khan', 'seraj@gmail.com', '12278ded4047f1e459c4a83dfa6e78b5', 'https://cdn.pixabay.com/photo/2018/08/28/12/41/avatar-3637425__340.png'),
(2, 'Aman Kumr', 'Aman@kr.com', 'ccda1683d8c97f8f2dff2ea7d649b42c', 'https://cdn3.iconfinder.com/data/icons/office-flat-3/512/office__business__avtar_-512.png'),
(3, 'Kshitij', 'Kshitij@kr.com', '517d7a57bd7e6c167fab9cb519ce5849', 'https://assets.materialup.com/uploads/b78ca002-cd6c-4f84-befb-c09dd9261025/preview.png'),
(4, 'Abhishek', 'Abhishek@kr.com', 'f589a6959f3e04037eb2b3eb0ff726ac', 'https://assets.materialup.com/uploads/b78ca002-cd6c-4f84-befb-c09dd9261025/preview.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`iFid`),
  ADD KEY `iUserId` (`iUserId`),
  ADD KEY `iFriendId` (`iFriendId`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`iPostId`),
  ADD KEY `iUserId` (`iUserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iUserId`),
  ADD KEY `vUserEmail` (`vUserEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `iFid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `iPostId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`),
  ADD CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`iFriendId`) REFERENCES `users` (`iUserId`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`iUserId`) REFERENCES `users` (`iUserId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
