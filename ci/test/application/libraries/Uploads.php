<?php
defined('BASEPATH') or exit("Page can't accessible");

class Uploads
{
    // multiple file upload
    public function upload_multiple($files, $path)
    {
        // print_r($_FILES);exit;
        foreach ($files as $key => $file) {
            $_FILES['userfiles'] = $file;
            $config['upload_path']          = $path;
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2048;
            $config['max_width']            = 2048;
            $config['max_height']           = 2048;
            $config['file_name'] = time() . $_FILES["userfiles"]['name'];

            // assign the CodeIgniter object
            $CI = &get_instance();
            $CI->load->library('upload');
            $CI->upload->initialize($config);
            if (!$CI->upload->do_upload('userfiles')) {
                $error = array('error' => $CI->upload->display_errors());
                print_r($error);
            } else {
                return $CI->upload->data();
            }
        }
    }

    // Upload single image
    public function upload_single($file, $path)
    {
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 2048;
        $config['max_width']            = 2048;
        $config['max_height']           = 2048;
        $config['file_name'] = time()."-".$_FILES["post_image"]['name'];

        // assign the CodeIgniter object
        $CI = &get_instance();
        $CI->load->library('upload');
        $CI->upload->initialize($config);
        if (!$CI->upload->do_upload('post_image')) {
            $error = array('error' => $CI->upload->display_errors());
            print_r($error);
        } else {
            $CI->upload->data();
        }
    }
}
