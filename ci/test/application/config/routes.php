<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['login'] = 'main';
$route['login_verify'] = 'requests/login_verify';
$route['logout'] = 'requests/logout';
$route['homepage'] = 'main/homepage';
$route['delete_post/(:num)'] = 'requests/delete_post/$1';
$route['set_post'] = 'requests/set_post';
$route['get_post'] = 'requests/get_post';
$route['find_friends'] = 'main/find_friends';
$route['find_friends_ajax'] = 'requests/find_friends';
$route['all_friends'] = 'requests/all_friends';
$route['unfriend/(:num)'] = 'requests/unfriend/$1';
$route['my_friends'] = 'main/my_friends';
$route['friends_request'] = 'main/friends_request';
$route['friend_request_list'] = 'requests/friend_request_list';
$route['accept_request/(:num)'] = 'requests/accept_request/$1';
$route['reject_request/(:num)'] = 'requests/reject_request/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
