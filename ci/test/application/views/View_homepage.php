<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- flash message data -->
<?php
if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success alert-dismissible d-flex align-items-center fade show" role="alert">
        <i class="material-icons">check_circle_outline</i>
        <?php echo $this->session->flashdata('message'); ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
    </div>
<?php } ?>
<!-- flash message data -->

<link rel="stylesheet" href="public/css/home.css" />
<div class="container mt-5 mb-3">
    <div class="row">
        <div class="col-md-12"><img src="https://www.shutterstock.com/image-photo/young-african-businesswoman-wearing-glasses-laughing-1043390350" alt="" srcset="">

            <div class="card ml-auto mr-auto shadow-sm ml-auto app-post-card">
                <div class="card-body">
                    <form id='upload_post' enctype="multipart/form-data">
                        <div class="mb-3">
                            <textarea class="w-100 app-add-post-text" type="text" id="post_desc" value="" placeholder="create a new post"></textarea>
                        </div>
                        <div class="d-flex justify-content-end align-items-center">
                            <a class="upload-image mr-2" title="Upload Image" href="#">
                                <input id='post_image' type='file' name='post_image' accept=".jpeg, .jpg, .png" hidden />
                                <i class="material-icons" id="btn_post_image">add_a_photo</i>
                            </a>
                            <button class="btn btn-outline-primary w-50" type="submit">Post</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="app-post-container mt-5">
                <!-- data from ajax -->
            </div>
            <div class="ml-auto mr-auto mt-3 load-more-btn-cont mb-3">
                <button id="load_more" class="btn btn-outline-primary w-100">Load More Posts</button>
            </div>
        </div>
    </div>
</div>
<div class="app-scroll-top" style="display: none;">
    <button class="btn btn-primary btn-circle btn-circle-lg m-1"><i class="material-icons">arrow_upward</i></button>
</div>

<script type="text/javascript" src="public/js/homepage.js"></script>
