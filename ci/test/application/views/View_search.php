<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<link rel="stylesheet" href="public/css/my-friends.css" />

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-between">
                <div class="w-50 ml-auto mr-auto">
                    <input type="text" class="w-100 border-none text-center app-search-bar" id="search_input" value="" placeholder="search users" />
                </div>
            </div>
            <hr />
        </div>
    </div>
    <div class="row friends_list"></div>

    <!-- <div class="ml-auto mr-auto mt-3 load-more-btn-cont mb-3 w-25 ml-auto mr-auto">
        <button class="btn btn-outline-primary w-100" type="submit">Load More Friends</button>
    </div> -->

</div>
<!-- <div class="app-scroll-top">
    <button class="btn btn-primary btn-circle btn-circle-lg m-1"><i class="material-icons">arrow_upward</i></button>
</div> -->

<script src="public/js/find_friends.js"></script>