<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!-- Header -->
<nav class="shadow-sm app-top-nav navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?=BASE_URL?>"><i class="material-icons">mood</i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="material-icons">home</i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL ?>find_friends"><i class="material-icons">search</i>find friends</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL ?>my_friends"><i class="material-icons">person_outline</i>my friends</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL ?>friends_request"><i class="material-icons">history</i>friend requests</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL ?>logout"><i class="material-icons">power_settings_new</i>logout</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= BASE_URL?>"><b>Welcome <?= $this->session->userdata('name') ?>!!</b></a>
            </li>
        </ul>
    </div>
</nav>
<!-- Header end -->