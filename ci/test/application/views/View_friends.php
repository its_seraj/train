<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<link rel="stylesheet" href="public/css/my-friends.css" />


<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-between">
                <div>
                    <h6>My Friends</h6>
                </div>
                <div>
                    <input type="text" class="border-none text-center app-search-bar" id="search_input" value="" placeholder="search by name" />
                </div>
            </div>

            <hr />
        </div>
    </div>
    <div class="row friends_list">
        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 mb-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text text-center mb-2">
                        <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" data-holder-rendered="true" />
                    </div>
                    <h6 class="card-title text-center">Cathy Jones</h6>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Unfriend</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- <div class="app-scroll-top">
    <button class="btn btn-primary btn-circle btn-circle-lg m-1"><i class="material-icons">arrow_upward</i></button>
</div> -->

<script type="text/javascript" src="public/js/friends.js"></script>
