<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<link rel="stylesheet" href="public/css/login.css" />

<div class="container px-4 py-5 mx-auto">
    <div class="card card0">
        <!-- flash error data -->
        <?php
        if ($this->session->flashdata('message')) { ?>
            <div class="alert alert-success alert-dismissible d-flex align-items-center fade show" role="alert">
                <i class="material-icons">check_circle_outline</i>
                <?php echo $this->session->flashdata('message'); ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
            </div>
        <?php } ?>
        <?php
        if ($this->session->flashdata('errors')) { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="material-icons">error</i>
                <?php echo $this->session->flashdata('errors'); ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
            </div>
        <?php } ?>
        <!-- flash error data -->
        <div class="d-flex flex-lg-row flex-column-reverse">
            <div class="card card1">
                <div class="row justify-content-center my-auto">
                    <div class="col-md-8 col-10 my-5">
                        <div class="row justify-content-center px-3 mb-3"> <img id="logo" src="https://i.imgur.com/PSXxjNY.png"> </div>
                        <h3 class="mb-5 text-center heading">Sign In</h3>
                        <h6 class="msg-info">Please sign in to your account</h6>
                        <form id="login_form">
                            <div class="form-group"> <label class="form-control-label text-muted">Username</label>
                                <input type="text" id="email" name="email" placeholder="Email id" class="form-control" required>
                            </div>
                            <div class="form-group"> <label class="form-control-label text-muted">Password</label>
                                <input type="password" id="psw" name="psw" placeholder="Password" class="form-control" required>
                            </div>
                            <div class="row justify-content-center my-3 px-3">
                                <button class="btn-block btn-color" type="submit">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card card2">
                <div class="my-auto mx-md-5 px-md-5 right">
                    <h3 class="text-white">Lorem Ispum</h3> <small class="text-white">Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat.</small>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="public/js/login.js"></script>