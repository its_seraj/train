<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

	public function index()
	{
		if (!empty($this->session->userdata('email'))) header("Location: ".BASE_URL."homepage");
		else {
			$data['data'] = array(
				'layout/View_header',
				'View_login',
				'layout/View_footer'
			);
			$this->load->view('layout/View_container', $data);
			
		}
	}

	public function homepage()
	{
		if (empty($this->session->userdata('email'))) header("Location: ".BASE_URL);
		else {
			$data['data'] = array(
				'layout/View_header',
				'layout/View_nav',
				'View_homepage',
				'layout/View_footer'
			);
			$this->load->view('layout/View_container', $data);
		}
	}

	public function find_friends(){
		if (empty($this->session->userdata('email'))) header("Location: ".BASE_URL);
		else {
			$data['data'] = array(
				'layout/View_header',
				'layout/View_nav',
				'View_search',
				'layout/View_footer'
			);
			$this->load->view('layout/View_container', $data);
		}
	}

	public function my_friends(){
		if (empty($this->session->userdata('email'))) header("Location: ".BASE_URL);
		else {
			$data['data'] = array(
				'layout/View_header',
				'layout/View_nav',
				'View_friends',
				'layout/View_footer'
			);
			$this->load->view('layout/View_container', $data);
		}
	}

	public function friends_request(){
		if (empty($this->session->userdata('email'))) header("Location: ".BASE_URL);
		else {
			$data['data'] = array(
				'layout/View_header',
				'layout/View_nav',
				'View_friends_request',
				'layout/View_footer'
			);
			$this->load->view('layout/View_container', $data);
		}
	}
}
