<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Requests extends CI_Controller
{
    public function login_verify()
    {
        if (!empty($this->input->post())) {
            // print_r($this->input->post()); exit;

            $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array(
                    'errors' => validation_errors()
                );
                $this->session->set_flashdata($data);
            } else {
                $this->load->model('Model_user');
                $user = $this->Model_user->login($this->input->post());


                if (!empty($user)) {
                    $user_data = array(
                        'id' => $user->id,
                        'email' => $user->email,
                        'name' => $user->name,
                        'pic' => $user->pic
                    );
                    // print_r($user_data);exit;

                    $this->session->set_userdata($user_data);
                    $this->session->set_flashdata('message', 'Login Successfully');

                    echo "success";
                } else {
                    $this->session->set_flashdata('errors', 'Invalid credentials!');
                }
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata(array(
            'id',
            'email',
            'name',
            'pic'
        ));
        // $this->session->sess_destroy();
        $this->session->set_flashdata('message', 'Successfully! Logged Out!');
        header("Location: " . BASE_URL);
    }

    public function set_post()
    {
        // print_r($_FILES);
        // print_r($_POST);exit;

        // upload file using library
        $this->load->library("uploads");
        $this->uploads->upload_single($_FILES, 'public/upload/posts/');

        $this->load->model('Model_user');
        $_POST["post_image"] = time() . "-" . $_FILES["post_image"]["name"]; // append image name in
        $result = $this->Model_user->upload_post($this->input->post());
        echo "success";
    }

    public function get_post()
    {
        $this->load->model('Model_user');
        $result['data'] = $this->Model_user->get_post($this->input->post());
        $result['session_id'] = $this->session->userdata('id');
        print_r(json_encode($result));
    }

    public function all_friends()
    {
        $this->load->model('Model_user');
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->all_friends($this->input->post());
        // print_r($result);exit;
        echo json_encode($result);
    }

    public function find_friends()
    {
        $this->load->model('Model_user');
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->find_friends($this->input->post());
        // print_r($result);exit;
        echo json_encode($result);
    }
    
    public function unfriend($id){
        $this->load->model('Model_user');
        $_POST['friend_id'] = $id;
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->unfriend($this->input->post());
    }

    public function friend_request_list(){
        $this->load->model('Model_user');
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->friend_request_list($this->input->post());
        echo json_encode($result);
    }

    public function accept_request($id){
        $this->load->model('Model_user');
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->accept_request($this->input->post());
    }

    public function reject_request($id){
        $this->load->model('Model_user');
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->reject_request($this->input->post());
    }
    
    public function delete_post($id){
        $this->load->model('Model_user');
        $_POST['post_id'] = $id;
        $_POST['id'] = $this->session->userdata('id');
        $result['data'] = $this->Model_user->delete_post($this->input->post());
    }
}
