<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_user extends CI_Model
{

    public function login($data)
    {
        return $this->db->select('iUserId AS id, vUserName AS name, vUserEmail AS email, vProfilePic AS pic')
            ->where('vPassword', md5($data['password']))
            ->where('vUserEmail', $data['email'])
            ->get('users')->row();
    }

    public function upload_post($data)
    {
        $data = array(
            "iUserId" => $this->session->userdata('id'),
            "vPostPic" => $data["post_image"],
            "vPostDesc" => $data["post_desc"]
        );
        $this->db->insert('posts', $data);
        return $this->db->last_query();
    }

    public function get_post($data)
    {
        return $this->db->select('p.iUserId AS id, p.vPostPic AS pic, p.tPostTime AS time, p.vPostDesc AS post_desc, u.vUserName AS name, u.vProfilePic AS avatar, u.vUserEmail AS email, p.iPostId AS post_id')
            ->from('posts AS p')
            ->join('friends AS f', 'f.iFriendId = p.iUserId AND f.bIsFriend = 1 AND f.iUserId = 1', 'left')
            ->join('users AS u', 'u.iUserId = p.iUserId')
            ->where('p.iUserId = 1 OR f.iUserId = 1')
            ->order_by('p.tPostTime', 'DESC')
            ->limit(3, $data['start'])
            ->get()->result_array();
    }

    public function all_friends($data)
    {
        $this->db->select('u.iUserId AS id, u.vUserName AS name, u.vProfilePic AS pic');
        $this->db->from('users AS u');
        $this->db->join('friends AS f', 'f.iFriendId = u.iUserId AND f.bIsFriend = 1');
        if (trim($data['search']) != '') {
            $this->db->like('u.vUserName', $data['search']);
        }
        $this->db->where('f.iUserId ', $data['id']);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function find_friends($data){
        $this->db->select('u.iUserId AS id, u.vUserName AS name, u.vProfilePic AS pic');
        $this->db->from('users AS u');
        $this->db->join('friends AS f', 'f.iFriendId = u.iUserId', 'left');
        if (trim($data['search']) != '') {
            $this->db->like('u.vUserName', $data['search']);
        }
        // $this->db->where('f.bIsFriend != 1');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function unfriend($data){
        $this->db->where("iUserId", $data['id']);
        $this->db->where("iFriendId", $data['friend_id']);
        $this->db->update('friends', array(
            'bIsFriend' => 0
        ));
    }

    public function friend_request_list($data){
        $this->db->select('u.iUserId AS id, u.vUserName AS name, u.vProfilePic AS pic');
        $this->db->from('users AS u');
        $this->db->join('friends AS f', 'f.iFriendId = u.iUserId');
        if (trim($data['search']) != '') {
            $this->db->like('u.vUserName', $data['search']);
        }
        $this->db->where('f.iUserId ', $data['id']);
        $this->db->where('f.bIsFriend = 0');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function accept_request($data){
        $this->db->where("iUserId", $data['id']);
        $this->db->where("iFriendId", $data['friend_id']);
        $this->db->update('friends', array(
            'bIsFriend' => 1
        ));
    }

    public function reject_request($data){
        $this->db->where("iUserId", $data['id']);
        $this->db->where("iFriendId", $data['friend_id']);
        $this->db->delete('friends');
    }

    public function delete_post($data){
        $this->db->where("iPostId", $data['post_id']);
        $this->db->delete('posts');
    }
}
