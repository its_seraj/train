$(() => {
    // upload post
    $("#upload_post").validate({
        ignore: [],
        rules: {
            post_image: {
                required: true,
            }
        },
        messages: {
            post_image: 'Please select image'
        }
    })
    $("#btn_post_image").on("click", () => {
        $("#post_image").click();
    })
    $("#upload_post").on("submit", (e) => {
        e.preventDefault();
        if ($(e.target).valid() == true) {
            var formData = new FormData();
            formData.append('post_image', $("#post_image")[0].files[0])
            formData.append('post_desc', $("#post_desc").val());
            $.ajax({
                url: BASE_URL + "set_post",
                type: 'post',
                contentType: false,
                processData: false,
                cache: false,
                data: formData,
                success: (data) => {
                    // console.log(data)
                    $("#post_image").val('');
                    $("#post_desc").val('');
                    start = 0;
                    load_data();
                }
            })
        }

    })

    var start = 0;
    function load_data(){
        $.ajax({
            url: BASE_URL + "get_post",
            type: "post",
            data: { start: start },
            dataType: "json",
            success: (data) => {
                // console.log(data);
                var output = "";
                $.each(data['data'], (i, row) => {
                    output += `
                    <div class="card ml-auto mr-auto app-post-card shadow-sm">
                        <img class="card-img-top" src="${BASE_URL}public/upload/posts/${row.pic}" alt="Card image cap">
                        <div class="card-body">
                            <div class="app-post-add-img mb-3">
                                <div class="d-flex  align-items-center">
                                    <img class="rounded-circle z-depth-2 mr-3" alt="100x100" src="${row.avatar}" data-holder-rendered="true" />
                                    <div class="app-post-name">By ${row.name} <span> - ${row.time} ago</span></div>
                                </div>
                            </div>
                            <p class="card-text">${row.post_desc}</p>
                        </div>
                        `;
                        if(row.id == data['session_id'])
                            output += 
                                `<div class="app-del-post">
                                    <button class="btn btn-primary btn-circle btn-circle-lg m-1 delete_post" >
                                        <i class="material-icons" data-id="${row.post_id}">clear</i>
                                    </button>
                                </div>`;
                        output +=`</div>`;
                })
                $(".app-post-container").append(output);

                $(".delete_post").on("click", (e) => {
                    var id = $(e.target).attr("data-id");
                    $.ajax({
                        url: BASE_URL + "delete_post/" + id,
                        success: (data) => {
                            load_data();
                        }
                    })
                })
            }
        })
    }
    load_data();

    $("#load_more").on("click", () => {
        start += 3;
        load_data();
    })

    $(window).on("scroll", () => {
        if($(window).scrollTop() == 0) $(".app-scroll-top").css("display", "none");
        if($(window).scrollTop() >= $(window).height()){
            console.log("h")
            // window.scrollTo({ top: 0, behavior: 'smooth' });
            $(".app-scroll-top").css("display", "block");
            $(".app-scroll-top").on("click", () => {
                window.scrollTo({ top: 0, behavior: 'smooth' });
            })
        }
    })

})