$(() => {
    function all_friends(){
        var search = $("#search_input").val();
        $.ajax({
            url: BASE_URL + "all_friends",
            type: "post",
            data: { search: search },
            dataType: "json",
            success: (data) => {
                console.log(data);
                var output = "";
                $.each(data['data'], (i, row) => {
                    output += `
                    <div class="col-sm-3 mb-3">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <div class="card-text text-center mb-2">
                                    <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="${row.pic}" data-holder-rendered="true" />
                                </div>
                                <h6 class="card-title text-center">${row.name}</h6>
                                <div class="text-center">
                                    <button class="unfriend btn btn-primary" data-id="${row.id}">Unfriend</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;

                })
                $(".friends_list").html(output);
            }
        })
    }
    all_friends();

    $("#search_input").on("keyup", () => {all_friends()})

    $(document).on("click", ".unfriend", (e) => {
        var id = $(e.target).data('id');
        $.ajax({
            url: BASE_URL + "unfriend/" + $id,
            success: (data) => {
                all_friends();
            }
        })
    })
})