$(() => {
    function all_requests(){
        var search = $("#search_input").val();
        $.ajax({
            url: BASE_URL + "friend_request_list",
            type: "post",
            data: { search: search },
            dataType: "json",
            success: (data) => {
                // console.log(data);
                var output = "";
                $.each(data['data'], (i, row) => {
                    output += `
                    <div class="col-sm-3 mb-3">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <div class="card-text text-center mb-2">
                                    <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="${row.pic}" data-holder-rendered="true" />
                                </div>
                                <h6 class="card-title text-center">${row.name}</h6>
                                <div class="text-center">
                                    <button data-id="${row.id}" class="accept btn btn-outline-success btn-outline">Accept</button>
                                    <button data-id="${row.id}" class="reject btn btn-outline-danger btn-outline">Reject</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;

                })
                $(".requests_list").html(output);
            }
        })
    }
    all_requests();

    $("#search_input").on("keyup", () => {all_requests()})


    $(document).on("click", ".accept", (e) => {
        var id = $(e.target).data("id");
        $.ajax({
            url: BASE_URL + "accept_request/" + id,
            success: (data) => {
                //
            }
        })
    })

    $(document).on("click", ".reject", (e) => {
        var id = $(e.target).data("id");
        $.ajax({
            url: BASE_URL + "reject_request/" + id,
            success: (data) => {
                //
            }
        })
    })
})