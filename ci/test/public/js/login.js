$(() => {
    // login validation
    $("#login_form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            psw: {
                required: true,
                minlength: 3
            }
        }
    })

    // login verify
    $("#login_form").on("submit", (e) => {
        e.preventDefault();
        if ($(e.target).valid() == true) {
            var email = $("#email").val();
            var password = $("#psw").val();
            $.ajax({
                url: BASE_URL + "login_verify",
                type: "post",
                data: { email: email, password: password },
                success: (data) => {
                    location.href = BASE_URL + "homepage";
                    // console.log(data);
                    // $(e.target).trigger("reset");
                }
            })

        }
    })

    
})