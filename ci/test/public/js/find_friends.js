$(() => {
    function all_friends(){
        var search = $("#search_input").val();
        $.ajax({
            url: BASE_URL + "find_friends_ajax",
            type: "post",
            data: { search: search },
            dataType: "json",
            success: (data) => {
                console.log(data);
                var output = "";
                $.each(data['data'], (i, row) => {
                    output += `
                    <div class="col-sm-3 mb-3" data-id="${row.id}">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <div class="card-text text-center mb-2">
                                    <img class="rounded-circle z-depth-2 w-50" alt="100x100" src="${row.pic}" data-holder-rendered="true" />
                                </div>
                                <h6 class="card-title text-center">${row.name}</h6>
                                <div class="text-center">
                                    <button class="btn btn-warning">Friend Request Sent</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;

                })
                $(".friends_list").html(output);
            }
        })
    }
    all_friends();

    $("#search_input").on("keyup", () => {all_friends()})
})